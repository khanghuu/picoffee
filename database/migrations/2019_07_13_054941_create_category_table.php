<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->increments('Category_ID');
            $table->String('Category_Code', 5);
            $table->string("Category_Name");
            $table->string('Category_Parent')->nullable();
            $table->integer('Category_Order')->default(0)->nullable();
            $table->tinyInteger('Category_Status');
            $table->dateTime('Category_Datetime');
            $table->integer('Category_Shop');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
