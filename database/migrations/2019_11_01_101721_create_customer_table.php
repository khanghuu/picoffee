<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->bigIncrements('Customer_ID');
            $table->string('Customer_First_Name',10);
            $table->string('Customer_Last_Name',50);
            $table->date('Customer_Birthday');
            $table->string('Customer_Phone_Number',20);
            $table->tinyInteger('Customer_Status');
            $table->unsignedInteger('Customer_Point');
            $table->dateTime('Created_At');
            $table->dateTime('Updated_At');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
