<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditAllColumnsNameInIngredientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ingredient', function(Blueprint $table) {
            $table->dropColumn('ingredient_DateTime');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
        Schema::table('ingredient', function(Blueprint $table) {
            $table->renameColumn('ingredient_ID', 'Ingredient_ID');
            $table->renameColumn('ingredient_Code', 'Ingredient_Code');
            $table->renameColumn('ingredient_Name', 'Ingredient_Name');
            $table->renameColumn('ingredient_Number', 'Ingredient_Number');
            $table->renameColumn('ingredient_Unit', 'Ingredient_Unit');
            $table->renameColumn('ingredient_Params', 'Ingredient_Params');
            $table->renameColumn('ingredient_Status', 'Ingredient_Status');
            $table->dateTime('Created_At');
            $table->dateTime('Updated_At');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ingredient', function (Blueprint $table) {
            //
        });
    }
}
