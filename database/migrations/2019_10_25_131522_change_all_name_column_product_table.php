<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAllNameColumnProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product', function(Blueprint $table) {
            $table->dropColumn('product_Datetime');
        });
        Schema::table('product', function(Blueprint $table) {
            $table->renameColumn('product_ID', 'Product_ID');
            $table->renameColumn('product_Code', 'Product_Code');
            $table->renameColumn('product_Name', 'Product_Name');
            $table->renameColumn('product_Price', 'Product_Price');
            $table->renameColumn('product_Img', 'Product_Image');
            $table->renameColumn('product_Status', 'Product_Params');
            $table->renameColumn('product_Param', 'Product_Status');
            $table->integer('Product_Shop');
            $table->dateTime('Created_At');
            $table->dateTime('Updated_At');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
