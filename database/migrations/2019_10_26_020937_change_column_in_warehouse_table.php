<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnInWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warehouse', function (Blueprint $table) {
//             $table->renameColumn('Warehouse_Name','Warehouse_Shop');
//             $table->renameColumn('Warehouse_Time_Import','Created_At');
            $table->unsignedDecimal('Warehouse_Total',20,10);
            $table->unsignedDecimal('Warehouse_Price',20,10)->change();
            $table->unsignedDecimal('Warehouse_Quantity',20,10)->change();
//             $table->timestamp('Update_At');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warehouse', function (Blueprint $table) {
            //
        });
    }
}
