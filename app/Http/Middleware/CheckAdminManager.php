<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdminManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = session('user');
        if ($user->User_Level == 1 || $user->User_Level == 2) {
            return $next($request);
        }
        return responseRedirect(0, 'Lỗi, bạn không đủ quyền để thực hiện thao tác này!');
    }
}
