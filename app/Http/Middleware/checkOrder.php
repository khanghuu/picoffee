<?php

namespace App\Http\Middleware;

use Closure;

class checkOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userLevel = session('user')->User_Level;
        if ($userLevel == 1 and $userLevel == 3) {
            return redirect()->route('system.dashboard');
        }
        return $next($request);
    }
}
