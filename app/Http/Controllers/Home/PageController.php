<?php

namespace App\Http\Controllers\Home;

use App\Model\Ingredient;
use App\Model\RawMaterial;
use App\Model\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Model\Category;
use App\Model\Product;
use App\Model\Bill;
use App\Model\BillProduct;
use App\Model\BillTemp;
use App\Model\BillProductTemp;

class PageController extends Controller
{
    public function getIndex(Request $request){
        $user = session('user');
        $products = Product::where('Product_Status', 1)->Where('Product_Shop', $user->User_Location_Store)->orderBy('Product_Category')->get();
        $categories = Category::where('Category_Status', 1)->Where('Category_Shop', $user->User_Location_Store)->get();
        $shopName = Shop::where('Shop_ID', $user->User_Location_Store)->value('Shop_Name');
        $uri = $request->path();
        $order_wait = BillTemp::where('Bill_Status', 1)->where('Bill_Shop_ID', $user->User_Location_Store)->orderBy('Bill_Status')->select('Bill_ID')->get();
        return view('Index', compact('products', 'categories', 'uri', 'order_wait', 'shopName'));
    }

    public function getProductInfo(Request $req){
        $product = Product::find($req->id);
        if(!$product){
            return null;
        }
        return response()->json(array('status'=>true, 'product'=>$product));
    }

    public function getProductKeyword(Request $req){
        $user = session('user');
        $products = Product::whereRaw('Product_Name LIKE "%'.$req->keyword.'%"')->where('Product_Shop', $user->User_Location_Store)->get();
        if($req->keyword == ''){
            $products = Product::where('Product_Status', 1)->where('Product_Shop', $user->User_Location_Store)->orderBy('Product_Category')->get();
        }
        return response()->json(array('status'=>true, 'products'=>$products));
    }

    public function postPay(Request $req){

 		$user = Session('user');
 		if ($user->User_Level == 1) {
            return response()->json(array('status'=>false, 'message'=>'Admin không thể thực hiện order!'));
        }
        if($req->products and count($req->products) > 0){
            $totalPrice = 0;
            $bill = new Bill;
            $bill->save();
            foreach($req->products as $p){

                $product = Product::find($p['id']);
                $a[] = $product;
                if(!$product){
                    return response()->json(array('status'=>false, 'message'=>'Không tìm thấy sản phẩm '.$product->Product_Name));
                    $bill->delete();
                }

                //Tru so luong nguyen lieu vao kho
                $this->subtractionMaterial($p['id'], $p['quantity']);

                $totalPrice += $p['quantity'] * $product->Product_Price;
                $arrIDProduct[] = array('Bill_Product_ProductID'=>$p['id'],
                    'Bill_Product_Quantity'=>$p['quantity'],
                    'Bill_Product_Time'=>date('Y-m-d H:i:s'),
                    'Bill_Product_Status'=>1,
                    'Bill_Product_BillID'=>$bill->Bill_ID,
                );
            }
            if($req->signed_bill == 'true'){
                $bill->Bill_IsSigned = 1;
            }
            if($req->tax_vat == 'true'){
                $bill->Bill_IsTaxVAT = 1;
                $totalPrice += $totalPrice*0.1;
            }

            $bill->Bill_TotalPrice = $totalPrice;

            $bill->Bill_TableNumber = $req->table_number;
            if($req->amount_discount > 0){
                if($req->type_discount == 1){
                    $bill->Bill_DiscountAmount = $req->amount_discount;
                    $bill->Bill_ExtantPrice = $totalPrice - $bill->Bill_DiscountAmount;
                }else{
                    $bill->Bill_DiscountPercent = $req->amount_discount;
                    $bill->Bill_ExtantPrice = $totalPrice - ($totalPrice*($bill->Bill_DiscountPercent/100));
                }
            }else{
                $bill->Bill_ExtantPrice = $totalPrice;
            }
            $bill->Bill_Time = date('Y-m-d H:i:s');
            $bill->Bill_User = $user->User_ID;
            $bill->Bill_Status = 1;
            $bill->Bill_Shop_ID = $user->User_Location_Store;
            $bill->save();
            BillProduct::insert($arrIDProduct);
            if ($req->temp_bill) {
                BillTemp::where('Bill_ID', $req->temp_bill)->delete();
                BillProductTemp::where('Bill_Product_BillID', $req->temp_bill)->delete();
            }
            writeLog('Bán hàng hoá đơn ID: '.$bill->Bill_ID);
            return response()->json(array('status'=>true, 'id'=>$bill->Bill_ID));
        }

        return response()->json(array('status'=>false, 'message'=>'Không có sản phẩm nào được chọn!'));
    }

    public function postPayTemp(Request $req)
    {
        $user = Session('user');
        if ($user->User_Level == 1) {
            return response()->json(array('status'=>false, 'message'=>'Admin không thể thực hiện order!'));
        }
        $id = $req->get('id');
        if ($user->User_Level == 1) {
            return response()->json(array('status'=>false, 'message'=>'Admin không thể thực hiện order!'));
        }
        if($req->products and count($req->products) > 0){
            $totalPrice = 0;
            if ($id) {
                $bill = BillTemp::find($id);
            }
            else {
                $bill = new BillTemp;
                $bill->save();
            }

            foreach($req->products as $p){

                $product = Product::find($p['id']);
                $a[] = $product;
                if(!$product){
                    return response()->json(array('status'=>false, 'message'=>'Không tìm thấy sản phẩm '.$product->Product_Name));
                    $bill->delete();
                }
                $totalPrice += $p['quantity'] * $product->Product_Price;
                $arrIDProduct[] = array('Bill_Product_ProductID'=>$p['id'],
                    'Bill_Product_Quantity'=>$p['quantity'],
                    'Bill_Product_Time'=>date('Y-m-d H:i:s'),
                    'Bill_Product_Status'=>1,
                    'Bill_Product_BillID'=>$bill->Bill_ID,

                );
            }
            if($req->signed_bill == 'true'){
                $bill->Bill_IsSigned = 1;
            }
            if($req->tax_vat == 'true'){
                $bill->Bill_IsTaxVAT = 1;
                $totalPrice += $totalPrice*0.1;
            }

            $bill->Bill_TotalPrice = $totalPrice;

            $bill->Bill_TableNumber = $req->table_number;
            if($req->amount_discount > 0){
                if($req->type_discount == 1){
                    $bill->Bill_DiscountAmount = $req->amount_discount;
                    $bill->Bill_ExtantPrice = $totalPrice - $bill->Bill_DiscountAmount;
                }else{
                    $bill->Bill_DiscountPercent = $req->amount_discount;
                    $bill->Bill_ExtantPrice = $totalPrice - ($totalPrice*($bill->Bill_DiscountPercent/100));
                }
            }else{
                $bill->Bill_ExtantPrice = $totalPrice;
            }
            $bill->Bill_Time = date('Y-m-d H:i:s');
            $bill->Bill_User = $user->User_ID;
            $bill->Bill_Status = 1;
            $bill->Bill_Shop_ID = $user->User_Location_Store;
            $bill->save();
            if ($req->id) {
                BillProductTemp::where('Bill_Product_BillID', $req->id)->delete();
                BillProductTemp::insert($arrIDProduct);
            }
            else {
                BillProductTemp::insert($arrIDProduct);
            }


           writeLog('Bán hàng hoá đơn ID: '.$bill->Bill_ID);

            return response()->json(array('status'=>true, 'id'=>$bill->Bill_ID));
        }

        return responseJsonMess(0,'Không có sản phẩm nào được chọn!');
    }

    public function getWaitOrder(Request $request)
    {
        $order_id = $request->get('id');
        if ($order_id) {
            $product = BillProductTemp::where('Bill_Product_BillID', $order_id)
                ->join('product', 'Bill_Product_ProductID', 'Product_ID')
                ->select('bill_product_temp.Bill_Product_Quantity', 'bill_product_temp.Bill_Product_ProductID', 'product.Product_Name', 'product.Product_Price')
                ->get()->toArray();
            $bill = BillTemp::where('Bill_ID', $order_id)
                ->select('Bill_DiscountAmount', 'Bill_ExtantPrice', 'Bill_IsSigned', 'Bill_IsTaxVAT', 'Bill_TableNumber')
                ->get()
                ->toArray();
            return response()->json(array('status'=>true, 'products'=>$product, 'bill'=>$bill[0]));
        }
        return response()->json(array('status'=>false));
    }

    public function deletePrintBillTemp($id)
    {
        $deleteBillTemp = BillTemp::where('Bill_ID', $id)->delete();
        $deleteBillProductTemp =BillProductTemp::where('Bill_Product_BillID', $id)->delete();
        if ($deleteBillTemp && $deleteBillProductTemp) {
            writeLog('Xóa hoá đơn tạm ID: '.$id);
            return redirect()->route('getIndex')->with(['flash_level'=>'success','flash_message'=>'Xóa hóa đơn tạm thành công']);
        }
        return redirect()->route('getIndex')->with(['flash_level'=>'error','flash_message'=>'Có lỗi xảy ra vui lòng liên hệ admin ']);
    }

    public function getPrintBill(Request $req){
        $bill = Bill::find($req->id);
        $shopAddress = Shop::where('Shop_ID', session('user')->User_Location_Store)->value('Shop_Address');
        return view('Home.Bill', compact('bill', 'shopAddress'));
    }

    public function getPrintBillTemp(Request $req){
        $bill = BillTemp::find($req->id);
        $shopAddress = Shop::where('Shop_ID', session('user')->User_Location_Store)->value('Shop_Address');
        return view('Home.Bill-Temp', compact('bill', 'shopAddress'));
    }

    public function subtractionMaterial($productID, $productQuantity)
    {
        $productParam = Product::where('Product_ID', $productID)->value('Product_Params');
        $ingredientsList = json_decode($productParam);
        foreach ($ingredientsList as $ingredientID => $ingredientQuantity) {
            $ingredientParam = Ingredient::where('Ingredient_ID', $ingredientID)->value('Ingredient_Params');
            if ($ingredientParam) {
                $rawList = json_decode($ingredientParam);
                foreach ($rawList as $rawID =>$rawQuantity) {
                    $whRawQuantity = RawMaterial::where('Raw_Material_ID', $rawID)->value('Raw_Material_Quantity');
                    $newRawQuantity = $whRawQuantity - $rawQuantity*$ingredientQuantity*$productQuantity;
                    RawMaterial::where('Raw_Material_ID', $rawID)->update(['Raw_Material_Quantity' => $newRawQuantity]);
                }
            }
            $whInQuantity = Ingredient::where('Ingredient_ID', $ingredientID)->value('Ingredient_Quantity');
            $newQuantity = $whInQuantity - $productQuantity*$ingredientQuantity;
            Ingredient::where('Ingredient_ID', $ingredientID)->update(['Ingredient_Quantity' => $newQuantity]);
        }
    }

}
