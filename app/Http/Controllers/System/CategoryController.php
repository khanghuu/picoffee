<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Shop;
use \DB;
class CategoryController extends Controller
{
    public function getManage(Request $req){
        $category = Category::select('Category_ID','Category_Name',  'Category_Order', 'Category_Code')->where('Category_Status', 1)->orderBy('Category_Order')->get();
        $row = Category::query();
        if($req->code){
            $row->where('Category_Code',$req->code);
        }

        if($req->unit){
            $row->where('Category_Order',$req->unit);
        }

        if($req->name){

            $row->where('Category_Name','like', "%$req->name%");
        }

        if($req->shop){

            $row->where('Category_Shop','like', "%$req->shop%");
        }

        if($req->search_category_status != ""){
            $row->where('Category_Status', $req->search_category_status);
        }

        $shop = Shop::where('Shop_Status',1)->get();
        if(Session('user')->User_Level != 1 && Session('user')->User_Level != 3){
            $row = $row->orderBy('Category_Order', 'ASC')
            ->where([
                ['Category_Status', '>', -1],
                ['Category_Shop',Session('user')->User_Location_Store]
            ])
            ->join('shop','category.Category_Shop','shop.Shop_ID')->get();

            $count = Category::where([
                ['Category_Status', '>', -1],
                ['Category_Shop',Session('user')->User_Location_Store]
            ])->groupBy('Category_Order')->get();
        }
        else{
            $row = $row->orderBy('Category_Order', 'ASC')->where('Category_Status', '>', -1)->join('shop','category.Category_Shop','shop.Shop_ID')->get();

            $count = Category::where('Category_Status', '>', -1)->groupBy('Category_Order')->get();
        }
        return view('System.Data.Category.category-manage', compact('row', 'category', 'shop', 'count'));
    }

    public function postAdd(Request $req){

        $store = $req->shop ?? Session('user')->User_Location_Store;
        if(!$req->name){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Vui lòng nhập tên danh mục']);
        }
        else{
            $category_name = Category::where([
                ['Category_Shop'  ,  $store],
                ['Category_Name'  , $req->name],
                ['Category_Status', '>', -1]
            ])->get();
            if(count($category_name)){
                return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Tên danh mục đã tồn tại']);
            }
        }
        if(!$store){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Vui lòng chọn cửa hàng']);
        }
        if(!$req->datetime){
            $create = date('Y-m-d H:i:s');
        }else{
            $create = $req->datetime;
        }
        $category_code = generateCategoryCode($req->name);
        $arrayInsert = array(
            'Category_Name' => $req->name,
            'Category_Code' => $category_code,
            'Category_Parent' => $req->parent,
            'Category_Order' => $req->order,
            'Category_Status' => 1,
            'Category_Datetime' => $create,
            'Category_Shop' => $store,
        );
        if(!$req->order){
            $category = Category::where('Category_Shop',$store)->where('Category_Status', '>', -1)->orderBy('Category_ID','desc')->first();

            if($category){
                $arrayInsert['Category_Order'] = $category->Category_Order + 1;
            }
            else{
                $arrayInsert['Category_Order'] = 1;
            }
        }
        $insertID = Category::insertGetId($arrayInsert);
        if($insertID){
            writeLog('Thêm danh mục ID: '.$insertID);
            return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Thêm danh mục thành công!']);
        }else{
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>config('global.DB_ERROR')]);
        }
    }

    public function postEdit(Request $req){
        $store = $req->EditShop ?? Session('user')->User_Location_Store;
        if(!$req->EditName){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Vui lòng nhập tên danh mục']);
        }
        else{
            $category_name = Category::where([
                ['Category_Shop'  ,  $store],
                ['Category_Name'  , $req->EditName],
                ['Category_Status', '>', -1]
            ])->get();
            $category_edit = Category::find($req->editId);
            if($req->EditName != $category_edit->Category_Name){
                if(count($category_name)){
                    return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Tên danh mục đã tồn tại']);
                }
            }
        }

        if(!$store){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Vui lòng chọn cửa hàng']);
        }

        $updateArr['Category_Name'] = $req->EditName;

        $updateArr['Category_Shop'] = $store;

        if(!is_numeric($req->EditOrder)){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Vui lòng nhập thứ tự danh mục ']);
        }
        $updateArr['Category_Order'] = $req->EditOrder;
        $updateArr['Category_Status'] = 0;

        if($req->Editstatus){
            $updateArr['Category_Status'] = 1;
        }
        $update = Category::where('Category_ID', $req->editId)->update($updateArr);

        writeLog('Sửa danh mục: '.$req->editId);
        return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Sửa danh mục thành công!']);
    }

    public function getCategory(Request $req){
        if(!$req->id){
            return response()->json(array('status'=>false, 'message'=>'Thiếu ID'), 200);
        }
        else {
            $row = Category::where('Category_ID', $req->id)->first();

            if(!$row){
                return response()->json(array('status'=>false, 'message'=>'Nguyên liệu thô không tồn tại'), 200);
            }
            else {
                return response()->json(array('status'=>true, 'data'=>$row), 200);
            }
        }
    }

    public function getDelete($id){
        Category::where('Category_ID', $id)->update(['Category_Status' => -1]);
        writeLog('Xóa danh mục ID: '.$id);
        return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Xoá danh mục thành công!']);
    }

}
