<?php

namespace App\Http\Controllers\System;

use App\Model\RawMaterial;
use App\Model\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Ingredient;
use App\Model\User;
use App\Model\Warehouse;
use DB;

class WarehouseController extends Controller
{
    public function getManage(Request $req)
    {
        $user = session('user');
        if($user->User_Level > 3) {
            return responseRedirect(0, 'Bạn không đủ quyền để truy cập');
        }

        $data['warehouse_raw'] = Warehouse::query();
        $data['warehouse_ing'] = Warehouse::query();

        if($user->User_Level == 2)
        {
            $data['warehouse_raw']->where('Warehouse_Shop',$user->User_Location_Store);
            $data['warehouse_ing']->where('Warehouse_Shop',$user->User_Location_Store);
        }

        $data['warehouse_raw']->where('Warehouse_Raw','<>',NULL)
        ->join('raw_material','warehouse.Warehouse_Raw','raw_material.Raw_Material_ID')
        ->where('Raw_Material_Status',1)
        ->join('shop','raw_material.Raw_Material_Shop','shop.Shop_ID')
        ->where('Shop_Status',1)
        ->groupBy('Warehouse_Raw')
        ->selectRaw('Raw_Material_ID, Raw_Material_Code, Raw_Material_Name, Raw_Material_Quantity, Raw_Material_Unit, Shop_Name, Raw_Material_Status, SUM(Warehouse_Quantity) as Warehouse_Total_Import');


        $data['warehouse_ing']->where('Warehouse_Ingredient','<>',NULL)
        ->join('ingredient','warehouse.Warehouse_Ingredient','ingredient.Ingredient_ID')
        ->where('Ingredient_Status',1)
        ->join('shop','ingredient.Ingredient_Shop','shop.Shop_ID')
        ->where('Shop_Status',1)
        ->groupBy('Warehouse_Ingredient')
        ->selectRaw('Ingredient_ID, Ingredient_Code, Ingredient_Name, Ingredient_Quantity, Ingredient_Unit, Shop_Name, Ingredient_Status, SUM(Warehouse_Quantity) as Warehouse_Total_Import');

        if($req->warehouse_raw_number){
            $data['warehouse_raw']->where('Raw_Material_Code','like',"%$req->warehouse_raw_number%");
        }

        if($req->warehouse_raw_name){
            $data['warehouse_raw']->where('Raw_Material_Name','like',"%$req->warehouse_raw_name%");
        }

        if($req->warehouse_raw_shop){
            $data['warehouse_raw']->where('Raw_Material_Shop',$req->warehouse_raw_shop);
        }


        if($req->warehouse_ing_number){
            $data['warehouse_ing']->where('Ingredient_Code','like',"%$req->warehouse_ing_number%");
        }

        if($req->warehouse_ing_name){
            $data['warehouse_ing']->where('Ingredient_Name','like',"%$req->warehouse_ing_name%");
        }

        if($req->warehouse_ing_shop){
            $data['warehouse_ing']->where('Ingredient_Shop',$req->warehouse_ing_shop);
        }

        $data['shop_list']= Shop::all();
        return view('System.Warehouse.manage', $data);
    }

    public function getEditRawManage(Request $req){
        $data_raw = RawMaterial::where('Raw_Material_ID',$req->edit_raw_id)->first();
        if (!$data_raw) {
            return responseJsonMess(0, 'Dữ liệu không tồn tại!');
        }
        return responseJsonData(1, $data_raw);
    }

    public function postEditRawManage(Request $req){
        $real_integer = filter_var($req->edit_raw_quantity, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_THOUSAND);
        if($real_integer == ""){
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Số lượng không hợp lệ']);
        }
        $real_integer = floatval(str_replace(',', '.', $real_integer));

        if(gettype($real_integer)=='double'){
            $data_raw = RawMaterial::where('Raw_Material_ID',$req->edit_raw_id)->first();
            if (!$data_raw) {
                return responseJsonMess(0, 'Dữ liệu không tồn tại!');
            }
            $data_raw->Raw_Material_Quantity = $real_integer;
            $data_raw->save();

            $arrayInsert = array(
                'Warehouse_User' => Session('user')->User_ID,
                'Warehouse_Raw' => $req->edit_raw_id,
                // 'Warehouse_Ingredient' => NULL,
                'Warehouse_Number' => NULL,
                'Warehouse_Shop' => $req->edit_raw_shop ,
                // 'Warehouse_Total' => 0,
                // 'Warehouse_Price' => 0,
                'Warehouse_Quantity' => $real_integer,
                'Warehouse_Description' => $req->edit_raw_description,
                'Created_At' => $req->edit_raw_date,
                'Warehouse_Status' => 0,
                'Warehouse_IsDestroy' => 0,
            );
            Warehouse::insertGetId($arrayInsert);

            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Cập nhật kho thành công']);
        }
        return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Số lượng không hợp lệ']);
    }

    public function getEditIngManage(Request $req){
        $data_ing = Ingredient::where('Ingredient_ID',$req->edit_ing_id)->first();
        if (!$data_ing) {
            return responseJsonMess(0, 'Dữ liệu không tồn tại!');
        }
        return responseJsonData(1, $data_ing);
    }

    public function postEditIngManage(Request $req){
        $real_integer = filter_var($req->edit_ing_quantity, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_THOUSAND);
        if($real_integer == ""){
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Số lượng không hợp lệ']);
        }
        $real_integer = floatval(str_replace(',', '.', $real_integer));

        if(gettype($real_integer)=='double'){
            $data_raw = Ingredient::where('Ingredient_ID',$req->edit_ing_id)->first();
            if (!$data_raw) {
                return responseJsonMess(0, 'Dữ liệu không tồn tại!');
            }
            $data_raw->Ingredient_Quantity = $real_integer;
            $data_raw->save();
            $arrayInsert = array(
                'Warehouse_User' => Session('user')->User_ID,
                // 'Warehouse_Raw' => 0,
                'Warehouse_Ingredient' => $req->edit_ing_id,
                'Warehouse_Number' => NULL,
                'Warehouse_Shop' => $req->edit_ing_shop ,
                // 'Warehouse_Total' => 0,
                // 'Warehouse_Price' => 0,
                'Warehouse_Quantity' => $real_integer,
                'Warehouse_Description' => $req->edit_ing_description,
                'Created_At' => $req->edit_ing_date,
                'Warehouse_Status' => 0,
                'Warehouse_IsDestroy' => 0,
            );
            Warehouse::insertGetId($arrayInsert);

            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Cập nhật kho thành công']);
        }
        return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Số lượng không hợp lệ']);
    }

    public function getHistory(Request $req){

        $user = session('user');
        if($user->User_Level > 3) {
            return responseRedirect(0, 'Bạn không đủ quyền để truy cập');
        }

        $check_req = false;

        $data['warehouse_history_raw'] = Warehouse::query()
        ->join('users','users.User_ID','warehouse.Warehouse_User')->where('User_Status',1)
        ->join('raw_material','raw_material.Raw_Material_ID','warehouse.Warehouse_Raw')
        ->where('Raw_Material_Status',1);


        $data['warehouse_history_ing'] = Warehouse::query()
        ->join('users','users.User_ID','warehouse.Warehouse_User')->where('User_Status',1)
        ->join('ingredient','ingredient.Ingredient_ID','warehouse.Warehouse_Ingredient')
        ->where('Ingredient_Status',1);

        if($user->User_Level == 2)
        {
            $data['warehouse_history_raw']->where('Warehouse_Shop',$user->User_Location_Store);
            $data['warehouse_history_ing']->where('Warehouse_Shop',$user->User_Location_Store);
        }
        $date_form =$req->warehouse_history_start;
        $date_to = $req->warehouse_history_end;
        if($date_form && $date_to){
            if($date_form > $date_to){
                return responseRedirect(0, 'Thời gian không hợp lệ');
            }
            else{
                if($date_form == $date_to){
                    $data['warehouse_history_raw']->where('warehouse.Created_At','like',"%$date_form%");
                    $data['warehouse_history_ing']->where('warehouse.Created_At','like',"%$date_form%");
                }
                else{
                    $data['warehouse_history_raw']->whereBetween('warehouse.Created_At',[$date_form, $date_to]);
                    $data['warehouse_history_ing']->whereBetween('warehouse.Created_At',[$date_form, $date_to]);
                }
            }
        }
        if($date_form && !$date_to ||
        !$date_to && $date_form){
            return responseRedirect(0, 'Thời gian không hợp lệ');
        }

        if($req->warehouse_history_number){
            $data['warehouse_history_raw']->where('Warehouse_Number',$req->warehouse_history_number);
            $data['warehouse_history_ing']->where('Warehouse_Number',$req->warehouse_history_number);
            $check_req = true;
        }

        if($req->warehouse_history_user){
            $data['warehouse_history_raw']->where('User_Name','like',"%$req->warehouse_history_user%");
            $data['warehouse_history_ing']->where('User_Name','like',"%$req->warehouse_history_user%");
            $check_req = true;
        }

        if($req->warehouse_history_name){
            $data['warehouse_history_raw']->where('Raw_Material_Name','like',"%$req->warehouse_history_name%");
            $data['warehouse_history_ing']->where('Ingredient_Name','like',"%$req->warehouse_history_name%");
            $check_req = true;
        }

        if($req->warehouse_history_shop){
            $data['warehouse_history_raw']->where('Warehouse_Shop',$req->warehouse_history_shop);
            $data['warehouse_history_ing']->where('Warehouse_Shop',$req->warehouse_history_shop);
            $check_req = true;
        }

        $data['warehouse_history_raw'] = $data['warehouse_history_raw']
        ->join('shop','shop.Shop_ID','warehouse.Warehouse_Shop')
        ->where('Shop_Status',1)
        ->select('raw_material.Raw_Material_Name','users.User_Name' ,'warehouse.*','shop.Shop_Name')
        ->orderBy('Created_At','desc');

        $data['warehouse_history_ing'] = $data['warehouse_history_ing']
        ->join('shop','shop.Shop_ID','warehouse.Warehouse_Shop')
        ->where('Shop_Status',1)
        ->select('ingredient.Ingredient_Name','users.User_Name' ,'warehouse.*','shop.Shop_Name')
        ->orderBy('Created_At','desc');

        if($check_req){
            $data['warehouse_history_raw'] = $data['warehouse_history_raw']->get();
            $data['warehouse_history_ing'] = $data['warehouse_history_ing']->get();
        }
        else{
            $limit = 25;
            $data['warehouse_history_raw'] = $data['warehouse_history_raw']->take($limit)->get();
            $data['warehouse_history_ing'] = $data['warehouse_history_ing']->take($limit)->get();
        }
        $data['shop_list'] = Shop::all();

        $data['merge'] = $data['warehouse_history_raw']->merge($data['warehouse_history_ing']);


        $data['merge'] = $data['merge']->sortByDesc(function ($product, $key) {
            return $product->Created_At;
        });
        return view('System.Warehouse.history',$data);
    }

    public function getImport(Request $req)
    {
        $user = session('user');
        if($user->User_Level > 3) {
            return responseRedirect(0, 'Bạn không đủ quyền để truy cập');
        }

        $warehouse_number = Warehouse::count();
        $import_number = date('Ymd') . $warehouse_number;
        if($user->User_Level == 1){
            $shop_list = Shop::all();
            return view('System.Warehouse.import', compact('user', 'import_number', 'shop_list'));
        }
        if($user->User_Level == 2){
            $raw_list = RawMaterial::where('Raw_Material_Shop',$user->User_Location_Store)->get();
            $ing_list = Ingredient::where('Ingredient_Shop',$user->User_Location_Store)->get();
            return view('System.Warehouse.import',compact('user', 'import_number', 'raw_list' , 'ing_list'));
        }
    }

    public function getSelectShopImportAjax(Request $request)
    {
        if(!$request->id){
            return responseJsonMess(0, 'Thiếu ID');
        }
        else{
            $check_shop = Shop::where('Shop_ID',$request->id)->first();
            if(!$check_shop){
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Cửa hàng không tồn tại']);
            }
            $data['ware_raw'] = RawMaterial::where('Raw_Material_Shop',$request->id)->where('Raw_Material_Status',1)->get();
            $data['ware_ingredient'] = Ingredient::where('Ingredient_Shop',$request->id)->where('Ingredient_Status',1)->get();
            return json_encode($data);
        }
    }


    public function getSelectRawImportAjax(Request $request)
    {
        if(!$request->id){
            return responseJsonMess(0, 'Thiếu ID');
        }
        else{
            $row = RawMaterial::where('Raw_Material_ID', $request->id)->where('Raw_Material_Status',1)->first();
            if(!$row){
                return responseJsonMess(0, 'Nguyên liệu thô không tồn tại');
            }
            return responseJsonData(1, $row);
        }
    }

    public function getSelectIngredientImportAjax(Request $request)
    {
        if(!$request->id){
            return responseJsonMess(0, 'Thiếu ID');
        }
        else{
            $row = Ingredient::where('Ingredient_ID', $request->id)->where('Ingredient_Status',1)->first();
            if(!$row){
                return responseJsonMess(0, 'Nguyên liệu tinh không tồn tại');
            }
            return responseJsonData(1, $row);
        }
    }

    public function postAdd(Request $req)
    {

        // dd($req->all());
        $store = $req->warehouse_shop ?? Session('user')->User_Location_Store;

        if (!$req->quantity_raw && !$req->price_raw && !$req->quantity_ingredient && !$req->price_ingredient) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng chọn loại nguyên liệu']);
        }

        $total = 0;
        $arr_raw_quantity = [];
        $arr_raw_price = [];
        $arr_ing_quantity = [];
        $arr_ing_price = [];
        if ($req->quantity_raw && $req->price_raw) {
            foreach ($req->quantity_raw as $key => $value) {
                if ($value <= 0) {
                    return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng nhập số lượng nguyên liệu thô lớn hơn 0']);
                }
                $total += $value * $req->price_raw[$key];
                $arr_raw_quantity[] = $key;
            }

            foreach ($req->price_raw as $key => $value) {
                if ($value <= 0) {
                    return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng nhập đơn giá nguyên liệu thô lớn hơn 0']);
                }
                $arr_raw_price[] = $key;
            }
        }
        $status = 1;
        if ($req->quantity_ingredient && $req->price_ingredient) {
            foreach ($req->quantity_ingredient as $key => $value) {
                if ($value <= 0) {
                    return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng nhập số lượng nguyên liệu tinh lớn hơn 0']);
                }
                $total += $value * $req->price_ingredient[$key];
                $arr_ing_quantity[] = $key;
            }

            foreach ($req->price_ingredient as $key => $value) {
                if ($value <= 0) {
                    return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng nhập đơn giá nguyên liệu tinh lớn hơn 0']);
                }
                $arr_ing_price[] = $key;
            }


            if($req->warehouse_status!= 1 && $req->warehouse_status!= 2){
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng chọn hình thức']);
            }

            $status =  $req->warehouse_status;
        }
        $total_number = number_format($total,3,",",".");
        // dd($a, $req->warehouse_total);
        if ($total_number != $req->warehouse_total) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Tổng tiền không hợp lệ']);
        }

        if (count($arr_raw_quantity)) {
            $check_raw = RawMaterial::whereIn('Raw_Material_ID', $arr_raw_quantity)->where('Raw_Material_Status',1)->where('Raw_Material_Shop',$store)->get();
            if (count($check_raw) != count($arr_raw_quantity)) {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Nguyên liệu thô không tồn tại']);
            }
        }

        if (count($arr_raw_price)) {
            $check_raw = RawMaterial::whereIn('Raw_Material_ID', $arr_raw_price)->where('Raw_Material_Status',1)->where('Raw_Material_Shop',$store)->get();
            if (count($check_raw) != count($arr_raw_price)) {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Nguyên liệu thô không tồn tại']);
            }
        }

        if (count($arr_ing_quantity)) {
            $check_ing = Ingredient::whereIn('Ingredient_ID', $arr_ing_quantity)->where('Ingredient_Status',1)->where('Ingredient_Shop',$store)->get();
            if (count($check_ing) != count($arr_ing_quantity)) {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Nguyên liệu tinh không tồn tại']);
            }
        }

        if (count($arr_ing_price)) {
            $check_ing = Ingredient::whereIn('Ingredient_ID', $arr_ing_price)->where('Ingredient_Status',1)->where('Ingredient_Shop',$store)->get();
            if (count($check_ing) != count($arr_ing_price)) {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Nguyên liệu tinh không tồn tại']);
            }
        }

        $check_user = User::where('User_ID', $req->userid)->where('User_Status',1)->first();
        if (!$check_user) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Người nhập không tồn tại']);
        }

        $check_shop = Shop::where('Shop_ID', $store)->where('Shop_Status',1)->first();
        if (!$check_shop) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Cửa hàng không tồn tại']);
        }

        $date_now = date('Y-m-d H:i:s');

        if($status != 1){
            $ing_update = Ingredient::whereIn('Ingredient_ID',$arr_ing_quantity)->get();
            $check_update = [];
            $arr_quantity_update = [];
            foreach($ing_update as $value_ing){
                $manage = json_decode($value_ing->Ingredient_Params, true); //string to objec
                $arr_ing = json_decode(json_encode($manage), true); //object to array
                // dd($arr_ing);
                foreach($arr_ing as $key => $value_raw){
                    $raw_update = RawMaterial::where('Raw_Material_ID',$key)->first();
                    $quantity_req = 0;
                    if(!in_array($key, $check_update)){
                        if(in_array($key, $arr_raw_quantity)){
                            $quantity_req = $req->quantity_raw[$key];
                            $check_update[] = $key;
                        }
                    }
                    $mul_req_ing = $req->quantity_ingredient[$value_ing->Ingredient_ID];//số lượng nguyên liệu tinh nhập vào

                    if(array_key_exists($key,$arr_quantity_update)){
                        $quantity_update = $arr_quantity_update[$key] + $quantity_req - ( $mul_req_ing * $value_raw );
                    }
                    else{
                        $quantity_update = $raw_update->Raw_Material_Quantity + $quantity_req - ( $mul_req_ing * $value_raw );
                    }

                    if($quantity_update < 0){
                        return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Nguyên liệu thô không đủ']);
                    }

                    $arr_quantity_update[$key] = $quantity_update;

                    // print_r ($arr_quantity_update);
                    // echo "<pre></pre>";
                }
            }
            foreach($arr_quantity_update as $key => $value){
                $raw_update = RawMaterial::where('Raw_Material_ID',$key)->first();
                $raw_update->Raw_Material_Quantity = $value;
                $raw_update->save();
            }
        }
        //add Thô trước
        if (count($arr_raw_quantity)) {
            foreach ($req->quantity_raw as $key => $value) {
                if($status == 1){
                    $raw_update = RawMaterial::where('Raw_Material_ID',$key)->first();
                    $quantity = $raw_update->Raw_Material_Quantity + $req->quantity_raw[$key];
                    $raw_update->Raw_Material_Quantity = $quantity;
                    $raw_update->save();
                }
                $arrayInsert = array(
                    'Warehouse_User' => $req->userid,
                    'Warehouse_Raw' => $key,
                    'Warehouse_Ingredient' => NULL,
                    'Warehouse_Number' => $req->warehouse_number,
                    'Warehouse_Shop' => $store,
                    'Warehouse_Total' => $req->price_raw[$key]*$req->quantity_raw[$key],
                    'Warehouse_Price' => $req->price_raw[$key],
                    'Warehouse_Quantity' => $req->quantity_raw[$key],
                    'Warehouse_Description' => $req->warehouse_description,
                    'Created_At' => $date_now,
                    'Warehouse_Status' => $status,
                    'Warehouse_IsDestroy' => 0,
                );
                $insertID = Warehouse::insertGetId($arrayInsert);
            }
        }

        if (count($arr_ing_quantity)) {
            foreach ($req->quantity_ingredient as $key => $value) {
                $ing_update = Ingredient::where('Ingredient_ID',$key)->first();
                $quantity = $ing_update->Ingredient_Quantity + $req->quantity_ingredient[$key];
                $ing_update->Ingredient_Quantity = $quantity;
                $ing_update->save();

                $arrayInsert = array(
                    'Warehouse_User' => $req->userid,
                    'Warehouse_Raw' => NULL,
                    'Warehouse_Ingredient' => $key,
                    'Warehouse_Number' => $req->warehouse_number,
                    'Warehouse_Shop' => $store,
                    'Warehouse_Total' => $req->price_ingredient[$key] * $req->quantity_ingredient[$key],
                    'Warehouse_Price' => $req->price_ingredient[$key],
                    'Warehouse_Quantity' =>  $req->quantity_ingredient[$key],
                    'Warehouse_Description' => $req->warehouse_description,
                    'Created_At' => $date_now,
                    'Warehouse_Status' => $status,
                    'Warehouse_IsDestroy' => 0,
                );
                $insertID = Warehouse::insertGetId($arrayInsert);
            }
        }



        if ($insertID) {
            writeLog('Nhập kho nguyên liệu: ' . $insertID);
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Nhập kho nguyên liệu thành công!']);
        } else {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Nhập kho nguyên liệu thất bại. vui lòng liên hệ admin']);
        }

    }

        /**
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
         */

        public function getEdit($id)
        {
            $user = Session('user');
            if ($user->User_Level != 1) {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Bạn không thể sửa!']);
            }
            Warehouse::findOrFail($id);
            return view('System.Warehouse.Import', compact('ListUser', 'user'));

        }


        public function postEdit(Request $req, $id)
        {
            $user = Session('user');
            if ($user->User_Level != 1) {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Bạn không thể sửa!']);
            }

            Warehouse::where('Warehouse_ID', $id)->first();
            if (!$req->user) {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng chọn người nhập kho']);
            }

            if ($req->raw == "0") {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng chọn loại nguyên liệu']);
            }

            if ($req->expiry) {
                $currentData = date("Y-m-d");
                if ($req->expiry < $currentData) {
                    return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Ngày hết hạn không hợp lệ']);
                }
            }

            if (!$req->name) {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng chọn tên nguyên liệu']);
            }

            if (!$req->quantity) {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng nhập số lượng nhập']);
            }

            if (!$req->price) {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng nhập giá nguyên liệu']);
            }
            if ($req->raw == 1) {
                $raw = DB::table('Raw_Material')->where('Raw_Material_ID', $req->material_id)->first();
                $currentQuantity = $raw->Raw_Material_Quantity;
                DB::table('Raw_Material')->where('Raw_Material_ID', $req->material_id)->update(['Raw_Material_Quantity' => $req->quantity + $currentQuantity]);
            } else {
                $ingredient = DB::table('ingredient')->where('ingredient_ID', $req->material_id)->first();
                $currentQuantity = $ingredient->ingredient_Quantity;
                DB::table('ingredient')->where('ingredient_ID', $req->material_id)->update(['ingredient_Quantity' => $req->quantity + $currentQuantity]);
            }
            $arrayUpdate = array(
                'Warehouse_User' => $req->userid,
                'Warehouse_Raw' => $req->raw == 1 ? $req->material_id : 0,
                'Warehouse_Ingredient' => $req->raw == 1 ? 0 : $req->material_id,
                'Warehouse_Number' => $req->number,
                'Warehouse_Expiry_Date' => $req->expiry,
                'Warehouse_Price' => $req->price,
                'Warehouse_Quantity' => $req->quantity,
                'Warehouse_Description' => $req->description,
                'Warehouse_Time_Import' => date('Y-m-d H:i:s'),
                'Warehouse_Status' => 1,
            );

            $insert = Warehouse::updated($arrayUpdate);
            if ($insert) {
                writeLog('Cập nhật kho nguyên liệu: ' . $id);
                return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Cập nhật kho nguyên liệu thành công!']);
            } else {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Cập nhật kho nguyên liệu thất bại. vui lòng liên hệ admin']);
            }

        }


        public function getRaw(Request $req)
        {
            if (!$req->id) {
                return response()->json(array('status' => false, 'message' => 'Thiếu ID'), 200);
            } else {
                if ($req->id == 1) {
                    $Raw = DB::table('Raw_Material')->where('Raw_Material_Status', 1)->get();
                    return response()->json(array('status' => true, 'raw' => true, 'data' => $Raw), 200);
                } else {
                    $Ingredient = DB::table('ingredient')->where('ingredient_Status', 1)->get();
                    return response()->json(array('status' => true, 'raw' => false, 'data' => $Ingredient), 200);
                }
            }
        }


     public function getDestroyRaw(Request $request)
    {
        $user = session('user');
        $destroyRawList = Warehouse::query()->join('raw_material', 'Warehouse_Raw', 'Raw_Material_ID')
            ->join('users', 'Warehouse_User', 'User_ID')
            ->join('shop', 'Warehouse_Shop', 'Shop_ID');
        if($user->User_Level == 2){
            $destroyRawList = $destroyRawList->where('Warehouse_Shop',$user->User_Location_Store);
        }
        $shopList = '';

        if ($user->User_Level == 1 || $user->User_Level == 3) {
            $shopList = Shop::all();
            $request->search_destroy_raw_staff ? $destroyRawList->where('User_Name', $request->search_destroy_raw_staff) : 0;
        }
        $request->search_destroy_raw_id ? $destroyRawList->where('Warehouse_ID', $request->search_destroy_raw_id) : 0;

        $request->search_destroy_raw_name ? $destroyRawList->where('Raw_Material_Name', $request->search_destroy_raw_name) : 0;

        $request->search_destroy_raw_unit ? $destroyRawList->where('Raw_Material_Unit', $request->search_destroy_raw_unit) : 0;

        $request->search_destroy_raw_status != null ? $destroyRawList->where('Warehouse_Status', $request->search_destroy_raw_status) : 0;

        $request->search_destroy_raw_from && $request->search_destroy_raw_to ?
            $destroyRawList->whereRaw("DATE(warehouse.Updated_At) >= '$request->search_destroy_raw_from' and DATE(warehouse.Updated_At)  <= '$request->search_destroy_raw_to'") : 0;


        $request->search_destroy_raw_from && !$request->search_destroy_raw_to ?
            $destroyRawList->whereRaw("DATE(warehouse.Updated_At)  <= '$request->search_destroy_raw_to'") : 0;

        !$request->search_destroy_raw_from && $request->search_destroy_raw_to ?
            $destroyRawList->whereRaw("DATE(warehouse.Updated_At)  <= '$request->search_destroy_raw_to'") : 0;

        $user->User_Level == 2 ? $rawList = RawMaterial::where('Raw_Material_Shop', $user->User_Location_Store)->select('Raw_Material_ID', 'Raw_Material_Name', 'Raw_Material_Unit')->get() : $rawList = null;
        $destroyRawList = $destroyRawList->where("Warehouse_isDestroy", 1)
            ->whereNotNull('Warehouse_Raw')
            ->select('User_Name', 'Shop_Name', 'Raw_Material_Name', 'Raw_Material_Unit', 'Warehouse_ID', 'Warehouse_Quantity', 'Warehouse_Description', 'warehouse.Updated_At', 'Warehouse_Status')
            ->get();
        $unitList = RawMaterial::select('Raw_Material_Unit')->groupby('Raw_Material_Unit')->get();
        return view('System.Warehouse.Destroy-Raw.Destroy-Raw-Manage', compact('destroyRawList', 'rawList', 'shopList', 'unitList'));

    }

     public function postDestroyRaw(Request $request)
    {
        $user = Session('user');
        $request->validate([
            'add_destroy_raw_id' => 'bail|required',
            'add_destroy_raw_quantity' => 'bail|required|numeric|min:0|not_in:0',
        ], [
            'add_destroy_raw_id.required' => ERROR_VALIDATE['require'] . ' nguyên liệu.',
            'add_destroy_raw_quantity.required' => ERROR_VALIDATE['require'] . ' số lượng.',
            'add_destroy_raw_quantity.numeric' => ERROR_VALIDATE['numeric'] . ' số lượng.',
            'add_destroy_raw_quantity.not_in' => ERROR_VALIDATE['not_in'] . ' số lượng.',
            'add_destroy_raw_quantity.min' => ERROR_VALIDATE['min'] . ' số lượng',
        ]);
        $shop_id = '';
        if ($user->User_Level == 1) {
            $request->validate([
                'add_destroy_raw_shop' => 'bail|required|exists:shop,Shop_ID'
            ], [
                'add_destroy_raw_shop.required' => ERROR_VALIDATE['require'] . ' cửa hàng.',
                'add_destroy_raw_shop.exists' => ERROR_VALIDATE['exists'] . ' cửa hàng.',
            ]);
            if (!RawMaterial::where('Raw_Material_ID', $request->add_destroy_raw_id)->where('Raw_Material_Shop', $request->add_destroy_raw_shop)->first()) {
                return responseRedirect(0, 'Dữ liệu nhập không chính xác.');
            }
            $shop_id = $request->add_destroy_raw_shop;
        }
        $user->User_Level == 2 ? $shop_id = $user->User_Location_Store : 0;
        $arrayInsert = array(
            'Warehouse_User' => $user->User_ID,
            'Warehouse_Shop' => $shop_id,
            'Warehouse_Raw' => $request->add_destroy_raw_id,
            'Warehouse_Quantity' => -$request->add_destroy_raw_quantity,
            'Warehouse_Description' => $request->add_destroy_raw_description,
            'Warehouse_IsDestroy' => 1,
            'Warehouse_Status' => 1,
            'Created_At' => date('Y-m-d H:i:s'),
            'Updated_At' => date('Y-m-d H:i:s'),
        );
        $insertID = Warehouse::insertGetId($arrayInsert);
        $update_raw = RawMaterial::where('Raw_Material_ID', $request->add_destroy_raw_id)->first();
        $update_raw->Raw_Material_Quantity -= $request->add_destroy_raw_quantity;
        $update_raw->save();

        if ($insertID) {
            //ghi log
            writeLog('Huỷ kho nguyên liệu ID: ' . $insertID);
            return responseRedirect(1, 'Huỷ nguyên liệu thành công!');
        }
        return responseRedirect(0, ERROR_CONTACT_AMDIN);


    }

     public function getDataDestroyRaw(Request $request)
    {
        if (!$request->shop_id) {
            return responseJsonMess(0, 'Thiếu ID');
        }
        if (!Shop::where('Shop_ID', $request->Shop_ID)->get()) {
            return responseJsonMess(0, 'Cửa hàng không đúng.');
        }
        $rawList = RawMaterial::where('Raw_Material_Shop', $request->shop_id)
            ->select('Raw_Material_ID', 'Raw_Material_Name', 'Raw_Material_Unit')->get();
        return responseJsonData(1, $rawList);
    }


    public function postEditDestroyRaw(Request $request)
    {
        $request->validate([
            'edit_destroy_raw_warehouse_id' => 'bail|required|exists:warehouse,Warehouse_ID',
            'edit_destroy_raw_id' => 'bail|required|exists:raw_material,Raw_Material_ID',
            'edit_destroy_raw_quantity' => 'bail|required|numeric|min:0|not_in:0',
        ], [
            'edit_destroy_raw_warehouse_id.required' => ERROR_VALIDATE['require'] . ' trong kho.',
            'edit_destroy_raw_warehouse_id.exists' => ERROR_VALIDATE['exists'] . ' trong kho.',
            'edit_destroy_raw_id.required' => ERROR_VALIDATE['require'] . ' nguyên liệu thô.',
            'edit_destroy_raw_id.exists' => ERROR_VALIDATE['exists'] . ' nguyên liệu thô.',
            'edit_destroy_raw_quantity.required' => ERROR_VALIDATE['require'] . ' số lượng.',
            'edit_destroy_raw_quantity.numeric' => ERROR_VALIDATE['numeric'] . ' số lượng.',
            'edit_destroy_raw_quantity.not_in' => ERROR_VALIDATE['not_in'] . ' số lượng.',
            'edit_destroy_raw_quantity.min' => ERROR_VALIDATE['min'] . ' số lượng',
        ]);


        $warehouse = Warehouse::where('Warehouse_ID', $request->edit_destroy_raw_warehouse_id)->first();
        $quantity_raw = $warehouse->Warehouse_Quantity*-1;

        $update_raw = RawMaterial::where('Raw_Material_ID', $warehouse->Warehouse_Raw)->first();
        $update_raw->Raw_Material_Quantity = $update_raw->Raw_Material_Quantity + $quantity_raw - $request->edit_destroy_raw_quantity;
        $update_raw->save();


        // dd($quantity_raw,$request->edit_destroy_raw_quantity);

        $editData['Warehouse_Quantity'] = -$request->edit_destroy_raw_quantity;
        $editData['Warehouse_Description'] = $request->edit_destroy_description;
        $editData['Warehouse_Raw'] = $request->edit_destroy_raw_id;
        $editData['Updated_At'] = date('Y-m-d H:i:s');

        $updateStatus = Warehouse::where('Warehouse_ID', $request->edit_destroy_raw_warehouse_id)->update($editData);
        if ($updateStatus) {
            // ghi log
            writeLog('Sửa nguyên liệu huỷ : ID' . $request->edit_destroy_raw_warehouse_id);
            return responseRedirect(1, 'Sửa nguyên liệu huỷ thành công!');
        }
        return responseRedirect(0, ERROR_CONTACT_AMDIN);

    }


    public function getDataEditDestroyRaw(Request $request)
    {
        $warehouseData = Warehouse::where('Warehouse_ID', $request->warehouse_id)
            ->join('shop', 'Warehouse_Shop', 'Shop_ID')->first();
        if (!$request->warehouse_id || !$warehouseData) {
            return responseJsonMess(0, 'Dữ liệu kho không tồn tại!');
        }
        return responseJsonData(1, $warehouseData);
    }


     public function postDeleteDestroyRaw($id)
    {
        $deleteRaw = Warehouse::where('Warehouse_ID', $id)->first();

        $quantity_raw = $deleteRaw->Warehouse_Quantity*-1;

        $update_raw = RawMaterial::where('Raw_Material_ID', $deleteRaw->Warehouse_Raw)->first();
        $update_raw->Raw_Material_Quantity = $update_raw->Raw_Material_Quantity + $quantity_raw;
        $update_raw->save();
        if (!$deleteRaw) {
            return responseRedirect(0, "ID $id không tồn tại.");
        }
        $updateStatus = Warehouse::where('Warehouse_ID', $id)->update(['Warehouse_Status' => 0, 'Updated_At' => date('Y-m-d H:i:s')]);
        if ($updateStatus) {
            // ghi log
            writeLog('Xoá hủy nguyên liệu huỷ thô : ID' . $id);
            return responseRedirect(1, 'Huỷ lệnh thành công!');
        }
        return responseRedirect(0, ERROR_CONTACT_AMDIN);
    }


    public function postEnableDestroyRaw($id)
    {
        $enableRaw = Warehouse::where('Warehouse_ID', $id)->first();
        $quantity_raw = $enableRaw->Warehouse_Quantity;

        $update_raw = RawMaterial::where('Raw_Material_ID', $enableRaw->Warehouse_Raw)->first();
        $update_raw->Raw_Material_Quantity = $update_raw->Raw_Material_Quantity + $quantity_raw;
        $update_raw->save();
        if (!$enableRaw) {
            return responseRedirect(0, "ID $id không tồn tại.");
        }
        $updateStatus = Warehouse::where('Warehouse_ID', $id)->update(['Warehouse_Status' => 1, 'Updated_At' => date('Y-m-d H:i:s')]);
        if ($updateStatus) {
            // ghi log
            writeLog('Mở hủy nguyên liệu thô thành công : ID' . $id);
            return responseRedirect(1, 'Mở hủy nguyên liệu thô thành công!');
        }
        return responseRedirect(0, ERROR_CONTACT_AMDIN);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function getDestroyIngredient(Request $request)
    {
        $user = session('user');
        $shopList = [];
        $ingredientList = null;
        $destroyIngredientList = Warehouse::query()->where('Warehouse_isDestroy', 1)->whereNotNull('Warehouse_Ingredient')
            ->join('users', 'Warehouse_User', 'User_ID')
            ->join('shop','Warehouse_Shop', 'Shop_ID')
            ->join('ingredient', 'Warehouse_Ingredient', 'Ingredient_ID');

        if($user->User_Level == 1 ) {
            $shopList = Shop::select('Shop_ID', 'Shop_Name')->get();
            $request->search_destroy_ingredient_shop != null?$destroyIngredientList->where('Warehouse_Shop', $request->search_destroy_ingredient_shop):0;
        }
        if($user->User_Level == 2) {
            $ingredientList = Ingredient::select('Ingredient_ID', 'Ingredient_Name', 'Ingredient_Unit')->where('Ingredient_Shop', $user->User_Location_Store)->get();
            $destroyIngredientList->where('Warehouse_Shop', $user->User_Location_Store);
        }

        $request->search_destroy_ingredient_id?$destroyIngredientList->where('Warehouse_ID', $request->search_destroy_ingredient_id):0;
        $request->search_destroy_ingredient_staff?$destroyIngredientList->where('User_Name', $request->search_destroy_ingredient_staff):0;
        $request->search_destroy_ingredient_name?$destroyIngredientList->where('Ingredient_Name', $request->search_destroy_ingredient_name):0;
        $request->search_destroy_ingredient_unit != null?$destroyIngredientList->where('Ingredient_unit', $request->search_destroy_ingredient_unit):0;
        $request->search_destroy_ingredient_status != null?$destroyIngredientList->where('Warehouse_Status', $request->search_destroy_ingredient_status):0;
        $request->search_destroy_ingredient_from  && $request->search_destroy_ingredient_to?$destroyIngredientList->whereRaw("DATE(warehouse.Updated_At) >= '$request->search_destroy_ingredient_from' and DATE(warehouse.Updated_At)  <= '$request->search_destroy_ingredient_to'"):0;
        $request->search_destroy_ingredient_from  && !$request->search_destroy_ingredient_to?$destroyIngredientList->whereRaw("DATE(warehouse.Updated_At) >= '$request->search_destroy_ingredient_from' "):0;
        !$request->search_destroy_ingredient_from  && $request->search_destroy_ingredient_to?$destroyIngredientList->whereRaw("DATE(warehouse.Updated_At) <= '$request->search_destroy_ingredient_to'"):0;

        $destroyIngredientList = $destroyIngredientList->select([
            'Warehouse_ID',
            'Warehouse_User',
            'Warehouse_Quantity',
            'Warehouse_Status',
            'Warehouse_Description',
            'warehouse.Updated_At',
            'Ingredient_Name',
            'Ingredient_Unit',
            'User_Name',
            'Shop_Name',

        ])->paginate(25);
        $unitList = Warehouse::join('ingredient', 'Warehouse_Ingredient','Ingredient_ID')->select('Ingredient_Unit')->groupBy('Ingredient_Unit')->get();
        return view('System.Warehouse.Destroy-Ingredient.Destroy-Ingredient', compact('destroyIngredientList','shopList', 'ingredientList', 'unitList'));

    }

    public function getDataDestroyIngredient(Request $request) {
        if (!$request->shop_id) {
            return responseJsonMess(0, 'Thiếu ID');
        }
        if (!Shop::where('Shop_ID', $request->Shop_ID)->get()) {
            return responseJsonMess(0, 'Cửa hàng không đúng.');
        }
        $ingredientList = Ingredient::where('Ingredient_Shop', $request->shop_id)
            ->select('Ingredient_ID', 'Ingredient_Name', 'Ingredient_Unit')->get();
        return responseJsonData(1, $ingredientList);

    }

    public function postDestroyIngredient(Request $request)
    {
        $user = Session('user');
        $request->validate([
            'add_destroy_ingredint_quantity' => 'bail|required|numeric|min:0|not_in:0',
        ], [
            'add_destroy_ingredint_quantity.required' => ERROR_VALIDATE['require'] . ' số lượng.',
            'add_destroy_ingredint_quantity.numeric' => ERROR_VALIDATE['numeric'] . ' số lượng.',
            'add_destroy_ingredint_quantity.not_in' => ERROR_VALIDATE['not_in'] . ' số lượng.',
            'add_destroy_ingredint_quantity.min' => ERROR_VALIDATE['min'] . ' số lượng',
        ]);
        $warehouse_shop = '';
        if ($user->User_Level == 1) {
            $request->validate([
                'add_destroy_ingredient_shop' => 'bail|required|exists:shop,shop_ID',

            ], [
                'add_destroy_ingredient_shop.required' => ERROR_VALIDATE['require'] . ' cửa hàng.',
                'add_destroy_ingredient_shop.exists' => ERROR_VALIDATE['exists'] . ' cửa hàng.',

            ]);
            $checkIngredientExist = Ingredient::where([
                ['Ingredient_ID', $request->add_destroy_ingredient_ingredient_id],
                ['Ingredient_Shop', $request->add_destroy_ingredient_shop]
                ])->get();

            if (!$checkIngredientExist) {
                return responseRedirect(0, 'Nguyên liệu tinh không tồn tại.');
            }
            $warehouse_shop = $request->add_destroy_ingredient_shop;

        }

        if ($user->User_Level == 2) {
            $checkIngredientExist = Ingredient::where([
                ['Ingredient_ID', $request->add_destroy_ingredient_ingredient_id],
                ['Ingredient_Shop', $user->User_Location_Store]
            ])->get();
            if (!$checkIngredientExist) {
                return responseRedirect(0, 'Nguyên liệu tinh không tồn tại.');
            }
            $warehouse_shop =  $user->User_Location_Store;
        }

        $arrayInsert = array(
            'Warehouse_User' => $user->User_ID,
            'Warehouse_Shop' => $warehouse_shop,
            'Warehouse_Ingredient' => $request->add_destroy_ingredient_ingredient_id,
            'Warehouse_Quantity' => -$request->add_destroy_ingredint_quantity,
            'Warehouse_Description' => $request->add_destroy_ingredint_description,
            'Warehouse_IsDestroy' => 1,
            'Created_At' => date('Y-m-d H:i:s'),
            'Updated_At' => date('Y-m-d H:i:s'),
            'Warehouse_Status' => 1,
        );

        $insertID = Warehouse::insertGetId($arrayInsert);

        $update_ing = Ingredient::where('Ingredient_ID', $request->add_destroy_ingredient_ingredient_id)->first();
        $update_ing->Ingredient_Quantity -= $request->add_destroy_ingredint_quantity;
        $update_ing->save();

        if ($insertID){
            writeLog('Huỷ nguyên liệu tinh ID: ' . $insertID);
            return responseRedirect(1, 'Huỷ nguyên liệu tinh thành công!');
        }
        return responseRedirect(0, 'Huỷ nguyên liệu tinh thất bại. vui lòng liên hệ admin');

    }

    public function postDeleteDestroyIngredient($id) {
        $deleteIngredient = Warehouse::where('Warehouse_ID', $id)->first();

        $update_ing = Ingredient::where('Ingredient_ID', $deleteIngredient->Warehouse_Ingredient)->first();
        $update_ing->Ingredient_Quantity -= $deleteIngredient->Warehouse_Quantity;
        $update_ing->save();
        if (!$deleteIngredient) {
            return responseRedirect(0, "ID $id không tồn tại.");
        }
        $updateStatus = Warehouse::where('Warehouse_ID', $id)->update(['Warehouse_Status' => 0, 'Updated_At' => date('Y-m-d H:i:s')]);
        if ($updateStatus) {
            // ghi log
            writeLog('Xoá hủy nguyên liệu huỷ tinh : ID' . $id);
            return responseRedirect(1, 'Huỷ lệnh thành công!');
        }
        return responseRedirect(0, ERROR_CONTACT_AMDIN);
    }

    public function postEnableDestroyIngredient($id) {
        $enableIngredient = Warehouse::where('Warehouse_ID', $id)->first();
        $update_ing = Ingredient::where('Ingredient_ID', $enableIngredient->Warehouse_Ingredient)->first();
        $update_ing->Ingredient_Quantity += $enableIngredient->Warehouse_Quantity;
        $update_ing->save();
        if (!$enableIngredient) {
            return responseRedirect(0, "ID $id không tồn tại.");
        }
        $updateStatus = Warehouse::where('Warehouse_ID', $id)->update(['Warehouse_Status' => 1, 'Updated_At' => date('Y-m-d H:i:s')]);
        if ($updateStatus) {
            // ghi log
            writeLog('Mở hủy nguyên liệu tinh thành công : ID' . $id);
            return responseRedirect(1, 'Mở hủy nguyên liệu tinh thành công!');
        }
        return responseRedirect(0, ERROR_CONTACT_AMDIN);
    }

    public function getDataEditDestroyIngredient(Request $request) {
        $warehouseData = Warehouse::where('Warehouse_ID', $request->warehouse_id)
            ->join('shop', 'Warehouse_Shop', 'Shop_ID')->first();
        if (!$request->warehouse_id || !$warehouseData) {
            return responseJsonMess(0, 'Dữ liệu kho không tồn tại!');
        }
        return responseJsonData(1, $warehouseData);
    }

    public function postEditDestroyIngredient(Request $request) {
        $request->validate([
            'edit_destroy_ingredint_quantity' => 'bail|required|numeric|min:0|not_in:0',
            'edit_destroy_ingredient_ingredient_id' => 'bail|required',
        ], [
            'edit_destroy_ingredint_quantity.required' => ERROR_VALIDATE['require'] . ' số lượng.',
            'edit_destroy_ingredint_quantity.numeric' => ERROR_VALIDATE['numeric'] . ' số lượng.',
            'edit_destroy_ingredint_quantity.not_in' => ERROR_VALIDATE['not_in'] . ' số lượng.',
            'edit_destroy_ingredint_quantity.min' => ERROR_VALIDATE['min'] . ' số lượng',
            'edit_destroy_ingredient_ingredient_id.required' => ERROR_VALIDATE['require'] . ' nguyên liệu tinh.',
        ]);

        // dd($request->all());
        $warehouse = Warehouse::where('Warehouse_ID', $request->edit_destroy_ingredient_id)->first();
        $quantity_raw = $warehouse->Warehouse_Quantity*-1;

        $update_raw = Ingredient::where('Ingredient_ID', $warehouse->Warehouse_Ingredient)->first();
        $update_raw->Ingredient_Quantity = $update_raw->Ingredient_Quantity + $quantity_raw - $request->edit_destroy_ingredint_quantity;
        $update_raw->save();


        // dd($quantity_raw,$request->edit_destroy_raw_quantity);

        $editData['Warehouse_Quantity'] = -$request->edit_destroy_ingredint_quantity;
        $editData['Warehouse_Description'] = $request->edit_destroy_ingredint_description;
        $editData['Warehouse_Ingredient'] = $request->edit_destroy_ingredient_ingredient_id;
        $editData['Updated_At'] = date('Y-m-d H:i:s');

        $updateStatus = Warehouse::where('Warehouse_ID', $request->edit_destroy_ingredient_id)->update($editData);
        if ($updateStatus) {
            // ghi log
            writeLog('Sửa nguyên liệu huỷ : ID' . $request->edit_destroy_raw_warehouse_id);
            return responseRedirect(1, 'Sửa nguyên liệu huỷ thành công!');
        }
        return responseRedirect(0, ERROR_CONTACT_AMDIN);
    }

        /**
         * @param Request $req
         * @return \Illuminate\Http\JsonResponse
         */
        public
        function getUnit(Request $req)
        {
            if ($req->type_material == 1) {
                $raw = DB::table('Raw_Material')->where('Raw_Material_ID', $req->rawMaterial)->first();
                $unit = $raw->Raw_Material_Unit;
                return response()->json(array('status' => true, 'data' => $unit), 200);
            } else {
                $ingredient = DB::table('ingredient')->where('ingredient_ID', $req->rawMaterial)->first();
                $unit = $ingredient->ingredient_Unit;
                return response()->json(array('status' => true, 'raw' => false, 'data' => $unit), 200);
            }

        }


        /**
         * @param $id
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public
        function getEditImport($id)
        {

            $importData = DB::table('Warehouse')->where('Warehouse_ID', $id)->value('Warehouse_Ingredient');
            if ($importData) {
                $importData = DB::table('Warehouse')->where('Warehouse_ID', $id)
                    ->join('ingredient', 'Warehouse_Ingredient', '=', 'ingredient_ID')
                    ->join('users', 'Warehouse_User', '=', 'User_ID')
                    ->select('Warehouse_ID', 'Warehouse_Quantity', 'Warehouse_Status', 'users.User_ID', 'users.User_Name', 'Warehouse_Ingredient', 'ingredient.ingredient_Name', 'Warehouse_Number', 'Warehouse_Expiry_Date', 'Warehouse_Price', 'Warehouse_Description', 'Warehouse_Name')
                    ->first();
            } else {
                $importData = DB::table('Warehouse')->where('Warehouse_ID', $id)
                    ->join('Raw_Material', 'Warehouse_Raw', '=', 'Raw_Material_ID')
                    ->join('users', 'Warehouse_User', '=', 'User_ID')
                    ->select('Warehouse_ID', 'Warehouse_Quantity', 'Warehouse_Status', 'users.User_ID', 'users.User_Name', 'Warehouse_Raw', 'Raw_Material.Raw_Material_Name', 'Warehouse_Number', 'Warehouse_Expiry_Date', 'Warehouse_Price', 'Warehouse_Description', 'Warehouse_Name')
                    ->first();
            }
            return view('System.Warehouse.edit-import', compact('importData'));
        }


    /**
     * @param Request $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEditImport(Request $req)
    {
        $user = Session('user');
        if ($user->User_Level != 1) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Bạn không thể sửa!']);
        }

        Warehouse::where('Warehouse_ID', $req->warehouse_id)->first();

        if ($req->raw == "0") {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng chọn loại nguyên liệu']);
        }

        if (!$req->material_id) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng chọn tên nguyên liệu']);
        }

        if (!$req->quantity) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng nhập số lượng nhập']);
        }

        if (!$req->price) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng nhập giá nguyên liệu']);
        }

        if ($req->raw == 1) {
            $raw = DB::table('Raw_Material')->where('Raw_Material_ID', $req->material_id)->first();
            $currentQuantity = $raw->Raw_Material_Quantity;
            DB::table('Raw_Material')->where('Raw_Material_ID', $req->material_id)->update(['Raw_Material_Quantity' => $req->quantity + $currentQuantity]);
        } else {
            $ingredient = DB::table('ingredient')->where('ingredient_ID', $req->material_id)->first();
            $currentQuantity = $ingredient->ingredient_Quantity;
            DB::table('ingredient')->where('ingredient_ID', $req->material_id)->update(['ingredient_Quantity' => $req->quantity + $currentQuantity]);
        }
        $arrayUpdate = array(
            'Warehouse_User' => $req->username,
            'Warehouse_Raw' => $req->raw == 1 ? $req->material_id : 0,
            'Warehouse_Ingredient' => $req->raw == 1 ? 0 : $req->material_id,
            'Warehouse_Number' => $req->number,
            'Warehouse_Expiry_Date' => $req->expiry,
            'Warehouse_Price' => $req->price,
            'Warehouse_Quantity' => $req->quantity,
            'Warehouse_Description' => $req->description,
            'Warehouse_Time_Import' => date('Y-m-d H:i:s'),
            'Warehouse_Status' => 1,
            'Warehouse_Name' => session('user')->User_Location_Store
        );

        $insert = DB::table('Warehouse')->where('Warehouse_ID', $req->warehouse_id)->update($arrayUpdate);
        if ($insert) {
            writeLog('Sửa kho nguyên liệu: ' . $req->warehouse_id);
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Sửa kho nguyên liệu thành công!']);
        } else {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Sửa kho nguyên liệu thất bại. vui lòng liên hệ admin']);
        }

    }
}
