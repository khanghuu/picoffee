<?php

namespace App\Http\Controllers\System;

use App\Model\Ingredient;
use App\Model\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Model\RawMaterial;

class IngredientController extends Controller
{
    public function getManageIngredient(Request $request)
    {
        $ingredientList = Ingredient::query();
        $user = session('user');
        if ($user->User_Level == 2) {
            $ingredientList->where("Ingredient_Shop", $user->User_Location_Store);
        }
        if ($request->search_ingredient_code) {
            $ingredientList->where("Ingredient_Code", $request->search_ingredient_code);
        }
        if ($request->search_ingredient_name) {
            $ingredientList->where("Ingredient_Name", $request->search_ingredient_name);
        }
        if ($request->search_ingredient_unit) {
            $ingredientList->where("Ingredient_Unit", $request->search_ingredient_unit);
        }
        if ($request->search_ingredient_status != null) {
            $ingredientList->where("Ingredient_Status", $request->search_ingredient_status);
        }

        $ingredientList =$ingredientList->join('shop', 'Ingredient_Shop', 'Shop_ID')->orderBy('ingredient.Created_At','desc')->paginate(25);

        $unitList = Ingredient::select('Ingredient_Unit')->whereIn('Ingredient_Status', [0,1])->groupBy('Ingredient_Unit')->get();
        $rawList = RawMaterial::select('Raw_Material_Name', 'Raw_Material_ID', 'Raw_Material_Unit')->where('Raw_Material_Status', 1)->where('Raw_material_Shop', $user->User_Location_Shop)->get();
        $shopList = Shop::select('Shop_ID', 'Shop_Name')->get();
        return view('System.Data.Ingredient.Ingredient-Manage', compact('ingredientList', 'unitList',  'rawList', 'shopList'));

    }

    public function getAjaxIngredient(Request $request)
    {
        if(!$request->id){
            return responseJsonMess(0, 'Thiếu ID');
        }
        $ingredient =Ingredient::join('shop', 'Ingredient_Shop', 'Shop_ID')->where('Ingredient_ID', $request->id)->first();

        if(!$ingredient){
            return responseJsonMess(0, 'Nguyên liệu tinh không tồn tại');
        }
        if($ingredient->Ingredient_Params){
            $json = json_decode($ingredient->Ingredient_Params);
            $keyArr = array();
            $tempArr = array();
            foreach($json as $k=>$v){
                $keyArr[] = $k;
                $tempArr[$k] = $v;
            }
            $rawList = RawMaterial::whereIn('Raw_Material_ID', $keyArr)->select('Raw_Material_ID', 'Raw_Material_Name', 'Raw_Material_Unit')->get();

            foreach($rawList as $v){
                $key = $v->Raw_Material_ID;
                $v->number = $tempArr[$key];
            }
            $ingredient->Ingredient_Params = $rawList;

        }
        else{
            $ingredient->Ingredient_Params = null;
        }
        $data['Ingredient'] = $ingredient;

        $data['RawList'] = RawMaterial::where('Raw_Material_Shop', $ingredient->Ingredient_Shop)->where('Raw_Material_Status', 1)->get();
        $data['RawList'] = RawMaterial::where('Raw_Material_Shop', $ingredient->Ingredient_Shop)->where('Raw_Material_Status', 1)->get();
        return responseJsonData(1, $data);
    }

    public function postAddIngredient(Request $request)
    {
        $request->validate([
            'add_ingredient_name' => 'bail|required|max:255',
            // 'add_ingredient_number' => 'bail|required|numeric|min:0|not_in:0',
            'add_ingredient_number' => 'bail|required|numeric|in:1',
            'add_ingredient_unit' => 'bail|required',
            "raw"  => "required|array",
        ],
        [
            'add_ingredient_name.required' => ERROR_VALIDATE['require'].' tên.',
            'add_ingredient_name.max' => ERROR_VALIDATE['max'].' tên.',
            'add_ingredient_number.required' => ERROR_VALIDATE['require'].' số lượng.',
            'add_ingredient_number.numeric' => ERROR_VALIDATE['numeric'].' số lượng.',
            'add_ingredient_number.in' => "Số lượng không hợp lệ",
            // 'add_ingredient_number.not_in' => ERROR_VALIDATE['not_in'].' số lượng.',
            'add_ingredient_unit.required' => ERROR_VALIDATE['require'].' đơn vị.',
            'raw.required' => ERROR_VALIDATE['require'].' nguyên liệu thô.',
        ]);
        $shopID = null;
        $user = session('user');
        if ($user->User_Level == 1) {
            $request->validate([
                'add_ingredient_shop' => 'bail|required|exists:shop,Shop_ID'
            ],[
                'add_ingredient_shop.required' => ERROR_VALIDATE['require'].' cửa hàng.',
                'add_ingredient_shop.exists' => ERROR_VALIDATE['exists'].' cửa hàng.',
            ]);
            $shopID = $request->add_ingredient_shop;
        }
        if ($user->User_Level == 2) {
            $shopID = $user->User_Location_Store;
        }
        $checkIngredientName = Ingredient::where('Ingredient_Name', $request->add_ingredient_name)->where('Ingredient_Shop', $shopID)->first();
        if ($checkIngredientName) {
            return responseRedirect(0, "Lỗi, tên nguyên liệu đã tồn tại!");
        }
        foreach($request->raw as $key => $value){
            $rawData = RawMaterial::where('Raw_Material_ID', $key)->first();
            if ($rawData) {
                if ($value <= 0) {
                    return responseRedirect(0, 'Lỗi, Số lượng của nguyên liệu thô phải lớn hơn 0.');
                }
            }
            else {
                return responseRedirect(0, 'Lỗi, Nguyên liệu thô không tồn tại.');
            }
        }

        $jsonRawData = json_encode($request->raw);
        $ingredient_code = $this->generateIngedientCode($request->add_ingredient_name);

        $arrayInsert = array(
            'Ingredient_Code' => $ingredient_code,
            'Ingredient_Name' => $request->add_ingredient_name,
            // 'Ingredient_Number' => $request->add_ingredient_number,
            'Ingredient_Number' => 1,
            'Ingredient_Unit' => $request->add_ingredient_unit,
            'Ingredient_Params' => $jsonRawData,
            'Ingredient_Status' => $request->add_ingredient_status?1:0,
            'Ingredient_Shop' => $shopID,
            'Created_At' => date('Y-m-d H:i:s'),
            'Updated_At' => date('Y-m-d H:i:s'),
        );

        $insertID = Ingredient::insertGetId($arrayInsert);
        if($insertID){
            writeLog('Thêm nguyên liệu tinh ID: '.$insertID);
            return responseRedirect(1,'Thêm nguyên liệu tinh thành công!');
        }else{
            responseRedirect(0,ERROR_CONTACT_AMDIN);
        }

    }

    public function postEditIngredient(Request $request)
    {
        $request->validate([
            'edit_ingredient_name' => 'bail|required|max:255',
            // 'edit_ingredient_number' => 'bail|required|numeric|min:0|not_in:0',
            'edit_ingredient_number' => 'bail|required|numeric|in:1',
            'edit_ingredient_unit' => 'bail|required',
            "raw"  => "required|array",
        ],
            [
                'edit_ingredient_name.required' => ERROR_VALIDATE['require'].' tên.',
                'edit_ingredient_name.max' => ERROR_VALIDATE['max'].' tên.',
                'edit_ingredient_number.required' => ERROR_VALIDATE['require'].' số lượng.',
                'edit_ingredient_number.numeric' => ERROR_VALIDATE['numeric'].' số lượng.',
                // 'edit_ingredient_number.not_in' => ERROR_VALIDATE['not_in'].' số lượng.',
                'edit_ingredient_number.in' => "Số lượng không hợp lệ",
                'edit_ingredient_unit.required' => ERROR_VALIDATE['require'].' đơn vị.',
                'raw.required' => ERROR_VALIDATE['require'].' nguyên liệu thô.',
            ]);
        $shopID = null;
        $user = session('user');


        foreach($request->raw as $key => $value){
            $rawData = RawMaterial::where('Raw_Material_ID', $key)->first();
            if ($rawData) {
                if ($value <= 0) {
                    return responseRedirect(0, 'Lỗi, Số lượng của nguyên liệu thô phải lớn hơn 0.');
                }
            }
            else {
                return responseRedirect(0, 'Lỗi, Nguyên liệu thô không tồn tại.');
            }
        }

        $jsonRawData = json_encode($request->raw);
        $arrayUpdate = array(
            'Ingredient_Name' => $request->edit_ingredient_name,
            'Ingredient_Number' => $request->edit_ingredient_number,
            'Ingredient_Unit' => $request->edit_ingredient_unit,
            'Ingredient_Params' => $jsonRawData,
            'Ingredient_Status' => $request->edit_ingredient_status?1:0,

            'Updated_At' => date('Y-m-d H:i:s'),
        );

        $updateStatus = Ingredient::where('Ingredient_ID', $request->edit_ingredient_id)->update($arrayUpdate);
        if($updateStatus){
            writeLog('Sửa nguyên liệu tinh ID: '. $request->edit_ingredient_id);
            return responseRedirect(1,'Sửa nguyên liệu tinh thành công!');
        }else{
            return responseRedirect(0,ERROR_CONTACT_AMDIN);
        }

    }

    public function getDeleteIngredient($id)
    {
        $deleteStatus = Ingredient::where('Ingredient_ID', $id)->delete();
        if ($deleteStatus) {
            writeLog('Xóa nguyên liệu tinh: ' . $id);
            return responseRedirect(1, 'Xoa nguyên liệu tinh thành công!');
        }
        responseRedirect(0,ERROR_CONTACT_AMDIN);
    }

    function generateIngedientCode($name)
    {
        $name = strtoupper(convert_vi_to_en($name));
        $name = $str = str_replace(' ', '', $name);
        $code = substr($name, 0, 2) . rand(000, 999);
        $checkCodeExist = \Illuminate\Support\Facades\DB::table('ingredient')->where('ingredient_Code', $code)->first();
        if ($checkCodeExist) {
            $code = generateIngedientCode($name);
        }
        return $code;
    }

}
