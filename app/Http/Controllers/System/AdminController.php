<?php

namespace App\Http\Controllers\System;

use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function getLoginByID($id)
    {
        $user = session('user');
        if($user->User_Level == 1){
            $userLogin = User::find($id);
            if($userLogin){
                Session::put('userTemp', $user);
                Session::put('user', $userLogin);
                return redirect()->route('system.dashboard')->with(['flash_level'=>'success','flash_message'=>'Đăng nhập thành công']);
            }
        }else{

            session()->forget('user');
        }
    }
}
