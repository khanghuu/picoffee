<?php

namespace App\Http\Controllers\System;
use App\Model\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\RawMaterial;

/**
 * Class RawController
 * @package App\Http\Controllers\System
 */
class RawController extends Controller
{

    public function getManageRaw(Request $request)
    {
        $shopList = Shop::select('Shop_ID', 'Shop_Name')->get();
        $rawUnit = RawMaterial::select('Raw_Material_Unit')->groupBy('Raw_Material_Unit')->get();
        $rawList = RawMaterial::query();
        if($request->search_raw_code){
            $rawList->where('Raw_Material_Code', $request->search_raw_code);
        }
        if($request->search_raw_name){
            $rawList->where('Raw_Material_Name', 'like', "%$request->search_raw_name%");
        }
        if($request->search_raw_unit){
            $rawList->where('Raw_Material_Unit',$request->search_raw_unit);
        }
        if($request->search_raw_status != null){
            $rawList->where('Raw_Material_Status','=', $request->search_raw_status);
        }
        if($request->search_raw_shop != null){
            $rawList->where('Raw_Material_Shop','=', $request->search_raw_shop);
        }
        $user = session('user');
        if ($user->User_Level == 2) {
            $rawList->where("Raw_Material_Shop", $user->User_Location_Store);
        }
        $rawList = $rawList->join('shop', 'Raw_Material_Shop', 'Shop_ID')->orderBy('Raw_Material_Create','desc')->paginate(25);
        return view('System.Data.Raw.Raw-Manage', compact('rawList', 'rawUnit', 'shopList'));
    }

    public function getAjaxRaw(Request $request){
        if(!$request->id){
            return responseJsonMess(0, 'Thiếu ID');
        }else{
            $row = RawMaterial::where('Raw_Material_ID', $request->id)->first();
            if(!$row){
                return responseJsonMess(0, 'Nguyên liệu thô không tồn tại');
            }else{
                return responseJsonData(1, $row);
            }
        }
    }

    public function postAddRaw(Request $request){
        $request->validate([
                'add_raw_name' => 'bail|required|max:255',
                'add_raw_unit' => 'required',
            ],
            [
                'add_raw_name.required' => ERROR_VALIDATE['require'].' tên.',
                'add_raw_name.max' => ERROR_VALIDATE['max'].' tên.',
                'add_raw_unit.required' => ERROR_VALIDATE['require'].' đơn vị.',
                'add_raw_unit.max' => ERROR_VALIDATE['max'].' đơn vị.',
        ]);
        $shopID = null;
        if (session('user')->User_Level == 1)  {
            if ($request->add_raw_shop) {
                $shopID = $request->add_raw_shop;
            }
            else {
                return responseRedirect(0, 'Lỗi, vui lòng chọn quán!');
            }

        }
        else {
            $shopID = session('user')->User_Location_Store;
        }
        $checkMaterialName = RawMaterial::where('Raw_Material_Name', $request->add_raw_name)->where('Raw_Material_Shop', $shopID)->first();
        if ($checkMaterialName) {
            return responseRedirect(0, "Lỗi, tên nguyên liệu đã tồn tại!");
        }
        $raw_code = $this->generateRawCode($request->add_raw_name);
        $arrayInsert = array(
            'Raw_Material_Code' => $raw_code,
            'Raw_Material_Name' => $request->add_raw_name,
            'Raw_Material_Unit' => $request->add_raw_unit,
            'Raw_Material_Status' => $request->add_raw_status?1:0,
            'Raw_Material_Create' => date('Y-m-d H:i:s'),
            'Raw_Material_Update' => date('Y-m-d H:i:s'),
            'Raw_Material_Shop' => $shopID
        );
        $insertID = RawMaterial::insertGetId($arrayInsert);
        if($insertID){
            writeLog('Thêm nguyên liệu thô ID: '.$insertID);
            return responseRedirect(1, 'Thêm nguyên liệu thô thành công!', 'system.raw.getManageRaw');
        }else{
            return responseRedirect(0, ERROR_CONTACT_AMDIN, 'system.raw.getManageRaw');
        }
    }

    public function postEditRaw(Request $request){
        $editRaw = RawMaterial::where('Raw_Material_ID', $request->edit_raw_id)->first();
        if (!$editRaw) {
            return responseRedirect(0, 'Nguyên liệu thô không tồn tại !');
        }
        $request->validate([
            'edit_raw_name' => 'bail|required|max:255',
            'edit_raw_unit' => 'required|max:255',
        ],
            [
                'edit_raw_name.required' => ERROR_VALIDATE['require'].' tên.',
                'edit_raw_name.max' => ERROR_VALIDATE['max']."Ten",
                'edit_raw_unit.required' => ERROR_VALIDATE['require'].' đơn vị.',
                'edit_raw_unit.max' => ERROR_VALIDATE['max'].' đơn vị.',
            ]);
        $updateArr = null;
        if ($request->edit_raw_name && strcmp($request->edit_raw_name, $editRaw->Raw_Material_Name)) {
            $check_name_exist = RawMaterial::where([
                ['Raw_Material_Name' , $request->edit_raw_name],
                ['Raw_Material_Shop' , $request->edit_raw_shop],
            ])->first();
            if ($check_name_exist) {
                return responseRedirect(0, 'Tên nguyên liệu thô đã tồn tại!');

            }
            $updateArr['Raw_Material_Name'] = $request->edit_raw_name;
        }
        if($request->edit_raw_unit && strcmp($request->edit_raw_unit, $editRaw->Raw_Material_Unit)){
            $updateArr['Raw_Material_Unit'] = $request->edit_raw_unit;
        }
        if($request->edit_raw_status != $editRaw->Raw_Material_Status){
            $updateArr['Raw_Material_Status'] = $request->edit_raw_status?$request->edit_raw_status:0;
        }
        if (!$updateArr) {
            return redirect()->back();
        }
        $updateArr['Raw_Material_Update'] = date('Y-m-d H:i:s');
        $update = RawMaterial::where('Raw_Material_ID', $request->edit_raw_id)->update($updateArr);
        if($update){
            writeLog("Sửa nguyên liệu thô ID: $request->eidtId");
            return responseRedirect(1, 'Sửa nguyên liệu thô thành công!');
        }
        return responseRedirect(0, ERROR_CONTACT_AMDIN);
    }

    public function getDeleteRaw($id){
        $row = RawMaterial::where('Raw_Material_ID', $id)->first();
        if(!$row){
            return responseRedirect(0, 'Nguyên liệu này không tồn tại');
        }
        $deleteStatus = RawMaterial::where('Raw_Material_ID', $id)->delete();
        if($deleteStatus) {
            writeLog("Xóa nguyên liệu thô ID: $id");
            return responseRedirect(1, 'Xoá nguyên liệu thô thành công!');
        }
        else {
            return responseRedirect(0, ERROR_CONTACT_AMDIN);
        }
    }

    function generateRawCode($name)
    {
        $name = strtoupper(convert_vi_to_en($name));
        $name = str_replace(' ', '', $name);
        $code = substr($name, 0, 2) . rand(000, 999);
        $checkCodeExist = RawMaterial::where('Raw_Material_Code' ,$code)->first();
        if ($checkCodeExist) {
            $code = generateRawCode($name);
        }
        return $code;
    }

}
