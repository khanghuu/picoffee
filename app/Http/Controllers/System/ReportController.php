<?php

namespace App\Http\Controllers\System;

use App\Model\Ingredient;
use App\Model\Product;
use App\Model\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Exports\CustomerRevenueReport;
use App\Exports\StaffRevenueReport;
use App\Exports\ProductRevenueReport;
use App\Exports\TimeShiftRevenueReport;
use App\Exports\DateRevenueReport;
use App\Model\Bill;
use App\Model\BillProduct;
use DB;
use Excel;
use App\Exports\CollectionExport;
use function MongoDB\BSON\toJSON;

class ReportController extends Controller
{

    public function getSales(Request $request)
    {
        $Statistic = Bill::where('Bill_Status', 1)
            ->selectRaw("
                            SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent` / 100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
                            SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0))             AS TotalSigned,
                            SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice` * 0.1 / 1.1), 0)) AS TotalVAT,
                            SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                            COUNT(*)                                                         AS TotalBill,
                            SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                            SUM(`Bill_ExtantPrice`)                                          AS TotaExtantlSales
                        ")
            ->first();
        if (Input::get('export')) {

            //xuất excel
            $listHistoryExcel[] = array('Tổng doanh thu', 'Tổng giảm giá', 'Tổng ký bill', 'Tổng thuế', 'Tổng doanh thu thuần');

            $listHistoryExcel[] = array($Statistic->TotalSales, $Statistic->TotalDiscount, $Statistic->TotalSigned, $Statistic->TotalVAT, ($Statistic->TotalSales - $Statistic->TotalDiscount - $Statistic->TotalSigned - $Statistic->TotalVAT));

            $export = new CollectionExport([
                $listHistoryExcel
            ]);

            return Excel::download($export, 'report_sales_' . date('Ymd') . ".xls");
        }


        $discount = Bill::where('Bill_Status', 1)
            ->where('Bill_DiscountPercent', '<>', 0)
            ->orWhere('Bill_DiscountAmount', '<>', 0)
            ->orderByDesc('Bill_ID')
            ->get();


        $signed = Bill::where('Bill_Status', 1)
            ->where('Bill_IsSigned', 1)
            ->orderByDesc('Bill_ID')
            ->get();


        $taxvat = Bill::where('Bill_Status', 1)
            ->where('Bill_IsTaxVAT', 1)
            ->orderByDesc('Bill_ID')
            ->get();

        $uri = $request->path();

        return view('System.Report.Sales', compact('Statistic', 'discount', 'signed', 'taxvat', 'uri'));
    }

    public function getRevenue()
    {
        $user = session('user');
        $shopID = '';
        $user->User_Level == 2?$shopID = $user->User_Location_Store:$shopID = Shop::first()->Shop_ID;
        $salesHour = Bill::where('Bill_Status', 1)
            ->selectRaw('
							    SUM(IF(HOUR(`Bill_Time`) = "06", Bill_ExtantPrice, 0)) AS "hour1",
							    SUM(IF(HOUR(`Bill_Time`) = "07", Bill_ExtantPrice, 0)) AS "hour2",
							    SUM(IF(HOUR(`Bill_Time`) = "08", Bill_ExtantPrice, 0)) AS "hour3",
							    SUM(IF(HOUR(`Bill_Time`) = "09", Bill_ExtantPrice, 0)) AS "hour4",
							    SUM(IF(HOUR(`Bill_Time`) = "10", Bill_ExtantPrice, 0)) AS "hour5",
							    SUM(IF(HOUR(`Bill_Time`) = "11", Bill_ExtantPrice, 0)) AS "hour6",
							    SUM(IF(HOUR(`Bill_Time`) = "12", Bill_ExtantPrice, 0)) AS "hour7",
							    SUM(IF(HOUR(`Bill_Time`) = "13", Bill_ExtantPrice, 0)) AS "hour8",
							    SUM(IF(HOUR(`Bill_Time`) = "14", Bill_ExtantPrice, 0)) AS "hour9",
							    SUM(IF(HOUR(`Bill_Time`) = "15", Bill_ExtantPrice, 0)) AS "hour10",
							    SUM(IF(HOUR(`Bill_Time`) = "16", Bill_ExtantPrice, 0)) AS "hour11",
							    SUM(IF(HOUR(`Bill_Time`) = "17", Bill_ExtantPrice, 0)) AS "hour12",
							    SUM(IF(HOUR(`Bill_Time`) = "18", Bill_ExtantPrice, 0)) AS "hour13",
							    SUM(IF(HOUR(`Bill_Time`) = "19", Bill_ExtantPrice, 0)) AS "hour14",
							    SUM(IF(HOUR(`Bill_Time`) = "20", Bill_ExtantPrice, 0)) AS "hour15",
							    SUM(IF(HOUR(`Bill_Time`) = "21", Bill_ExtantPrice, 0)) AS "hour16",
							    SUM(IF(HOUR(`Bill_Time`) = "22", Bill_ExtantPrice, 0)) AS "hour17"
							    ')
            ->where('Bill_Shop_ID', $shopID)
            ->first();

        $countHour = Bill::where('Bill_Status', 1)
            ->selectRaw('
							    COUNT(IF(HOUR(`Bill_Time`) = "06", Bill_ExtantPrice, NULL)) AS "1",
							    COUNT(IF(HOUR(`Bill_Time`) = "07", Bill_ExtantPrice, NULL)) AS "2",
							    COUNT(IF(HOUR(`Bill_Time`) = "08", Bill_ExtantPrice, NULL)) AS "3",
							    COUNT(IF(HOUR(`Bill_Time`) = "09", Bill_ExtantPrice, NULL)) AS "4",
							    COUNT(IF(HOUR(`Bill_Time`) = "10", Bill_ExtantPrice, NULL)) AS "5",
							    COUNT(IF(HOUR(`Bill_Time`) = "11", Bill_ExtantPrice, NULL)) AS "6",
							    COUNT(IF(HOUR(`Bill_Time`) = "12", Bill_ExtantPrice, NULL)) AS "7",
							    COUNT(IF(HOUR(`Bill_Time`) = "13", Bill_ExtantPrice, NULL)) AS "8",
							    COUNT(IF(HOUR(`Bill_Time`) = "14", Bill_ExtantPrice, NULL)) AS "9",
							    COUNT(IF(HOUR(`Bill_Time`) = "15", Bill_ExtantPrice, NULL)) AS "10",
							    COUNT(IF(HOUR(`Bill_Time`) = "16", Bill_ExtantPrice, NULL)) AS "11",
							    COUNT(IF(HOUR(`Bill_Time`) = "17", Bill_ExtantPrice, NULL)) AS "12",
							    COUNT(IF(HOUR(`Bill_Time`) = "18", Bill_ExtantPrice, NULL)) AS "13",
							    COUNT(IF(HOUR(`Bill_Time`) = "19", Bill_ExtantPrice, NULL)) AS "14",
							    COUNT(IF(HOUR(`Bill_Time`) = "20", Bill_ExtantPrice, NULL)) AS "15",
							    COUNT(IF(HOUR(`Bill_Time`) = "21", Bill_ExtantPrice, NULL)) AS "16",
							    COUNT(IF(HOUR(`Bill_Time`) = "22", Bill_ExtantPrice, NULL)) AS "17"
							    ')
            ->where('Bill_Shop_ID', $shopID)
            ->first();
        $totalSales = 0;
        for ($i = 1; $i < 18; $i++) {
            $label = ($i + 5) . ':00-' . ($i + 5) . ':59';
            $totalSales += $salesHour['hour' . $i];
            $dataHour[] = ['hour' => $label, 'count' => $countHour[$i], 'sales' => $salesHour['hour' . $i]];
        }

        return view('System.Report.Revenue', compact('totalSales', 'dataHour'));
    }

    public function getTopProduct()
    {
        $user = session('user');
        $shopID = '';
        $user->User_Level == 2?$shopID = $user->User_Location_Store:$shopID = Shop::first()->Shop_ID;
        $Statistic = BillProduct::join('bill', 'Bill_ID', 'Bill_Product_BillID')->where('Bill_Shop_ID', $shopID)
                                    ->where('Bill_Status', 1)
                                    ->where('Bill_Product_Status', 1)
                                    ->selectRaw("SUM(Bill_Product_Quantity) AS quantity, Bill_Product_ProductID")
                                    ->groupBy('Bill_Product_ProductID')
                                    ->orderByDesc('quantity')
                                    ->limit(10)
                                    ->get();
        $totalQuantity = $Statistic->sum('quantity');
        $arrColor = array('#7cb5ec', '#91e8e1', '#f45b5b', '#2b908f', '#e4d353', '#ed7a53', '#8085e9', '#90ed7c', '#242473', '#f7a25c');
        $arrDataChart[] = array();

        foreach ($Statistic as $key => $v) {
            array_push($arrDataChart, ['label' => $v->Product->Product_Name, 'data' => number_format($v->quantity / $totalQuantity * 100), 'color' => $arrColor[$key]]);
        }
        $dataChart = json_encode($arrDataChart);

        return view('System.Report.TopProduct', compact('Statistic', 'totalQuantity', 'dataChart', 'arrDataChart'));
    }


//    public function getRevenueRaw(Request $request)
//    {
//        $from_date = $request->from_date;
//        $to_date = $request->to_date;
//        if ($from_date and $to_date) {
//            $productIDs = Bill::where('Bill_Status', 1)
//                ->whereRaw("DATE_FORMAT(Bill_Time, '%Y-%m-%d') BETWEEN '$from_date' AND '$to_date'")
//                ->select('Bill_ID')->get()->toArray();
//        }
//
//        elseif (!$from_date and $to_date) {
//            $productIDs = Bill::where('Bill_Status', 1)
//                ->whereRaw("DATE_FORMAT(Bill_Time, '%Y-%m-%d') = '$to_date'")
//                ->select('Bill_ID')->get()->toArray();
//        }
//
//        elseif ($from_date and !$to_date) {
//            $productIDs = Bill::where('Bill_Status', 1)
//                ->whereRaw("DATE_FORMAT(Bill_Time, '%Y-%m-%d') = '$from_date'")
//                ->select('Bill_ID')->get()->toArray();
//        }
//        else {
//            $productIDs = Bill::where('Bill_Status', 1)
//                ->select('Bill_ID')->get()->toArray();
//        }
//
//        $rawOrderList = [];
//
//        foreach ($productIDs as $item) {
//
//            $rawOrder = $this->getRawMaterial($item->Bill_ID);
//
//            array_push($rawOrderList, $rawOrder);
//        }
//        return view('System.Report.Raws', compact('rawOrderList'));
//    }

//    public function getRevenueIngredient(Request $request)
//    {
//
//        $billList = Bill::where('Bill_Status', 1)->select('Bill_ID')->get();
//
//        $ingredientOrderList = [];
//        foreach ($billList as $item) {
//            $ingredientOrder = $this->getIngredient($item->Bill_ID);
//            array_push($ingredientOrderList, $ingredientOrder);
//
//        }
//        return view('System.Report.Ingredient', compact('ingredientOrderList'));
//    }

    public function getSignedBill(Request $request)
    {
        $user = session('user');
        $signed = Bill::query()->join('shop', 'Bill_Shop_ID', 'Shop_ID');
        $user->User_Level == 2? $signed->where('Bill_Shop_ID', $user->User_Location_Store):0;
        $shopList = [];
        if ($user->User_Level == 1 || $user->User_Level == 2) {
            $shopList = Shop::select('Shop_ID','Shop_Name')->get();
            $request->shop_id?$signed->where('Bill_Shop_ID', $request->shop_id):0;
        }
        $request->customer_name?$signed->where('Bill_CustomerName', $request->customer_name):0;
        $request->from_date && $request->to_date?$signed->whereRaw("DATE(Bill_Time) >= '$request->from_date' and DATE(Bill_Time) <= '$request->from_to'"):0;
        $request->from_date && !$request->to_date?$signed->whereRaw("DATE(Bill_Time) >= '$request->from_date'"):0;
        !$request->from_date && $request->to_date?$signed->whereRaw("DATE(Bill_Time) <= '$request->to_date'"):0;
        $signed = $signed->select('Shop_Name', 'Bill_Time', 'Bill_CustomerName', 'Bill_TotalPrice')
            ->where('Bill_Status', 1)->where('Bill_IsSigned', 1)->orderByDesc('Bill_ID')->paginate(25);
        return view('System.Report.SignedBill', compact('signed', 'shopList'));
    }

    public function getAllBill()
    {
        $user = session('user');
        if ($user->User_Level == 1 || $user->User_Level == 3) {
            $allOrder = Bill::orderByDesc('Bill_ID')
                ->join('shop', 'Bill_Shop_ID', 'Shop_ID')
                ->join('users','Bill_User','User_ID')
                ->where("Bill_Status", 1)->orwhere('Bill_Status', 0)->paginate(25);
        }
        if ($user->User_Level == 2) {
            $allOrder = Bill::orderByDesc('Bill_ID')
                ->join('shop', 'Bill_Shop_ID', 'Shop_ID')
                ->join('users','Bill_User','User_ID')
                ->where('Bill_Shop_ID', $user->User_Location_Store)
                ->where("Bill_Status", 1)
                ->orwhere('Bill_Status', 0)->paginate(25);
        }
        return view('System.Order.AllOrder', compact('allOrder'));
    }

    public function getDetailBill(Request $request) {
        $billData = Bill::query()->join('shop', 'Bill_Shop_ID', 'Shop_ID')->where('Bill_ID', $request->bill_id);
        $user = session('user');
        if ($user->User_Level == 2) {
            $billData = $billData->where('Bill_Shop_ID', $user->User_Location_Store);
        }
        if (!$billData) {
            return responseJsonMess(0, 'Hóa đơn không tồn tại!');
        }
        $data['bill'] = $billData->get()->toArray();

        $data['product'] = BillProduct::join('product', 'Bill_Product_ProductID', 'Product_ID')
            ->where('Bill_Product_BillID', $request->bill_id)
            ->select('Product_Name', 'Bill_Product_Quantity', 'Product_ID', 'Product_Price')->get();
        return responseJsonData(1, $data);

    }
    public function getAjaxBill(Request $req)
    {
        $info = '';
        if ($req->info_bill == 'discount') {
            $info = Bill::where('Bill_Status', 1)
                ->where('Bill_DiscountPercent', '<>', 0)
                ->orWhere('Bill_DiscountAmount', '<>', 0)
                ->orderByDesc('Bill_ID')
                ->get();

        } elseif ($req->info_bill == 'signed') {
            $info = Bill::where('Bill_Status', 1)
                ->where('Bill_IsSigned', 1)
                ->orderByDesc('Bill_ID')
                ->get();

        } else {
            $info = Bill::where('Bill_Status', 1)
                ->where('Bill_IsTaxVAT', 1)
                ->orderByDesc('Bill_ID')
                ->get();
        }

        return response()->json(['status' => true, 'data' => $info], 200);
    }

    public function getPenddingBill()
    {

        $allOrder = Bill::where('Bill_Shop_ID', session('user')->User_Location_Store)
            ->orderByDesc('Bill_ID')
            ->where("Bill_Status", 2)
            ->get();
        return view('System.Order.pendding-order', compact('allOrder'));
    }

    public function changePenddingOrder($id)
    {
        $changePenddingStatus = Bill::where('Bill_ID', $id)->update(['Bill_Status' => 1]);
        if ($changePenddingStatus) {
            writeLog('Thanh toán hóa đơn ký gửi ID: ' . $id);
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Đơn hàng đã thanh toán thành công']);
        }
        return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Có lỗi xảy ra, vui lòng liên hệ admin!']);
    }

    public function deletePenddingOrder($id)
    {
        $deletePenddingStatus = Bill::where('Bill_ID', $id)->delete();
        if ($deletePenddingStatus) {
            writeLog('Xóa đơn hàng ký gửi ID: ' . $id);
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Xóa đơn hàng kí gửi thành công']);
        }
        return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Có lỗi xảy ra, vui lòng liên hệ admin!']);
    }

    public function getRevenueCustomer(Request $request)
    {
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $customerRevenue = '';
        if ($from_date and $to_date) {
            $customerRevenue = Bill::selectRaw('Bill_CustomerName as name,
                        Bill_CustomerEmail as email,
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			')
                ->whereRaw("DATE_FORMAT(Bill_Time, '%Y-%m-%d') BETWEEN '$from_date' AND '$to_date'")
                ->groupBy('Bill_CustomerName', 'Bill_CustomerEmail')
                ->get()->toArray();
        } elseif (!$from_date and $to_date) {
            $customerRevenue = Bill::selectRaw('Bill_CustomerName as name,
                        Bill_CustomerEmail as email,
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			')
                ->whereRaw("DATE_FORMAT(Bill_Time,'%Y-%m-%d') BETWEEN '$to_date' AND '$from_date' ")
                ->groupBy('Bill_CustomerName', 'Bill_CustomerEmail')
                ->get()->toArray();
        } elseif (!$to_date and $from_date) {
            $customerRevenue = Bill::selectRaw('Bill_CustomerName as name,
                        Bill_CustomerEmail as email,
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			')
                ->whereRaw("DATE_FORMAT(Bill_Time,'%Y-%m-%d') = '$from_date'")
                ->groupBy('Bill_CustomerName', 'Bill_CustomerEmail')
                ->get()->toArray();
        } else {
            $customerRevenue = Bill::selectRaw('Bill_CustomerName as name,
                        Bill_CustomerEmail as email,
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			')
                ->groupBy('Bill_CustomerName', 'Bill_CustomerEmail')
                ->get()->toArray();
        }
        return view('System.Report.Revenue-Customer', compact('customerRevenue'));
    }

    public function getRevenueStaff(Request $request)
    {
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        if ($from_date and $to_date) {
            $staffRevenue = Bill::where('Bill_Status', 1)
                ->whereRaw("DATE_FORMAT(Bill_Time, '%Y-%m-%d') BETWEEN '$from_date' AND '$to_date'")
                ->join('users', 'ill.Bill_User', '=', 'users.User_ID')
                ->selectRaw('users.User_Name as name,
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			')
                ->groupBy('users.User_Name')
                ->get()->toArray();
        } elseif (!$from_date and $to_date) {
            $staffRevenue = Bill::where('Bill_Status', 1)
                ->whereRaw("DATE_FORMAT(Bill_Time,'%Y-%m-%d') = '$to_date'")
                ->join('users', 'bill.Bill_User', '=', 'users.User_ID')
                ->selectRaw('users.User_Name as name,
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			')
                ->groupBy('users.User_Name')
                ->get()->toArray();
        } elseif ($from_date and !$to_date) {
            $staffRevenue = Bill::where('Bill_Status', 1)
                ->whereRaw("DATE_FORMAT(Bill_Time,'%Y-%m-%d') = '$from_date'")
                ->join('users', 'bill.Bill_User', '=', 'users.User_ID')
                ->selectRaw('users.User_Name as name,
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			')
                ->groupBy('users.User_Name')
                ->get()->toArray();
        } else {
            $staffRevenue = Bill::where('Bill_Status', 1)
                ->join('users', 'bill.Bill_User', '=', 'users.User_ID')
                ->selectRaw('users.User_Name as name,
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			')
                ->groupBy('users.User_Name')
                ->get()->toArray();
        }
        return view('System.Report.Revenue-Staff', compact('staffRevenue'));
    }

    public function getRevenueProduct(Request $request)
    {
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $productRevenue = '';
        if ($from_date and $to_date) {
            $productRevenue = Bill::where('Bill_Status', 1)
                ->join('bill_product', 'bill.Bill_ID', '=', 'bill_product.bill_product_BillID')
                ->join('product', 'bill_product.bill_product_ProductID', '=', 'product.product_ID')
                ->selectRaw('product.product_Name as name,
                        SUM(`bill_product_Quantity`) AS Quantity  ,
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			')
                ->whereRaw("DATE_FORMAT(Bill_Time, '%Y-%m-%d') BETWEEN '$from_date' AND '$to_date'")
                ->groupBy('product.product_Name')
                ->orderByDesc('Quantity')
                ->get()->toArray();
        } elseif (!$from_date and $to_date) {
            $productRevenue = Bill::where('Bill_Status', 1)
                ->join('bill_product', 'bill.Bill_ID', '=', 'bill_product.bill_product_BillID')
                ->join('product', 'bill_product.bill_product_ProductID', '=', 'product.product_ID')
                ->selectRaw('product.product_Name as name,
                        SUM(`bill_product_Quantity`) AS Quantity  ,
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			')
                ->whereRaw("DATE_FORMAT(Bill_Time, '%Y-%m-%d') = '$to_date")
                ->groupBy('product.product_Name')
                ->orderByDesc('Quantity')
                ->get()->toArray();
        } elseif ($from_date and !$to_date) {
            $productRevenue = Bill::where('Bill_Status', 1)
                ->whereRaw("DATE_FORMAT(Bill_Time, '%Y-%m-%d') = '$from_date'")
                ->join('bill_product', 'bill.Bill_ID', '=', 'bill_product.bill_product_BillID')
                ->join('product', 'bill_product.bill_product_ProductID', '=', 'product.product_ID')
                ->selectRaw('product.product_Name as name,
                        SUM(`bill_product_Quantity`) AS Quantity  ,
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			')
                ->groupBy('product.product_Name')
                ->orderByDesc('Quantity')
                ->get()->toArray();
        } else {
            $productRevenue = Bill::where('Bill_Status', 1)
                ->join('bill_product', 'bill.Bill_ID', '=', 'bill_product.bill_product_BillID')
                ->join('product', 'bill_product.bill_product_ProductID', '=', 'product.product_ID')
                ->selectRaw('product.product_Name as name,
                        SUM(`bill_product_Quantity`) AS Quantity  ,
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			')
                ->groupBy('product.product_Name')
                ->orderByDesc('Quantity')
                ->get()->toArray();
        }
        return view('System.Report.Revenue-Product', compact('productRevenue'));
    }

    public function getRevenueDate(Request $request)
    {

        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $dateRevenue = '';
        $user = session('user');
        $shopID = '';
        $user->User_Level == 2?$shopID = $user->User_Location_Store:$shopID = Shop::first()->Shop_ID;
        if ($from_date and $to_date) {
            $dateRevenue = Bill::where('Bill_Status', 1)
                ->selectRaw("DATE_FORMAT(Bill_Time ,'%d/%m/%Y') as 'DateTime',
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			")->where('Bill_Shop_ID', $shopID)
                ->whereRaw("DATE_FORMAT(Bill_Time, '%Y-%m-%d') BETWEEN '$from_date' AND '$to_date'")
                ->groupBy('DateTime')
                ->get()->toArray();
        } elseif (!$from_date and $to_date) {
            $dateRevenue = Bill::where('Bill_Status', 1)
                ->selectRaw("DATE_FORMAT(Bill_Time ,'%d/%m/%Y') as 'DateTime',
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			")->where('Bill_Shop_ID', $shopID)
                ->whereRaw("DATE_FORMAT(Bill_Time, '%Y-%m-%d') = '$to_date'")
                ->groupBy('DateTime')
                ->get()->toArray();
        } elseif ($from_date and !$to_date) {
            $dateRevenue = Bill::where('Bill_Status', 1)
                ->selectRaw("DATE_FORMAT(Bill_Time ,'%d/%m/%Y') as 'DateTime',
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			")->where('Bill_Shop_ID', $shopID)
                ->whereRaw("DATE_FORMAT(Bill_Time, '%Y-%m-%d') = '$from_date'")
                ->groupBy('DateTime')
                ->get()->toArray();
        } else {
            $dateRevenue = Bill::where('Bill_Status', 1)
                ->selectRaw("DATE_FORMAT(Bill_Time ,'%d/%m/%Y') as 'DateTime',
	    				SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
	    				SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
	    				SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
	    				SUM(`Bill_TotalPrice`) AS TotalSales,
	    				SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
			")->where('Bill_Shop_ID', $shopID)
                ->groupBy('DateTime')
                ->get()->toArray();
        }
        // dd($dateRevenue);
        return view('System.Report.Revenue-Date', compact('dateRevenue'));
    }

    public function getRevenueShift(Request $request)
    {
        $user = session('user');
        $shopID = '';
        $user->User_Level == 2?$shopID = $user->User_Location_Store:$shopID = Shop::first()->Shop_ID;
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $shiftRevenue = '';
        if ($from_date and $to_date) {
            $shiftRevenue = DB::select(
                "
                    select DATE_FORMAT(Bill_Time, '%d/%m/%Y') as 'DateTime',
                                    '1'  AS 'CA',
                                    SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent` / 100 * `Bill_TotalPrice`),
                                           `Bill_DiscountAmount`))                                   AS TotalDiscount,
                                    SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0))             AS TotalSigned,
                                    SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice` * 0.1 / 1.1), 0)) AS TotalVAT,
                                    SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                                    SUM(`Bill_ExtantPrice`)                                          AS TotaExtantlSales
                             from `bill`
                             where  Bill_Shop_ID = $shopID AND DATE_FORMAT(Bill_Time, '%Y-%m-%d') BETWEEN '$from_date' AND '$to_date'  AND Bill_Time =  TIME_FORMAT(Bill_Time, '%H:%i:%s') between '06:00:00' and '13:59:59'
                             group by 'DateTime'

                    UNION
                        select DATE_FORMAT(Bill_Time, '%d/%m/%Y') as 'DateTime',
                                '2'  AS 'CA',
                               SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent` / 100 * `Bill_TotalPrice`),
                                      `Bill_DiscountAmount`))                                   AS TotalDiscount,
                               SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0))             AS TotalSigned,
                               SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice` * 0.1 / 1.1), 0)) AS TotalVAT,
                               SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                               SUM(`Bill_ExtantPrice`)                                          AS TotaExtantlSales
                        from `bill`
                        where Bill_Shop_ID = $shopID AND DATE_FORMAT(Bill_Time, '%Y-%m-%d') BETWEEN '$from_date' AND '$to_date'  AND  TIME_FORMAT(Bill_Time, '%H:%i:%s') >= '14:00:00'
                        group by 'DateTime'
        ");
        } elseif (!$from_date and $to_date) {
            $shiftRevenue = DB::select(
                "
        select  DATE_FORMAT(Bill_Time, '%d/%m/%Y') as 'DateTime',
                        '1'  AS 'CA',
                        SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent` / 100 * `Bill_TotalPrice`),
                               `Bill_DiscountAmount`))                                   AS TotalDiscount,
                        SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0))             AS TotalSigned,
                        SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice` * 0.1 / 1.1), 0)) AS TotalVAT,
                        SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                        SUM(`Bill_ExtantPrice`)                                          AS TotaExtantlSales
                 from `bill`
                 where Bill_Shop_ID = $shopID AND DATE_FORMAT(Bill_Time, '%Y-%m-%d') = '$to_date' AND TIME_FORMAT(Bill_Time, '%H:%i:%s') between '06:00:00' and '13:59:59'
                 group by 'DateTime'

        UNION
            select DATE_FORMAT(Bill_Time, '%d/%m/%Y') as 'DateTime',
                    '2'  AS 'CA',
                   SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent` / 100 * `Bill_TotalPrice`),
                          `Bill_DiscountAmount`))                                   AS TotalDiscount,
                   SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0))             AS TotalSigned,
                   SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice` * 0.1 / 1.1), 0)) AS TotalVAT,
                   SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                   SUM(`Bill_ExtantPrice`)                                          AS TotaExtantlSales
            from `bill`
            where Bill_Shop_ID = $shopID AND DATE_FORMAT(Bill_Time, '%Y-%m-%d') = '$to_date'  AND TIME_FORMAT(Bill_Time, '%H:%i:%s') >= '14:00:00'
            group by 'DateTime'
        "
            );
        }
        elseif ($from_date and !$to_date) {

            $shiftRevenue = DB::select(
                "
                    select DATE_FORMAT(Bill_Time, '%d/%m/%Y') as 'DateTime',
                                    '1'  AS 'CA',
                                    SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent` / 100 * `Bill_TotalPrice`),
                                           `Bill_DiscountAmount`))                                   AS TotalDiscount,
                                    SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0))             AS TotalSigned,
                                    SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice` * 0.1 / 1.1), 0)) AS TotalVAT,
                                    SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                                    SUM(`Bill_ExtantPrice`)                                          AS TotaExtantlSales
                             from `bill`
                             where DATE_FORMAT(Bill_Time, '%Y-%m-%d') = '$from_date' AND TIME_FORMAT(Bill_Time, '%H:%i:%s') between '06:00:00' and '13:59:59'
                             group by 'DateTime'

                    UNION
                        select DATE_FORMAT(Bill_Time, '%d/%m/%Y') as 'DateTime',
                                '2'  AS 'CA',
                               SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent` / 100 * `Bill_TotalPrice`),
                                      `Bill_DiscountAmount`))                                   AS TotalDiscount,
                               SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0))             AS TotalSigned,
                               SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice` * 0.1 / 1.1), 0)) AS TotalVAT,
                               SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                               SUM(`Bill_ExtantPrice`)                                          AS TotaExtantlSales
                        from `bill`
                        where Bill_Shop_ID = $shopID AND DATE_FORMAT(Bill_Time, '%Y-%m-%d') = '$from_date'  AND TIME_FORMAT(Bill_Time, '%H:%i:%s') >= '14:00:00'
                        group by 'DateTime'
                    "
            );
        }
        else {
            $shiftRevenue = DB::select(
                "
                select DATE_FORMAT(Bill_Time, '%d/%m/%Y') as 'DateTime',
                                '1'  AS 'CA',
                                SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent` / 100 * `Bill_TotalPrice`),
                                       `Bill_DiscountAmount`))                                   AS TotalDiscount,
                                SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0))             AS TotalSigned,
                                SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice` * 0.1 / 1.1), 0)) AS TotalVAT,
                                SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                                SUM(`Bill_ExtantPrice`)                                          AS TotaExtantlSales
                         from `bill`
                         where   TIME_FORMAT(Bill_Time, '%H:%i:%s') between '06:00:00' and '13:59:59'
                         group by 'DateTime'

                UNION
                    select DATE_FORMAT(Bill_Time, '%d/%m/%Y') as 'DateTime',
                            '2'  AS 'CA',
                           SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent` / 100 * `Bill_TotalPrice`),
                                  `Bill_DiscountAmount`))                                   AS TotalDiscount,
                           SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0))             AS TotalSigned,
                           SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice` * 0.1 / 1.1), 0)) AS TotalVAT,
                           SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                           SUM(`Bill_ExtantPrice`)                                          AS TotaExtantlSales
                    from `bill`
                    where Bill_Shop_ID = $shopID AND TIME_FORMAT(Bill_Time, '%H:%i:%s') >= '14:00:00'
                    group by 'DateTime'
                "
            );
        }

        sort($shiftRevenue);
//        return response()->json($shiftRevenue, 200);
        return view('System.Report.Revenue-shift', compact('shiftRevenue'));
    }

    public function getIngredient($billID)
    {
        $billData = Bill::where('Bill_ID', $billID)
            ->join('users', 'Bill_User', 'users.User_ID')
            ->join('bill_product', 'Bill_ID', '=', 'bill_product.Bill_Product_BillID')
            ->select('users.User_Name as name', 'Bill_ID', 'bill_product.Bill_Product_ProductID', 'bill_product.Bill_Product_Quantity', 'Bill_Time')
            ->get();
        foreach ($billData as $item) {
            $productID = $item->Bill_Product_ProductID;
            $productParams = Product::where('Product_ID', $productID)->value('Product_Params');
            $ingredientList = json_decode($productParams);
            $ingredients = [];
            foreach ($ingredientList as $ingredientID => $ingredientQuantity) {
                $ing = Ingredient::where('Ingredient_ID', $ingredientID)->select('Ingredient_Name', 'Ingredient_Unit')->first();
                $ingredient = ['name' => $ing->Ingredient_Name, 'unit' => $ing->Ingredient_Unit, 'quantity' => ($ingredientQuantity * $item->Bill_Product_Quantity)];
                array_push($ingredients, $ingredient);
            }
            $item->ingredient = $ingredients;
        }
        return $billData;
    }

    public function getRawMaterial($billID)
    {
        $Bill_Data = Bill::where('Bill_ID', $billID)
            ->join('users', 'bill.Bill_User', '=', 'users.User_ID')
            ->join('bill_product', 'bill.Bill_ID', '=', 'bill_product.bill_product_BillID')
            ->selectRaw('users.User_Name as name,
                        Bill_ID,
                        bill_product.bill_product_ProductID,
                        bill_product.bill_product_Quantity,
	    				Bill_Time
			')
            ->get()->toArray();
        foreach ($Bill_Data as $item) {
            $productID = $item->bill_product_ProductID;
            $productParam = DB::table('product')->where('product_ID', $productID)->value('product_Param');
            $ingredientList = json_decode($productParam);
            $raws = [];
            foreach ($ingredientList as $ingredientID => $ingredientQuantity) {
                $ingredientParams = DB::table('ingredient')->where('ingredient_ID', $ingredientID)->value('ingredient_Params');
                $rawList = json_decode($ingredientParams);
                if ($rawList) {
                    foreach ($rawList as $rawID => $rawQuantity) {
                        $raw = DB::table('Raw_Material')->where('Raw_Material_ID', $rawID)->select('Raw_Material_Name', 'Raw_Material_Unit')->first();
                        $raw = ['name' => $raw->Raw_Material_Name, 'unit' => $raw->Raw_Material_Unit, 'quantity' => ($ingredientQuantity * $item->bill_product_Quantity * $rawQuantity)];
                        array_push($raws, $raw);
                    }
                }
            }
            $item->raw = $raws;
        }
        return $Bill_Data;
    }
}
