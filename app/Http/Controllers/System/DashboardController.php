<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Money;
use App\Model\Bill;
use App\Model\BillProduct;
use App\Model\Shop;
use Session;
class DashboardController extends Controller{

    public function getDashboard(Request $req){
	    $user = Session('user');
		$shopList = Shop::where('Shop_Status', '<>', -1)->get();
	    $store = "User_Location_Store = 1";
	    if($user->User_Location_Store){
		    $store = "User_Location_Store = $user->User_Location_Store";
	    }elseif($req->Store){
		    $store = "User_Location_Store = $req->Store";
	    }
	    //Thống kê doanh thu
	    $getSales = Bill::join('users', 'Bill_User', 'User_ID')
						->whereRaw('1 AND '.$store)
	    				->where('Bill_Status', 1)
						->selectRaw('
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'", Bill_ExtantPrice, 0)) AS salesToday,
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'", Bill_ExtantPrice, 0)) AS salesYesterday,
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'", 1, NULL)) AS countToday,
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'", 1, NULL)) AS countYesterday
							    ')
						->first();
// 		dd($getSales);
		// Top sản phẩm
	    $topProduct = BillProduct::join('bill', 'Bill_ID', 'Bill_Product_BillID')
	    						->join('users', 'Bill_User', 'User_ID')
			    				->where('Bill_Status', 1)
			    				->where('Bill_Product_Status', 1)
			    				->selectRaw('
			    								SUM(`Bill_Product_Quantity`) AS quantity,
			    								Bill_Product_ProductID
											')
								->whereRaw('1 AND '.$store)
								->groupBy('Bill_Product_ProductID')
								->orderByDesc('quantity')
								->limit(10)
								->get();
		$totalQuantity = $topProduct->sum('quantity');
		$arrColor = array('#7cb5ec','#91e8e1', '#f45b5b', '#2b908f', '#e4d353', '#ed7a53', '#8085e9', '#90ed7c', '#242473', '#f7a25c');
		$arrDataChart = array();
		foreach($topProduct as $key=>$v){
			$arrDataChart[] = ['label'=>$v->Product->product_Name, 'data'=>number_format($v->quantity/$totalQuantity * 100), 'color'=>$arrColor[$key]];
		}
		$dataChart = json_encode($arrDataChart);
		//doanh thu theo giờ
		$salesHour = Bill::join('users', 'Bill_User', 'User_ID')
	    				->where('Bill_Status', 1)
						->whereRaw('1 AND '.$store)
						->selectRaw('
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "06", Bill_ExtantPrice, 0)) AS "h6",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "07", Bill_ExtantPrice, 0)) AS "h7",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "08", Bill_ExtantPrice, 0)) AS "h8",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "09", Bill_ExtantPrice, 0)) AS "h9",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "10", Bill_ExtantPrice, 0)) AS "h10",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "11", Bill_ExtantPrice, 0)) AS "h11",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "12", Bill_ExtantPrice, 0)) AS "h12",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "13", Bill_ExtantPrice, 0)) AS "h13",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "14", Bill_ExtantPrice, 0)) AS "h14",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "15", Bill_ExtantPrice, 0)) AS "h15",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "16", Bill_ExtantPrice, 0)) AS "h16",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "17", Bill_ExtantPrice, 0)) AS "h17",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "18", Bill_ExtantPrice, 0)) AS "h18",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "19", Bill_ExtantPrice, 0)) AS "h19",

							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "06", Bill_ExtantPrice, 0)) AS "h20",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "07", Bill_ExtantPrice, 0)) AS "h21",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "08", Bill_ExtantPrice, 0)) AS "h22",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "09", Bill_ExtantPrice, 0)) AS "h23",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "10", Bill_ExtantPrice, 0)) AS "h24",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "11", Bill_ExtantPrice, 0)) AS "h25",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "12", Bill_ExtantPrice, 0)) AS "h26",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "13", Bill_ExtantPrice, 0)) AS "h27",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "14", Bill_ExtantPrice, 0)) AS "h28",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "15", Bill_ExtantPrice, 0)) AS "h29",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "16", Bill_ExtantPrice, 0)) AS "h30",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "17", Bill_ExtantPrice, 0)) AS "h31",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "18", Bill_ExtantPrice, 0)) AS "h32",
							    SUM(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "19", Bill_ExtantPrice, 0)) AS "h33"
							    ')
						->first();
// 		dd($salesHour);
		for($i = 6; $i<=19; $i++){
			$label = $i.':00-'.$i.':59';
			$j = $i+14;
			$temp1 = "h$i";
			$temp2 = "h$j";
			$barChart[] = ['y'=>$label, 'a'=>$salesHour->$temp1, 'b'=>$salesHour->$temp2];
		}
		//line chart
		$salesHour = Bill::join('users', 'Bill_User', 'User_ID')
	    				->where('Bill_Status', 1)
						->whereRaw('1 AND '.$store)
						->selectRaw('
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "06", Bill_ExtantPrice, NULL)) AS "h6",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "07", Bill_ExtantPrice, NULL)) AS "h7",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "08", Bill_ExtantPrice, NULL)) AS "h8",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "09", Bill_ExtantPrice, NULL)) AS "h9",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "10", Bill_ExtantPrice, NULL)) AS "h10",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "11", Bill_ExtantPrice, NULL)) AS "h11",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "12", Bill_ExtantPrice, NULL)) AS "h12",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "13", Bill_ExtantPrice, NULL)) AS "h13",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "14", Bill_ExtantPrice, NULL)) AS "h14",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "15", Bill_ExtantPrice, NULL)) AS "h15",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "16", Bill_ExtantPrice, NULL)) AS "h16",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "17", Bill_ExtantPrice, NULL)) AS "h17",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "18", Bill_ExtantPrice, NULL)) AS "h18",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d',strtotime('-1 days')).'" AND HOUR(`Bill_Time`) = "19", Bill_ExtantPrice, NULL)) AS "h19",

							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "06", Bill_ExtantPrice, NULL)) AS "h20",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "07", Bill_ExtantPrice, NULL)) AS "h21",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "08", Bill_ExtantPrice, NULL)) AS "h22",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "09", Bill_ExtantPrice, NULL)) AS "h23",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "10", Bill_ExtantPrice, NULL)) AS "h24",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "11", Bill_ExtantPrice, NULL)) AS "h25",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "12", Bill_ExtantPrice, NULL)) AS "h26",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "13", Bill_ExtantPrice, NULL)) AS "h27",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "14", Bill_ExtantPrice, NULL)) AS "h28",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "15", Bill_ExtantPrice, NULL)) AS "h29",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "16", Bill_ExtantPrice, NULL)) AS "h30",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "17", Bill_ExtantPrice, NULL)) AS "h31",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "18", Bill_ExtantPrice, NULL)) AS "h32",
							    COUNT(IF(DATE(`Bill_Time`) = "'. date('Y-m-d').'" AND HOUR(`Bill_Time`) = "19", Bill_ExtantPrice, NULL)) AS "h33"
							    ')
						->first();

		for($i = 6; $i<=19; $i++){
			$label = $i.':00-'.$i.':59';
			$j = $i+14;
			$temp1 = "h$i";
			$temp2 = "h$j";
			$lineChart[] = ['y'=>$label, 'a'=>$salesHour->$temp1, 'b'=>$salesHour->$temp2];
		}

		return view('System.Dashboard.Index', compact('getSales', 'dataChart', 'arrDataChart', 'barChart', 'lineChart', 'shopList'));
    }
}
