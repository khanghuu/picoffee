<?php

namespace App\Http\Controllers\System;

use App\Model\Ingredient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\Shop;
use App\Model\Category;
use App\Model\RawMaterial;
use File;
use DB;


class ProductController extends Controller{

    public function getManage(Request $request){
        $user = session('user');
        $productList = Product::query();
        $request->search_product_code?$productList->where('Product_Code', $request->search_product_code):0;
        $request->search_product_name?$productList->where('Product_Name', $request->search_product_name):0;
        $request->search_product_category?$productList->where('Product_Category', $request->search_product_category):0;
        $request->search_product_status != null?$productList->where('Product_Status', $request->search_product_status):0;
        if ($user->User_Level == 1 || $user->User_Level == 3) {
            $request->search_product_shop?$productList->where('Product_Shop', $request->search_product_shop):0;
        }
        $shopList = '';
        if ($user->User_Level == 1 || $user->User_Level == 3) {
            $productList = $productList->join('category', 'Product_Category', 'Category_ID')
                ->join('shop', 'Product_Shop', 'Shop_ID')->get();
            $categoryList = Category::all();
            $shopList = Shop::all();
        }
        if ($user->User_Level == 2) {
            $productList = $productList->join('category', 'Product_Category', 'Category_ID')
                ->join('shop', 'Product_Shop', 'Shop_ID')
                ->where('Product_Shop', $user->User_Location_Store)->get();
            $categoryList = Category::where('Category_Shop', $user->User_Location_Store)->get();
        }
        return view('System.Data.Product.product-manage', compact('productList', 'categoryList', 'shopList'));
    }

    public function getProductAdd()
    {
        $shopList = Shop::select('Shop_ID', 'Shop_Name')->where('Shop_Status', 1)->get();
        $user = session('user');
        $categoryList = Category::where('Category_Shop', $user->User_Location_Store)->select('Category_ID', 'Category_Name')->get();
        $ingredientList = Ingredient::where('Ingredient_Shop', $user->User_Location_Store)->select('Ingredient_ID', 'Ingredient_Name', 'Ingredient_Unit')->get();
        return view('System.Data.Product.add-product', compact('shopList', 'categoryList', 'ingredientList'));
    }

    public function getDataShop(Request $req){
	    $raw = RawMaterial::where('Raw_Material_Shop', $req->id)->get();
	    $ingredient = Ingredient::where('Ingredient_Shop', $req->id)->get();

	    return response()->json(['status'=>true, 'raw'=>$raw, 'ingredient'=>$ingredient], 200);
    }

    public function postAdd(Request $request)
    {
        $request->validate([
                'add_product_name' => 'bail|required|max:255',
                'add_product_category' => 'bail|required|exists:category,Category_ID',
                'add_product_price' => 'bail|required|numeric|min:0|not_in:0',
                'ingredient' => 'bail|required|array',
                'add_product_status' => 'required|min:0|max:1'
            ],
            [
                'add_product_name.required' => ERROR_VALIDATE['require'].' tên.',
                'add_product_name.max' => ERROR_VALIDATE['max'].' tên.',
                'add_product_category.required' => ERROR_VALIDATE['require'].' danh mục.',
                'add_product_category.exists' => ERROR_VALIDATE['exists'].' danh mục.',
                'add_product_price.required' => ERROR_VALIDATE['require'].' giá tiền.',
                'add_product_price.numeric' => ERROR_VALIDATE['numeric'].' giá tiền.',
                'add_product_price.min' => ERROR_VALIDATE['min'].' giá tiền.',
                'add_product_price.not_in' => ERROR_VALIDATE['not_in'].' giá tiền.',
                'ingredient.required' => ERROR_VALIDATE['require'].' nguyên liệu.',
                'add_product_status.required' => ERROR_VALIDATE['require'].' trạng thái.',
                'add_product_status.min' => ERROR_VALIDATE['min'].' trạng thái.',
                'add_product_status.max' => ERROR_VALIDATE['max'].' trạng thái.',
            ]
        );
        $user = session('user');
        $product_shop_id = null;
        if ($user->User_Level == 1) {
            $request->validate([
                'add_product_shop' => 'bail|required|exists:shop,Shop_ID',
                ],
                [
                    'add_product_shop.required' => ERROR_VALIDATE['require'].' cửa hàng.',
                    'add_product_shop.exists' => ERROR_VALIDATE['exists'].' cửa hàng.'
                ]
            );
            $product_shop_id = $request->add_product_shop;
        }

        $check_exist_product_name = Product::where('Product_Name', $request->add_product_name)
            ->where('Product_Shop', $product_shop_id)->first();
        if  ($check_exist_product_name) {
            return responseRedirect(0,'Tên sản phẩm đã tồn tại.');
        }
        if ($user->User_Level == 2) {
            $product_shop_id = $user->User_Location_Store;
        }

        // hình ảnh
        if($request->hasFile('add_product_image')) {
            $extArray = array('png', 'gif', 'jpg', 'jpeg');
            $file = $request->add_product_image;
            $extention = $file->getClientOriginalExtension();
            if(!in_array($extention, $extArray)){
                return responseRedirect(0,'Hình ảnh không định dạng.');
            }
            $uploadPath = public_path('/product');
            $imgName = $this->generateString($request->add_product_name,$extention);
            $fileName = $imgName.'.'.$extention;

            $file->move($uploadPath, $fileName);

        }else{
            return responseRedirect(0,'Vui lòng tải hình sản phẩm');
        }

        foreach($request->ingredient as $key => $value){
            $check_ingredient_exist = Ingredient::where('Ingredient_ID', $key)->first();
            if (!$check_ingredient_exist) {
                return responseRedirect(0,'Lỗi, Nguyên liệu không tồn tại!');
            }
            if($value <= 0){
                return responseRedirect(0,'Lỗi, Số lượng gnuyên liệu không hợp lệ!');
            }
        }
        $json = json_encode($request->ingredient);
        $product_Code = $this->generateCode($request->add_product_name);
        $arrayInsert = array(
            'Product_Name' => $request->add_product_name,
            'Product_Code' => strtoupper($product_Code),
            'Product_Category' => $request->add_product_category,
            'Product_Price' => $request->add_product_price,
            'Product_Image' => $fileName,
            'product_Status' => $request->add_product_status,
            'Product_Params' => $json,
            'Product_Shop' => $product_shop_id
        );

        $insert = Product::insertGetId($arrayInsert);
        if($insert){
           writeLog("Thêm sản phẩm ID: $insert");
            return responseRedirect(1,'Thêm sản phẩm thành công!');

        }
        return responseRedirect(0,ERROR_CONTACT_AMDIN);

    }

    public function getProductAddParams(Request $request) {
        if ($request->shop_id != null) {
            $product['ingredientList'] = Ingredient::where([
                ['Ingredient_Shop', $request->shop_id],
                ['Ingredient_Status',1]
            ])->select('Ingredient_ID', 'Ingredient_Name', 'Ingredient_Unit')->get();
            $product['categoryList'] = Category::where([
                ['Category_Shop', $request->shop_id],
                ['Category_Status', 1]
            ])->select('Category_ID', 'Category_Name')->get();
            return responseJsonData(1, $product);
        }

    }

    public function getProductEdit($productID)
    {

        $editProduct = Product::where('Product_ID', $productID)->join('shop','Product_Shop', 'Shop_ID')->first();
        if (!$editProduct) {
            return responseRedirect( 0,'Sản phẩm không tồn tại');
        }
        if ($editProduct->Product_Params) {
            $jsonParams = json_decode($editProduct->Product_Params);
            $ingredientArr = [];
            $tempArr = [];
            foreach ($jsonParams as $ingredient => $quantity) {
                array_push($ingredientArr, $ingredient);
                $tempArr[$ingredient] = $quantity;
            }
            $ingredientList = Ingredient::whereIn('Ingredient_ID', $ingredientArr)->select('Ingredient_ID', 'Ingredient_Name', 'Ingredient_Unit')->get();
            foreach ($ingredientList as $ingredient) {
                $ingredient->Ingredient_Quantity = $tempArr[$ingredient->Ingredient_ID];
            }
            $editProduct->Product_Params = $ingredientList;
        }
        $ingredientList = Ingredient::select('Ingredient_ID', 'Ingredient_Name', 'Ingredient_Unit')
            ->where('Ingredient_Status', 1)
            ->where('Ingredient_Shop', $editProduct->Product_Shop)->get();
        $categoryList = Product::select('Category_ID', 'Category_Name')->where('Product_ID', $productID)->where('Category_Status', 1)
            ->join('category', 'Product_Shop', 'Category_Shop')->get();
        return view('System.Data.Product.edit-product', compact('editProduct', 'ingredientList', 'categoryList'));

    }

    public function postEdit(Request $request){
        $request->validate([
            'edit_product_name' => 'bail|required|max:255',
            'edit_product_category' => 'bail|required|exists:category,Category_ID',
            'edit_product_price' => 'bail|required|numeric|min:0|not_in:0',
            'ingredient' => 'bail|required|array',
            'edit_product_status' => 'required|min:0|max:1'
        ],
            [
                'edit_product_name.required' => ERROR_VALIDATE['require'].' tên.',
                'edit_product_name.max' => ERROR_VALIDATE['max'].' tên.',
                'edit_product_category.required' => ERROR_VALIDATE['require'].' danh mục.',
                'edit_product_category.exists' => ERROR_VALIDATE['exists'].' danh mục.',
                'edit_product_price.required' => ERROR_VALIDATE['require'].' giá tiền.',
                'edit_product_price.numeric' => ERROR_VALIDATE['numeric'].' giá tiền.',
                'edit_product_price.min' => ERROR_VALIDATE['min'].' giá tiền.',
                'edit_product_price.not_in' => ERROR_VALIDATE['not_in'].' giá tiền.',
                'ingredient.required' => ERROR_VALIDATE['require'].' nguyên liệu.',
                'edit_product_status.required' => ERROR_VALIDATE['require'].' trạng thái.',
                'edit_product_status.min' => ERROR_VALIDATE['min'].' trạng thái.',
                'edit_product_status.max' => ERROR_VALIDATE['max'].' trạng thái.',
            ]
        );
        $productExist = Product::where('Product_Code', $request->edit_product_code)->first();
        if(!$productExist){
            return responseRedirect(0,'Sản phẩm không tồn tại');
        }
        $productUpdateData = [];
        if(strcmp($request->edit_product_name, $productExist->Product_Name)){
            $checkExistProductName = Product::where('Product_Name', $request->edit_product_name)
                ->where('Product_Shop', $productExist->Product_Shop)->first();
            if ($checkExistProductName) {
                return responseRedirect(0,'Tên sản phẩm đã tồn tại.');
            }
            $productUpdateData['Product_Name'] = $request->edit_product_name;
        }
        if (Category::where('Category_ID', $request->edit_product_category)->where('Category_Shop', $productExist->Product_Shop)->get()) {
            $request->edit_product_category != $productExist->Product_Category?$productUpdateData['Product_Category'] = $request->edit_product_category:0;
        }
        $request->edit_product_price != $productExist->Product_Price?$productUpdateData['Product_Price'] = $request->edit_product_price:0;
        $request->edit_product_status != $productExist->Product_Status?$productUpdateData['Product_Status'] = $request->edit_product_status:0;
        // hình ảnh
        if($request->hasFile('edit_product_image')) {
            $extArray = array('png', 'gif', 'jpg', 'jpeg');

            $file = $request->edit_product_image;
            $ext = $file->getClientOriginalExtension();
            if(!in_array($ext, $extArray)){
                return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Hình ảnh không định dạng']);
            }

            $uploadPath = public_path('/product');
            $imgName = $request->edit_product_name;
            $fileName = $this->generateString($imgName, $ext);


            $file->move($uploadPath, $fileName);

            $productUpdateData['Product_Image'] = $fileName;

            // xoá ảnh củ
            $uploadPathOld = public_path('/product'.$productExist->Product_Image);

            if(File::exists($uploadPathOld)) {
                File::delete($uploadPathOld);
            }
        }
        $json = '';
        foreach($request->ingredient as $ingredient => $quantity){
            $check_ingredient_exist = Ingredient::where('Ingredient_ID', $ingredient)->first();
            if (!$check_ingredient_exist) {
                return responseRedirect(0,'Lỗi, Nguyên liệu không tồn tại!');
            }
            if($quantity <= 0){
                return responseRedirect(0,'Lỗi, Số lượng gnuyên liệu không hợp lệ!');
            }
        }
        $productUpdateData['Product_Params'] = json_encode($request->ingredient);
        $updateResult = Product::where('Product_ID', $productExist->Product_ID)->update($productUpdateData);
        // ghi log
        if($updateResult){
            writeLog('Sửa sản phẩm ID: '.$productExist->Product_ID);
            return responseRedirect(1,'Sửa sản phẩm thành công!');
        }
        return responseRedirect(0,ERROR_CONTACT_AMDIN);

    }

    public function getDelete($id){
        $editProduct = Product::where('product_ID', $id)->first();
        if(!$editProduct){
            return responseRedirect(0,'Lỗi, Sản phẩm này không tồn tại');
        }
        $deleteStatus = Product::where('product_ID', $id)->delete();
        if($deleteStatus) {
            writeLog('Xoá sản phẩm ID: '.$id);
            return responseRedirect(1,'Xoá sản phẩm thành công!', 'system.product.getManage');
        }
        else {
            return responseRedirect(0,'Có lỗi xẩy ra vui lòng liên hệ admin!', 'system.product.getManage');
        }

    }

    function generateCode($name)
    {
        $name = strtoupper(convert_vi_to_en($name));
        $name = $str = str_replace(' ', '', $name);
        $code = substr($name, 0, 2) . rand(000, 999);
        strlen($code) < 5? $code .= rand(1,9):0;
        $checkCodeExist = Product::where('product_Code', $code)->first();
        if ($checkCodeExist) {
            $code = generateCode($name);
        }
        return $code;
    }


    function generateString(string  $name, string $extension)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 10; $i++) {
            $randstring = $name."_".$characters[rand(0, (strlen($characters) - 1))].".".$extension;
        }
        $checkExistImagePath = Product::where('Product_Image', $randstring)->first();
        if ($checkExistImagePath) {
            $randstring = $this->generateString($name, $extension);
        }
        return $randstring;
    }

}
