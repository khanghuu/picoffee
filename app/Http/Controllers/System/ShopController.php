<?php

namespace App\Http\Controllers\System;

use App\Model\User;
use App\Model\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ShopController extends Controller
{
    public function getAllShop(Request $req){
	    $row = new Shop;
	    $shopIDs = Shop::select('Shop_ID')->get();

	    if($req->id){
            $row = $row->where('Shop_ID',$req->id);
        }
        
        if($req->name){
            $row = $row->where('Shop_Name','like', "%$req->name%");
        }

        if($req->address){
            $row = $row->where('Shop_Address','like', "%$req->shop%");
        }

        if($req->status !== null){
            $row = $row->where('Shop_Status', $req->status);
        }
        $row = $row->where('Shop_Status', '<>', -1)->orderByDesc('Shop_ID')->get();
	    return view('System.Shop.Index', compact('row','shopIDs'));
    }
    
    public function postAdd(Request $req){
        if(!$req->name){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Vui lòng nhập tên cửa hàng']);
        }
        if(!$req->datetime){
            $create = date('Y-m-d H:i:s');
        }else{
            $create = $req->datetime;
        }
        if(!$req->address){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Vui lòng điền địa chỉ cửa hàng']);
        }
        
        $arrayInsert = array(
            'Shop_Name' => $req->name,
            'Shop_Address' => $req->address,
            'created_at' => $create,
            'updated_at' => $create,
            'Shop_Status' => 1
        );
        $insertID = Shop::insertGetId($arrayInsert);
        
        if($insertID){
            writeLog('Thêm cửa hàng ID: '.$insertID);
            return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Thêm cửa hàng thành công!']);
        }else{
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>config('global.DB_ERROR')]);
        }
    }
    
    public function getShop(Request $req){
        $getShop = Shop::find($req->id);
        if($getShop){
        	return response()->json(['status'=>true, 'data'=>$getShop]);
        }else{
	        return response()->json(['status'=>false, 'message'=>'Cửa hàng không tồn tại!']);
        }
    }

    public function postEdit(Request $req){
        if(!$req->name){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Vui lòng nhập tên cửa hàng']);
        }
        
        if(!$req->address){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Vui lòng điền địa chỉ cửa hàng']);
        }
        
        $getShop = Shop::find($req->id);
        if(!$getShop){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Không tìm thấy cửa hàng!']);
        }
        $getShop->Shop_Name = $req->name;
        $getShop->Shop_Address = $req->address;
        $getShop->Shop_Status = $req->status;
        $getShop->save();
        
        if($getShop){
            writeLog('Sửa cửa hàng ID: '.$getShop->Shop_ID);
            return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Sửa thông tin cửa hàng thành công!']);
        }else{
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>config('global.DB_ERROR')]);
        }
    }
	
    public function getDelete($id){
        Shop::where('Shop_ID', $id)->update(['Shop_Status' => -1]);
        writeLog('Xóa Cửa hàng ID: '.$id);
        return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Xoá cửa hàng thành công!']);
    }

}
