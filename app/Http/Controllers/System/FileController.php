<?php

namespace App\Http\Controllers\System;

use App\Exports\BillExport;
use App\Exports\CategoryExport;
use App\Exports\DestroyIngredientExport;
use App\Exports\DestroyRawExport;
use App\Exports\IngredientExport;
use App\Exports\ProductExport;
use App\Exports\RawMaterialExport;
use App\Exports\RevenueCustomerExport;
use App\Exports\RevenueDateExport;
use App\Exports\RevenueExport;
use App\Exports\RevenueProductExport;
use App\Exports\RevenueShiftExport;
use App\Exports\RevenueStaffExport;
use App\Exports\SalesExport;
use App\Exports\SignedBillExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\WarehouseRawExport;
use App\Exports\WarehouseIngExport;
use App\Exports\WarehouseHistoryExport;
use App\Exports\WarehouseExportTemplate;
use App\Imports\WarehouseIngImport;
use App\Imports\WarehouseRawImport;
use App\Model\Ingredient;
use App\Model\RawMaterial;
use App\Model\Shop;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\HeadingRowImport;
use File;
use Session;

class FileController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function warehouseExportTemplate()
    {
        ob_end_clean();
        ob_start();
        return (new WarehouseExportTemplate)->download('Mau_Nhap_Lieu_Kho.xlsx');
    }

    public function warehouseRawExport()
    {
        ob_end_clean();
        ob_start();
        return (new WarehouseRawExport)->download('Thong_Ke_Nhap_Kho_Nguyen_Lieu_Tho.xlsx');
    }

    public function warehouseIngExport()
    {
        ob_end_clean();
        ob_start();
        return (new WarehouseIngExport)->download('Thong_Ke_Nhap_Kho_Nguyen_Lieu_Tinh.xlsx');
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function warehouseImport(Request $request)
    {
        ob_end_clean();
        ob_start();

        $store = $request->warehouse_import_shop ?? Session('user')->User_Location_Store;
        if($request->hasFile('file'))
        {
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension != "xlsx" && $extension != "xls" && $extension != "csv") {
                return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'File nhập không hợp lệ']);
            }
        }
        else{
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Vui lòng chọn file để nhập kho']);
        }

        $check_shop = Shop::find($store);
        if(!$check_shop){
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Cửa hàng không tồn tại']);
        }
        if(!$request->file){
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Cần chọn file để nhập kho']);
        }
        if($request->warehouse_import_material == 1){

            Excel::import(new WarehouseRawImport($request) , request()->file('file'));
        }
        else{
            Excel::import(new WarehouseIngImport($request) , request()->file('file'));
        }
        if(Session::get('check_file')){
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'File không đúng mẫu']);
        }
        $arr_quantity_file = Session::get('quantity_file')[count(Session::get('quantity_file'))-1];

        if($request->warehouse_import_material == 1){
            foreach($arr_quantity_file as $key => $value){
                $update_raw = RawMaterial::where('Raw_Material_ID',$key)->first();
                $update_raw->Raw_Material_Quantity += $value;
                $update_raw->save();
            }
        }
        else{
            foreach($arr_quantity_file as $key => $value){
                $update_ing = Ingredient::where('Ingredient_ID',$key)->first();
                $update_ing->Ingredient_Quantity += $value;
                $update_ing->save();
            }
        }
        Session::forget('quantity_file');
        return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Nhập kho thành công']);
    }

    public function warehouseHistoryExport()
    {
        ob_end_clean();
        ob_start();
        return (new WarehouseHistoryExport)->download('Lich_Su_Nhap_Kho.xlsx');
    }

    public function productExport(){
        ob_end_clean();
        ob_start();
        return (new ProductExport)->download('Danh_Sach_San_Pham.xlsx');
    }

    public function categoryExport(){
        ob_end_clean();
        ob_start();
        return (new CategoryExport)->download('Danh_Sach_Danh_Muc.xlsx');
    }

    public function rawExport(){
        ob_end_clean();
        ob_start();
        return (new RawMaterialExport)->download('Danh_Sach_Nguyen_Lieu_Tho.xlsx');
    }

    public function ingExport(){
        ob_end_clean();
        ob_start();
        return (new IngredientExport)->download('Danh_Sach_Nguyen_Lieu_Tinh.xlsx');
    }

    public function billExport(){
        ob_end_clean();
        ob_start();
        return (new BillExport)->download('Tat_Ca_Don_Hang.xlsx');
    }

    public function revenueExport(){
        ob_end_clean();
        ob_start();
        return (new RevenueExport)->download('Doanh_Thu_Hang_Gio.xlsx');
    }

    public function signedBillExport(){
        ob_end_clean();
        ob_start();
        return (new SignedBillExport)->download('Ky_Bill.xlsx');
    }

    public function salesExport(){
        ob_end_clean();
        ob_start();
        return (new SalesExport)->download('Thong_Ke_Ban_Hang.xlsx');
    }

    public function destroyRawExport(){
        ob_end_clean();
        ob_start();
        return (new DestroyRawExport)->download('Danh_Sach_Huy_Nguyen_Lieu_Tho.xlsx');
    }

    public function destroyIngredientExport(){
        ob_end_clean();
        ob_start();
        return (new DestroyIngredientExport)->download('Danh_Sach_Huy_Nguyen_Lieu_Tinh.xlsx');
    }

    public function revenueShiftExport(){
        ob_end_clean();
        ob_start();
        return (new RevenueShiftExport)->download('Danh_Thu_Theo_Ca.xlsx');
    }
    public function exportRevenueDate(){
        ob_end_clean();
        ob_start();
        return (new RevenueDateExport)->download('Danh_Thu_Theo_Ngay.xlsx');
    }
    public function exportRevenueStaff(){
        ob_end_clean();
        ob_start();
        return (new RevenueStaffExport)->download('Danh_Thu_Theo_Nhan_Vien.xlsx');
    }
    public function exportRevenueProduct(){
        ob_end_clean();
        ob_start();
        return (new RevenueProductExport)->download('Danh_Thu_Theo_San_Pham.xlsx');
    }
    public function exportRevenueCustomer(){
        ob_end_clean();
        ob_start();
        return (new RevenueCustomerExport)->download('Danh_Thu_Theo_Khach_Hang.xlsx');
    }
}
