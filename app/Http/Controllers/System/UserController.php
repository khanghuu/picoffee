<?php

namespace App\Http\Controllers\System;

use App\Model\Countries;
use App\Model\Log;
use App\Model\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use \Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\EditRequest;
use App\Http\Requests\EditUserInID;
use DB;


class UserController extends Controller
{
    public function getProfile()
    {
        if(Session('user')->User_Level != 1 && Session('user')->User_Level != 3){
            $user = User::where('User_ID', Session('user')->User_ID)->join('level','users.User_Level','level.level_ID')
            ->join('shop','shop.Shop_ID','users.User_Location_Store')->first();
        }
        else{
            $user = User::where('User_ID', Session('user')->User_ID)->join('level','users.User_Level','level.level_ID')->first();
        }

        return view('System.User.profile', compact('user'));
    }

    public function getAllUsers()
    {
        $user = session('user');
        $AllUsers = User::query();
        if ($user->User_Level == 2) {
            $AllUsers = $AllUsers->where('User_Status', 1)->where('User_Location_Store', $user->User_Location_Store);
        }
        if ($user->User_Level == 1 || $user->User_Level == 3) {
            $AllUsers = $AllUsers->where('User_Status', 1)->leftJoin('shop','shop.Shop_ID','users.User_Location_Store');
        }
        if ($user->User_Level == 1 || $user->User_Level == 3) {
            $level = DB::table('level')->get();
            $shop = DB::table('shop')->get();
        }
        if ($user->User_Level ==  2) {
            $level = DB::table('level')->where('level_ID', 4)->get();
            $shop = DB::table('shop')->where('Shop_ID', $user->User_Location_Store)->get();
        }

        $AllUsers = $AllUsers->join('level','users.User_Level','level.level_ID')->get();
        
        return view('System.User.all-users', compact('user', 'AllUsers', 'shop', 'level'));
    }

    public function uploadImage(Request $request)
    {
        $file = $request->file;

        if(!$request->hasFile('file') || $file->isValid()) {

        }

        $User_ID = Session('user')->User_ID;

        $filename = "kjdfh$User_ID.".$file->getClientOriginalExtension();
        $dir_path = "users/$User_ID/";

        $file->move(public_path($dir_path), $filename);

        $file_path = ["User_Image" => "../$dir_path$filename"];

        User::where('User_ID', $User_ID)
            ->update($file_path);

        return redirect()->back()->with(['flash_level'=>'success', 'flash_message'=>'Sửa người dùng thành công!']);

    }

    public function postEditUserInID(EditUserInID $req)
    {
        $user = Session('user');
        $userEdit = User::where('User_ID',$user->User_ID)->first();
        if(!$userEdit){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'User does not exist']);
        }

        if($userEdit->User_Email != $req->edit_email){
            $check_mail = User::where('User_Email',$req->edit_email)->first();
            if($check_mail){
                return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Email đã tồn tại']);
            }
        }
        $userEdit->User_Name = $req->edit_name;
        $userEdit->User_Email = strtolower($req->edit_email);
        $userEdit->User_Sex = $req->edit_gender;
        $userEdit->User_Birthday = $req->edit_birthday;
        $userEdit->User_Address = $req->edit_address;
        $userEdit->User_Phone = $req->edit_phone;

        //Tiến Hành Update DB
        $userEdit->save();
        // ghi log

        writeLog('Sửa người dùng: '.$userEdit->User_Email, $user->User_ID);

        return redirect()->back()->with(['flash_level'=>'success', 'flash_message'=>'Sửa người dùng thành công!']);
    }

    public function postEditUser(EditRequest $req){
        $store = $req->edit_store;
        if($req->edit_level == 1 || $req->edit_level == 3){
            $store = "";
        }
        $user = Session('user');
        $userEdit = User::where('User_ID',$req->id_user)->first();
        if(!$userEdit){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'User does not exist']);

        }

        if($userEdit->User_Email != $req->edit_email){
            $check_mail = User::where('User_Email',$req->edit_email)->first();
            if($check_mail){
                return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Email đã tồn tại']);
            }
        }
        $userEdit->User_Name = $req->edit_name;
        $userEdit->User_Email = strtolower($req->edit_email);
        $userEdit->User_Location_Store = $store;
        $userEdit->User_Sex = $req->edit_gender;
        $userEdit->User_Birthday = $req->edit_birthday;
        $userEdit->User_Address = $req->edit_address;
        $userEdit->User_Level = $req->edit_level;
        $userEdit->User_RegisteredDateTime = $req->edit_start_date;
        $userEdit->User_Phone = $req->edit_phone;

        //Tiến Hành Update DB
        $userEdit->save();
        // ghi log

        writeLog('Sửa người dùng: '.$userEdit->User_Email, $user->User_ID);

        return redirect()->back()->with(['flash_level'=>'success', 'flash_message'=>'Sửa người dùng thành công!']);
    }

    public function getEditUser(Request $req){
        $user = User::where('User_ID', $req->id)->first();

        return response()->json(array('status'=>true, 'info_user'=>$user));
    }

    function buildTree(array $elements, $parentId) {
        $branch = array();

        foreach ($elements as $element) {

            $element['text'] = $element['User_ID'] . " (" . $element['User_Email'] . ")";
            if ($element['User_Parent'] == $parentId){
                $nodes = $this->buildTree($elements, $element['User_ID']);
                if ($nodes) {
                    $element['nodes'] = $nodes;
                }

                $branch[] = $element;
            }
        }
        return $branch;
    }

    public function getActionEmployees(){
        $user = session('user');
        $actionUsers = '';
        if ($user->User_Level == 1) {
            $actionUsers = Log::orderByDesc('Log_Name_ID')
                ->where('Log_Name_Log', 'not like' ,'Tài khoản đăng%')->paginate(25);

        }
        if ($user->User_Level == 2) {
            $actionUsers = Log::orderByDesc('Log_Name_ID')->join('users', 'Log_Name_User', 'users.User_ID')
                ->where('users.User_Location_Store', $user->User_Location_Store)
                ->where('Log_Name_Log', 'not like' ,'Tài khoản đăng%')->paginate();
        }
        return view('System.User.ActionEmployees', compact('actionUsers'));
    }

    public function getAction(){
        $user = session('user');
        if ($user->User_Level == 1) {
            $actionUsers = Log::where('Log_Name_Log', 'like', 'Tài khoản đăng%')->orderByDesc('Log_Name_ID')->paginate();
        }
        if ($user->User_Level == 2) {
            $actionUsers = Log::join('users', 'Log_Name_User', 'users.User_ID')->where('users.User_Location_Store', $user->User_Location_Store)
            ->where('Log_Name_Log', 'like', 'Tài khoản đăng%')->orderByDesc('Log_Name_ID')->paginate();
        }
        return view('System.User.Activity', compact('user', 'actionUsers'));
    }

}
