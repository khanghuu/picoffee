<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Roles;
use App\Model\Permissions;

class RolesController extends Controller
{
    public function getManage()
    {
       $roles = Roles::all();
      return view('System/Roles/roles-manage', compact('roles'));
    }

    public function createRolesGroup() {
        $permissions = Permissions::all();
        return view('System.Roles.create-role-group', compact('permissions'));

    }

    public function postcreateRolesGroup(Request $request)
    {
        $formData =  $request->all();

        if(empty(array_key_exists('data', $formData))) {

            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Vui long chon Permission']);
        }
        $role_group_name =  $formData['rolename'];

        $role_group_name_exist = Roles::where('Role_Name', $role_group_name)->first();
        if($role_group_name_exist) {
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Tên role group đã tồn tại']);
        }

        $permission_list= array();
        foreach($formData['data'] as $key => $value) {
            $permission_id = $key;

            if (array_key_exists("view",$value)) {
                $view = (int)$value['view'];
            }
            else {
                $view = 0;
            }

            if (array_key_exists("add",$value)) {
                $add = (int)$value['add'];
            }
            else {
                $add = 0;
            }

            if (array_key_exists("edit",$value)) {
                $edit = (int)$value['edit'];
            }
            else {
                $edit = 0;
            }

            if (array_key_exists("del",$value)) {
                $del = (int)$value['del'];
            }
            else {
                $del = 0;
            }
            $permission_list[$permission_id] = [$view, $add, $edit, $del];

        }
        $permission_list = json_encode($permission_list);

        //kiem tra trung permisstions
        $permission_list_exist = Roles::where('Permission_List', $permission_list)->first();
        if($permission_list_exist) {
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Role Group này đã tồn tài']);
        }

        $role = new Roles;
        $role->Role_Name = $role_group_name;
        $role->Permission_List = $permission_list;
        $role->timestamps;
        $role->save();
        return redirect()->route('system.roles.getManage')->with(['flash_level'=>'success','flash_message'=>'Thêm Role Group Thanh Công!']);

    }

    public function editRolesGroup($id)
    {
        $role = Roles::where('Role_ID', $id)->first();
        $permission_list = $role->Permission_List;
        $permissions = Permissions::all();
        return view('System.Roles.edit-role-group', compact('permissions', 'role', 'permission_list'));
    }

    public function posteditRolesGroup(Request $request, $id)
    {
        $formData =  $request->all();

        if(empty(array_key_exists('data', $formData))) {

            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Vui long chon Permission']);
        }

        $role_group_name =  $formData['rolename'];
        $role = Roles::where('Role_ID', $id)->first();
        $role_group_name_db = $role->Role_Name;
        $a = strcmp($role_group_name, $role_group_name_db);

        if(strcmp($role_group_name, $role_group_name_db) != 0)
        {
            $role_group_name_exist = Roles::where('Role_Name', $role_group_name)->first();

            if($role_group_name_exist) {
                return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Tên role group đã tồn tại']);
            }

        }


        $permission_list= array();
        foreach($formData['data'] as $key => $value) {
            $permission_id = $key;

            if (array_key_exists("view",$value)) {
                $view = (int)$value['view'];
            }
            else {
                $view = 0;
            }

            if (array_key_exists("add",$value)) {
                $add = (int)$value['add'];
            }
            else {
                $add = 0;
            }

            if (array_key_exists("edit",$value)) {
                $edit = (int)$value['edit'];
            }
            else {
                $edit = 0;
            }

            if (array_key_exists("del",$value)) {
                $del = (int)$value['del'];
            }
            else {
                $del = 0;
            }
            $permission_list[$permission_id] = [$view, $add, $edit, $del];

        }
        $permission_list = json_encode($permission_list);

        //kiem tra trung permisstions
        $permission_list_exist = Roles::where('Permission_List', $permission_list)->first();

        if($permission_list_exist) {
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Role Group Permission này đã tồn tài']);
        }

        $role->Role_Name = $role_group_name;
        $role->Permission_List = $permission_list;
        $role->timestamps;
        $role->save();
        return redirect()->route('system.roles.getManage')->with(['flash_level'=>'success','flash_message'=>'Edit Role Group Thanh Công!']);
    }

}
