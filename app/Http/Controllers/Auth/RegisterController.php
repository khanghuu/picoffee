<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\EditRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\RegisterRequest;
use App\Model\User;
use DB;
use Mail;

class  RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function getRegister(){
        return view('System.Auth.register');
    }

    public function postRegister(RegisterRequest $request){
        $User_ID = $this->RandonIDUser();
        $store = $request->shop;
        if($request->level == 1 || $request->level == 3){
            $store = "";
        }
        $Register = array(
            'User_ID' => $User_ID,
            'User_Name' => $request->name,
            'User_Email' => strtolower($request->email),
            'User_EmailActive' => 1,
            'User_Address' => $request->address,
            'User_Password' => bcrypt($request->password),
            'User_RegisteredDatetime' => $request->start_date,
            'User_Birthday' => $request->birthday,
            'User_Location_Store' =>  $store,
            'User_Phone' => $request->phone,
            'User_Level' => $request->level,
            'User_Status' => 1,
            'User_Verify' => 1,
            'User_Confirm' =>1,
            'User_OTP' => 0,
            'User_Sex' =>$request->gender,
        );
        //Tiến Hành Insert DB
        User::insert($Register);
        if(session('user')) {
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' =>'Thêm tài khoản thành công']);
        }
        return redirect()->route('getLogin');
    }

    public function RandonIDUser(){

        $id = rand(100000, 999999);
        $user = User::where('User_ID',$id)->first();
        if(!$user){
            return $id;
        }else{
            return $this->RandonIDUser();
        }
    }

}
