<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\User;
use \Hash;
use  \Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function getLogin()
    {
        return view('System.Auth.login');
    }

    public function postLogin(Request $request){
        $this->validate($request,
            [
                'email'      =>  'required',
                'password'   =>  'required'
            ]
        );

        $loginInfo = array(
            'User_Email' => $request->email,
            'User_Password' => $request->password
        );

        $user = User::where('User_Email',$loginInfo['User_Email'])->where('User_Status',1)->first();

        if($user){

            if (Hash::check($loginInfo['User_Password'], $user->User_Password)) {

                Session::put('user', $user);
                writeLog('Tài khoản đăng nhập');
                return redirect()->route('system.dashboard')->with(['flash_level'=>'success', 'flash_message'=>'Đăng nhập thành công!']);

            }else{
                return redirect()->route('getLogin')->with(['flash_level'=>'error', 'flash_message'=>'Tài khoản hoặc mật khẩu không đúng']);
            }
        }
        else{
            return redirect()->route('getLogin')->with(['flash_level'=>'error', 'flash_message'=>'Tài khoản hoặc mật khẩu không đúng']);
        }

        return redirect()->route('getLogin')->with(['flash_level'=>'error', 'flash_message'=>'Tài khoản hoặc mật khẩu không đúng']);
    }

    public function getLogout(){
        if(session('userTemp')){
            $sessionOld = session('userTemp');
            // bỏ session củ
            writeLog('Tài khoản đăng xuất');
            Session::forget('user');
            Session::forget('userTemp');

            // tạo session mới

            Session::put('user', $sessionOld);
            writeLog('Tài khoản đăng nhập');
            return redirect()->route('system.dashboard')->with(['flash_level'=>'success', 'flash_message'=>'Đăng xuất thành công']);
        }
        writeLog('Tài khoản đăng xuất');
        Session::forget('user');
        return redirect()->route('getLogin');
    }
}
