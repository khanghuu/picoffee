<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Model\User;
use \Hash;
use Mail;

class ForgotPasswordController extends Controller
{
    public function getForgotPass(){
        return view('System.Auth.Passwords.email');
    }

    public function postForgotPass(Request $req){
        $result= User::where('User_Email', $req->email)->first();
        if(!$result){
            return redirect()->route('getForgotPass')->with(['flash_level'=>'error', 'flash_message'=>'Missing Email']);
        }
        include(app_path() . '/functions/xxtea.php');


        $pass = $this->generateRandomString(6);
        // update lại pass
        $token = base64_encode(xxtea_encrypt(json_encode(array('UserID'=>$result->User_ID, 'pass'=>$pass, 'time'=>time())),'picoffee'));
        $data = array('UserID'=>$result->User_ID, 'pass'=>$pass,'token'=>$token);

        DB::table('users')
            ->where('User_ID', $result->User_ID)
            ->update(['User_Password' => bcrypt($pass)]);
        Mail::send('Mail.Forgot', $data, function($msg) use ($result){
            $msg->from('do-not-reply@picoffee.com','Pi Coffee');
            $msg->to($result->User_Email)->subject('New Password');
        });
        return redirect()->route('getForgotPass')->with(['flash_level'=>'success', 'flash_message'=>'Please. check your email! We sent a new password to the email address you are register']);
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getChangePass(Request $request) {
        $this->validate($request,
            [
                'txtPassword' => 'required',
                'txtNewPassword' => 'required|min:6',
                'txtNewPasswordConfirm' => 'required|same:txtNewPassword',
            ],
            [
                'txtPassword.required' => ERROR_VALIDATE['require'].' mật khẩu hiện tại.',
                'txtNewPassword.required' => ERROR_VALIDATE['require'].' mật khẩu mới.',
                'txtNewPassword.min' => "Mật khẩu mới ít nhất 6 kí tự",
                'txtNewPasswordConfirm.required' => ERROR_VALIDATE['require'].' mật khẩu xác nhận.',
                'txtNewPasswordConfirm.same' => "Mật khẩu xác nhận phải giống mật khẩu mới",
            ]
        );
        $user = Session::get('user');
        if (Hash::check($request->txtPassword, $user->User_Password)) {

            User::where('User_ID', $user->User_ID)->update([
                'User_Password' => bcrypt($request->txtNewPassword)
            ]);
            return redirect()->route('getLogin')->with(['flash_level' => 'success', 'flash_message' => 'Thay đổi mật khẩu thành công']);
        }
        else {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Mật khẩu hiện tại không chính xác']);
        }
    }
}
