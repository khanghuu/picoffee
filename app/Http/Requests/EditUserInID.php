<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditUserInID extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit_name' => 'required|max:255',
            'edit_email' => 'required',
            'edit_gender' => "required|in:Male,Female,Other",
            'edit_birthday' => "required|date_format:Y-m-d|before:now|before:18 years ago",
            'edit_address' => "required|max:512",
            'edit_phone' => "required|numeric|digits_between:6,10"
        ];
    }
    public function messages() {
        return [
            'edit_name.required'        => ERROR_VALIDATE['require'].' tên.',
            'edit_name.max'             => ERROR_VALIDATE['max'] . ' tên.',
            'edit_email.required'       => ERROR_VALIDATE['require'].' email.',
            'edit_gender.required'      => ERROR_VALIDATE['require'] . ' giới tính.',
            'edit_birthday.required'    => ERROR_VALIDATE['require'] . ' ngày sinh.',
            'edit_birthday.date_format' => ERROR_VALIDATE['date_format'],
            'edit_birthday.before'      => "Ngày sinh không đủ 18 tuổi",
            'edit_address.required'        => ERROR_VALIDATE['require'].' địa chỉ.',
            'edit_address.max'             => ERROR_VALIDATE['max'] . ' địa chỉ.',
            'edit_phone.required'        => ERROR_VALIDATE['require'].' số điện thoại.',
            'edit_phone.numeric'        => ERROR_VALIDATE['numeric'].' số điện thoại.',
            'edit_phone.digits_between'  => "Độ dài số điện thoại phải từ 6 - 10 số",
        ];
    }
}
