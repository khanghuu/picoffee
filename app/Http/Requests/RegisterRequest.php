<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(session('user')) {
            return [
                'name' => 'required|max:255',
                'email' => 'required|unique:users,User_Email',
                'password' => 'required|min:6',
                'shop' => 'required|exists:shop,Shop_ID',
                'gender' => "required|in:Male,Female,Other",
                'birthday' => "required|date_format:Y-m-d|before:now|before:18 years ago",
                'address' => "required|max:512",
                'level' => 'required|exists:level,level_ID',
                'start_date' => "required|date_format:Y-m-d|after:birthday|before:today+1",
                'phone' => "required|numeric|digits_between:6,10"
            ];
        }
        return [
            'txtEmail' => 'required|unique:users,User_Email',
            'txtPassword' => 'required|min:6',
            'txtPasswordConfirm' => 'required|same:txtPassword',
        ];
    }
    public function messages() {
        return [
            'name.required'        => ERROR_VALIDATE['require'].' tên.',
            'name.max'             => ERROR_VALIDATE['max'] . ' tên.',
            'email.required'       => ERROR_VALIDATE['require'].' email.',
            'email.unique'          => ERROR_VALIDATE['unique'] . ' email.',
            'gender.required'      => ERROR_VALIDATE['require'] . ' giới tính.',
            'birthday.required'    => ERROR_VALIDATE['require'] . ' ngày sinh.',
            'birthday.date_format' => ERROR_VALIDATE['date_format'],
            'birthday.before'      => "Ngày sinh không đủ 18 tuổi",
            'address.required'        => ERROR_VALIDATE['require'].' địa chỉ.',
            'address.max'             => ERROR_VALIDATE['max'] . ' địa chỉ.',
            'phone.required'        => ERROR_VALIDATE['require'].' số điện thoại.',
            'phone.numeric'        => ERROR_VALIDATE['numeric'].' số điện thoại.',
            'phone.digits_between'  => "Độ dài số điện thoại phải từ 6 - 10 số",
            'shop.required'       => ERROR_VALIDATE['require'].' địa chỉ cửa hàng.',
            'level.required'       => ERROR_VALIDATE['require'].' cấp bậc.',
            'start_date.required'  => ERROR_VALIDATE['require'].' ngày vào làm.',
            'start_date.date_format' => ERROR_VALIDATE['date_format'],
            'start_date.after' => "Ngày vào làm phải sau ngày sinh",
            'start_date.before' => "Ngày vào làm phải từ hôm nay trở về trước",
            'password.required' => ERROR_VALIDATE['require'].' mật khẩu.',
            'password.min' => "Mật khẩu không được ít hơn 6 kí tự",
        ];
    }
}
