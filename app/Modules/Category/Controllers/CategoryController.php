<?php

namespace App\Modules\Category\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function __construct()
    {
        #parent::__contruct();
    }

    public function  index( Request $request)
    {
        return view('Category::index');
    }
}
