<?php

$namespace = "App\Modules\Category\Controllers";

Route::group(
    ['module' => 'Category', 'namespace' => $namespace],
    function () {
        Route::get('category', [
            'as' => 'index',
            'uses' => 'CategoryController@index'
        ]);
    }
);
