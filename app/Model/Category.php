<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    protected $table = "category";

    protected $fillable = ['Category_ID','Category_Name', 'Category_Parent', 'Category_Shop', 'Category_Order', 'Category_Status','Category_Datetime'];

	public $timestamps = false;

	protected $primaryKey = 'Category_ID';

	public function Product(){
		return $this->hasMany('App\Model\Product', 'product_Category');
	}
}
