<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Log extends Model
{
    protected $table = "log_name";

    protected $fillable = ['Log_Name_ID','Log_Name_Log', 'Log_Name_DateTime', 'Log_Name_IP', 'Log_Name_System', 'Log_Name_User'];

    public $timestamps = false;

    protected $primaryKey = 'Log_Name_ID';

    public function User(){
        return $this->belongsTo('App\Model\User', 'Log_Name_User');
    }
}
