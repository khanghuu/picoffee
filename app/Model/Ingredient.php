<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $table = 'ingredient';
    protected $primaryKey = 'Ingredient_ID';
    protected $fillable = [
        'Ingredient_Code',
        'Ingredient_Name',
        'Ingredient_Number',
        'Ingredient_Unit',
        'Ingredient_Params',
        'Raw_Material_Update'
    ];

    const CREATED_AT = 'Created_At';
    const UPDATED_AT = 'Updated_At';

}

