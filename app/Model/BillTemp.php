<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class BillTemp extends Model
{
    protected $table = "bill_temp";

    protected $fillable = ['Bill_ID','Bill_CustomerName', 'Bill_CustomerEmail', 'Bill_CustomerPhone', 'Bill_TotalPrice','Bill_DiscountAmount', 'Bill_ExtantPrice', 'Bill_Time', 'Bill_User', 'Bill_Status', 'Bill_Products'];

    public $timestamps = false;

    protected $primaryKey = 'Bill_ID';

    public function BillProduct(){
        return $this->hasMany('App\Model\BillProductTemp', 'Bill_Product_BillID');
    }
    public function User(){
        return $this->belongsTo('App\Model\User', 'Bill_User');
    }
}
