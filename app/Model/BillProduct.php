<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class BillProduct extends Model
{
    protected $table = "bill_product";

    protected $fillable = ['Bill_Product_ID','Bill_Product_ProductID', 'Bill_Product_Quantity', 'Bill_Product_Time', 'Bill_Product_Status','Bill_Product_BillID'];

	public $timestamps = false;

	protected $primaryKey = 'Bill_Product_ID';

	public function Bill(){
		return $this->belongsTo('App\Model\Bill', 'Bill_Product_BillID');
	}

	public function Product(){
		return $this->belongsTo('App\Model\Product', 'Bill_Product_ProductID');
	}
}
