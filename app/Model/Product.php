<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    protected $table = "product";

    protected $primaryKey = 'Product_ID';

    protected $fillable = ['Product_ID','Product_Name', 'Product_Code', 'Product_Category', 'Product_Price','Product_Img', 'Product_Status', 'Product_Param', 'Created_At', 'Update_At'];

    public const CREATED_AT =  'Created_At';

    public const UPDATED_AT = 'Updated_At';

}
