<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table = "countries";

    protected $fillable = ['Countries_ID','Countries_SortName', 'Countries_Name', 'Countries_PhoneCode'];

    public $timestamps = false;

    protected $primaryKey = 'Countries_ID';

}
