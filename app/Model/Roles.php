<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = "role_groups";

    protected $primaryKey = 'Role_ID';

    protected $fillable = ['Role_ID', 'Role_Name', 'Permission_List'];
}
