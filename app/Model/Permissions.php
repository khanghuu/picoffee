<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    protected $table = "permissions";
    protected $primaryKey = 'Permission_ID';
    protected $fillable = ['Permission_ID', 'Permission_Name'];
}
