<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RawMaterial extends Model
{
    protected $table = 'raw_material';
    protected $primaryKey = 'Raw_Material_ID';
    protected $fillable = [
        'Raw_Material_Code',
        'Raw_Material_Name',
        'Raw_Material_Unit',
        'Raw_Material_Status',
        'Raw_Material_Create',
        'Raw_Material_Update'
    ];

    const CREATED_AT = 'Raw_Material_Create';
    const UPDATED_AT = 'Raw_Material_Update';

}
