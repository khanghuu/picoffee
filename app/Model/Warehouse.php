<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Warehouse extends Model
{
    protected $table = "warehouse";

    protected $fillable = ['Warehouse_ID','Warehouse_User','Warehouse_Shop','Warehouse_Number', 'Created_At', 'Warehouse_Raw','Warehouse_Total', 'Warehouse_Ingredient', 'Warehouse_Price', 'Warehouse_Document', 'Warehouse_Expiry_Date','Warehouse_Unit', 'Warehouse_Quantity', 'Warehouse_Description', 'Warehouse_Time_Import', 'Warehouse_Status', 'Warehouse_Name'];

	public $timestamps = false;

	protected $primaryKey = 'Warehouse_ID';
	const UPDATED_AT = 'Updated_At';
	const CREATED_AT = 'Created_At';

}
