<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Export extends Model
{
    protected $table = 'export';
    protected $primaryKey = 'Export_ID';
    public $timestamps = false;
    protected $fillable = ['Export_ID', 'Export_User'];
}

