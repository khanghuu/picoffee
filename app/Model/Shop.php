<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $table = "shop";

    protected $fillable = ['Shop_ID','Shop_Name', 'Shop_Address'];

	public $timestamps = false;

	protected $primaryKey = 'Shop_ID';

}
