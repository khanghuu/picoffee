<?php

namespace App\Imports;

use App\Model\RawMaterial;
use App\Model\Warehouse;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class WarehouseRawImport implements ToModel, WithHeadingRow, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    use Importable;

    protected $req,$import_number,$arr=[];
    public function __construct($req)
    {
        $this->req = $req;
        $warehouse_number = Warehouse::count();
        $this->import_number = date('Ymd') . $warehouse_number;
        $this->req->warehouse_import_shop = $this->req->warehouse_import_shop ?? Session('user')->User_Location_Store;
    }
    public function model(array $row)
    {
        Session::forget('quantity_file');

        Session::put('check_file', true);
        if(!isset($row['id_nguyen_lieu'])){
            return;
        }
        if(!isset($row['don_gia'])){
            return;
        }
        if(!isset($row['so_luong'])){
            return;
        }
        if(!isset($row['mo_ta'])){
            return;
        }
        Session::put('check_file', false);


        Session::push('quantity_file', $this->arr);

        if(array_key_exists(intval($row['id_nguyen_lieu']), Session::get('quantity_file')[0])){
            $this->arr[$row['id_nguyen_lieu']] += $row['so_luong'];
        }
        else{
            $this->arr[$row['id_nguyen_lieu']] = $row['so_luong'];
        }

        Session::push('quantity_file', $this->arr);

        return new Warehouse([
            'Warehouse_User'        => Session('user')->User_ID,
            'Warehouse_Raw'         => $row['id_nguyen_lieu'],
            'Warehouse_Number'      => $this->import_number,
            'Warehouse_Price'       => $row['don_gia'],
            'Warehouse_Quantity'    => $row['so_luong'],
            'Warehouse_Total'       => $row['so_luong'] * $row['don_gia'],
            'Warehouse_Description' => $row['mo_ta'],
            'Created_At'            => date('Y-m-d H:i:s'),
            'Warehouse_Status'      => 1,
            'Warehouse_Shop'        => $this->req->warehouse_import_shop,
        ]);
    }
    public function rules(): array
    {
        return [
             'id_nguyen_lieu' => function($attribute, $value, $onFailure) {

                  if (!$value) {
                       $onFailure(': ID Nguyên Liệu Không Được Để Trống');
                  }
                  else{
                      $check_id = RawMaterial::where([
                          ['Raw_Material_Shop',$this->req->warehouse_import_shop],
                          ['Raw_Material_ID',$value]
                          ])->first();
                      if(!$check_id){
                        $onFailure(': ID Nguyên Liệu Không Tồn Tại');
                      }
                  }
              },
              'don_gia' =>  function($attribute, $value, $onFailure) {
                if (!$value) {
                     $onFailure(': Đơn Giá Không Được Để Trống');
                }
                else{
                    if(gettype($value)!="double"){
                        $onFailure(': Đơn Giá Không Hợp Lệ');
                    }
                }
            },
            'so_luong' =>  function($attribute, $value, $onFailure) {
                if (!$value) {
                     $onFailure(': Số Lượng Không Được Để Trống');
                }
                else{
                    if(gettype($value)!="double"){
                        $onFailure(': Số Lượng Không Hợp Lệ');
                    }
                }
            },
        ];
    }
}
