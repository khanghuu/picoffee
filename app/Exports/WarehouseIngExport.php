<?php

namespace App\Exports;

use App\Model\Warehouse;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class WarehouseIngExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function collection()
    {
        $user = session('user');

        $warehouse = Warehouse::query();
        if($user->User_Level == 2)
        {
            $warehouse->where('Warehouse_Shop',$user->User_Location_Store);
        }

        $warehouse->where('Warehouse_Ingredient','<>',NULL)
        ->join('ingredient','warehouse.Warehouse_Ingredient','ingredient.Ingredient_ID')
        ->where('Ingredient_Status',1)
        ->join('shop','ingredient.Ingredient_Shop','shop.Shop_ID')
        ->where('Shop_Status',1)
        ->groupBy('Warehouse_Ingredient')
        ->selectRaw('Ingredient_ID, Ingredient_Code, Ingredient_Name, Ingredient_Quantity, Ingredient_Unit, Shop_Name, Ingredient_Status, SUM(Warehouse_Quantity) as Warehouse_Total_Import');

        $result = [];
        if($user->User_Level == 1 || $user->User_Level == 3){
            foreach ($warehouse->get() as $row) {
                $result[] = array(
                    '0' => $row->Ingredient_ID,
                    '1' => $row->Ingredient_Code,
                    '2' => $row->Ingredient_Name,
                    '3' => $row->Ingredient_Quantity,
                    '4' => $row->Warehouse_Total_Import,
                    '5' => $row->Ingredient_Unit,
                    '6' => $row->Shop_Name
                );
            }
        }
        else{
            foreach ($warehouse->get() as $row) {
                $result[] = array(
                    '0' => $row->Ingredient_ID,
                    '1' => $row->Ingredient_Code,
                    '2' => $row->Ingredient_Name,
                    '3' => $row->Ingredient_Quantity,
                    '4' => $row->Warehouse_Total_Import,
                    '5' => $row->Ingredient_Unit,
                );
            }
        }
        return (collect($result));
    }
    public function headings(): array
    {
        if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3){
            return [
                'ID Nguyên Liệu Tinh',
                'Mã Nguyên Liệu Tinh',
                'Tên Nguyên Liệu Tinh',
                'Số lượng tồn',
                'Số lượng nhập',
                'Đơn vị',
                'Tên Cửa Hàng'
            ];
        }
        else{
            return [
                'ID Nguyên Liệu Tinh',
                'Mã Nguyên Liệu Tinh',
                'Tên Nguyên Liệu Tinh',
                'Số lượng tồn',
                'Số lượng nhập',
                'Đơn vị'
            ];
        }
    }
}
