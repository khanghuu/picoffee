<?php

namespace App\Exports;

use App\Model\Ingredient;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class IngredientExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function collection()
    {
        $user = session('user');
        $ingredientList = Ingredient::query();
        if ($user->User_Level == 2) {
            $ingredientList->where("Ingredient_Shop", $user->User_Location_Store);
        }
        $ingredientList =$ingredientList->join('shop', 'Ingredient_Shop', 'Shop_ID')->orderBy('ingredient.Created_At','desc')->get();

        $result = [];
        if($user->User_Level == 1 || $user->User_Level == 3){
            foreach ($ingredientList as $row) {
                if($row->Ingredient_Status == 1){
                    $string_status = "Mở";
                }
                else{
                    $string_status = "Đóng";
                }
                $result[] = array(
                    '0' => $row->Ingredient_Code,
                    '1' => $row->Ingredient_Name,
                    '2' => $row->Ingredient_Unit,
                    '3' => $row->Created_At,
                    '4' => $row->Shop_Name,
                    '5' => $string_status
                );
            }
        }
        else{
            foreach ($ingredientList as $row) {
                if($row->Ingredient_Status == 1){
                    $string_status = "Mở";
                }
                else{
                    $string_status = "Đóng";
                }
                $result[] = array(
                    '0' => $row->Ingredient_Code,
                    '1' => $row->Ingredient_Name,
                    '2' => $row->Ingredient_Unit,
                    '3' => $row->Created_At,
                    '4' => $string_status
                );
            }
        }
        return (collect($result));
    }
    public function headings(): array
    {
        if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3){
            return [
                'Mã Nguyên Liệu Tinh',
                'Tên Nguyên Liệu Tinh',
                'Đơn Vị',
                'Ngày Tạo',
                'Cửa Hàng',
                'Trạng Thái',
            ];
        }
        else{
            return [
                'Mã Nguyên Liệu Tinh',
                'Tên Nguyên Liệu Tinh',
                'Đơn Vị',
                'Ngày Tạo',
                'Trạng Thái',
            ];
        }
    }
}
