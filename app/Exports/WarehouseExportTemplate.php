<?php

namespace App\Exports;

use App\Warehouse;
use Maatwebsite\Excel\Concerns\FromCollection;
use Excel;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class WarehouseExportTemplate implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function collection()
    {
        $warehouse = [];
        return (collect($warehouse));
    }
    public function headings(): array
    {
        return [
            'ID Nguyên Liệu (*)',
            'Số lượng (*)',
            'Đơn giá (*)',
            'Mô tả',
            // 'Trạng thái (1: Nhập tinh | 2: Chế tinh)', Mặc định là nhập
        ];
    }
}
