<?php

namespace App\Exports;

use App\Model\Warehouse;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class DestroyRawExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function collection()
    {
        $user = session('user');
        $destroyRawList = Warehouse::query()->join('raw_material', 'Warehouse_Raw', 'Raw_Material_ID')
            ->join('users', 'Warehouse_User', 'User_ID')
            ->join('shop', 'Warehouse_Shop', 'Shop_ID');
        if($user->User_Level == 2){
            $destroyRawList = $destroyRawList->where('Warehouse_Shop',$user->User_Location_Store);
        }
        $destroyRawList = $destroyRawList->where("Warehouse_isDestroy", 1)
        ->whereNotNull('Warehouse_Raw')
        ->select('User_Name', 'Shop_Name', 'Raw_Material_Name', 'Raw_Material_Unit', 'Warehouse_ID', 'Warehouse_Quantity', 'Warehouse_Description', 'warehouse.Updated_At', 'Warehouse_Status')
        ->get();

        $result = [];
        if($user->User_Level == 1 || $user->User_Level == 3){
            foreach ($destroyRawList as $row) {
                if($row->Warehouse_Status == 1){
                    $string_status = "Kích Hoạt";
                }
                else{
                    $string_status = "Hủy";
                }
                $result[] = array(
                    '0' => $row->Warehouse_ID,
                    '1' => $row->User_Name,
                    '2' => $row->Raw_Material_Name,
                    '3' => abs($row->Warehouse_Quantity),
                    '4' => $row->Raw_Material_Unit,
                    '5' => $row->Warehouse_Description,
                    '6' => $string_status,
                    '7' => $row->Updated_At,
                    '8' => $row->Shop_Name,
                );
            }
        }
        else{
            foreach ($destroyRawList as $row) {
                if($row->Warehouse_Status == 1){
                    $string_status = "Kích Hoạt";
                }
                else{
                    $string_status = "Hủy";
                }
                $result[] = array(
                    '0' => $row->Warehouse_ID,
                    '1' => $row->User_Name,
                    '2' => $row->Raw_Material_Name,
                    '3' => abs($row->Warehouse_Quantity),
                    '4' => $row->Raw_Material_Unit,
                    '5' => $row->Warehouse_Description,
                    '6' => $string_status,
                    '7' => $row->Updated_At,
                );
            }
        }
        return (collect($result));
    }
    public function headings(): array
    {
        if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3){
            return [
                'ID',
                'Người Hủy',
                'Tên Nguyên Liệu',
                'Số Lượng',
                'Đơn Vị',
                'Ghi Chú',
                'Trạng Thái',
                'Ngày Giờ',
                'Cửa Hàng',
            ];
        }
        else{
            return [
                'ID',
                'Người Hủy',
                'Tên Nguyên Liệu',
                'Số Lượng',
                'Đơn Vị',
                'Ghi Chú',
                'Trạng Thái',
                'Ngày Giờ',
            ];
        }
    }
}
