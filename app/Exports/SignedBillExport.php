<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use App\Model\Bill;

class SignedBillExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function collection()
    {
        $user = session('user');
        $signed = Bill::query()->join('shop', 'Bill_Shop_ID', 'Shop_ID');
        $user->User_Level == 2 ? $signed->where('Bill_Shop_ID', $user->User_Location_Store) : 0;
        $signed = $signed->select('Shop_Name', 'Bill_Time', 'Bill_CustomerName', 'Bill_TotalPrice')
        ->where('Bill_Status', 1)->where('Bill_IsSigned', 1)->orderByDesc('Bill_ID')->get();

        if($user->User_Level == 1 || $user->User_Level == 3){
            foreach ($signed as $row) {
                $result[] = array(
                    '0' => $row->Shop_Name,
                    '1' => $row->Bill_CustomerName,
                    '2' => number_format($row->Bill_TotalPrice),
                    '3' => $row->Bill_Time,
                );
            }
        }
        else{
            foreach ($signed as $row) {
                $result[] = array(
                    '0' => $row->Bill_CustomerName,
                    '1' => number_format($row->Bill_TotalPrice),
                    '2' => $row->Bill_Time,
                );
            }
        }
        return (collect($result));
    }
    public function headings(): array
    {
        if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3){
            return [
                'Cửa Hàng',
                'Khách Hàng',
                'Tổng',
                'Ngày Giờ',
            ];
        }
        else{
            return [
                'Khách Hàng',
                'Tổng',
                'Ngày Giờ',
            ];
        }
    }
}
