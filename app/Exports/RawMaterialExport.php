<?php

namespace App\Exports;

use App\Model\RawMaterial;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RawMaterialExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function collection()
    {
        $user = session('user');
        $rawList = RawMaterial::query();
        if ($user->User_Level == 2) {
            $rawList->where("Raw_Material_Shop", $user->User_Location_Store);
        }
        $rawList = $rawList->join('shop', 'Raw_Material_Shop', 'Shop_ID')->orderBy('Raw_Material_Create','desc')->get();

        $result = [];
        if($user->User_Level == 1 || $user->User_Level == 3){
            foreach ($rawList as $row) {
                if($row->Raw_Material_Status == 1){
                    $string_status = "Mở";
                }
                else{
                    $string_status = "Đóng";
                }
                $result[] = array(
                    '0' => $row->Raw_Material_Code,
                    '1' => $row->Raw_Material_Name,
                    '2' => $row->Raw_Material_Unit,
                    '3' => $row->Raw_Material_Create,
                    '4' => $row->Shop_Name,
                    '5' => $string_status
                );
            }
        }
        else{
            foreach ($rawList as $row) {
                if($row->Raw_Material_Status == 1){
                    $string_status = "Mở";
                }
                else{
                    $string_status = "Đóng";
                }
                $result[] = array(
                    '0' => $row->Raw_Material_Code,
                    '1' => $row->Raw_Material_Name,
                    '2' => $row->Raw_Material_Unit,
                    '3' => $row->Raw_Material_Create,
                    '4' => $string_status
                );
            }
        }
        return (collect($result));
    }
    public function headings(): array
    {
        if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3){
            return [
                'Mã Nguyên Liệu Thô',
                'Tên Nguyên Liệu Thô',
                'Đơn Vị',
                'Ngày Tạo',
                'Cửa Hàng',
                'Trạng Thái',
            ];
        }
        else{
            return [
                'Mã Nguyên Liệu Thô',
                'Tên Nguyên Liệu Thô',
                'Đơn Vị',
                'Ngày Tạo',
                'Trạng Thái',
            ];
        }
    }
}
