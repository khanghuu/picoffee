<?php

namespace App\Exports;

use App\Model\Warehouse;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
class WarehouseExport implements FromCollection, WithHeadings
{
    use Exportable;
    public function collection()
    {
        $warehouse = Warehouse::all();
        foreach ($warehouse as $row) {
            $warehouse[] = array(
                '0' => $row->Warehouse_ID,
                '1' => $row->Warehouse_User,
                '2' => $row->Warehouse_Raw,
                '3' => $row->Warehouse_Ingredient,
                '4' => $row->Warehouse_Number,
                '5' => $row->Warehouse_Expiry_Date,
                '6' => $row->Warehouse_Price,
                '7' => $row->Warehouse_Quantity,
                '8' => $row->Warehouse_Total,
                '9' => $row->Warehouse_Description,
                '10' => $row->Warehouse_IsDestroy,
                '11' => $row->Created_At,
                '12' => $row->Warehouse_Status,
                '13' => $row->Warehouse_Shop,
                '14' => $row->Updated_At,
            );
        }
        return (collect($warehouse));
    }
    public function headings(): array
    {
        return [
            'Warehouse_ID',
            'Warehouse_User',
            'Warehouse_Raw',
            'Warehouse_Ingredient',
            'Warehouse_Number',
            'Warehouse_Expiry_Date',
            'Warehouse_Price',
            'Warehouse_Quantity',
            'Warehouse_Total',
            'Warehouse_Description',
            'Warehouse_IsDestroy',
            'Created_At',
            'Warehouse_Status',
            'Warehouse_Shop',
            'Updated_At',
        ];
    }
}
