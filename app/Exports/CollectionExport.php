<?php

namespace App\Exports;




use Maatwebsite\Excel\Concerns\FromArray;


class CollectionExport implements FromArray
{

    protected $sale_data;

    public function __construct(array $sale_data)
    {
        $this->sale_data = $sale_data;
    }

    public function array(): array
    {
        return $this->sale_data;
    }


}
