<?php

namespace App\Exports;

use App\Model\Warehouse;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class DestroyIngredientExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function collection()
    {
        $user = session('user');
        $destroyIngredientList = Warehouse::query()->where('Warehouse_isDestroy', 1)->whereNotNull('Warehouse_Ingredient')
        ->join('users', 'Warehouse_User', 'User_ID')
        ->join('shop','Warehouse_Shop', 'Shop_ID')
        ->join('ingredient', 'Warehouse_Ingredient', 'Ingredient_ID');
        if($user->User_Level == 2) {
            $destroyIngredientList->where('Warehouse_Shop', $user->User_Location_Store);
        }
        $destroyIngredientList = $destroyIngredientList->select([
            'Warehouse_ID',
            'Warehouse_User',
            'Warehouse_Quantity',
            'Warehouse_Status',
            'Warehouse_Description',
            'warehouse.Updated_At',
            'Ingredient_Name',
            'Ingredient_Unit',
            'User_Name',
            'Shop_Name',

        ])->get();

        $result = [];
        if($user->User_Level == 1 || $user->User_Level == 3){
            foreach ($destroyIngredientList as $row) {
                if($row->Warehouse_Status == 1){
                    $string_status = "Kích Hoạt";
                }
                else{
                    $string_status = "Hủy";
                }
                $result[] = array(
                    '0' => $row->Warehouse_ID,
                    '1' => $row->User_Name,
                    '2' => $row->Ingredient_Name,
                    '3' => abs($row->Warehouse_Quantity),
                    '4' => $row->Ingredient_Unit,
                    '5' => $row->Warehouse_Description,
                    '6' => $string_status,
                    '7' => $row->Updated_At,
                    '8' => $row->Shop_Name,
                );
            }
        }
        else{
            foreach ($destroyIngredientList as $row) {
                if($row->Warehouse_Status == 1){
                    $string_status = "Kích Hoạt";
                }
                else{
                    $string_status = "Hủy";
                }
                $result[] = array(
                    '0' => $row->Warehouse_ID,
                    '1' => $row->User_Name,
                    '2' => $row->Ingredient_Name,
                    '3' => abs($row->Warehouse_Quantity),
                    '4' => $row->Ingredient_Unit,
                    '5' => $row->Warehouse_Description,
                    '6' => $string_status,
                    '7' => $row->Updated_At,
                );
            }
        }
        return (collect($result));
    }
    public function headings(): array
    {
        if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3){
            return [
                'ID',
                'Người Hủy',
                'Tên Nguyên Liệu',
                'Số Lượng',
                'Đơn Vị',
                'Ghi Chú',
                'Trạng Thái',
                'Ngày Giờ',
                'Cửa Hàng',
            ];
        }
        else{
            return [
                'ID',
                'Người Hủy',
                'Tên Nguyên Liệu',
                'Số Lượng',
                'Đơn Vị',
                'Ghi Chú',
                'Trạng Thái',
                'Ngày Giờ',
            ];
        }
    }
}
