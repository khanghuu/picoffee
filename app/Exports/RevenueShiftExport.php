<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use App\Model\Shop;
use DB;

class RevenueShiftExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function collection()
    {
        $user = session('user');
        $shopID = '';
        $user->User_Level == 2?$shopID = $user->User_Location_Store:$shopID = Shop::first()->Shop_ID;
        $shiftRevenue = DB::select(
            "
            select DATE_FORMAT(Bill_Time, '%d/%m/%Y') as 'DateTime',
                            '1'  AS 'CA',
                            SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent` / 100 * `Bill_TotalPrice`),
                                   `Bill_DiscountAmount`))                                   AS TotalDiscount,
                            SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0))             AS TotalSigned,
                            SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice` * 0.1 / 1.1), 0)) AS TotalVAT,
                            SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                            SUM(`Bill_ExtantPrice`)                                          AS TotaExtantlSales
                     from `bill`
                     where   TIME_FORMAT(Bill_Time, '%H:%i:%s') between '06:00:00' and '13:59:59'
                     group by 'DateTime'

            UNION
                select DATE_FORMAT(Bill_Time, '%d/%m/%Y') as 'DateTime',
                        '2'  AS 'CA',
                       SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent` / 100 * `Bill_TotalPrice`),
                              `Bill_DiscountAmount`))                                   AS TotalDiscount,
                       SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0))             AS TotalSigned,
                       SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice` * 0.1 / 1.1), 0)) AS TotalVAT,
                       SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                       SUM(`Bill_ExtantPrice`)                                          AS TotaExtantlSales
                from `bill`
                where Bill_Shop_ID = $shopID AND TIME_FORMAT(Bill_Time, '%H:%i:%s') >= '14:00:00'
                group by 'DateTime'
            "
        );
        sort($shiftRevenue);

        $result = [];
        foreach ($shiftRevenue as $row) {
            $result[] = array(
                '0' => $row->DateTime,
                '1' => $row->CA,
                '2' => number_format($row->TotalDiscount),
                '3' => number_format($row->TotalVAT),
                '4' => number_format($row->TotalSigned),
                '5' => number_format($row->TotalSales),
                '6' => number_format($row->TotaExtantlSales),
            );
        }
        return (collect($result));
    }
    public function headings(): array
    {
        return [
            'Ngày',
            'Ca',
            'Tổng Giảm Giá',
            'Tổng VAT',
            'Tổng Ký Bill',
            'Tổng Tiền',
            'Tổng Thanh Toán',
        ];
    }
}
