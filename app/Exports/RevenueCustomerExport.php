<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use App\Model\Shop;
use App\Model\Bill;

class RevenueCustomerExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function collection()
    {
        $customerRevenue = Bill::selectRaw('Bill_CustomerName as name,
        Bill_CustomerEmail as email,
        SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
        SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
        SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
        SUM(`Bill_TotalPrice`) AS TotalSales,
        SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
        ')
        ->groupBy('Bill_CustomerName', 'Bill_CustomerEmail')
        ->get()->toArray();
        $result = [];
        foreach ($customerRevenue as $row) {
            $result[] = array(
                '0' => $row['name'],
                '1' => $row['email'],
                '2' => number_format($row['TotalDiscount']),
                '3' => number_format($row['TotalVAT']),
                '4' => number_format($row['TotalSigned']),
                '5' => number_format($row['TotaExtantlSales']),
                '6' => number_format($row['TotaExtantlSales']),
            );
        }
        return (collect($result));
    }
    public function headings(): array
    {
        return [
            'Họ Tên',
            'Email',
            'Tổng Giảm Giá',
            'Tổng VAT',
            'Tổng Ký Bill',
            'Tổng Tiền',
            'Tổng Thanh Toán',
        ];
    }
}
