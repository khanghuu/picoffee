<?php

namespace App\Exports;

use App\Model\Warehouse;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class WarehouseRawExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function collection()
    {
        $user = session('user');

        $warehouse = Warehouse::query();
        if($user->User_Level == 2)
        {
            $warehouse->where('Warehouse_Shop',$user->User_Location_Store);
        }

        $warehouse->where('Warehouse_Raw','<>',NULL)
        ->join('raw_material','warehouse.Warehouse_Raw','raw_material.Raw_Material_ID')
        ->where('Raw_Material_Status',1)
        ->join('shop','raw_material.Raw_Material_Shop','shop.Shop_ID')
        ->where('Shop_Status',1)
        ->groupBy('Warehouse_Raw')
        ->selectRaw('Raw_Material_ID, Raw_Material_Code, Raw_Material_Name, Raw_Material_Quantity, Raw_Material_Unit, Shop_Name, Raw_Material_Status, SUM(Warehouse_Quantity) as Warehouse_Total_Import');

        $result = [];
        if($user->User_Level == 1 || $user->User_Level == 3){
            foreach ($warehouse->get() as $row) {
                $result[] = array(
                    '0' => $row->Raw_Material_ID,
                    '1' => $row->Raw_Material_Code,
                    '2' => $row->Raw_Material_Name,
                    '3' => $row->Raw_Material_Quantity,
                    '4' => $row->Warehouse_Total_Import,
                    '5' => $row->Raw_Material_Unit,
                    '6' => $row->Shop_Name
                );
            }
        }
        else{
            foreach ($warehouse->get() as $row) {
                $result[] = array(
                    '0' => $row->Raw_Material_ID,
                    '1' => $row->Raw_Material_Code,
                    '2' => $row->Raw_Material_Name,
                    '3' => $row->Raw_Material_Quantity,
                    '4' => $row->Warehouse_Total_Import,
                    '5' => $row->Raw_Material_Unit,
                );
            }
        }
        return (collect($result));
    }
    public function headings(): array
    {
        if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3){
            return [
                'ID Nguyên Liệu Thô',
                'Mã Nguyên Liệu Thô',
                'Tên Nguyên Liệu Thô',
                'Số lượng tồn',
                'Số lượng nhập',
                'Đơn vị',
                'Tên Cửa Hàng'
            ];
        }
        else{
            return [
                'ID Nguyên Liệu Thô',
                'Mã Nguyên Liệu Thô',
                'Tên Nguyên Liệu Thô',
                'Số lượng tồn',
                'Số lượng nhập',
                'Đơn vị'
            ];
        }
    }
}
