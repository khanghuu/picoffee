<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use App\Model\Bill;

class BillExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function collection()
    {
        $user = session('user');
        if ($user->User_Level == 1 || $user->User_Level == 3) {
            $allOrder = Bill::orderByDesc('Bill_ID')
                ->join('shop', 'Bill_Shop_ID', 'Shop_ID')
                ->join('users','Bill_User','User_ID')
                ->where("Bill_Status", 1)->orwhere('Bill_Status', 0)->paginate(25);
        }
        if ($user->User_Level == 2) {
            $allOrder = Bill::orderByDesc('Bill_ID')
                ->join('shop', 'Bill_Shop_ID', 'Shop_ID')
                ->join('users','Bill_User','User_ID')
                ->where('Bill_Shop_ID', $user->User_Location_Store)
                ->where("Bill_Status", 1)
                ->orwhere('Bill_Status', 0)->paginate(25);
        }

        $result = [];
        if($user->User_Level == 1 || $user->User_Level == 3){
            foreach ($allOrder as $row) {
                if($row->Bill_Status == 1){
                    $string_status = "Đã Thanh Toán";
                }
                else{
                    $string_status = "Chưa Thanh Toán";
                }
                $result[] = array(
                    '0' => $row->Bill_ID,
                    '1' => $row->Bill_CustomerName,
                    '2' => $row->Bill_DiscountPercent,
                    '3' => $row->Bill_IsTaxVAT * 10,
                    '4' => $row->Bill_DiscountAmount,
                    '5' => $row->Bill_ExtantPrice,
                    '6' => $row->Bill_Time,
                    '7' => $row->User_Name,
                    '8' => $string_status,
                    '9' => $row->Shop_Name,
                );
            }
        }
        else{
            foreach ($allOrder as $row) {
                if($row->Bill_Status == 1){
                    $string_status = "Đã Thanh Toán";
                }
                else{
                    $string_status = "Chưa Thanh Toán";
                }
                $result[] = array(
                    '0' => $row->Bill_ID,
                    '1' => $row->Bill_CustomerName,
                    '2' => $row->Bill_DiscountPercent,
                    '3' => $row->Bill_IsTaxVAT * 10,
                    '4' => $row->Bill_DiscountAmount,
                    '5' => $row->Bill_ExtantPrice,
                    '6' => $row->Bill_Time,
                    '7' => $row->User_Name,
                    '8' => $string_status,
                );
            }
        }
        return (collect($result));
    }
    public function headings(): array
    {
        if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3){
            return [
                'ID',
                'Tên',
                'Giảm Giá %',
                'VAT %',
                'Giảm Giá Tiền',
                'Tổng',
                'Ngày Giờ',
                'Người Tạo',
                'Trạng Thái',
                'Cửa Hàng',
            ];
        }
        else{
            return [
                'ID',
                'Tên',
                'Giảm Giá %',
                'VAT %',
                'Giảm Giá Tiền',
                'Tổng',
                'Ngày Giờ',
                'Người Tạo',
                'Trạng Thái',
            ];
        }
    }
}
