<?php

namespace App\Exports;

use App\Model\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function collection()
    {
        $user = session('user');
        $productList = Product::query();
        if ($user->User_Level == 2) {
            $productList = $productList->where('Product_Shop', $user->User_Location_Store);
        }
        $productList = $productList->join('category', 'Product_Category', 'Category_ID')
        ->join('shop', 'Product_Shop', 'Shop_ID')->get();

        $result = [];
        if($user->User_Level == 1 || $user->User_Level == 3){
            foreach ($productList as $row) {
                if($row->Product_Status == 1){
                    $string_status = "Đang bán";
                }
                else{
                    $string_status = "Đã đóng";
                }
                $result[] = array(
                    '0' => $row->Product_Code,
                    '1' => $row->Product_Name,
                    '2' => $row->Product_Price,
                    '3' => $row->Shop_Name,
                    '4' => $row->Category_Name,
                    '5' => $string_status
                );
            }
        }
        else{
            foreach ($productList as $row) {
                if($row->Product_Status == 1){
                    $string_status = "Đang bán";
                }
                else{
                    $string_status = "Đã đóng";
                }
                $result[] = array(
                    '0' => $row->Product_Code,
                    '1' => $row->Product_Name,
                    '2' => $row->Product_Price,
                    '3' => $row->Category_Name,
                    '4' => $string_status
                );
            }
        }
        return (collect($result));
    }
    public function headings(): array
    {
        if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3){
            return [
                'Mã Sản Phẩm',
                'Tên Sản Phẩm',
                'Giá Sản Phẩm',
                'Cửa Hàng',
                'Danh Mục',
                'Trạng Thái',
            ];
        }
        else{
            return [
                'Mã Sản Phẩm',
                'Tên Sản Phẩm',
                'Giá Sản Phẩm',
                'Danh Mục',
                'Trạng Thái',
            ];
        }
    }
}
