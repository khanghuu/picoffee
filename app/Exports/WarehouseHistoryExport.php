<?php

namespace App\Exports;

use App\Model\Warehouse;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;


class WarehouseHistoryExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;


    public function array_sort_by_column(&$array, $column, $direction = SORT_DESC) {
        $reference_array = array();

        foreach($array as $key => $row) {
            $reference_array[$key] = $row[$column];
        }

        array_multisort($reference_array, $direction, $array);
    }

    public function collection()
    {
        $user = session('user');
        $data['warehouse_history_raw'] = Warehouse::query()
        ->join('users','users.User_ID','warehouse.Warehouse_User')->where('User_Status',1)
        ->join('raw_material','raw_material.Raw_Material_ID','warehouse.Warehouse_Raw')
        ->where('Raw_Material_Status',1);


        $data['warehouse_history_ing'] = Warehouse::query()
        ->join('users','users.User_ID','warehouse.Warehouse_User')->where('User_Status',1)
        ->join('ingredient','ingredient.Ingredient_ID','warehouse.Warehouse_Ingredient')
        ->where('Ingredient_Status',1);
        if($user->User_Level == 2)
        {
            $data['warehouse_history_raw']->where('Warehouse_Shop',$user->User_Location_Store);
            $data['warehouse_history_ing']->where('Warehouse_Shop',$user->User_Location_Store);
        }

        $data['warehouse_history_raw'] = $data['warehouse_history_raw']
        ->join('shop','shop.Shop_ID','warehouse.Warehouse_Shop')
        ->where('Shop_Status',1)
        ->select('raw_material.Raw_Material_Name as Data_Name','users.User_Name' ,'warehouse.*','shop.Shop_Name')
        ->orderBy('Created_At','desc')->get()->toArray();

        // dd($data['warehouse_history_raw']);

        $data['warehouse_history_ing'] = $data['warehouse_history_ing']
        ->join('shop','shop.Shop_ID','warehouse.Warehouse_Shop')
        ->where('Shop_Status',1)
        ->select('ingredient.Ingredient_Name as Data_Name','users.User_Name' ,'warehouse.*','shop.Shop_Name')
        ->orderBy('Created_At','desc')->get()->toArray();

        // dd($data['warehouse_history_ing']);

        $data['merge'] = array_merge($data['warehouse_history_raw'],$data['warehouse_history_ing']);

        // dd($data['merge']);

        $this->array_sort_by_column($data['merge'], 'Created_At');

        // dd($data['merge']);

        $result = [];
        if($user->User_Level == 1 || $user->User_Level == 3){
            foreach ($data['merge'] as $row) {
                $check = false;
                $string = "";
                if ($row['Warehouse_Status']==0){
                    $check = true;
                }
                if($check){
                    $string = "Điều chỉnh chênh lệch kho";
                }
                else{
                    if($row['Warehouse_IsDestroy']){
                        if($row['Ingredient_Name'] ?? 0){
                            $string = "Hủy nguyên liệu tinh";
                        }
                        else{
                            $string = "Hủy nguyên liệu thô";
                        }
                    }
                    elseif($row['Ingredient_Name'] ?? 0){
                        if ($row['Warehouse_Status']==2){
                            $string = "Chế biến nguyên liệu tinh";
                        }
                        elseif ($row['Warehouse_Status']==1){
                            $string = "Nhập nguyên liệu tinh";
                        }
                        else{
                            $string = "";
                        }
                    }
                    else{
                        $string = "";
                    }
                }
                $result[] = array(
                    '0' => $row['Warehouse_Number'],
                    '1' => $row['User_Name'],
                    '2' => $row['Data_Name'],
                    '3' => number_format($row['Warehouse_Quantity'],3,",","."),
                    '4' => number_format($row['Warehouse_Price'],3,",",".") != 0 ? number_format($row['Warehouse_Price'],3,",",".") : "",
                    '5' => number_format($row['Warehouse_Total'],3,",",".") != 0 ? number_format($row['Warehouse_Total'],3,",",".") : "",
                    '6' => $row['Warehouse_Description'],
                    '7' => $string,
                    '8' => $row['Shop_Name'],
                    '9' => $row['Created_At'],
                );
            }
        }
        else{
            foreach ($data['merge'] as $row) {
                $check = false;
                $string = "";
                if ($row['Warehouse_Status']==0){
                    $check = true;
                }
                if($check){
                    $string = "Điều chỉnh chênh lệch kho";
                }
                else{
                    if($row['Warehouse_IsDestroy']){
                        if($row['Ingredient_Name'] ?? 0){
                            $string = "Hủy nguyên liệu tinh";
                        }
                        else{
                            $string = "Hủy nguyên liệu thô";
                        }
                    }
                    elseif($row['Ingredient_Name'] ?? 0){
                        if ($row['Warehouse_Status']==2){
                            $string = "Chế biến nguyên liệu tinh";
                        }
                        elseif ($row['Warehouse_Status']==1){
                            $string = "Nhập nguyên liệu tinh";
                        }
                        else{
                            $string = "";
                        }
                    }
                    else{
                        $string = "";
                    }
                }
                $result[] = array(
                    '0' => $row['Warehouse_Number'],
                    '1' => $row['User_Name'],
                    '2' => $row['Data_Name'],
                    '3' => number_format($row['Warehouse_Quantity'],3,",","."),
                    '4' => number_format($row['Warehouse_Price'],3,",",".") != 0 ? number_format($row['Warehouse_Price'],3,",",".") : "",
                    '5' => number_format($row['Warehouse_Total'],3,",",".") != 0 ? number_format($row['Warehouse_Total'],3,",",".") : "",
                    '6' => $row['Warehouse_Description'],
                    '7' => $string,
                    '8' => $row['Created_At'],
                );
            }
        }
        return (collect($result));
    }
    public function headings(): array
    {
        if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3){
            return [
                'Số Chứng Từ',
                'Người Nhập',
                'Nguyên Liệu',
                'Số lượng',
                'Đơn Giá',
                'Thành Tiền',
                'Ghi Chú',
                'Mô Tả',
                'Cửa Hàng',
                'Ngày Nhập',
            ];
        }
        else{
            return [
                'Số Chứng Từ',
                'Người Nhập',
                'Nguyên Liệu',
                'Số lượng',
                'Đơn Giá',
                'Thành Tiền',
                'Ghi Chú',
                'Mô Tả',
                'Ngày Nhập',
            ];
        }
    }
}
