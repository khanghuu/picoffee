<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use App\Model\Shop;
use App\Model\Bill;

class RevenueProductExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function collection()
    {
        $productRevenue = Bill::where('Bill_Status', 1)
        ->join('bill_product', 'bill.Bill_ID', '=', 'bill_product.bill_product_BillID')
        ->join('product', 'bill_product.bill_product_ProductID', '=', 'product.product_ID')
        ->selectRaw('product.product_Name as name,
                SUM(`bill_product_Quantity`) AS Quantity  ,
                SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
                SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
                SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
                SUM(`Bill_TotalPrice`) AS TotalSales,
                SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
        ')
        ->groupBy('product.product_Name')
        ->orderByDesc('Quantity')
        ->get()->toArray();
        $result = [];
        foreach ($productRevenue as $row) {
            $result[] = array(
                '0' => $row['name'],
                '1' => $row['Quantity'],
                '2' => number_format($row['TotalDiscount']),
                '3' => number_format($row['TotalVAT']),
                '4' => number_format($row['TotalSigned']),
                '5' => number_format($row['TotaExtantlSales']),
                '6' => number_format($row['TotaExtantlSales']),
            );
        }
        return (collect($result));
    }
    public function headings(): array
    {
        return [
            'Sản Phẩm',
            'Số Lượng',
            'Tổng Giảm Giá',
            'Tổng VAT',
            'Tổng Ký Bill',
            'Tổng Tiền',
            'Tổng Thanh Toán',
        ];
    }
}
