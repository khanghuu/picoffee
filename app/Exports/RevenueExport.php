<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use App\Model\Bill;
use App\Model\Shop;

class RevenueExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function collection()
    {
        $user = session('user');
        $shopID = '';
        $user->User_Level == 2?$shopID = $user->User_Location_Store:$shopID = Shop::first()->Shop_ID;
        $salesHour = Bill::where('Bill_Status', 1)
            ->selectRaw('
                            SUM(IF(HOUR(`Bill_Time`) = "06", Bill_ExtantPrice, 0)) AS "hour1",
                            SUM(IF(HOUR(`Bill_Time`) = "07", Bill_ExtantPrice, 0)) AS "hour2",
                            SUM(IF(HOUR(`Bill_Time`) = "08", Bill_ExtantPrice, 0)) AS "hour3",
                            SUM(IF(HOUR(`Bill_Time`) = "09", Bill_ExtantPrice, 0)) AS "hour4",
                            SUM(IF(HOUR(`Bill_Time`) = "10", Bill_ExtantPrice, 0)) AS "hour5",
                            SUM(IF(HOUR(`Bill_Time`) = "11", Bill_ExtantPrice, 0)) AS "hour6",
                            SUM(IF(HOUR(`Bill_Time`) = "12", Bill_ExtantPrice, 0)) AS "hour7",
                            SUM(IF(HOUR(`Bill_Time`) = "13", Bill_ExtantPrice, 0)) AS "hour8",
                            SUM(IF(HOUR(`Bill_Time`) = "14", Bill_ExtantPrice, 0)) AS "hour9",
                            SUM(IF(HOUR(`Bill_Time`) = "15", Bill_ExtantPrice, 0)) AS "hour10",
                            SUM(IF(HOUR(`Bill_Time`) = "16", Bill_ExtantPrice, 0)) AS "hour11",
                            SUM(IF(HOUR(`Bill_Time`) = "17", Bill_ExtantPrice, 0)) AS "hour12",
                            SUM(IF(HOUR(`Bill_Time`) = "18", Bill_ExtantPrice, 0)) AS "hour13",
                            SUM(IF(HOUR(`Bill_Time`) = "19", Bill_ExtantPrice, 0)) AS "hour14",
                            SUM(IF(HOUR(`Bill_Time`) = "20", Bill_ExtantPrice, 0)) AS "hour15",
                            SUM(IF(HOUR(`Bill_Time`) = "21", Bill_ExtantPrice, 0)) AS "hour16",
                            SUM(IF(HOUR(`Bill_Time`) = "22", Bill_ExtantPrice, 0)) AS "hour17"
                            ')
        ->where('Bill_Shop_ID', $shopID)
        ->first();

        $countHour = Bill::where('Bill_Status', 1)
            ->selectRaw('
							    COUNT(IF(HOUR(`Bill_Time`) = "06", Bill_ExtantPrice, NULL)) AS "1",
							    COUNT(IF(HOUR(`Bill_Time`) = "07", Bill_ExtantPrice, NULL)) AS "2",
							    COUNT(IF(HOUR(`Bill_Time`) = "08", Bill_ExtantPrice, NULL)) AS "3",
							    COUNT(IF(HOUR(`Bill_Time`) = "09", Bill_ExtantPrice, NULL)) AS "4",
							    COUNT(IF(HOUR(`Bill_Time`) = "10", Bill_ExtantPrice, NULL)) AS "5",
							    COUNT(IF(HOUR(`Bill_Time`) = "11", Bill_ExtantPrice, NULL)) AS "6",
							    COUNT(IF(HOUR(`Bill_Time`) = "12", Bill_ExtantPrice, NULL)) AS "7",
							    COUNT(IF(HOUR(`Bill_Time`) = "13", Bill_ExtantPrice, NULL)) AS "8",
							    COUNT(IF(HOUR(`Bill_Time`) = "14", Bill_ExtantPrice, NULL)) AS "9",
							    COUNT(IF(HOUR(`Bill_Time`) = "15", Bill_ExtantPrice, NULL)) AS "10",
							    COUNT(IF(HOUR(`Bill_Time`) = "16", Bill_ExtantPrice, NULL)) AS "11",
							    COUNT(IF(HOUR(`Bill_Time`) = "17", Bill_ExtantPrice, NULL)) AS "12",
							    COUNT(IF(HOUR(`Bill_Time`) = "18", Bill_ExtantPrice, NULL)) AS "13",
							    COUNT(IF(HOUR(`Bill_Time`) = "19", Bill_ExtantPrice, NULL)) AS "14",
							    COUNT(IF(HOUR(`Bill_Time`) = "20", Bill_ExtantPrice, NULL)) AS "15",
							    COUNT(IF(HOUR(`Bill_Time`) = "21", Bill_ExtantPrice, NULL)) AS "16",
							    COUNT(IF(HOUR(`Bill_Time`) = "22", Bill_ExtantPrice, NULL)) AS "17"
							    ')
            ->where('Bill_Shop_ID', $shopID)
            ->first();
        $totalSales = 0;
        for ($i = 1; $i < 18; $i++) {
            $label = ($i + 5) . ':00-' . ($i + 5) . ':59';
            $totalSales += $salesHour['hour' . $i];
            $dataHour[] = ['hour' => $label, 'count' => $countHour[$i], 'sales' => $salesHour['hour' . $i]];
        }

        $result = [];
        if($user->User_Level == 1 || $user->User_Level == 3){
            foreach ($dataHour as $row) {
                $result[] = array(
                    '0' => $row['hour'],
                    '1' => $row['count'],
                    '2' => $row['count'] == 0 ? 0 : number_format($row['sales']/$row['count']),
                    '3' => number_format($row['sales']),
                    '4' => $totalSales == 0 ? 0 : number_format($row['sales']/$totalSales*100),
                );
            }
        }
        else{
            foreach ($dataHour as $row) {
                $result[] = array(
                    '0' => $row['hour'],
                    '1' => $row['count'],
                    '2' => $row['count'] == 0 ? 0 : number_format($row['sales']/$row['count']),
                    '3' => number_format($row['sales']),
                    '4' => $totalSales == 0 ? 0 : number_format($row['sales']/$totalSales*100),
                );
            }
        }
        return (collect($result));
    }
    public function headings(): array
    {
        if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3){
            return [
                'Thời Gian (Giờ)',
                'Tổng Giao Dịch',
                'Trung Bình Cộng',
                'Tổng Doanh Thu',
                '% Tổng Doanh Thu',
            ];
        }
        else{
            return [
                'Thời Gian (Giờ)',
                'Tổng Giao Dịch',
                'Trung Bình Cộng',
                'Tổng Doanh Thu',
                '% Tổng Doanh Thu',
            ];
        }
    }
}
