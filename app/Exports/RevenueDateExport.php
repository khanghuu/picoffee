<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use App\Model\Shop;
use App\Model\Bill;
use DB;

class RevenueDateExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function collection()
    {
        $dateRevenue = '';
        $user = session('user');
        $shopID = '';
        $user->User_Level == 2?$shopID = $user->User_Location_Store:$shopID = Shop::first()->Shop_ID;
        $dateRevenue = Bill::where('Bill_Status', 1)
        ->selectRaw("DATE_FORMAT(Bill_Time ,'%d/%m/%Y') as 'DateTime',
                    SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent`/100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
                    SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0)) AS TotalSigned,
                    SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice`*0.1/1.1), 0)) AS TotalVAT,
                    SUM(`Bill_TotalPrice`) AS TotalSales,
                    SUM(`Bill_ExtantPrice`) AS TotaExtantlSales
                ")->where('Bill_Shop_ID', $shopID)
        ->groupBy('DateTime')
        ->get()->toArray();
        $result = [];
        foreach ($dateRevenue as $row) {
            $result[] = array(
                '0' => $row['DateTime'],
                '1' => number_format($row['TotalDiscount']),
                '2' => number_format($row['TotalVAT']),
                '3' => number_format($row['TotalSigned']),
                '4' => number_format($row['TotaExtantlSales']),
                '5' => number_format($row['TotaExtantlSales']),
            );
        }
        return (collect($result));
    }
    public function headings(): array
    {
        return [
            'Ngày',
            'Tổng Giảm Giá',
            'Tổng VAT',
            'Tổng Ký Bill',
            'Tổng Tiền',
            'Tổng Thanh Toán',
        ];
    }
}
