<?php

namespace App\Exports;

use App\Model\Category;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;


class CategoryExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function collection()
    {
        $user = session('user');
        $categoryList = Category::query();
        if ($user->User_Level == 2) {
            $categoryList = $categoryList->where('Category_Shop',$user->User_Location_Store) ;
        }

        $categoryList = $categoryList->where([
            ['Category_Status', '>', -1]
        ])
        ->join('shop','category.Category_Shop','shop.Shop_ID')->orderBy('Category_Order', 'ASC')->get();

        $result = [];
        if($user->User_Level == 1 || $user->User_Level == 3){
            foreach ($categoryList as $row) {
                if($row->Category_Status == 1){
                    $string_status = "Mở";
                }
                else{
                    $string_status = "Đóng";
                }
                $result[] = array(
                    '0' => $row->Category_Code,
                    '1' => $row->Category_Name,
                    '2' => $row->Category_Order,
                    '3' => $row->Category_Datetime,
                    '4' => $row->Shop_Name,
                    '5' => $string_status
                );
            }
        }
        else{
            foreach ($categoryList as $row) {
                if($row->Category_Status == 1){
                    $string_status = "Mở";
                }
                else{
                    $string_status = "Đóng";
                }
                $result[] = array(
                    '0' => $row->Category_Code,
                    '1' => $row->Category_Name,
                    '2' => $row->Category_Order,
                    '3' => $row->Category_Datetime,
                    '4' => $string_status
                );
            }
        }
        return (collect($result));
    }
    public function headings(): array
    {
        if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3){
            return [
                'Mã Danh Mục',
                'Tên Danh Mục',
                'Thứ Tự Danh Mục',
                'Ngày Tạo',
                'Cửa Hàng',
                'Trạng Thái',
            ];
        }
        else{
            return [
                'Mã Danh Mục',
                'Tên Danh Mục',
                'Thứ Tự Danh Mục',
                'Ngày Tạo',
                'Trạng Thái',
            ];
        }
    }
}
