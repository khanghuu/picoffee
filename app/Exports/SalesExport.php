<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use App\Model\Bill;

class SalesExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function collection()
    {

        $Statistic = Bill::where('Bill_Status', 1)
        ->selectRaw("
                        SUM(IF(`Bill_DiscountPercent` <> 0, (`Bill_DiscountPercent` / 100 * `Bill_TotalPrice`), `Bill_DiscountAmount`)) AS TotalDiscount,
                        SUM(IF(`Bill_IsSigned` = 1, (`Bill_TotalPrice`), 0))             AS TotalSigned,
                        SUM(IF(`Bill_IsTaxVAT` = 1, (`Bill_TotalPrice` * 0.1 / 1.1), 0)) AS TotalVAT,
                        SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                        COUNT(*)                                                         AS TotalBill,
                        SUM(`Bill_TotalPrice`)                                           AS TotalSales,
                        SUM(`Bill_ExtantPrice`)                                          AS TotaExtantlSales
                    ")
        ->first();
        // dd($Statistic->TotalBill);
        $result = [];
        $result[] = array(
            '0' => $Statistic->TotalBill,
            '1' => number_format($Statistic->TotalSales),
            '2' => number_format($Statistic->TotalDiscount),
            '3' => number_format($Statistic->TotalSigned),
            '4' => number_format($Statistic->TotalVAT),
            '5' => number_format($Statistic->TotaExtantlSales)
        );
        return (collect($result));
    }
    public function headings(): array
    {
        return [
            'Tổng Giao Dịch',
            'Tổng Doanh Thu',
            'Tổng Giảm Giá',
            'Tổng Ký Bill',
            'Tổng Thuế',
            'Tổng Doanh Thu Thuần',
        ];
    }
}
