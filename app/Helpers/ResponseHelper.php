<?php
define('STATUS_REDIRECT' ,['error', 'success']);
define('STATUS_JSON' ,[false, true]);

if (!function_exists('responseRedirect')) {
    function responseRedirect(int $status, string $message, string $routePath = null)
    {
        if ($routePath) {
            return redirect()->route($routePath)->with(['flash_level'=>STATUS_REDIRECT[$status],'flash_message'=>$message]);
        }
        return redirect()->back()->with(['flash_level'=>STATUS_REDIRECT[$status],'flash_message'=>$message]);
    }
}

if (!function_exists('responseJsonMess')) {
    function responseJsonMess(int $status,string $message)
    {
        return response()->json(array('status'=>STATUS_JSON[$status], 'message'=>$message), 200);
    }
}

if (!function_exists('responseJsonData')) {
    function responseJsonData(int $status, $data)
    {
        return response()->json(array('status'=>STATUS_JSON[$status], 'dataResponse' => $data), 200);
    }
}
