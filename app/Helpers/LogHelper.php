<?php

use Jenssegers\Agent\Agent;

if (!function_exists('writeLog')) {
    function writeLog($log = null, $user = 0)
    {
        $user = Session('user');
        if ($user) {
            $user = $user->User_ID;
        } else {
            $user = 0;
        }
        $agent = new Agent();
        // ghi log
        $logInsert = array(
            'Log_Name_Log' => $log,
            'Log_Name_DateTime' => date('Y-m-d H:i:s'),
            'Log_Name_IP' => \Request::ip(),
            'Log_Name_System' => $agent->getUserAgent(),
            'Log_Name_User' => $user,
        );
        DB::table('log_name')->insert($logInsert);
    }
}


