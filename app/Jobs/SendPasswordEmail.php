<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendPasswordEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected  $result;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userData, $result)
    {
        $this->data = $userData;
        $this->result = $result;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $i  = 1;
            Mail::send('Mail.Forgot', $this->data, function ($msg) {
                $msg->from('do-not-reply@picoffee.com', 'Pi Coffee');
                $msg->to($this->result->User_Email)->subject('New Password');
            });
            $i++;

    }
}
