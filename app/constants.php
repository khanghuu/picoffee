
<?php
define('ERROR_CONTACT_AMDIN' ,  'Lỗi, Vui lòng thông báo quản trị viên!');
define(
    'ERROR_VALIDATE',
    [
        'require' => 'Lỗi, Vui lòng nhập ',
        'unique' => 'Lỗi, Đã tồn tại ',
        'max' => 'Lỗi, Vượt quá giới hạn giá trị ',
        'min' => 'Lỗi, nhỏ hơn giới hạn giá trị ',
        'date_format' => 'Lỗi, Định đạng phải là năm/tháng/ngày',
        'numeric'   => 'Lỗi, Không nhập kí tự vào',
        'not_in' => 'Lỗi, Nhập số lượng không hợp lệ vào',
        'exists' => 'Lỗi, Không tồn tại ',
]);
