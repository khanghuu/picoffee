@extends('System.Layouts.master')

@section('titile', 'Dashboard')


@section('css')
	<!-- Morris Charts CSS -->
    <link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
    <style>
	    .morris-hover-row-label {
		    color:black!important;
	    }
    </style>
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		@if(Session('user')->User_Level == 1)
		<form method="get" action="">
		    <div class="col-lg-6">
			    <div class="form-group">
				    <h4 class="mb-5 weight-500">Cửa hàng</h4>
					<select name="Store" class="form-control">
						@foreach($shopList as $shop)
						<option value="{{$shop->Shop_ID}}">{{$shop->Shop_Name}}</option>
						@endforeach
					</select>
			    </div>
		    </div>
		    <div class="col-lg-6">
			    <div class="form-group">
				    <h4 class="mb-5 weight-500" style="opacity: 0;"> a</h4>
					<button class="btn btn-rounded btn-primary">Change Store</button>
			    </div>
		    </div>
		</form>
		@endif
	    <div class="col-lg-12">
	        <div class="panel panel-default border-panel card-view">
		        <div class="panel-wrapper collapse in">
				    <div class="product-detail-wrap">
					    <h4 class="mb-5 weight-500">Kết quả bán hàng hôm nay</h4>
					    <div>Tổng doanh thu: Hôm qua: <span class="product-price head-font mb-15">{{number_format($getSales->salesYesterday)}}đ</span>, Hôm nay: <span class="product-price head-font mb-15">{{number_format($getSales->salesToday)}}đ<span></div>
					    <div class="mb-3">Tổng đơn hàng: Hôm qua: <span class="product-price head-font mb-15">{{$getSales->countYesterday}}</span>, Hôm nay: <span class="product-price head-font mb-15"> {{$getSales->countToday}}</span></div>

					</div>
		        </div>
	        </div>
	    </div>
		<div class="col-lg-12">
			<div class="panel panel-default border-panel card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Hóa đơn</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div id="morris_extra_bar_chart" class="morris-chart"></div>
				</div>
			</div>
		</div>
	    <div class="col-lg-7">
	        <div class="panel panel-default border-panel card-view">
	            <div class="panel-heading">
	                <div class="pull-left">
	                    <h6 class="panel-title txt-dark">Đơn hàng </h6>
	                </div>
	                <div class="clearfix"></div>
	            </div>
	            <div class="panel-wrapper collapse in">
	                <div id="morris_extra_line_chart" class="morris-chart"></div>
	            </div>
	        </div>
	    </div>
		<div class="col-lg-5" style="margin:auto;">
			<div class="panel panel-default border-panel card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Top Sản phẩm</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="flot-container" style="height:250px">
							<div id="flot_pie_chart" class="demo-placeholder"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
    <!-- Morris Charts JavaScript -->
    <script src="vendors/bower_components/raphael/raphael.min.js"></script>
    <script src="vendors/bower_components/morris.js/morris.min.js"></script>
    <!-- Flot Charts JavaScript -->
    <script src="vendors/bower_components/Flot/excanvas.min.js"></script>
    <script src="vendors/bower_components/Flot/jquery.flot.js"></script>
    <script src="vendors/bower_components/Flot/jquery.flot.pie.js"></script>
    <script src="vendors/bower_components/Flot/jquery.flot.resize.js"></script>
    <script src="vendors/bower_components/Flot/jquery.flot.time.js"></script>
    <script src="vendors/bower_components/Flot/jquery.flot.stack.js"></script>
    <script src="vendors/bower_components/Flot/jquery.flot.crosshair.js"></script>
    <script src="vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>



	<script>
		/***Pie Chart***/
		$(function () {
            if( $('#flot_pie_chart').length > 0 ){
                    @if(count($arrDataChart)>0)
                var pie_data = [
                        {
                            label: "{{$arrDataChart[0]['label']}}",
                            data: {{$arrDataChart[0]['data']}},
                            color: "{{$arrDataChart[0]['color']}}",
                            @for($i=1;$i<count($arrDataChart) - 1; $i++)
                        },
                        {
                            label: "{{$arrDataChart[$i]['label']}}",
                            data: {{$arrDataChart[$i]['data']}},
                            color: "{{$arrDataChart[$i]['color']}}",
                        },
                        {
                            @endfor
                            label: "{{$arrDataChart[count($arrDataChart)-1]['label']}}",
                            data: {{$arrDataChart[count($arrDataChart)-1]['data']}},
                            color: "{{$arrDataChart[count($arrDataChart)-1]['color']}}",
                        }];
                    @else
                var pie_data = '';
                    @endif
                var pie_op = {
                        series: {
                            pie: {
                                innerRadius: 0.5,
                                show: true,
                                stroke: {
                                    width: 0,
                                }
                            }
                        },
                        legend : {
                            backgroundColor: 'transparent',
                        },
                        grid: {
                            hoverable: true
                        },
                        color: null,
                        tooltip: true,
                        tooltipOpts: {
                            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                            shifts: {
                                x: 20,
                                y: 0
                            },
                            defaultTheme: false
                        },
                    };
                $.plot($("#flot_pie_chart"), pie_data, pie_op);
            }


            if($('#morris_extra_bar_chart').length > 0){
                // Morris bar chart
                Morris.Bar({
                    element: 'morris_extra_bar_chart',
                    data: [{
                        y: '{{$barChart[0]['y']}}',
                        a: {{$barChart[0]['a']}},
                        b: {{$barChart[0]['b']}}
                    },{
                        @for($i=1;$i<count($barChart) - 1; $i++)
                        y: '{{$barChart[$i]['y']}}',
                        a: {{$barChart[$i]['a']}},
                        b: {{$barChart[$i]['b']}}
                    }, {
                        @endfor
                        y: '{{$barChart[count($barChart)-1]['y']}}',
                        a: {{$barChart[count($barChart)-1]['a']}},
                        b: {{$barChart[count($barChart)-1]['b']}}
                    }],
                    xkey: 'y',
                    ykeys: ['a', 'b'],
                    labels: ['Hôm qua', 'Hôm nay'],
                    barColors:['#7bb5ec', '#91e8e1'],
                    barOpacity:1,
                    hideHover: 'auto',
                    grid: false,
                    resize: true,
                    gridTextColor:'#878787',
                    gridTextFamily:"Roboto"
                });
            }
            if($('#morris_extra_line_chart').length > 0){
                Morris.Line({
                    element: 'morris_extra_line_chart',
                    data: [{
                        y: '{{$lineChart[0]['y']}}',
                        a: {{$lineChart[0]['a']}},
                        b: {{$lineChart[0]['b']}}
                    },{
                        @for($i=1;$i<count($lineChart) - 1; $i++)
                        y: '{{$lineChart[$i]['y']}}',
                        a: {{$lineChart[$i]['a']}},
                        b: {{$lineChart[$i]['b']}}
                    }, {
                        @endfor
                        y: '{{$lineChart[count($lineChart)-1]['y']}}',
                        a: {{$lineChart[count($lineChart)-1]['a']}},
                        b: {{$lineChart[count($lineChart)-1]['b']}}
                    }],
                    xkey: 'y',
                    ykeys: ['a', 'b'],
                    labels: ['Hôm qua', 'Hôm nay'],
                    pointSize: 2,
                    fillOpacity: 0,
                    lineWidth:2,
                    pointStrokeColors:['#7bb5ec', '#8085e9'],
                    behaveLikeLine: true,
                    grid: false,
                    hideHover: 'auto',
                    lineColors: ['#7bb5ec', '#8085e9'],
                    resize: true,
                    gridTextColor:'#878787',
                    gridTextFamily:"Roboto"

                });
            }
        })

	</script>
@endsection
