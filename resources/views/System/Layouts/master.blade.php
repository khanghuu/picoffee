<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>@yield('title','Pi Coffee & Tea')</title>
        <meta name="description" content="Admintres is a Dashboard & Admin Site Responsive Template by hencework." />
        <meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Admintres Admin, Admintresadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
        <meta name="author" content="hencework"/>
        <base href="{{asset('')}}">
        <!-- Favicon -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <!-- Toast CSS -->
        <link href="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">

        <!-- Morris Charts CSS -->
        <link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

        <!-- Chartist CSS -->
        <link href="vendors/bower_components/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css"/>


        <!-- vector map CSS -->
        <link href="vendors/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" type="text/css"/>

        <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
        <!-- Custom CSS -->
        <link href="dist/css/style.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="css/footable.standalone.css" rel="stylesheet" type="text/css">


        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css">
        @yield('css')
        <style>
            .btn-01 {
                background-color: #ff6028; /* Blue background */
                border: none; /* Remove borders */
                color: white; /* White text */
                padding: 7px 5px; /* Some padding */
                font-size: 16px;
                /* Set a font size */
                cursor: pointer; /* Mouse pointer on hover */
            }

            /* Darker background on mouse-over */
            .btn-01:hover {
                background-color: RoyalBlue;
            }
            #iconadd a:hover{
                color: #fff;

            }
            .xemchitiet a:hover{
                color: #04B45F;
            }
            .xemchitiet a{
                color: #045cb7;

            }
            #btn-them a{
                color: #fff;
                font-size: 14px;
            }
            #btn-them {
                padding: -7px -5px;
                border: 0;
            }
            #edit i{
                color: #e8b455;
            }
            #edit i:hover{
                color: #5589e8;
            }
            .img-brand{
                max-width: 98px!important;
                margin: -35px 0px 0 90px!important;
                }
            @media only screen and (max-device-width: 736px) {
                .img-brand {
                    max-width: 80px!important;
                    margin: -23px 0px 0 4px!important;
                }
            }
        </style>
    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader-it">
            <div class="la-anim-1"></div>
        </div>
        <!-- /Preloader -->

        <!--Content -->
        <div class="wrapper theme-2-active navbar-top-light horizontal-nav">
            <!-- Top Menu Items -->
            @include('System.Layouts.topbar-menu')
            <!-- /Top Menu Items -->

            <!-- Top Nav menu -->
            @include('System.Layouts.menu')
            <!-- /Top Nav Menu -->

            <!-- Main Content -->
            <div class="page-wrapper">
                <div class="container-fluid">
                    <div class="row">

                            @if(!empty($uri))
                                @section('content')
                                @show
                            @else
                            <!-- Left content -->
                                <div class="col-sm-9">
                                    @section('content')
                                    @show
                                </div>
                                <!-- /Left content -->

                                <!-- Right content -->
                                <div class="col-sm-3">
                                    @include('System.Layouts.log')
                                </div>
                                <!-- /Right content -->
                            @endif

                        <!-- Footer -->
                       @include('System.Layouts.footer')
                        <!-- /Footer -->
                    </div>
                </div>
            </div>
            <!-- /Main Content -->

        </div>
        <!-- /Content -->

        <!-- JavaScript -->

        <!-- jQuery -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <!--Ajax-->
        <!-- Bootstrap Core JavaScript -->
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        {{-- <!-- Data table JavaScript -->
        <script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>


        <script src="../vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="../vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="dist/js/export-table-data.js"></script> --}}
        <script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
        <script src="vendors/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="vendors/bower_components/pdfmake/build/pdfmake.min.js"></script>
        <script src="vendors/bower_components/pdfmake/build/vfs_fonts.js"></script>

        <script src="vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="dist/js/export-table-data.js"></script>

        <!-- Slimscroll JavaScript -->
        <script src="dist/js/jquery.slimscroll.js"></script>

        <!-- simpleWeather JavaScript -->
        <script src="vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>


        <!-- Progressbar Animation JavaScript -->
        <script src="vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
        <script src="vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

        <!-- Fancy Dropdown JS -->
        <script src="dist/js/dropdown-bootstrap-extended.js"></script>

        <!-- Sparkline JavaScript -->
{{--        <script src="vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>--}}

        <!-- Owl JavaScript -->
        <script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

        <!-- Switchery JavaScript -->
        <script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

        <!-- Vector Maps JavaScript -->
        <script src="vendors/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
        <script src="vendors/vectormap/jquery-jvectormap-us-aea-en.js"></script>
        <script src="vendors/vectormap/jquery-jvectormap-world-mill-en.js"></script>
        <script src="dist/js/vectormap-data.js"></script>

        <!-- Toast JavaScript -->
        <script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

        <!-- Piety JavaScript -->
        <script src="vendors/bower_components/peity/jquery.peity.min.js"></script>
        <script src="dist/js/peity-data.js"></script>

        <!-- Chartist JavaScript -->
        <script src="vendors/bower_components/chartist/dist/chartist.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="vendors/bower_components/raphael/raphael.min.js"></script>
        <script src="vendors/bower_components/morris.js/morris.min.js"></script>

        <!-- ChartJS JavaScript -->
        <script src="vendors/chart.js/Chart.min.js"></script>

        <!-- Init JavaScript -->
        <script src="dist/js/init.js"></script>
        <script src="dist/js/dashboard-data.js?v=12"></script>

        <script src="js/footable.js"></script>

        @section('script')
        @show
        <script>
	        baseURL = window.location.host;
            @if(Session::get('flash_level') == 'success')
            $.toast({
                heading: 'Success',
                text: '{{ Session::get('flash_message') }}',
                position: 'top-right',
                loaderBg:'#fec107',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            @elseif(Session::get('flash_level') == 'error')
            $.toast({
                heading: 'Error',
                text: '{{ Session::get('flash_message') }}',
                position: 'top-right',
                loaderBg:'#fec107',
                icon: 'error',
                hideAfter: 3500,
                stack: 6
            });
            @endif
            @if(count($errors)>0)
            @foreach($errors->all() as $error)
            $.toast({
                heading: 'Error',
                text: '{{ $error }}',
                position: 'top-right',
                loaderBg:'#fec107',
                icon: 'error',
                hideAfter: 3500,
                stack: 6
            });
            @endforeach
            @endif
        </script>
    </body>

</html>
