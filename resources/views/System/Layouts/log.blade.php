<div class="panel panel-default border-panel card-view">
    <div class="panel-heading">
        <div class="pull-left">
            <h6 class="panel-title txt-dark">Lịch sử</h6>
        </div>
        <div class="clearfix"></div>
    </div>
    <div  class="panel-wrapper collapse in">
        <?php
        $row = DB::table('log_name')->join('users', 'Log_Name_User', 'users.User_ID')->orderBy('Log_Name_ID', 'DESC')->limit(10)->get();
        ?>
        <div  class="panel-body">
            @foreach($row as $v)
                <blockquote>

                    <p>{{ $v->User_ID .': '. $v->Log_Name_Log }}</p>
                    <small>IP: {{ $v->Log_Name_IP }} <cite title="Source Title">Time: {{ $v->Log_Name_DateTime }}</cite></small>
                </blockquote>
            @endforeach
        </div>
    </div>
</div>
