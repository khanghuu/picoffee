<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li class="navigation-header">
            <hr/>
        </li>
        <li>
            <a class="active" href="{{ route('system.dashboard') }}" data-toggle="collapse" data-target="#app_dr"><div class="pull-left"><i class="ti-dashboard mr-20"></i><span class="right-nav-text">Bảng điều khiển </span></div><div class="pull-right"></div><div class="clearfix"></div></a>
        </li>
        @if(Session::get('user')->User_Level != 4)
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#ecom_dr"><div class="pull-left"><i class="ti-pencil-alt mr-20"></i><span class="right-nav-text">Báo cáo</span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
                <ul id="ecom_dr" class="collapse collapse-level-1">
                    <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#auth_dr">Báo cáo<div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
                        <ul id="auth_dr" class="collapse collapse-level-2 dr-change-pos">
                            <li>
                                <a href="{{route('system.report.getSales')}}">Bán hàng</a>
                            </li>
                            <li>
                                <a href="{{route('getRevenueShift')}}">Doanh thu theo ca</a>
                            </li>
                            <li>
                                <a href="{{route('getRevenueDate')}}">Danh Thu theo ngày </a>
                            </li>
                            <li>
                                <a href="{{route('getRevenueStaff')}}">Doanh thu theo nhân viên</a>
                            </li>
                            <li>
                                <a href="{{route('getRevenueProduct')}}">Doanh thu theo sản phẩm</a>
                            </li>
                            <li>
                                <a href="{{route('getRevenueCustomer')}}">Doanh thu theo khách hàng</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{route('system.report.getRevenue')}}">Doanh thu hàng giờ</a>
                    </li>
                    <li>
                        <a href="{{route('system.report.getTopProduct')}}">Top sản phẩm</a>
                    </li>
{{--                    <li>--}}
{{--                        <a href="{{route('getRevenueRaw')}}">Nguyên liệu thô</a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="{{route('getRevenueIngredient')}}">Nguyên liệu tinh</a>--}}
{{--                    </li>--}}
                    <li>
                        <a href="{{route('system.report.getSignedBill')}}">Ký bill</a>
                    </li>

                </ul>
            </li>
        @endif
        <li>
            <a class="active" href="javascript:void(0);" data-toggle="collapse" data-target="#app_dr"><div class="pull-left"><i class="ti-clipboard mr-20"></i><span class="right-nav-text">Đơn đặt hàng </span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="app_dr" class="collapse collapse-level-1">
                @if(Session::get('user')->User_Level == 3  || Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                <li>
                    <a href="{{route('system.getAllBill')}}">Tất cả đơn hàng</a>
                </li>
                @endif
                @if(Session::get('user')->User_Level != 3  and Session::get('user')->User_Level != 1)
                    <li>
                        <a href="{{route('getIndex')}}">Thêm đơn hàng mới</a>
                    </li>
                @endif
            </ul>
        </li>
        <li>
            <a class="active" href="javascript:void(0);" data-toggle="collapse" data-target="#app_dr"><div class="pull-left"><i class="ti-user  mr-20"></i><span class="right-nav-text">Người dùng </span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="app_dr" class="collapse collapse-level-1">
                <li>
                    <a href="{{route('system.user.getProfile')}}">Hồ sơ của bạn</a>
                </li>
                @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                    <li>
                        <a href="{{ route('system.user.getActionEmployees') }}">Hoạt động nhân viên</a>
                    </li>
                    <li>
                        <a href="{{ route('system.user.getAction') }}">Lịch sử truy cập</a>
                    </li>
                @endif
                @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2 || Session::get('user')->User_Level == 3)
                    <li>
                        <a href="{{ route('system.user.getAllUsers') }}">Người dùng</a>
                    </li>
                @endif

            </ul>
        </li>
        @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2 || Session::get('user')->User_Level == 3 )
            <li>
                <a class="active" href="javascript:void(0);" data-toggle="collapse" data-target="#app_dr"><div class="pull-left"><i class="ti-panel mr-20"></i><span class="right-nav-text">Kho </span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
                <ul id="app_dr" class="collapse collapse-level-1">

                        <li>
                            <a href="{{route('system.warehouse.getManage')}}">Thống kê</a>
                        </li>
                        @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                            <li>
                                <a href="{{ route('system.warehouse.getImport') }}">Nhập kho</a>
                            </li>

                        @endif
                        <li>
                            <a href="{{ route('system.warehouse.getDestroyRaw') }}">Huỷ nguyên liệu thô</a>
                        </li>
                        <li>
                            <a href="{{ route('system.warehouse.getDestroyIngredient') }}">Huỷ nguyên liệu tinh</a>
                        </li>

                        <li>
                            <a href="{{ route('system.warehouse.getHistory') }}">Lịch sử kho</a>
                        </li>
                </ul>
            </li>
        @endif
        <li>
            <a class="active" href="javascript:void(0);" data-toggle="collapse" data-target="#app_dr"><div class="pull-left"><i class="ti-receipt mr-20"></i><span class="right-nav-text">Chuẩn bị dữ liệu </span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="app_dr" class="collapse collapse-level-1">
                <li>
                    <a href="{{ route('system.product.getManage') }}">Sản phẩm</a>
                </li>
                @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2 || Session::get('user')->User_Level == 3)
                    <li>
                        <a href="{{ route('system.category.getManage') }}">Danh mục</a>
                    </li>
                    <li>
                        <a href="{{ route('system.ingredient.getManageIngredient') }}">Nguyên liệu tinh</a>
                    </li>
                    <li>
                        <a href="{{ route('system.raw.getManageRaw') }}">Nguyên liệu thô</a>
                    </li>
                @endif
            </ul>
        </li>
        @if(Session('user')->User_Level == 1)
        <li>
            <a class="active" href="{{ route('system.shop.getAllShop') }}" data-toggle="collapse" data-target="#app_dr"><div class="pull-left"><i class="ti-dashboard mr-20"></i><span class="right-nav-text">Quản lý cửa hàng </span></div><div class="pull-right"></div><div class="clearfix"></div></a>
        </li>
        @endif
    </ul>
</div>
@component('Components.modal-reset-password')
@endcomponent
