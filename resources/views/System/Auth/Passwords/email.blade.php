<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Pi Coffee & Tea - Reset password</title>
    <meta name="description" content="Admintres is a Dashboard & Admin Site Responsive Template by hencework." />
    <meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Admintres Admin, Admintresadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
    <meta name="author" content="hencework"/>
    <base href="{{asset('')}}">
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- vector map CSS -->
    <link href="../vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>



    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <style>
        .sp-logo-wrap {
            padding-top: 0px;
            margin: 0px;
        }
    </style>
</head>
<body>
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->

<div class="wrapper  pa-0">
    <header class="sp-header">
        <div class="sp-logo-wrap pull-left">
            <a href="{{route('getIndex')}}">
                <img class="brand-img mr-10" src="../img/logo-pineo.png" alt="brand"/>
                <span class="brand-text"><img style="width:100px; hight:100px;" src="../img/logo-pineo.png" alt="brand"/></span>
            </a>
        </div>
        <div class="form-group mb-0 pull-right">
            <a class="inline-block btn btn-orange btn-rounded " href="{{route('getLogin')}}">Sign In</a>
            <a class="inline-block btn btn-orange btn-rounded " href="{{route('getRegister')}}">Sign Up</a>
        </div>
        <div class="clearfix"></div>
    </header>

    <!-- Main Content -->
    <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container">
            <!-- Row -->
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float card-view pt-30 pb-30">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="mb-30">
                                    <h3 class="text-center txt-dark mb-10">Reset Password</h3>
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                </div>

                                <div class="form-wrap">
                                    <form action="{{ route('PostForget') }}" method="post">
                                        @csrf
                                        @if(Session::get('flash_level') == 'error')
                                            <div class="alert alert-danger" role="alert">
                                                {{ Session::get('flash_message') }}
                                            </div>
                                        @endif
                                        @if(Session::get('flash_level') == 'success')
                                            <div class="alert alert-success" role="alert">
                                                {{ Session::get('flash_message') }}
                                            </div>
                                        @endif
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputEmail_2">Enter your email address</label>
                                            <input type="email" name = "email" class="form-control" required="" id="exampleInputEmail_2" placeholder="Enter email">

                                        </div>
                                        @error('email')
                                        <div class="form-group">
                                            <span class="invalid-feedback text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                            </span>
                                        </div>
                                        @enderror
                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-orange btn-rounded">{{ __('Send Password Reset Link') }}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>

    </div>
    <!-- /Main Content -->

</div>

<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>

<!-- Slimscroll JavaScript -->
<script src="dist/js/jquery.slimscroll.js"></script>

<!-- Init JavaScript -->
<script src="dist/js/init.js"></script>
</body>
</html>
