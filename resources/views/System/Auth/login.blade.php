<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Pi Coffee & Tea - Login</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- vector map CSS -->
    <link href="../vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <!-- Toast CSS -->
    <link href="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <style>
        .sp-logo-wrap {
            padding-top: 0px;
            margin: 0px;
        }
    </style>
</head>
<body>
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->

<div class="wrapper  pa-0">
    <header class="sp-header">
        <div class="sp-logo-wrap pull-left">
            <a href="index.html">
                <img class="brand-img mr-10" src="../img/logo-pineo.png" alt="brand"/>
                <span class="brand-text"><img style="width:100px; hight:100px;" src="../img/logo-pineo.png" alt="brand"/></span>
            </a>
        </div>
        <div class="form-group mb-0 pull-right">
            <span class="inline-block pr-10">Chưa có tài khoản Picoffee?</span>
            <a class="inline-block btn btn-orange btn-rounded " href="{{route('getRegister')}}">Đăng ký </a>
        </div>
        <div class="clearfix"></div>
    </header>

    <!-- Main Content -->
    <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container">
            <!-- Row -->
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float card-view pt-30 pb-30">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="mb-30">
                                    <h3 class="text-center txt-dark mb-10">Đăng nhập vào PiCoffee</h3>
                                    <h6 class="text-center nonecase-font txt-grey">Nhập thông tin đăng nhập</h6>
                                </div>
                                <div class="form-wrap">
                                    <form action="" method="post">
                                        @csrf
                                        @if (Session::get('flash_level') =='error')
                                            <div class="alert alert-danger" role="alert">
                                                {{ Session::get('flash_message') }}
                                            </div>
                                        @endif
                                        @php Session::get('flash_level') =='error' @endphp
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputEmail_2">Email</label>
                                            <input type="email" name = "email" class="form-control" required placeholder="Enter email">
                                        </div>
                                        <div class="form-group">
                                            <label class="pull-left control-label mb-10" for="exampleInputpwd_2">Mật khẩu</label>
                                            <div class="clearfix"></div>
                                            <input type="password" name="password" class="form-control" required placeholder="Enter password">
                                        </div>
                                        <div class="form-group">
                                            <a class="text-warning" href="{{ route('getForgotPass') }}">
                                                {{ __('Quên mật khẩu?') }}
                                            </a>
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-orange btn-rounded">Đăng nhập </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>

    </div>
    <!-- /Main Content -->
</div>
<!-- jQuery -->
<script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>

<!-- Slimscroll JavaScript -->
<script src="dist/js/jquery.slimscroll.js"></script>

<!-- Init JavaScript -->
<script src="dist/js/init.js"></script>

<!-- Toast JavaScript -->
<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
<script>
    @if(Session::get('flash_level') == 'success')
    $.toast({
        heading: 'Success',
        text: '{{ Session::get('flash_message') }}',
        position: 'top-right',
        loaderBg:'#fec107',
        icon: 'success',
        hideAfter: 3500,
        stack: 6
    });
    @elseif(Session::get('flash_level') == 'error')
    $.toast({
        heading: 'Error',
        text: '{{ Session::get('flash_message') }}',
        position: 'top-right',
        loaderBg:'#fec107',
        icon: 'error',
        hideAfter: 3500,
        stack: 6
    });
    @endif
</script>
</body>
</html>
