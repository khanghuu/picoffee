@extends('System.Layouts.master')
@section('css')
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
    </style>
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
        td>a:hover{
            cursor: pointer;
        }
        table.dataTable thead .sorting::after, table.dataTable thead .sorting_asc::after, table.dataTable thead .sorting_desc::after{
            display: block!important;
        }
    </style>
@endsection
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default border-panel card-view">
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Tìm kiếm</h6>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="form-wrap">
                        <form class="form-horizontal">
                            <div class="form-group mb-0">
                                <form method="get">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                                <div class="col-sm-3">
                                                    <label class="control-label mb-10">Cửa hàng</label>
                                                    <select class="form-control" name="shop_id">
                                                        <option value="" selected>----Chọn cửa hàng----</option>
                                                        @foreach($shopList as $shop)
                                                            <option value="{{$shop->Shop_ID}}" {{$shop->Shop_ID == request()->get('shop_id')?'selected':' '}}>{{$shop->Shop_Name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @endif
                                            <div class="col-sm-3">
                                                <label class="control-label mb-10">Khách hàng</label>
                                                <input type="text" class="form-control" name="customer_name"  value="{{request()->get('customer_name')}}">
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="control-label mb-10">Từ ngày </label>
                                                <input type="date" class="form-control rounded-outline-input" name="from_date" value="{{request()->get('from_date')}}">
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="control-label mb-10">Đến ngày </label>
                                                <input type="date" class="form-control rounded-outline-input" name="to_date" value="{{request()->get('to_date')}}">
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 5px; text-align: left;">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-success">Tìm kiếm</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="col-sm-12">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Ký Bill</h6>
					</div>
					<div class="clearfix"></div>
                </div>
                <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <a style="margin: 10px;" href="{{route('signedBillExport')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                        </div>
                    </div>
				<div class="panel-body">
					<div class="table-wrap">
                        <table id="datatable_signed_bill" class="table table-striped table-bordered mb-0">
							<thead class="color-white-th">
								<tr>
                                    <th>Cửa hàng</th>
									<th>Khách hàng</th>
									<th>Tổng</th>
                                    <th>Ngày giờ</th>

								</tr>
							</thead>
							<tbody>
								@foreach($signed as $d)
								<tr>
                                    <td>{{$d->Shop_Name}}</td>
									<td>{{$d->Bill_CustomerName}}</td>
									<td>{{number_format($d->Bill_TotalPrice)}}đ</td>
                                    <td>{{$d->Bill_Time}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
                        <div style="text-align: right">
                            {{$signed->appends(request()->input())->onEachSide(1)->links()}}
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')

    <!-- Data table JavaScript -->
    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="dist/js/dataTables-data.js?v=1"></script>
	<script>
		$(document).ready(function() {
			$('#signed-bill').DataTable( {
				responsive: true
			} );

		    var arr = [5, 15, 110, 210, 550];
		    var index = arr.indexOf(210);

		    if (index > -1) {
		       arr.splice(index, 1);
		    }
		    console.log(arr);
		} );
	</script>
@endsection
