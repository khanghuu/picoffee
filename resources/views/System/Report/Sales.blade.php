@extends('System.Layouts.master')
@section('css')
	<!-- Data table CSS -->
    <style>
	body {
		color:black;
	}
	.text-weight {
		font-weight: bold;
	}
    .btn-success:hover{
        -webkit-box-shadow: 50px 0px 0 0 #31708f inset , -50px 0px 0 0 #31708f inset;
    }
    .btn-danger:hover{
        -webkit-box-shadow: 50px 0px 0 0 #31708f inset , -50px 0px 0 0 #31708f inset;
    }
    .color-white-th{
        background: #ff6028;
    }
    .color-white-th tr th{
        color: white!important;
    }
    .color-white-th{
        background: #ff6028;
    }
    .color-white-th tr th{
        color: white!important;
    }
    .pagenation {
        float:left;
    }
</style>
@endsection
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="col-lg-8">
			<div class="panel panel-default border-panel card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Thống Kê Bán Hàng</h6>
					</div>
					<div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <a style="margin: 10px;" href="{{route('salesExport')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                    </div>
                </div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table mb-0">
								<tbody>
                                    <tr>
                                        <td>Tổng giao dịch</td>
                                        <td>{{ $Statistic->TotalBill }}</td>
                                        <td></td>
                                    </tr>
								  <tr>
									<td>Tổng doanh thu</td>
									<td>{{number_format($Statistic->TotalSales)}}đ</td>
									<td></td>
								  </tr>
								  <tr>
									<td>Tổng giảm giá</td>
									<td>{{number_format($Statistic->TotalDiscount)}}đ</td>
									<td><button class="btn btn-default btn-outline btn-rounded btn-get-info" data-toggle="modal" data-target=".modal-datatable-discount" value="discount">Chi tiết</button></td>
								  </tr>
								  <tr>
									<td>Tổng ký bill</td>
									<td>{{number_format($Statistic->TotalSigned)}}đ</td>
									<td><button class="btn btn-default btn-outline btn-rounded btn-get-info" data-toggle="modal" data-target=".modal-datatable-signed" value="signed">Chi tiết</button></td>
								  </tr>
								  <tr>
									<td>Tổng thuế</td>
									<td>{{number_format($Statistic->TotalVAT)}}đ</td>
									<td><button class="btn btn-default btn-outline btn-rounded btn-get-info" data-toggle="modal" data-target=".modal-datatable-taxvat" value="tax-vat">Chi tiết</button></td>
								  </tr>
								  <tr class="text-weight">
									<td>Tổng doanh thu thuần</td>
									<td>{{number_format($Statistic->TotaExtantlSales)}}đ</td>
									<td></td>
								  </tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

	</div>
</div>
<!-- sample modal content -->
<div class="modal fade modal-datatable-discount" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h5 class="modal-title">Chi tiết giảm giá</h5>
			</div>
			<div class="modal-body">
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="table-wrap">
							<div class="table-responsive">
                                <table id="datable_admin" class="table table-hover table-bordered display mb-30">
                                    <thead class="color-white-th">
										<tr>
											<th>ID</th>
											<th>Tên</th>
											<th>Email</th>
											<th>Điện thoại</th>
											<th>Tổng tiền trên bill</th>
											<th>Giảm giá %</th>
											<th>Giảm giá tiền</th>
											<th>VAT</th>
											<th>Tổng thanh toán</th>
											<th>Ngày giờ</th>
											<th>Người tạo</th>
<!-- 											<th>Chi tiết</th> -->
										</tr>
									</thead>
									<tbody>
										@foreach($discount as $d)
										<tr>
											<td>{{$d->Bill_ID}}</td>
											<td>{{$d->Bill_CustomerName}}</td>
											<td>{{$d->Bill_CustomerEmail}}</td>
											<td>{{$d->Bill_CustomerPhone}}</td>
											<td>{{$d->Bill_TotalPrice}}</td>
											<td>{{$d->Bill_DiscountPercent}}%</td>
											<td>{{$d->Bill_DiscountPercent != 0 ? ($d->Bill_DiscountPercent/100 * $d->Bill_TotalPrice) : $d->Bill_DiscountAmount }}</td>
											<td>{{$d->Bill_IsTaxVAT == 0 ? 0 : 0.1 * $d->Bill_TotalPrice }}</td>
											<td>{{$d->Bill_ExtantPrice}}</td>
											<td>{{$d->Bill_Time}}</td>
											<td>{{$d->Bill_User}}</td>
<!-- 											<td>{{$d->Bill_ID}}</td> -->
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- sample modal content -->
<div class="modal fade modal-datatable-signed" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h5 class="modal-title">Chi tiết tổng ký bill </h5>
			</div>
			<div class="modal-body">
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="table-wrap">
							<div class="table-responsive">
                                <table id="datable_admin" class="table table-hover table-bordered display mb-30">
                                    <thead class="color-white-th">
                                    <tr>
											<th>ID</th>
											<th>Tên</th>
											<th>Email</th>
											<th>Điện thoại</th>
											<th>Tổng tiền trên bill</th>
											<th>Giảm giá %</th>
											<th>Giảm giá tiền</th>
											<th>VAT</th>
											<th>Tổng thanh toán</th>
											<th>Ngày giờ</th>
											<th>Người tạo</th>
<!-- 											<th>Chi tiết</th> -->
										</tr>
									</thead>
									<tbody>
										@foreach($signed as $d)
										<tr>
											<td>{{$d->Bill_ID}}</td>
											<td>{{$d->Bill_CustomerName}}</td>
											<td>{{$d->Bill_CustomerEmail}}</td>
											<td>{{$d->Bill_CustomerPhone}}</td>
											<td>{{$d->Bill_TotalPrice}}</td>
											<td>{{$d->Bill_DiscountPercent}}%</td>
											<td>{{$d->Bill_DiscountPercent != 0 ? $d->Bill_DiscountPercent/100 * $d->Bill_TotalPrice : $d->Bill_DiscountAmount }}</td>
											<td>{{$d->Bill_IsTaxVAT == 0 ? 0 : 0.1 * $d->Bill_TotalPrice }}</td>
											<td>{{$d->Bill_ExtantPrice}}</td>
											<td>{{$d->Bill_Time}}</td>
											<td>{{$d->Bill_User}}</td>
<!-- 											<td>{{$d->Bill_ID}}</td> -->
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- sample modal content -->
<div class="modal fade modal-datatable-taxvat" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h5 class="modal-title">Chi tiết Tổng Thuế </h5>
			</div>
			<div class="modal-body">
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="table-wrap">
							<div class="table-responsive">
                                <table id="datable_admin" class="table table-hover table-bordered display mb-30">
                                    <thead class="color-white-th">
										<tr>
											<th>ID</th>
											<th>Tên</th>
											<th>Email</th>
											<th>Điện thoại</th>
											<th>Tổng tiền trên bill</th>
											<th>Giảm giá %</th>
											<th>Giảm giá tiền</th>
											<th>VAT</th>
											<th>Tổng thanh toán</th>
											<th>Ngày giờ</th>
											<th>Người tạo</th>
										</tr>
									</thead>
									<tbody>
										@foreach($taxvat as $d)
										<tr>
											<td>{{$d->Bill_ID}}</td>
											<td>{{$d->Bill_CustomerName}}</td>
											<td>{{$d->Bill_CustomerEmail}}</td>
											<td>{{$d->Bill_CustomerPhone}}</td>
											<td>{{number_format($d->Bill_TotalPrice)}}đ</td>
											<td>{{$d->Bill_DiscountPercent}}%</td>
											<td>{{$d->Bill_DiscountPercent != 0 ? $d->Bill_DiscountPercent/100 * $d->Bill_TotalPrice : $d->Bill_DiscountAmount }}</td>
											<td>{{$d->Bill_IsTaxVAT == 0 ? 0 : 0.1 * $d->Bill_TotalPrice }}</td>
											<td>{{$d->Bill_ExtantPrice}}</td>
											<td>{{$d->Bill_Time}}</td>
											<td>{{$d->Bill_User}}</td>

										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection
@section('script')
        <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
        <script src="dist/js/dataTables-data.js?v=1"></script>
	<script>

    </script>
@endsection
