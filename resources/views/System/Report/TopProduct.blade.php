@extends('System.Layouts.master')
@section('css')
@endsection
@section('content')
<div class="row">
	<div class="col-lg-4" style="margin:auto;">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Top Sản phẩm</h6>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="flot-container" style="height:250px">
						<div id="flot_pie_chart" class="demo-placeholder"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-8">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-hover mb-0">
							<thead>
								<tr>
									<th >#</th>
									<th >Sản phẩm</th>
									<th >Số lượng</th>
									<th >Tổng giá</th>
									<th >Tổng giảm giá</th>
									<th >Tổng doanh thu</th>

								</tr>
							</thead>
							<tbody>
								@foreach($Statistic as $v)
								<tr>
									<td>{{$v->Product->Product_ID}}</td>
									<td>{{$v->Product->Product_Name}}</td>
									<td>{{$v->quantity}}</td>
									<td>{{number_format($v->Product->Product_Price * $v->quantity)}}</td>
									<td>0</td>
									<td>{{number_format($v->Product->Product_Price * $v->quantity)}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
	<!-- Flot Charts JavaScript -->
	<script src="vendors/bower_components/Flot/excanvas.min.js"></script>
	<script src="vendors/bower_components/Flot/jquery.flot.js"></script>
	<script src="vendors/bower_components/Flot/jquery.flot.pie.js"></script>
	<script src="vendors/bower_components/Flot/jquery.flot.stack.js"></script>
	<script src="vendors/bower_components/Flot/jquery.flot.crosshair.js"></script>
	<script src="vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
	<script>
        /***Pie Chart***/

        @if(empty($dataChart))
        window.alert('sdfgsdfg');
        alert('test');
            console.log({{$dataChart}})
            if( $('#flot_pie_chart').length > 0 ){
                var data = $('<div>').html('{{$dataChart}}').textContent;
                data.replace("label",'label');
                console.log(data);
                var pie_data = [{
                    label: "{{$arrDataChart[0]['label']}}",
                    data: {{$arrDataChart[0]['data']}},
                    color: "{{$arrDataChart[0]['color']}}",
                @for($i=1;$i<count($arrDataChart) - 2; $i++)
                }, {
                    label: "{{$arrDataChart[$i]['label']}}",
                    data: {{$arrDataChart[$i]['data']}},
                    color: "{{$arrDataChart[$i]['color']}}",
                }, {
                @endfor
                    label: "{{$arrDataChart[count($arrDataChart)-1]['label']}}",
                    data: {{$arrDataChart[count($arrDataChart)-1]['data']}},
                    color: "{{$arrDataChart[count($arrDataChart)-1]['color']}}",
                }];

                var pie_op = {
                    series: {
                        pie: {
                            innerRadius: 0.5,
                            show: true,
                            stroke: {
                                width: 0,
                            }
                        }
                    },
                    legend : {
                        backgroundColor: 'transparent',
                    },
                    grid: {
                        hoverable: true
                    },
                    color: null,
                    tooltip: true,
                    tooltipOpts: {
                        content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                        shifts: {
                            x: 20,
                            y: 0
                        },
                        defaultTheme: false
                    },
                };
                $.plot($("#flot_pie_chart"), pie_data, pie_op);
            }
        @endif
	</script>
@endsection
