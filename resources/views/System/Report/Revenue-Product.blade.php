@extends('System.Layouts.master')
@section('css')
    <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" />
    <style>
        body {
            color:black;
        }

    </style>
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
        td>a:hover{
            cursor: pointer;
        }
        table.dataTable thead .sorting::after, table.dataTable thead .sorting_asc::after, table.dataTable thead .sorting_desc::after{
            display: block!important;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Doanh thu theo sản phẩm</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row" style="border-bottom: 1px solid #eee;padding-bottom: 10px;">
                        <a style="margin:10px" href="{{route('exportRevenueProduct')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                        <form method="get" class="form-inline">
                            <div class="col-md-12 form-group">
                                <input class="form-control input-sm" type="date" name="from_date" >
                                <input class="form-control input-sm " type="date" name="to_date">
                                <button class="btn btn-success ">Lọc</button>
                            </div>
                        </form>
                    </div>
                    <div class="panel-body pt-0">

                        <div class="table-wrap">
                            <div class="table-responsive" style="margin-top: 2%;">
                                <table id="datatable_revenue_product" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Sản phẩm</th>
                                        <th>Số lượng</th>
                                        <th>Tổng giảm giá</th>
                                        <th>Tổng VAT</th>
                                        <th>Tổng ký bill</th>
                                        <th>Tổng tiền </th>
                                        <th>Tổng thanh toán</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($productRevenue as $item)
                                        <tr>
                                            <td>{{$item['name']}}</td>
                                            <td>{{$item['Quantity']}}</td>
                                            <td>{{number_format($item['TotalDiscount'])}}</td>
                                            <td>{{number_format($item['TotalVAT'])}}</td>
                                            <td>{{number_format($item['TotalSigned'])}}</td>
                                            <td>{{number_format($item['TotalSales'])}}</td>
                                            <td>{{number_format($item['TotaExtantlSales'])}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <!-- Data table JavaScript -->
    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="dist/js/dataTables-data.js?v=1"></script>
    <!-- Data table JavaScript -->
    <script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script>
        $('#revenue-product').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'print'
            ],
        });
    </script>
@endsection

