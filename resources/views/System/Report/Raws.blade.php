@extends('System.Layouts.master')
@section('css')
	<!-- Data table CSS -->
    <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" />
<style>
	body {
		color:black;
	}
	.text-weight {
		font-weight: bold;
	}
	.modal-dialog {
		min-width: 80%!important;
	}
</style>
@endsection
@section('content')
<div class="row">

	<div class="col-lg-12">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Nguyên liệu thô</h6>
					</div>
					<div class="clearfix"></div>
				</div>
                <div class="row" style="border-bottom: 1px solid #eee;">
                    <div class="col-md-12">
                        <form method="get" class="form-inline">
                            <div class="col-md-12 form-group">
                                <input class="form-control input-sm" type="date" name="from_date" >
                                <input class="form-control input-sm " type="date" name="to_date">
                                <button class="btn btn-success ">Lọc</button>
                            </div>
                        </form>
                    </div>
                </div>
				<div class="panel-body">
					<div class="table-wrap">
						<div class="">
                            <table  class="table table-striped table-bordered" id="revenue-ingredient" cellspacing="0" width="100%">
								<thead>
									<tr>

										<th>Mã hóa đơn</th>
										<th>Tên nguyên liệu</th>
                                        <th>Số lượng</th>
                                       <th>Đơn vị</th>
										<th>Ngày giờ</th>
										<th>Người tạo</th>
									</tr>
								</thead>
								<tbody>
                                @foreach($rawOrderList as $rawOrder)
                                    @foreach($rawOrder as $item)
                                        @foreach($item->raw as $raw)
                                            <tr>
                                                <td>{{$item->Bill_ID}}</td>
                                                <td>{{$raw['name']}}</td>
                                                <td>{{$raw['quantity']}}</td>
                                                <td>{{$raw['unit']}}</td>
                                                <td>{{$item->Bill_Time}}</td>
                                                <td>{{$item->name}}</td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                @endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('script')
	<!-- Data table JavaScript -->
	<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	<script src="vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script src="dist/js/responsive-datatable-data.js"></script>
    <script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
	<script>
        $(document).ready(function() {
            $('.table-bordered').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel', 'print'
                ],

            });
        });

	</script>
@endsection
