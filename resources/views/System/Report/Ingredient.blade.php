@extends('System.Layouts.master')
@section('css')
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        	<div class="col-lg-12">
        		<div class="panel panel-default border-panel card-view">
        			<div class="panel-wrapper collapse in">
        				<div class="panel-heading">
        					<div class="pull-left">
        						<h6 class="panel-title txt-dark">Nguyên liệu Tinh</h6>
        					</div>
        					<div class="clearfix"></div>
        				</div>
        				<div class="panel-body">
        					<div class="table-wrap">
        						<div class="">
                                    <table class="table table-striped table-bordered mb-0">
                                        <thead class="color-white-th">
        									<tr>
        										<th>Mã hóa đơn</th>
        										<th>Tên nguyên liệu</th>
                                                <th>Số lượng</th>
                                                <th>Đơn vị</th>
        										<th>Ngày giờ</th>
        										<th>Người tạo</th>
        									</tr>
        								</thead>
        								<tbody>
                                            @foreach($ingredientOrderList as $ingredientOrder)
                                                @foreach($ingredientOrder as $item)
                                                    @foreach($item->ingredient as $ing)
                                                        <tr>
                                                            <td>{{$item->Bill_ID}}</td>
                                                            <td>{{$ing['name']}}</td>
                                                            <td>{{$ing['quantity']}}</td>
                                                            <td>{{$ing['unit']}}</td>
                                                            <td>{{$item->Bill_Time}}</td>
                                                            <td>{{$item->name}}</td>
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                            @endforeach
        								</tbody>
        							</table>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>

    </div>

@endsection
@section('script')
    <script>

    </script>
@endsection
