@extends('System.Layouts.master')
@section('css')
    <style>
        .color-white-th {
            background: #ff6028;
        }

        .color-white-th tr th {
            color: white !important;
        }
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
    </style>
    <!-- Custom CSS -->
<link href="dist/css/style.css" rel="stylesheet" type="text/css">
<style>
    .color-white-th{
        background: #ff6028;
    }
    .color-white-th tr th{
        color: white!important;
    }
    td>a:hover{
        cursor: pointer;
    }
    table.dataTable thead .sorting::after, table.dataTable thead .sorting_asc::after, table.dataTable thead .sorting_desc::after{
        display: block!important;
    }
</style>
    <!-- Custom CSS -->
<link href="dist/css/style.css" rel="stylesheet" type="text/css">
<style>
    .color-white-th{
        background: #ff6028;
    }
    .color-white-th tr th{
        color: white!important;
    }
    td>a:hover{
        cursor: pointer;
    }
    table.dataTable thead .sorting::after, table.dataTable thead .sorting_asc::after, table.dataTable thead .sorting_desc::after{
        display: block!important;
    }
</style>

@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Doanh thu hàng giờ</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <a style="margin: 10px;" href="{{route('revenueExport')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table id="datatable_hour" class="table table-striped table-bordered mb-0">
                            <thead class="color-white-th">
                            <tr>
                                <th>Thời gian(Giờ)</th>
                                <th>Tổng giao dịch</th>
                                <th>Trung bình cộng</th>
                                <th>Tổng doanh thu</th>
                                <th>% Tổng doanh thu</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($dataHour as $d)

                                <tr>
                                    <td>{{$d['hour']}}</td>
                                    <td>{{$d['count']}}</td>
                                    <td>
                                        {{$d['count'] == 0 ? 0 : number_format($d['sales']/$d['count'])}}<sup>đ</sup>
                                    </td>
                                    <td>
                                        {{number_format($d['sales'])}}<sup>đ</sup>
                                    </td>
                                    <td>{{$totalSales == 0 ? 0 : number_format($d['sales']/$totalSales*100)}} %</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
<!-- Data table JavaScript -->
<script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="dist/js/dataTables-data.js?v=1"></script>
@endsection
