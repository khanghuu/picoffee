@extends('System.Layouts.master')
@section('title', 'Danh sách cửa hàng')
@section('css')
    <!-- Data table CSS -->
    <link href="../vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap Dropify CSS -->
    <link href="vendors/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css"/>

    <link href="vendors/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" type="text/css"/>

    <!-- Jasny-bootstrap CSS -->
    <link href="vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>

    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Danh sách cửa hàng</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                                <div class="col-md-6 col-sm-12 col-xs-12">
									<button class="btn btn-primary btn-rounded btn-icon left-icon" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive"> <i class="fa fa-plus"></i> <span>Thêm</span></button>
                                </div>
		                        @component('Components.modal-add-shop')
		                        @endcomponent()
                            @endif
                                <div class="col-md-6 col-sm-12 col-xs-12" style="text-align: right">
                                    <button class="btn btn-default btn-rounded btn-icon left-icon" id="btnLoc"> <i class="fa fa-filter"></i> <span>Lọc</span></button>
                                </div>
                                <!--Toggle filter product -->
                                @component('Components.toggle-filter-shop', ['shopIDs' =>$shopIDs])
                                @endcomponent
                                <!--/Toggle filter product -->
                        </div>
                        <div class="table-wrap mt-20">
                            <div class="table-responsive">
                                <table id="datable_2" class="table table-hover table-bordered display mb-30" >
                                    <thead>
                                    <tr>
                                        <th style="text-align: center">Mã</th>
                                        <th style="text-align: center">Tên</th>
                                        <th style="text-align: center">Địa chỉ</th>
                                        <th style="text-align: center">Ngày mở cửa</th>
                                        <th style="text-align: center">Trạng thái</th>
                                        @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                                            <th style="text-align: center">Hành động</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($row as $v)
                                        <tr>
                                            <td>{{ $v->Shop_ID }}</td>
                                            <td>{{ $v->Shop_Name }}</td>
                                            <td>{{ $v->Shop_Address }}</td>
                                            <td>{{ $v->created_at }}</td>
                                            <td>
                                                @if($v->Shop_Status == 1)
                                                    <span class="label label-success">Đang Mở Cửa</span>
                                                @elseif($v->Shop_Status == 0)
                                                    <span class="label label-info">Đã Đóng Cửa</span>
                                                @else
                                                    <span class="label label-info">Deleted</span>
                                                @endif
                                            </td>
                                            @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
		                                    <td><a href="#" title="Sửa" data-toggle="modal" data-target=".bs-edit-modal-lg" data-id="{{ $v->Shop_ID }}" class="openModel"><i class="fa fa-edit"></i></a> &nbsp <a href="{{ route('system.shop.getDelete', $v->Shop_ID) }}" onclick="return confirm('Bạn có chắc chắn')"><i class="fa fa-bitbucket"></i></a></td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Modal edit Category -->
    @component('Components.modal-edit-shop')
    @endcomponent
    <!--/Modal edit Category -->

@endsection
@section('script')
    <!-- Data table JavaScript -->
    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="dist/js/dataTables-data.js"></script>
    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="vendors/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>
    <script src="vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
    <!-- Form Flie Upload Data JavaScript -->
    <script src="dist/js/form-file-upload-data.js"></script>

    <!-- Slimscroll JavaScript -->
    <script src="dist/js/jquery.slimscroll.js"></script>


    <script>
        $( "#btnLoc" ).click(function() {
            $("#divLoc").toggle();
        });

        $(".openModel").click(function(){
            _id = $(this).data('id');
            $.ajax({
                url: '/system/shop/get-edit',
                type: "GET",
                dataType: "json",
                data: {id:_id},
                success: function (data){
	                    console.log(data);
                    if(data.status == false){
                        $.toast({
                            heading: 'Error',
                            text: data['message'],
                            position: 'top-right',
                            loaderBg:'#fec107',
                            icon: 'error',
                            hideAfter: 3500,
                            stack: 6
                        });
                    }else{
                        $('#editID').val(data.data.Shop_ID);
                        $('#editName').val(data.data.Shop_Name);
                        $('#editAddress').val(data.data.Shop_Address);
                        $('#editStatus').val(data.data.Shop_Status);
                    }
                }
            });
        });
    </script>
@endsection
