@extends('System.Layouts.master')
@section('title', 'Warehouse/Manage')
@section('css')
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
        td>a:hover{
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div class="panel panel-default border-panel card-view">

        <div class="panel-heading">
            <div class="pull-left">
                <h6 class="panel-title txt-dark">Thống kê nhập kho nguyên liệu thô</h6>
            </div>
            <div class="clearfix">

            </div>
        </div>

        <div class="panel-wrapper collapse in">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                            <a href="{{route('warehouseRawExport')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12" style="text-align: right">
                        <button class="btn btn-default btn-rounded btn-icon left-icon" id="btnLoc_Raw"> <i class="fa fa-filter"></i> <span>Lọc</span></button>
                    </div>
                    <!--Toggle filter material -->
                    <div class="col-md-12 col-sm-12 col-xs-12 mt-20" id="divLoc_Raw" style="display: none">
                            <div class="form-wrap">
                                <form class="form-horizontal" method="get">
                                    <div class="form-group">
                                        <label class="control-label mb-10 col-sm-2">Mã Nguyên Liệu:</label>
                                        <div class="col-sm-10">
                                            <input value="{{request()->get('warehouse_raw_number')}}" type="text" class="form-control" id="pwd_hr" name="warehouse_raw_number">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label mb-10 col-sm-2">Tên Nguyên Liệu:</label>
                                        <div class="col-sm-10">
                                            <input value="{{request()->get('warehouse_raw_name')}}" type="text" class="form-control" id="pwd_hr" name="warehouse_raw_name">
                                        </div>
                                    </div>
                                    @if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3)
                                    <div class="form-group">
                                        <label class="control-label mb-10 col-sm-2">Cửa hàng:</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="warehouse_raw_shop">
                                                <option value="">--- Chọn cửa hàng ---</option>
                                                @foreach($shop_list as $v)
                                                    <option {{$v->Shop_ID == request()->get('warehouse_raw_shop')?'selected':''}}  value="{{ $v->Shop_ID }}">{{ $v->Shop_Name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="form-group mb-0">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-orange btn-anim"><i class="fa fa-search"></i><span class="btn-text">Tìm kiếm</span></button>
                                            <a href="{{ route('system.warehouse.getManage') }}"><button type="button" class="btn btn-default btn-anim btn-anim"><i class="fa fa-times"></i><span class="btn-text">Huỷ</span></button></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--/Toggle filter material -->
                </div>
                <div class="table-wrap mt-20">
                    <div class="table-responsive">
                        <table id="datable_warehouse_raw" class="table table-hover table-bordered display mb-30 " >
                            <thead class="color-white-th">
                                <tr>
                                    <th>ID</th>
                                    <th>Mã</th>
                                    <th>Tên</th>
                                    <th>Số lượng tồn</th>
                                    <th>Số lượng nhập</th>
                                    <th>Đơn vị</th>
                                    @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                        <th>Cửa hàng</th>
                                    @endif
                                    <th>Trạng thái</th>
                                    @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                                        <th style="text-align: center">Hành động</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>

                            @foreach($warehouse_raw->get() as $v)
                                <tr>
                                    <td>{{ $v->Raw_Material_ID }}</td>
                                    <td>{{ $v->Raw_Material_Code }}</td>
                                    <td>{{ $v->Raw_Material_Name }}</td>
                                    <td>{{ number_format($v->Raw_Material_Quantity,3,",",".") }}</td>
                                    <td>{{ number_format($v->Warehouse_Total_Import,3,",",".") }}</td>
                                    <td>{{ $v->Raw_Material_Unit }}</td>
                                    @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                        <td>{{ $v->Shop_Name }}</td>
                                    @endif
                                    <td>
                                    @if($v->Raw_Material_Status)
                                        <span class="label label-success">Mở</span>
                                    @else
                                        <span class="label label-info">Đóng</span>
                                    @endif
                                    </td>
                                    @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                                        <td style="text-align: center">
                                            <a class="btn-open-edit-raw" data-value="{{$v->Raw_Material_ID}}"><i class="fa fa-edit"></i></a> &nbsp
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="modal-edit-raw" class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <form action="{{ route('system.warehouse.postEditRawManage') }}" method="post">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h5 class="modal-title" id="myLargeModalLabel">Sửa nguyên liệu thô trong kho</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="form-wrap">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                                    <input name="edit_raw_code" id="edit_raw_code" type="text" placeholder="Mã" class="form-control" disabled required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                                    <input name="edit_raw_name" id="edit_raw_name" type="text" placeholder="Tên" class="form-control" disabled required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-share-alt"></i></span>
                                                    <input name="edit_raw_quantity" id="edit_raw_quantity" type="text" placeholder="Số lượng" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input name="edit_raw_date" readonly="" type="text" class="form-control" value="{{ date('Y-m-d H:i:s') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                                                    <textarea style="min-width: 100%" id="edit_raw_description" rows="1" name="edit_raw_description" class="form-control" placeholder="Ghi chú"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-warning">Lưu</button>
                                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                                    <input type="hidden" name="edit_raw_id" id="edit_raw_id">
                                    <input type="hidden" name="edit_raw_shop" id="edit_raw_shop">
                                    {{ csrf_field() }}
                                </div>
                            </form>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            </div>

        </div>
    </div>
    <!--/Warehouse Manage -->
    <div class="panel panel-default border-panel card-view">

            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Thống kê nhập kho nguyên liệu tinh </h6>
                </div>
                <div class="clearfix">

                </div>
            </div>

            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <a href="{{route('warehouseIngExport')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12" style="text-align: right">
                            <button class="btn btn-default btn-rounded btn-icon left-icon" id="btnLoc_Ing"> <i class="fa fa-filter"></i> <span>Lọc</span></button>
                        </div>
                        <!--Toggle filter material -->
                        <div class="col-md-12 col-sm-12 col-xs-12 mt-20" id="divLoc_Ing" style="display: none">
                            <div class="form-wrap">
                                <form class="form-horizontal" method="get">
                                    <div class="form-group">
                                        <label class="control-label mb-10 col-sm-2">Mã Nguyên Liệu:</label>
                                        <div class="col-sm-10">
                                            <input value="{{request()->get('warehouse_ing_number')}}" type="text" class="form-control" id="pwd_hr" name="warehouse_ing_number">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label mb-10 col-sm-2">Tên Nguyên Liệu:</label>
                                        <div class="col-sm-10">
                                            <input value="{{request()->get('warehouse_ing_name')}}" type="text" class="form-control" id="pwd_hr" name="warehouse_ing_name">
                                        </div>
                                    </div>
                                    @if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3)
                                    <div class="form-group">
                                        <label class="control-label mb-10 col-sm-2">Cửa hàng:</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="warehouse_ing_shop">
                                                <option value="">--- Chọn cửa hàng ---</option>
                                                @foreach($shop_list as $v)
                                                    <option {{$v->Shop_ID == request()->get('warehouse_ing_shop')?'selected':''}}  value="{{ $v->Shop_ID }}">{{ $v->Shop_Name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="form-group mb-0">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-orange btn-anim"><i class="fa fa-search"></i><span class="btn-text">Tìm kiếm</span></button>
                                            <a href="{{ route('system.warehouse.getManage') }}"><button type="button" class="btn btn-default btn-anim btn-anim"><i class="fa fa-times"></i><span class="btn-text">Huỷ</span></button></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--/Toggle filter material -->
                    </div>
                    <div class="table-wrap mt-20">
                        <div class="table-responsive">
                            <table id="datable_warehouse_ing" class="table table-hover table-bordered display mb-30 " >
                                <thead class="color-white-th">
                                    <tr>
                                        <th>ID</th>
                                        <th>Mã</th>
                                        <th>Tên</th>
                                        <th>Số lượng tồn</th>
                                        <th>Số lượng nhập</th>
                                        <th>Đơn vị</th>
                                        @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                            <th>Cửa hàng</th>
                                        @endif
                                        <th>Trạng thái</th>
                                        @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                                            <th style="text-align: center">Hành động</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>

                                @foreach($warehouse_ing->get() as $v)
                                    <tr>
                                        <td>{{ $v->Ingredient_ID }}</td>
                                        <td>{{ $v->Ingredient_Code }}</td>
                                        <td>{{ $v->Ingredient_Name }}</td>
                                        <td>{{ number_format($v->Ingredient_Quantity,3,",",".") }}</td>
                                        <td>{{ number_format($v->Warehouse_Total_Import,3,",",".") }}</td>
                                        <td>{{ $v->Ingredient_Unit }}</td>
                                        @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                            <td>{{ $v->Shop_Name }}</td>
                                        @endif
                                        <td>
                                        @if($v->Ingredient_Status)
                                            <span class="label label-success">Mở</span>
                                        @else
                                            <span class="label label-info">Đóng</span>
                                        @endif
                                        </td>
                                        @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                                            <td style="text-align: center">
                                                <a class="btn-open-edit-ing" data-value="{{$v->Ingredient_ID}}"><i class="fa fa-edit"></i></a> &nbsp
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="modal-edit-ing" class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form action="{{ route('system.warehouse.postEditIngManage') }}" method="post">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h5 class="modal-title" id="myLargeModalLabel">Sửa nguyên liệu tinh trong kho</h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-wrap">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                                        <input name="edit_ing_code" id="edit_ing_code" type="text" placeholder="Mã" class="form-control" disabled required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                                        <input name="edit_ing_name" id="edit_ing_name" type="text" placeholder="Tên" class="form-control" disabled required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-share-alt"></i></span>
                                                        <input name="edit_ing_quantity" id="edit_ing_quantity" type="text" placeholder="Số lượng" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input name="edit_ing_date" readonly="" type="text" class="form-control" value="{{ date('Y-m-d H:i:s') }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                                                        <textarea style="min-width: 100%" id="edit_ing_description" rows="1" name="edit_ing_description" class="form-control" placeholder="Ghi chú"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-warning">Lưu</button>
                                        <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                                        <input type="hidden" name="edit_ing_id" id="edit_ing_id">
                                        <input type="hidden" name="edit_ing_shop" id="edit_ing_shop">
                                        {{ csrf_field() }}
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                </div>

            </div>
        </div>
        <!--/Warehouse Manage -->
@endsection
@section('script')
    <!-- Data table JavaScript -->
    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="dist/js/dataTables-data.js"></script>
    <script>
        $( "#btnLoc_Raw" ).click(function() {
            $("#divLoc_Raw").toggle();
        });
        $( "#btnLoc_Ing" ).click(function() {
            $("#divLoc_Ing").toggle();
        });
        $(".openModel").click(function(){
            _id = $(this).data('id');
            $.ajax({
                url: '/system/data/category/get',
                type: "GET",
                dataType: "json",
                data: {id:_id},
                success: function (data){
                    if(data['status'] == false){
                        $.toast({
                            heading: 'Error',
                            text: data['message'],
                            position: 'top-right',
                            loaderBg:'#fec107',
                            icon: 'error',
                            hideAfter: 3500,
                            stack: 6
                        });
                    }else{
                        $('input[name=EditName]').val(data['data']['Category_Name']);
                        $('input[name=EditOrder]').val(data['data']['Category_Order']);
                        $('input[name=editId]').val(data['data']['Category_ID']);
                        $("select[name=EditShop] > option").each(function() {

                            if($(this).val() == data['data']['Category_Shop']){
                                $(this).attr('selected', true);
                            }
                        });
                        if(data['data']['Category_Status'] == 1){
                            $('input[name=Editstatus]').attr('checked', true);
                        }
                    }
                }
            });
        });
        $('.btn-open-edit-raw').click(function () {
            _id = $(this).attr('data-value');
            $.ajax({
                url: "{{route('system.warehouse.getEditRawManage')}}",
                type: 'GET',
                dateType: 'json',
                data: {edit_raw_id: _id},
                success: function (data) {
                    if(data.status) {
                        $('#edit_raw_code').val(data.dataResponse.Raw_Material_Code);
                        $('#edit_raw_name').val(data.dataResponse.Raw_Material_Name);
                        $('#edit_raw_quantity').val(currencyFormatDE(parseFloat(data.dataResponse.Raw_Material_Quantity,3)));
                        $('#edit_raw_id').val(data.dataResponse.Raw_Material_ID);
                        $('#edit_raw_shop').val(data.dataResponse.Raw_Material_Shop);
                    }
                }
            });
            $('#modal-edit-raw').modal('show');
        });

        $('.btn-open-edit-ing').click(function () {
            _id = $(this).attr('data-value');
            $.ajax({
                url: "{{route('system.warehouse.getEditIngManage')}}",
                type: 'GET',
                dateType: 'json',
                data: {edit_ing_id: _id},
                success: function (data) {
                    console.log(data);
                    if(data.status) {
                        $('#edit_ing_code').val(data.dataResponse.Ingredient_Code);
                        $('#edit_ing_name').val(data.dataResponse.Ingredient_Name);
                        $('#edit_ing_quantity').val(currencyFormatDE(parseFloat(data.dataResponse.Ingredient_Quantity,3)));
                        $('#edit_ing_id').val(data.dataResponse.Ingredient_ID);
                        $('#edit_ing_shop').val(data.dataResponse.Ingredient_Shop);
                    }
                }
            });
            $('#modal-edit-ing').modal('show');
        });
    </script>
    <script>
    function currencyFormatDE(num) {
        return (
            num
            .toFixed(3)
            .replace('.', ',')
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
        )
    }

    </script>

    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>
@endsection
