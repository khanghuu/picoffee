@extends('System.Layouts.master')
@section('title', 'Danh sách huỷ nguyên liệu tinh')
@section('css')
    <!-- select2 CSS -->
		<link href="vendors/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>

		<!-- switchery CSS -->
		<link href="vendors/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" type="text/css"/>

		<!-- bootstrap-select CSS -->
		<link href="vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>

		<!-- bootstrap-tagsinput CSS -->
		<link href="vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" type="text/css"/>

		<!-- bootstrap-touchspin CSS -->
		<link href="vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css"/>

		<!-- multi-select CSS -->
		<link href="vendors/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css"/>

		<!-- Bootstrap Switches CSS -->
		<link href="vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>

		<!-- Bootstrap Datetimepicker CSS -->
		<link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
        a:hover{
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Danh sách huỷ nguyên liệu tinh</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                @if(session('user')->User_Level == 1 || session('user')->User_Level == 2)
                                    <button class="btn btn-primary btn-rounded btn-icon left-icon" data-toggle="modal" data-target="#add-destroy-ingredient" class="model_img img-responsive"> <i class="fa fa-plus"></i> <span>Thêm</span></button>
                                    <!-- Modal adding Destroy Ingredient warehouse -->
                                    @component('System.Warehouse.Destroy-Ingredient.Component.modal-add-destroy-ingredient-warehouse', ['shopList' => $shopList, 'ingredientList' => $ingredientList])
                                    @endcomponent
                                    <!-- Modal adding Destroy Ingredient warehouse -->
                                @endif
                                <a style="margin: 10px;" href="{{route('destroyIngredientExport')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12" style="text-align: right">
                                <button class="btn btn-default btn-rounded btn-icon left-icon" id="btn-search-destroy-ingredient"> <i class="fa fa-filter"></i> <span>Lọc</span></button>
                            </div>
                            <!--Toggle Filter Destroy Ingredient Warehouse -->
                            @component('System.Warehouse.Destroy-Ingredient.Component.toggle-filter-destroy-ingredient-warehouse', ['unitList' => $unitList, 'shopList' => $shopList])
                            @endcomponent
                        </div>
                        <div class="table-wrap mt-20">
                            <div class="table-responsive">
                                <table id="datable_destroy_ing" class="table table-hover table-bordered display mb-30" style="text-align: center">
                                    <thead class="color-white-th">
                                    <tr>
                                        <th style="text-align: center">ID</th>
                                        <th style="text-align: center">Người huỷ</th>
                                        <th style="text-align: center">Tên nguyên liệu</th>
                                        <th style="text-align: center">Số lượng</th>
                                        <th style="text-align: center">Đơn vị</th>
                                        <th style="text-align: center">Ghi chú</th>
                                        <th>Trạng thái</th>
                                        <th style="text-align: center">Ngày giờ</th>
                                        @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                            <th style="text-align: center">Cửa hàng</th>
                                        @endif
                                        @if(session('user')->User_Level == 1 || session('user')->User_Level == 2)
                                            <th style="text-align: center">Hành động</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($destroyIngredientList as $destroyIngredient)
                                        <tr>
                                            <td>{{$destroyIngredient->Warehouse_ID}}</td>
                                            <td>{{$destroyIngredient->User_Name}}</td>
                                            <td>{{$destroyIngredient->Ingredient_Name}}</td>
                                            <td>{{abs($destroyIngredient->Warehouse_Quantity)}}</td>
                                            <td>{{$destroyIngredient->Ingredient_Unit}}</td>
                                            <td>{{$destroyIngredient->Warehouse_Description}}</td>
                                            @if($destroyIngredient->Warehouse_Status == 1)
                                                <td><span class="label label-success">Kích hoạt</span></td>
                                            @elseif($destroyIngredient->Warehouse_Status == 0)
                                                <td><span class="label label-danger">Hủy</span></td>
                                            @endif
                                            <td>{{$destroyIngredient->Updated_At}}</td>
                                            @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                                <td>{{$destroyIngredient->Shop_Name}}</td>
                                            @endif
                                            @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                                                <td>
                                                    <a class="btn-open-edit-destroy-ingredient" data-value="{{$destroyIngredient->Warehouse_ID}}"><i class="fa fa-edit"></i></a> &nbsp
                                                    @if($destroyIngredient->Warehouse_Status == 1)
                                                        <a href="{{ route('system.warehouse.postDeleteDestroyIngredient', $destroyIngredient->Warehouse_ID) }}" onclick="return confirm('Bạn có chắc chắn')"><i class="fa fa-bitbucket"></i></a>
                                                    @else
                                                        <a href="{{ route('system.warehouse.postEnableDestroyIngredient', $destroyIngredient->Warehouse_ID) }}" onclick="return confirm('Bạn có chắc chắn')"><i class="fa fa-arrow-circle-o-up"></i></a>
                                                    @endif
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                                <div style="text-align: right;">
                                    {{ $destroyIngredientList->appends(request()->input())->onEachSide(1)->links() }}
                                </div>

                            </div>
                        </div>
                        <!--Modal Edit Destroy Ingredient Warehouse -->
                        @component('System.Warehouse.Destroy-Ingredient.Component.modal-edit-destroy-ingredient-warehouse', ['shopList' => $shopList])
                        @endcomponent
                        <!--/Modal Edit Destroy Ingredient Warehouse -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <!-- Switchery JavaScript -->
    <script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

    <!-- Select2 JavaScript -->
    <script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>

    <!-- Bootstrap Select JavaScript -->
    <script src="vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

    <!-- Bootstrap Tagsinput JavaScript -->
    <script src="vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

    <!-- Bootstrap Touchspin JavaScript -->
    <script src="vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Multiselect JavaScript -->
    <script src="vendors/bower_components/multiselect/js/jquery.multi-select.js"></script>
    <script>

        $('#add_destroy_ingredient_shop').change(function () {
            shop_id = $(this).val();
            $.ajax({
                url: '{{route('system.warehouse.getDataDestroyIngredient')}}',
                type: "GET",
                dataType: "json",
                data: {shop_id: shop_id},
                success: function (data) {
                    if(data.status) {
                        $('#add_destroy_ingredint_quantity').val(0);
                        $('#add_destroy_ingredint_status').prop('selectedIndex',0);
                        $("#add_destroy_ingredient_ingredient_id").empty();
                        $("#add_destroy_ingredient_ingredient_id").append(new Option('----Chọn nguyên liệu----', null));
                        $.each(data.dataResponse, function(index, ingredient) {
                            $("#add_destroy_ingredient_ingredient_id").append(new Option(ingredient.Ingredient_Name + '(' + ingredient.Ingredient_Unit +')', ingredient.Ingredient_ID));
                        });
                    }
                }
            });
        });

        $( "#btn-search-destroy-ingredient" ).click(function() {
            $("#search-destroy-ingredient").toggle();
        });

        $(".btn-open-edit-destroy-ingredient").click(function(){
            warehouse_id = $(this).attr('data-value');
            $.ajax({
                url: '{{ route('system.warehouse.getDataEditDestroyIngredient') }}',
                type: "GET",
                dataType: "json",
                data: {warehouse_id: warehouse_id},
                success: function (data){
                    if(data['status']){
                        $('#edit_destroy_ingredint_quantity').val(Math.abs(data.dataResponse.Warehouse_Quantity));
                        $('#edit_destroy_ingredint_description').val(data.dataResponse.Warehouse_Description);
                        $('#edit_destroy_ingredient_shop').append(new Option(data.dataResponse.Shop_Name, null));
                        $('#edit_destroy_ingredient_ingredient_id').val(warehouse_id);
                        $('#edit_destroy_ingredient_id').val(data.dataResponse.Warehouse_ID);
                        var warehouse_ingredient = data.dataResponse.Warehouse_Ingredient;
                        $.ajax({
                            url: '{{route('system.warehouse.getDataDestroyIngredient')}}',
                            type: "GET",
                            dataType: "json",
                            data: {shop_id: data.dataResponse.Shop_ID},
                            success: function (data) {
                                if(data.status) {
                                    $("#edit_destroy_ingredient_ingredient_id").empty();
                                    $("#edit_destroy_ingredient_ingredient_id").append(new Option('----Chọn nguyên liệu----', null));
                                    $.each(data.dataResponse, function(index, ingredient) {
                                        $("#edit_destroy_ingredient_ingredient_id").append(new Option(ingredient.Ingredient_Name + '(' + ingredient.Ingredient_Unit +')', ingredient.Ingredient_ID));
                                    });
                                    $('#edit_destroy_ingredient_ingredient_id').val(warehouse_ingredient).prop('selected', true);
                                }
                            }
                        });
                    }
                }
            });

            $('#edit-destroy-ingredient').modal('show');
        });

    </script>

    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="dist/js/dataTables-data.js?v=1"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>
@endsection
