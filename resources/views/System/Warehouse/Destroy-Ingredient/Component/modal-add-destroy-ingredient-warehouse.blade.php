<div class="modal fade bs-example-modal-lg" id="add-destroy-ingredient" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.warehouse.postDestroyIngredient') }}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Thêm nguyên liệu tinh bị huỷ</h5>
                </div>
                <div class="modal-body">
                    <div class="form-wrap">
                        <div class="row">
                            <!---- --->
                            @if(session('user')->User_Level == 1)
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-shopping-basket"></i></span>
                                        <select class="form-control select2" name="add_destroy_ingredient_shop" id="add_destroy_ingredient_shop">
                                            <option value="" selected> ---Chọn quán--- </option>
                                            @foreach($shopList as $shop)
                                                <option value="{{$shop->Shop_ID}}">{{$shop->Shop_Name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <!---- --->
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                        <select class="form-control select2" name="add_destroy_ingredient_ingredient_id" id="add_destroy_ingredient_ingredient_id">

                                        </select>
                                    </div>
                                </div>
                            @endif
                            @if(session('user')->User_Level == 2)
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                        <select class="form-control select2" name="add_destroy_ingredient_ingredient_id" id="add_destroy_ingredient_ingredient_id">
                                            <option value="" selected>----Chọn nguyên liệu----</option>
                                            @foreach($ingredientList as $ingredent)
                                                <option value="{{$ingredent->Ingredient_ID}}">{{$ingredent->Ingredient_Name}}({{$ingredent->Ingredient_Unit}})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <!---- --->
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-calculator"></i></span>
                                    <input name="add_destroy_ingredint_quantity" id="add_destroy_ingredint_quantity" type="number" placeholder="Số lượng" class="form-control" step="any">
                                </div>
                            </div>
{{--                            <!---- --->--}}
{{--                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">--}}
{{--                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-spinner"></i></span>--}}
{{--                                    <select class="form-control select2" name="add_destroy_ingredint_status" id="add_destroy_ingredint_status">--}}
{{--                                        <option value="" selected> ---Trạng thái--- </option>--}}
{{--                                        <option value="1">Đã hủy</option>--}}
{{--                                        <option value="0">Bỏ hủy</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}

                            <!---- --->
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group" style="float: left">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input readonly=""  type="text" class="form-control" value="{{ date('Y-m-d H:i:s') }}">
                                </div>
                            </div>

                            <!---- --->
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15" style="width: 100%">
                                    <textarea style="min-width: 100%" rows="3" name="add_destroy_ingredint_description" class="form-control" placeholder="Ghi chú"></textarea>
                                </div>
                            </div>
                            <!----/ --->

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning">Lưu</button>
                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                    {{ csrf_field() }}
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
