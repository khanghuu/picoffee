<div id="edit-destroy-ingredient" class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.warehouse.postEditDestroyIngredient') }}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Sửa hủy nguyên liệu tinh</h5>
                </div>
                <div class="modal-body">
                    <div class="form-wrap">
                        <div class="row">
                            <!---- --->
                            @if(session('user')->User_Level == 1)
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-shopping-basket"></i></span>
                                        <select class="form-control select2" name="edit_destroy_ingredient_shop" id="edit_destroy_ingredient_shop" readonly disabled>

                                        </select>
                                    </div>
                                </div>
                                <!---- --->
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                        <select class="form-control select2" name="edit_destroy_ingredient_ingredient_id" id="edit_destroy_ingredient_ingredient_id">

                                        </select>
                                    </div>
                                </div>
                            @endif
                            @if(session('user')->User_Level == 2)
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                        <select class="form-control select2" name="edit_destroy_ingredient_ingredient_id" id="edit_destroy_ingredient_ingredient_id">

                                        </select>
                                    </div>
                                </div>
                            @endif
                        <!---- --->
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-calculator"></i></span>
                                    <input name="edit_destroy_ingredint_quantity" id="edit_destroy_ingredint_quantity" type="number" placeholder="Số lượng" class="form-control" step="any">
                                </div>
                            </div>
                            <!---- --->

                            <!---- --->
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group" style="float: left">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input readonly="" type="text" class="form-control" value="{{ date('Y-m-d H:i:s') }}">
                                </div>
                            </div>

                            <!---- --->
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15" style="width: 100%">
                                    <textarea style="min-width: 100%" rows="3" name="edit_destroy_ingredint_description" id="edit_destroy_ingredint_description" class="form-control" placeholder="Ghi chú"></textarea>
                                </div>
                            </div>
                            <!----/ --->

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning">Lưu</button>
                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                    <input type="hidden" name="edit_destroy_ingredient_id" id='edit_destroy_ingredient_id' value="">
                    {{ csrf_field() }}
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
