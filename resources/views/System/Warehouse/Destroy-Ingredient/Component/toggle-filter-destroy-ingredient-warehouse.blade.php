<div class="col-md-12 col-sm-12 col-xs-12 mt-20" id="search-destroy-ingredient" style="display: none;" >
    <div class="form-wrap">
        <form class="form-horizontal" method="GET">
            <div class="form-group" >
                <label class="control-label mb-10 col-sm-2">ID:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="search_destroy_ingredient_id" name="search_destroy_ingredient_id" placeholder="Nhập ID" value="{{request()->get('search_destroy_ingredient_id')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Người hủy:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="search_destroy_ingredient_staff" name="search_destroy_ingredient_staff" placeholder="Nhập người hủy" value="{{request()->get('search_destroy_ingredient_staff')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Tên nguyên liệu:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="search_destroy_ingredient_name" name="search_destroy_ingredient_name" placeholder="Nhập tên nguyên liệu" value="{{request()->get('search_destroy_ingredient_name')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Đơn vị:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="search_destroy_ingredient_unit">
                        <option value="" disabled selected>----Chọn đơn vị----</option>
                        @foreach($unitList as $unit)
                            <option value="{{$unit->Ingredient_Unit}}" {{$unit->Ingredient_Unit == request()->get('search_destroy_ingredient_unit') ? 'selected':''}}>{{$unit->Ingredient_Unit}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Cửa hàng:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="search_destroy_ingredient_shop">
                        <option value="" disabled selected>----Chọn cửa hàng----</option>

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Trạng thái:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="search_destroy_ingredient_status">
                        <option value="" selected>----Chọn trạng thái----</option>
                        <option value="1" {{request()->get('search_destroy_ingredient_status') === "1" ? 'selected':''}}>Đã hủy</option>
                        <option value="0" {{request()->get('search_destroy_ingredient_status') === "0" ? 'selected':''}}>Bỏ hủy</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Từ ngày:</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" id="search_destroy_ingredient_from" name="search_destroy_ingredient_from" value="{{request()->get('search_destroy_ingredient_from')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Đến ngày:</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" id="search_destroy_ingredient_to" name="search_destroy_ingredient_to" value="{{request()->get('search_destroy_ingredient_to')}}">
                </div>
            </div>
            <div class="form-group mb-0">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-orange btn-anim"><i class="fa fa-search"></i><span class="btn-text">Tìm kiếm</span></button>
                    <a href="{{  route('system.warehouse.getDestroyIngredient') }}"><button type="button" class="btn btn-default btn-anim"><i class="fa fa-search"></i><span class="btn-text">Huỷ</span></button></a>
                </div>
            </div>

        </form>
    </div>
</div>
