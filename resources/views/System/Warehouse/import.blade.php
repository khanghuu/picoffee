@extends('System.Layouts.master')
@section('title', 'Warehouse/Import')
@section('css')
    <link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
        .pagination {
            float:right;
        }
        #datable_warehouse_history:hover{
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Bảng Nhập Kho Hàng</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <form action="{{ route('system.warehouse.postAdd') }}" method="post">
                                @csrf
                                <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                    <label class="control-label mb-10 text-left">Số chứng từ <i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                                    <input type="text" class="form-control" placeholder="Nhập số chứng từ" name="warehouse_number" readonly value="{{$import_number}}">
                                </div>
                                <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                    <label class="control-label mb-10 text-left">Người Nhập <i class="fa fa-user" aria-hidden="true"></i></label>
                                    <input type="text" class="form-control"  name="warehouse_user" readonly value="{{$user->User_Name}}">
                                </div>
                                <input type="hidden" name="userid" value="{{$user->User_ID}}">
                                @if(Session('user')->User_Level==1)
                                    <div class="form-group mt-30 mb-30 col-md-12 col-sm-12 col-xs-12">
                                        <label class="control-label mb-10 text-left">Cửa Hàng <span style="color:red">* </span><i class="fa fa-coffee" aria-hidden="true"></i></label>
                                        <select class="form-control" name="warehouse_shop" id="warehouse_shop">
                                            <option value="0" selected disabled>--- Chọn cừa hàng ---</option>
                                            @foreach ($shop_list as $item)
                                                <option value="{{$item->Shop_ID}}">{{$item->Shop_Name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                                <span  id='select_raw' >
                                    <div class="col-md-12 col-sm-12 col-xs-12"><label class="control-label mb-10 text-left">Nguyên liệu thô <i class="fa fa-glass" aria-hidden="true"></i></label></div>
                                    <div id="warehouse_raw_array"></div>
                                    <div class="form-group mt-30 mb-30 col-md-12 col-sm-12 col-xs-12">
                                        <select class="form-control" name="warehouse_raw" id="warehouse_raw">
                                            <option value="" selected disabled>--- Chọn nguyên liệu thô ---</option>
                                            @if(Session('user')->User_Level==2)
                                                @foreach ($raw_list as $item)
                                                    <option value="{{$item->Raw_Material_ID}}">{{$item->Raw_Material_Name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </span>
                                <span id='select_ing'>
                                    <div class="col-md-12 col-sm-12 col-xs-12"><label class="control-label mb-10 text-left">Nguyên liệu tinh <i class="fa fa-glass" aria-hidden="true"></i></label></div>
                                    <div id="warehouse_ingredient_array"></div>
                                    <div class="form-group mt-30 mb-30 col-md-12 col-sm-12 col-xs-12">
                                        <select class="form-control" name="warehouse_ingredient" id="warehouse_ingredient">
                                            <option value="0" selected disabled>--- Chọn nguyên liệu tinh ---</option>
                                            @if(Session('user')->User_Level==2)
                                                @foreach ($ing_list as $item)
                                                    <option value="{{$item->Ingredient_ID}}">{{$item->Ingredient_Name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </span>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label mb-10 text-left">Tổng tiền <i class="fa fa-usd" aria-hidden="true"></i></label>
                                    <input step="0.00000001" value="0" type="text" class="form-control" id="warehouse_total" readonly  name="warehouse_total">
                                </div>
                                <div id="warehouse_ingredient_array"></div>
                                <div class="form-group mt-30 mb-30 col-md-12 col-sm-12 col-xs-12" id="show_hinh_thuc" style="display: none">
                                    <label class="control-label mb-10 text-left">Hình thức <i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                                    <select required class="form-control" name="warehouse_status" id="warehouse_status" >
                                        <option value="0" selected disabled>--- Chọn hình thức ---</option>
                                        <option value="1" >Nhập nguyên liệu tinh</option>
                                        <option value="2" >Chế biến nguyên liệu tinh</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label mb-10 text-left">Ghi Chú <i class="fa fa-commenting-o" aria-hidden="true"></i></label>
                                    <textarea class="form-control" rows="5" name="warehouse_description"></textarea>
                                </div>
                                <div class="form-group pull-right">
                                        <a href="{{ route('warehouseExportTemplate') }}" class="btn01 btn-lg btn-warning">File mẫu nhập nguyên liệu <i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                                        <a id="btn-import" href="javascript:void(0)" class="btn01 btn-lg btn-primary">Nhập nguyên liệu (Excel) <i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                                        <button id="btn-them" type="submit" class="btn01 btn-lg btn-success">Nhập kho <i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
    <div id="modal-import-ing" class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form action="{{ route('warehouseImport') }}" method="post" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h5 class="modal-title" id="myLargeModalLabel">Chọn file nhập nguyên liệu</h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-wrap">
                            <div class="row">
                                @if(Session('user')->User_Level == 1)
                                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                        <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-coffee"></i></span>
                                            <select required class="form-control" name="warehouse_import_shop">
                                                <option value="" disabled selected>--- Chọn cửa hàng ---</option>
                                                @foreach($shop_list as $v)
                                                    <option value="{{ $v->Shop_ID }}">{{ $v->Shop_Name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-share-alt"></i></span>
                                        <select required class="form-control" name="warehouse_import_material">
                                            <option value="" disabled selected>--- Chọn loại nguyên liệu ---</option>
                                            <option value="1">Nguyên liệu thô</option>
                                            <option value="2">Nguyên liệu tinh</option>v
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                        <input required type="file" name="file">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Nhập kho</button>
                        <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                        {{ csrf_field() }}
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="dist/js/dataTables-data.js"></script>
    <script>
        $('#btn-import').on('click', function () {
            $('#modal-import-ing').modal('show');
        });
        jQuery(function($){
            $('.table').footable();
        });
        function currencyFormatDE(num) {
            return (
                num
                .toFixed(3)
                .replace('.', ',')
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            )
        }

        $( "#btnLoc" ).click(function() {
            $("#divLoc").toggle();
        });
        @if(Session('user')->User_Level != 2)
            $('#select_raw').hide();
            $('#select_ing').hide();
        @endif
        let arr_select_raw = "";
        let arr_select_ing = "";
        function Total(){
            let total = 0;
            let temp = 0;
            let arr_raw_quantity = [];
            let arr_raw_price = [];
            let arr_ing_quantity = [];
            let arr_ing_price = [];
            $('.raw_input_quantity').each(function () {
                temp = $(this).val();
                if (isNaN(temp))
                {
                    alert("Bạn phải nhập số");
                    $(this).val(0);
                    return false;
                }
                arr_raw_quantity.push(temp);
            });
            $('.raw_input_price').each(function () {
                temp = $(this).val();
                if (isNaN(temp))
                {
                    alert("Bạn phải nhập số");
                    $(this).val(0);
                    return false;
                }
                arr_raw_price.push(temp);
            });
            $('.ing_input_quantity').each(function () {
                temp = $(this).val();
                if (isNaN(temp))
                {
                    alert("Bạn phải nhập số");
                    $(this).val(0);
                    return false;
                }
                arr_ing_quantity.push(temp);
            });
            $('.ing_input_price').each(function () {
                temp = $(this).val();
                if (isNaN(temp))
                {
                    alert("Bạn phải nhập số");
                    $(this).val(0);
                    return false;
                }
                arr_ing_price.push(temp);
            });
            for (let i = 0; i < arr_raw_quantity.length; i++) {
                total += arr_raw_quantity[i] * arr_raw_price[i];
            }
            for (let i = 0; i < arr_ing_quantity.length; i++) {
                total += arr_ing_quantity[i] * arr_ing_price[i];
            }
            $('#warehouse_total').val(currencyFormatDE(total));
        }

        $('#warehouse_raw_array').on("keyup", ".raw_input_quantity", function(){
            Total();
        });

        $('#warehouse_raw_array').on("keyup", ".raw_input_price", function(){
            Total();
        });

        $('#warehouse_ingredient_array').on("keyup", ".ing_input_quantity", function(){
            Total();
        });

        $('#warehouse_ingredient_array').on("keyup", ".ing_input_price", function(){
            Total();
        });
        @if(Session('user')->User_Level==1)
            $("select[name=warehouse_shop]").change(function(){
            $('#select_raw').show();
            $('#select_ing').show();
            $("#show_hinh_thuc").hide();
            $('#warehouse_raw_array').html("");
            $('#warehouse_ingredient_array').html("");
            let _id = $(this).val();
            if(_id!=''){
                $.ajax({
                    url: '{{ route('system.warehouse.getSelectShopImportAjax') }}',
                    type: "GET",
                    dataType: "json",
                    data: {id:_id},
                    success: function (data){
                        var select_raw = $('#warehouse_raw');
                        select_raw.empty().append('<option value="">--- Chọn nguyên liệu thô ---</option>');
                        data['ware_raw'].forEach(element => {
                            select_raw.append('<option value="'+element.Raw_Material_ID+'">'+element.Raw_Material_Name+' ('+element.Raw_Material_Unit+')</option>');
                        });

                        var select_ingredient = $('#warehouse_ingredient');
                        select_ingredient.empty().append('<option value="">--- Chọn nguyên liệu tinh ---</option>');
                        data['ware_ingredient'].forEach(element => {
                            select_ingredient.append('<option value="'+element.Ingredient_ID+'">'+element.Ingredient_Name+' ('+element.Ingredient_Unit+')</option>');
                        });
                    }
                });
            }
        });
        @endif
        $("select[id=warehouse_raw]").change(function(){
            let _id = $(this).val();
            if(arr_select_raw.search(_id+",") == -1){
                arr_select_raw+=_id+",";
                if(_id!=''){
                    $.ajax({
                        url: '{{ route('system.warehouse.getSelectRawImportAjax') }}',
                        type: "GET",
                        dataType: "json",
                        data: {id:_id},
                        success: function (data){
                            if(data.status){
                                $('#warehouse_raw_array').append('<div class="col-md-6 col-sm-12 col-xs-12 form-group  item'+data['dataResponse']['Raw_Material_ID']+'"><div class="input-group mb-15"> <span class="input-group-addon">'+data['dataResponse']['Raw_Material_Name']+'</span><input value="0" name="quantity_raw['+data['dataResponse']['Raw_Material_ID']+']" type="number" placeholder="Số lượng nguyên liệu thô cần thiết" min="0.00000001" step="0.00000001" required class="form-control raw_input_quantity"><span class="input-group-addon" class="unit">'+data['dataResponse']['Raw_Material_Unit']+'</span><span class="input-group-addon deleteItem unit" style="background: red; color: #fff;cursor: pointer;" data-id="'+data['dataResponse']['Raw_Material_ID']+'"><i class="ti-trash"></i></span></div></div><div class="col-md-6 col-sm-12 col-xs-12 form-group  item'+data['dataResponse']['Raw_Material_ID']+'"><div class="input-group mb-15"> <span class="input-group-addon">Đơn giá</span><input value="0" min="0.00000001" step="0.00000001" required name="price_raw['+data['dataResponse']['Raw_Material_ID']+']" type="number" placeholder="" class="form-control raw_input_price"></div></div>');
                            }
                        }
                    });
                }
            }
            else{
                alert("Nguyên liệu thô đã được thêm");
            }
        });
        $("#warehouse_raw_array").on('click', '.deleteItem', function (){
            let _id = $(this).data('id');
            $('#warehouse_raw_array .item'+_id).remove();
            arr_select_raw = arr_select_raw.replace(_id+",", "");
            if($('#warehouse_raw_array').html() == ""){
                $("#show_hinh_thuc").hide();
            }
            Total();
        });
        $("select[id=warehouse_ingredient]").change(function(){
            let _id = $(this).val();
            if(arr_select_ing.search(_id+",") == -1){
                arr_select_ing+=_id+",";
                if(_id!=''){
                    $.ajax({
                        url: '{{ route('system.warehouse.getSelectIngredientImportAjax') }}',
                        type: "GET",
                        dataType: "json",
                        data: {id:_id},
                        success: function (data){
                            if(data.status){
                                if($('#warehouse_ingredient_array').html() == ""){
                                    $("#show_hinh_thuc").hide();
                                }
                                $("#show_hinh_thuc").show();
                                $('#warehouse_ingredient_array').append('<div class="col-md-6 col-sm-12 col-xs-12 form-group  item'+data['dataResponse']['Ingredient_ID']+'"><div class="input-group mb-15"> <span class="input-group-addon">'+data['dataResponse']['Ingredient_Name']+'</span><input value="0" min="0.00000001" step="0.00000001" required name="quantity_ingredient['+data['dataResponse']['Ingredient_ID']+']" type="number" placeholder="Số lượng nguyên liệu thô cần thiết" class="form-control ing_input_quantity"><span class="input-group-addon" class="unit">'+data['dataResponse']['Ingredient_Unit']+'</span><span class="input-group-addon deleteItem unit" style="background: red; color: #fff;cursor: pointer;" data-id="'+data['dataResponse']['Ingredient_ID']+'"><i class="ti-trash"></i></span></div></div><div class="col-md-6 col-sm-12 col-xs-12 form-group  item'+data['dataResponse']['Ingredient_ID']+'"><div class="input-group mb-15"> <span class="input-group-addon">Đơn giá</span><input value="0" min="0.00000001" step="0.00000001" required name="price_ingredient['+data['dataResponse']['Ingredient_ID']+']" type="number" placeholder="" class="form-control ing_input_price"></div></div>');
                            }
                        }
                    });
                }
            }
            else{
                alert("Nguyên liệu tinh đã được thêm");
            }
        });
        $("#warehouse_ingredient_array").on('click', '.deleteItem', function (){
            let _id = $(this).data('id');
            $('#warehouse_ingredient_array .item'+_id).remove();
            arr_select_ing = arr_select_ing.replace(_id+",", "");
            if($('#warehouse_ingredient_array').html() == ""){
                $("#show_hinh_thuc").hide();
            }
            Total();
        });
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false,
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
        }).on('dp.show', function() {
            if($(this).data("DateTimePicker").date() === null)
                $(this).data("DateTimePicker").date(moment());
        });
        $('#raw-material').change(function(){
            _id = $(this).val();
            $.ajax({
                url: '{{ route('system.warehouse.getRaw') }}',
                type: "GET",
                dataType: "json",
                data: {id:_id},
                success: function (data){
                    if(data['status'] == true){
                        _html = '';
                        if(data['raw'] == true){
                            $.each( data['data'], function( key, value ) {
                                _html += '<option value="'+value.Raw_Material_ID+'">' + value.Raw_Material_Name + '(' + value.Raw_Material_Unit + ')'  + '</option>'
                            });
                        }
                        else{
                            $.each( data['data'], function( key, value ) {
                                _html += '<option value="'+value.ingredient_ID+'">'+value.ingredient_Name + '(' + value.ingredient_Unit + ')' +'</option>'
                            });
                        }
                        $('#Name-Ware').html(_html);
                    }
                }
            });
        });
        _quantity = 0;
        _price = 0;
        $('#quantity-ware').keyup(function(){
            _quantity = $(this).val();
            _price = $('#price-ware').val();
            $('#total-price').val(_quantity*_price);
        });
        $('#price-ware').keyup(function(){
            _price = $(this).val();
            _quantity = $('#quantity-ware').val();
            $('#total-price').val(_quantity*_price);
        });

    </script>

@endsection
