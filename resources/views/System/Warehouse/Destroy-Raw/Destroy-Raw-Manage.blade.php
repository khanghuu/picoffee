@extends('System.Layouts.master')
@section('title', 'Danh sách huỷ nguyên liệu thô')
@section('css')
		<!-- select2 CSS -->
		<link href="vendors/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>

		<!-- switchery CSS -->
		<link href="vendors/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" type="text/css"/>

		<!-- bootstrap-select CSS -->
		<link href="vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>

		<!-- bootstrap-tagsinput CSS -->
		<link href="vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" type="text/css"/>

		<!-- bootstrap-touchspin CSS -->
		<link href="vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css"/>

		<!-- multi-select CSS -->
		<link href="vendors/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css"/>

		<!-- Bootstrap Switches CSS -->
		<link href="vendors/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>

		<!-- Bootstrap Datetimepicker CSS -->
		<link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
        a:hover{
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Danh sách huỷ nguyên liệu thô</h6>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="row">

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        @if(session('user')->User_Level == 1 || session('user')->User_Level == 2)
                            <button class="btn btn-primary btn-rounded btn-icon left-icon" data-toggle="modal" data-target="#add-destroy-raw" class="model_img img-responsive"> <i class="fa fa-plus"></i> <span>Thêm</span></button>
                            <!-- Modal adding destroy Warehouse -->

                            @component('System.Warehouse.Destroy-Raw.Component.modal-add-destroy-raw-warehouse', ['shopList'=>$shopList, 'rawList' =>$rawList])
                            @endcomponent
                            <!-- Modal adding destroy Warehouse -->
                        @endif
                        <a style="margin: 10px;" href="{{route('destroyRawExport')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12" style="text-align: right">
                        <button class="btn btn-default btn-rounded btn-icon left-icon" id="btnLoc"> <i class="fa fa-filter"></i> <span>Lọc</span></button>
                    </div>
                    <!--Toggle Filter Destroy Warehouse-->
                    @component('System.Warehouse.Destroy-Raw.Component.toggle-filter-destroy-raw-warehouse', ['unitList' => $unitList])
                    @endcomponent
                    <!--/Toggle Filter Destroy Warehouse-->

					</div>
					<div class="table-wrap mt-20">
						<div class="table-responsive">
                            <table id="datable_destroy_raw" class="table table-hover table-bordered display mb-30" style="text-align: center">
								<thead class="color-white-th">
									<tr>
										<th style="text-align: center">ID</th>
										<th style="text-align: center">Người huỷ</th>
										<th style="text-align: center">Tên nguyên liệu</th>
										<th style="text-align: center">Số lượng</th>
                                        <th style="text-align: center">Đơn vị</th>
										<th style="text-align: center">Ghi chú</th>
                                        <th>Trạng thái</th>
										<th style="text-align: center">Ngày giờ</th>
                                        @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                            <th style="text-align: center">Cửa hàng</th>
                                        @endif
                                        @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
										    <th style="text-align: center">Hành động</th>
                                        @endif
									</tr>

								</thead>
								<tbody>
									@foreach($destroyRawList as $destroyRaw)
                                        <tr>
                                            <td>{{ $destroyRaw->Warehouse_ID }}</td>
                                            <td>{{ $destroyRaw->User_Name }}</td>
                                            <td>{{ $destroyRaw->Raw_Material_Name }}</td>
                                            <td>{{ abs($destroyRaw->Warehouse_Quantity) }}</td>
                                            <th style="text-align: center">{{ $destroyRaw->Raw_Material_Unit }}</th>
                                            <td>{{ $destroyRaw->Warehouse_Description }}</td>
                                            <td>
                                                @if($destroyRaw->Warehouse_Status == 1)
                                                    <span class="label label-success">Kích hoạt</span>
                                                @else
                                                    <span class="label label-danger">Hủy</span>
                                                @endif
                                            </td>
                                            <td>{{ $destroyRaw->Updated_At }}</td>
                                            @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                                <td>{{ $destroyRaw->Shop_Name }}</td>
                                            @endif
                                            @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                                                <td>
                                                    <a class="btn-open-edit-destroy-raw" data-value="{{$destroyRaw->Warehouse_ID}}"><i class="fa fa-edit"></i></a> &nbsp
                                                    @if($destroyRaw->Warehouse_Status == 1)
                                                        <a href="{{ route('system.warehouse.postDeleteDestroyRaw', $destroyRaw->Warehouse_ID) }}" onclick="return confirm('Bạn có chắc chắn')"><i class="fa fa-bitbucket"></i></a>
                                                    @else
                                                        <a href="{{ route('system.warehouse.postEnableDestroyRaw', $destroyRaw->Warehouse_ID) }}" onclick="return confirm('Bạn có chắc chắn')"><i class="fa fa-arrow-circle-o-up"></i></a>
                                                    @endif
                                                </td>
                                            @endif
                                        </tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<!--Modal Edit Destroy raw warehouse-->
                    @component('System.Warehouse.Destroy-Raw.Component.modal-edit-destroy-raw-warehouse',['shopList'=>$shopList, 'rawList' =>$rawList])
                    @endcomponent
                    <!--Modal Edit Destroy raw warehouse-->
				</div>
			</div>
		</div>
	</div>
</div>

<!-- /Row -->
@endsection
@section('script')
    <!-- Switchery JavaScript -->
    <script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

    <!-- Select2 JavaScript -->
    <script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>

    <!-- Bootstrap Select JavaScript -->
    <script src="vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

    <!-- Bootstrap Tagsinput JavaScript -->
    <script src="vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

    <!-- Bootstrap Touchspin JavaScript -->
    <script src="vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Multiselect JavaScript -->
    <script src="vendors/bower_components/multiselect/js/jquery.multi-select.js"></script>
<script>
    $( "#btnLoc" ).click(function() {
        $("#toggle-search-destroy-raw").toggle();
    });
    $('#add_destroy_raw_shop').change(function () {
        shop_id = $(this).val();
        $.ajax({
            url: '{{route('system.warehouse.getDataDestroyRaw')}}',
            type: "GET",
            dataType: "json",
            data: {shop_id: shop_id},
            success: function (data) {
                if(data.status) {
                    $("#add_destroy_raw_id").empty();
                    $("#add_destroy_raw_id").append(new Option('----Chọn nguyên liệu----', null));
                    $.each(data.dataResponse, function(index, raw) {
                        $("#add_destroy_raw_id").append(new Option(raw.Raw_Material_Name + '(' + raw.Raw_Material_Unit +')', raw.Raw_Material_ID));
                    });
                }
            }
        });
    });

    $('.btn-open-edit-destroy-raw').click(function () {
        warehouse_id = $(this).attr('data-value');
        $.ajax({
            url: "{{route('system.warehouse.getDataEditDestroyRaw')}}",
            type: 'GET',
            dateType: 'json',
            data: {warehouse_id: warehouse_id},
            success: function (data) {
                if(data.status) {
                    console.log(data);
                    $('#edit_destroy_raw_quantity').val(Math.abs(data.dataResponse.Warehouse_Quantity));
                    $('#edit-destroy-description').val(data.dataResponse.Warehouse_Description);
                    $('#edit_destroy_raw_shop').append(new Option(data.dataResponse.Shop_Name, null));
                    $('#edit-destroy-raw-warehouse-id').val(warehouse_id);
                    var warehouse_raw = data.dataResponse.Warehouse_Raw;
                    $.ajax({
                        url: '{{route('system.warehouse.getDataDestroyRaw')}}',
                        type: "GET",
                        dataType: "json",
                        data: {shop_id: data.dataResponse.Warehouse_Shop},
                        success: function (data) {
                            if(data.status) {

                                $("#edit_destroy_raw_id").empty();
                                $("#edit_destroy_raw_id").append(new Option('----Chọn nguyên liệu----', null));
                                $.each(data.dataResponse, function(index, raw) {
                                    $("#edit_destroy_raw_id").append(new Option(raw.Raw_Material_Name + '(' + raw.Raw_Material_Unit +')', raw.Raw_Material_ID));
                                });
                                $('#edit_destroy_raw_id').val(warehouse_raw).prop('selected', true);
                            }
                        }
                    });
                }
            }
        });
        $('#modal-edit-destroy-raw').modal('show');
    });

</script>

<script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="dist/js/dataTables-data.js?v=1"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js"></script>
@endsection
