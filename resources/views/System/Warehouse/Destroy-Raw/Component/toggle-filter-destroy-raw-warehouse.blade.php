    <div class="col-md-12 col-sm-12 col-xs-12 mt-20" id="toggle-search-destroy-raw" style="display: none">
    <div class="form-wrap">
        <form class="form-horizontal"  method="get">
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Mã:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="search_destroy_raw_id" name="search_destroy_raw_id" value="{{request()->get('search_destroy_raw_id')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Tên nguyên liệu:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="search_destroy_raw_name" name="search_destroy_raw_name" value="{{request()->get('search_destroy_raw_name')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Đơn vị:</label>
                <div class="col-sm-10">

                    <select class="form-control" name="search_destroy_raw_unit" id="search_destroy_raw_unit">
                        <option value="">----Chọn đơn vị----</option>
                        @foreach($unitList as $unit)
                            <option value="{{$unit->Raw_Material_Unit}}" {{$unit->Raw_Material_Unit == request()->get('search_destroy_raw_unit') ? 'selected':''}}>{{$unit->Raw_Material_Unit}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Trạng thái:</label>
                <div class="col-sm-10">

                    <select class="form-control" name="search_destroy_raw_status" id="search_destroy_raw_status">
                        <option value="" selected>----Chọn trạng thái----</option>
                        <option value="1" {{request()->get('search_destroy_raw_status') === "1" ? 'selected':''}}>Đã xóa</option>
                        <option value="0" {{request()->get('search_destroy_raw_status') === "0" ? 'selected':''}}>Hủy xóa</option>
                    </select>
                </div>
            </div>
            @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                <div class="form-group">
                    <label class="control-label mb-10 col-sm-2">Nhập người tạo:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="search_destroy_raw_staff" name="search_destroy_raw_staff" value="{{request()->get('search_destroy_raw_staff')}}">
                    </div>
                </div>
            @endif
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Từ ngày:</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="search_destroy_raw_from" id="search_destroy_raw_from" value="{{request()->get('search_destroy_raw_from')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Đến ngày:</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="search_destroy_raw_to" id="search_destroy_raw_to"  value="{{request()->get('search_destroy_raw_to')}}">
                </div>
            </div>
            <div class="form-group mb-0">
                <div class="col-sm-offset-2 col-sm-10">
                    <button class="btn btn-orange btn-anim"><i class="fa fa-search"></i><span class="btn-text">Tìm kiếm</span></button>
                    <a href="{{route('system.warehouse.getDestroyRaw') }}"><button type="button" class="btn btn-default btn-anim"><i class="fa fa-search"></i><span class="btn-text">Huỷ</span></button></a>
                </div>
            </div>
        </form>
    </div>
</div>
