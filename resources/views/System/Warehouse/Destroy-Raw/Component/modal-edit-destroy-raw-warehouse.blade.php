<div id="modal-edit-destroy-raw" class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.warehouse.postEditDestroyRaw') }}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Sửa hủy nguyên liệu thô</h5>
                </div>
                <div class="modal-body">
                    <div class="form-wrap">
                        <div class="row">
                            @if(session('user')->User_Level == 1)
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                        <select class="form-control select2" name="edit_destroy_raw_shop" id="edit_destroy_raw_shop" required disabled>

                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                    <select class="form-control select2" name="edit_destroy_raw_id" id="edit_destroy_raw_id" required>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-share-alt"></i></span>
                                    <input name="edit_destroy_raw_quantity" id="edit_destroy_raw_quantity" type="number" placeholder="Số lượng" class="form-control" step="0.0000001" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input readonly="" type="text" class="form-control" value="{{ date('Y-m-d H:i:s') }}">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <textarea style="min-width: 100%" id="edit-destroy-description" rows="1" name="edit_destroy_description" class="form-control" placeholder="Ghi chú"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning">Lưu</button>
                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                    <input type="hidden" name="edit_destroy_raw_warehouse_id" id="edit-destroy-raw-warehouse-id">
                    {{ csrf_field() }}
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
