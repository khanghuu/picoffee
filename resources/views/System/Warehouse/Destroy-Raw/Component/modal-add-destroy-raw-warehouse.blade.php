<div id="add-destroy-raw" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.warehouse.postDestroyRaw') }}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Thêm nguyên liệu thô bị huỷ</h5>
                </div>
                <div class="modal-body">
                    <div class="form-wrap">
                        <div class="row">
                            @if(session('user')->User_Level == 1)
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                        <select class="form-control select2" name="add_destroy_raw_shop" id="add_destroy_raw_shop" required>
                                            <option value="">----Chọn cửa hàng----</option>
                                            @foreach($shopList as $shop)
                                                <option value="{{$shop->Shop_ID}}">{{$shop->Shop_Name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                        <select class="form-control select2" name="add_destroy_raw_id" id="add_destroy_raw_id" required>
                                            <option value="">----Chọn nguyên liệu----</option>
                                        </select>
                                    </div>
                                </div>
                            @endif
                            @if(session('user')->User_Level == 2)
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                        <select class="form-control select2" name="add_destroy_raw_id" id="add_destroy_raw_id" required>
                                            <option value="">----Chọn nguyên liệu----</option>
                                                    @foreach($rawList as $raw)
                                                        <option value="{{$raw->Raw_Material_ID}}">{{$raw->Raw_Material_Name}}({{$raw->Raw_Material_Unit}})</option>
                                                    @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-share-alt"></i></span>
                                    <input name="add_destroy_raw_quantity" type="number" placeholder="Số lượng" class="form-control" step="0.0000001" required>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <textarea style="min-width: 100%" rows="1" name="add_destroy_raw_description" class="form-control" placeholder="Ghi chú"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input readonly="" type="text" class="form-control" value="{{ date('Y-m-d H:i:s') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning">Lưu</button>
                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                    {{ csrf_field() }}
                </div>
            </form>
        </div>
    </div>
</div>

