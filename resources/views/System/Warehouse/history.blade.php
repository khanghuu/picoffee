@extends('System.Layouts.master')
@section('title', 'Warehouse/Import')
@section('css')
    <link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>

    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
        .pagination {
            float:right;
        }
        #datable_warehouse_history:hover{
            cursor: pointer;
        }
    </style>
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
        td>a:hover{
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
<div class="panel panel-default border-panel card-view">
    <div class="panel-heading">
        <div class="pull-left">
            <h6 class="panel-title txt-dark">Lịch sử nhập kho</h6>
        </div>
        <div class="clearfix">

        </div>
    </div>

    <div class="panel-wrapper collapse in">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                        <a href="{{route('warehouseHistoryExport')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12" style="text-align: right">
                    <button class="btn btn-default btn-rounded btn-icon left-icon" id="btnLoc"> <i class="fa fa-filter"></i> <span>Lọc</span></button>
                </div>
                <!--Toggle filter material -->
                <div class="col-md-12 col-sm-12 col-xs-12 mt-20" id="divLoc" style="display: none">
                    <div class="form-wrap">
                        <form class="form-horizontal" method="get">
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-2">Số chứng từ:</label>
                                <div class="col-sm-10">
                                    <input value="{{request()->get('warehouse_history_number')}}" type="text" class="form-control" id="pwd_hr" name="warehouse_history_number">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-2">Tên Người Nhập:</label>
                                <div class="col-sm-10">
                                    <input value="{{request()->get('warehouse_history_user')}}" type="text" class="form-control" id="pwd_hr" name="warehouse_history_user">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-2">Tên nguyên liệu:</label>
                                <div class="col-sm-10">
                                    <input value="{{request()->get('warehouse_history_name')}}" type="text" class="form-control" id="pwd_hr" name="warehouse_history_name">
                                </div>
                            </div>
                            @if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3)
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-2">Cửa hàng:</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="warehouse_history_shop">
                                        <option value="">--- Chọn cửa hàng ---</option>
                                        @foreach($shop_list as $v)
                                            <option {{$v->Shop_ID == request()->get('warehouse_history_shop')?'selected':''}}  value="{{ $v->Shop_ID }}">{{ $v->Shop_Name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-2">Bắt đầu:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control datetimepicker" name='warehouse_history_start' value="{{request()->get('warehouse_history_start')}}" placeholder="Ngày bắt đầu nhập kho">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label mb-10 col-sm-2">Kết thúc:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control datetimepicker" name='warehouse_history_end' value="{{request()->get('warehouse_history_end')}}" placeholder="Ngày kết thúc nhập kho">
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-orange btn-anim"><i class="fa fa-search"></i><span class="btn-text">Tìm kiếm</span></button>
                                    <a href="{{ route('system.warehouse.getImport') }}"><button type="button" class="btn btn-default btn-anim btn-anim"><i class="fa fa-times"></i><span class="btn-text">Huỷ</span></button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--/Toggle filter material -->
            </div>
            <div class="table-wrap mt-20">
                <div class="table-responsive">
                    <table id="datable_warehouse_history" data-show-toggle="true" data-expand-first="false" class="table table-hover table-bordered display mb-30">
                        <thead class="color-white-th">
                        <tr>
                            <th>Số chứng từ</th>
                            <th>Người nhập</th>
                            <th>Nguyên liệu</th>
                            <th>Số lượng</th>
                            <th>Đơn giá</th>
                            <th>Thành tiền</th>
                            <th data-breakpoints="all">Mô tả</th>
                            <th>Ghi chú</th>
                            @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                <th data-breakpoints="xs sm">Cửa hàng</th>
                            @endif
                            <th>Ngày nhập</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($merge as $v)
                            <tr>
                                <td>{{ $v->Warehouse_Number }}</td>
                                <td>{{ $v->User_Name }}</td>
                                <td>{{ $v->Raw_Material_Name ?? $v->Ingredient_Name}}</td>
                                <td>{{ number_format($v->Warehouse_Quantity,3,",",".") }}</td>
                                <td>{{ number_format($v->Warehouse_Price,3,",",".") != 0 ? number_format($v->Warehouse_Price,3,",",".") : "" }}</td>
                                <td>{{ number_format($v->Warehouse_Total,3,",",".") != 0 ? number_format($v->Warehouse_Total,3,",",".") : ""}}</td>
                                <td>{{ $v->Warehouse_Description }}</td>
                                @php
                                    $check = false;
                                @endphp
                                @if ($v->Warehouse_Status==0)
                                    @php
                                        $check = true;
                                    @endphp
                                @endif
                                @if($check)
                                    <td>Điều chỉnh chênh lệch kho</td>
                                @else
                                    @if($v->Warehouse_IsDestroy)
                                        @if($v->Ingredient_Name)
                                            <td>Hủy nguyên liệu tinh</td>
                                        @else
                                            <td>Hủy nguyên liệu thô</td>
                                        @endif
                                    @elseif($v->Ingredient_Name)
                                        @if ($v->Warehouse_Status==2)
                                            <td>Chế biến nguyên liệu tinh</td>
                                        @elseif ($v->Warehouse_Status==1)
                                            <td>Nhập nguyên liệu tinh</td>
                                        @else
                                            <td>{{""}}</td>
                                        @endif
                                    @else
                                        <td>{{""}}</td>
                                    @endif
                                @endif

                                @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                    <td>{{ $v->Shop_Name }}</td>
                                @endif
                                <td>{{ $v->Created_At }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{-- {{ $warehouse_history_raw->appends(request()->except('page'))->links() }} --}}
                </div>
            </div>

        </div>

    </div>
</div>
<!--/Warehouse Manage -->

@endsection
@section('script')
    <script type="text/javascript" src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="dist/js/dataTables-data.js"></script>
    <script>
        jQuery(function($){
            $('.table').footable();
        });
        function currencyFormatDE(num) {
            return (
                num
                .toFixed(3)
                .replace('.', ',')
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
            )
        }

        $( "#btnLoc" ).click(function() {
            $("#divLoc").toggle();
        });
        @if(Session('user')->User_Level != 2)
            $('#select_raw').hide();
            $('#select_ing').hide();
        @endif
        let arr_select_raw = "";
        let arr_select_ing = "";
        function Total(){
            let total = 0;
            let temp = 0;
            let arr_raw_quantity = [];
            let arr_raw_price = [];
            let arr_ing_quantity = [];
            let arr_ing_price = [];
            $('.raw_input_quantity').each(function () {
                temp = $(this).val();
                if (isNaN(temp))
                {
                    alert("Bạn phải nhập số");
                    $(this).val(0);
                    return false;
                }
                arr_raw_quantity.push(temp);
            });
            $('.raw_input_price').each(function () {
                temp = $(this).val();
                if (isNaN(temp))
                {
                    alert("Bạn phải nhập số");
                    $(this).val(0);
                    return false;
                }
                arr_raw_price.push(temp);
            });
            $('.ing_input_quantity').each(function () {
                temp = $(this).val();
                if (isNaN(temp))
                {
                    alert("Bạn phải nhập số");
                    $(this).val(0);
                    return false;
                }
                arr_ing_quantity.push(temp);
            });
            $('.ing_input_price').each(function () {
                temp = $(this).val();
                if (isNaN(temp))
                {
                    alert("Bạn phải nhập số");
                    $(this).val(0);
                    return false;
                }
                arr_ing_price.push(temp);
            });
            for (let i = 0; i < arr_raw_quantity.length; i++) {
                total += arr_raw_quantity[i] * arr_raw_price[i];
            }
            for (let i = 0; i < arr_ing_quantity.length; i++) {
                total += arr_ing_quantity[i] * arr_ing_price[i];
            }
            $('#warehouse_total').val(currencyFormatDE(total));
        }

        $('#warehouse_raw_array').on("keyup", ".raw_input_quantity", function(){
            Total();
        });

        $('#warehouse_raw_array').on("keyup", ".raw_input_price", function(){
            Total();
        });

        $('#warehouse_ingredient_array').on("keyup", ".ing_input_quantity", function(){
            Total();
        });

        $('#warehouse_ingredient_array').on("keyup", ".ing_input_price", function(){
            Total();
        });
        @if(Session('user')->User_Level==1)
            $("select[name=warehouse_shop]").change(function(){
            $('#select_raw').show();
            $('#select_ing').show();
            $("#show_hinh_thuc").hide();
            $('#warehouse_raw_array').html("");
            $('#warehouse_ingredient_array').html("");
            let _id = $(this).val();
            if(_id!=''){
                $.ajax({
                    url: '{{ route('system.warehouse.getSelectShopImportAjax') }}',
                    type: "GET",
                    dataType: "json",
                    data: {id:_id},
                    success: function (data){
                        var select_raw = $('#warehouse_raw');
                        select_raw.empty().append('<option value="">--- Chọn nguyên liệu thô ---</option>');
                        data['ware_raw'].forEach(element => {
                            select_raw.append('<option value="'+element.Raw_Material_ID+'">'+element.Raw_Material_Name+' ('+element.Raw_Material_Unit+')</option>');
                        });

                        var select_ingredient = $('#warehouse_ingredient');
                        select_ingredient.empty().append('<option value="">--- Chọn nguyên liệu tinh ---</option>');
                        data['ware_ingredient'].forEach(element => {
                            select_ingredient.append('<option value="'+element.Ingredient_ID+'">'+element.Ingredient_Name+' ('+element.Ingredient_Unit+')</option>');
                        });
                    }
                });
            }
        });
        @endif
        $("select[id=warehouse_raw]").change(function(){
            let _id = $(this).val();
            if(arr_select_raw.search(_id+",") == -1){
                arr_select_raw+=_id+",";
                if(_id!=''){
                    $.ajax({
                        url: '{{ route('system.warehouse.getSelectRawImportAjax') }}',
                        type: "GET",
                        dataType: "json",
                        data: {id:_id},
                        success: function (data){
                            if(data.status){
                                $('#warehouse_raw_array').append('<div class="col-md-6 col-sm-12 col-xs-12 form-group  item'+data['dataResponse']['Raw_Material_ID']+'"><div class="input-group mb-15"> <span class="input-group-addon">'+data['dataResponse']['Raw_Material_Name']+'</span><input value="0" name="quantity_raw['+data['dataResponse']['Raw_Material_ID']+']" type="number" placeholder="Số lượng nguyên liệu thô cần thiết" min="0.00000001" step="0.00000001" required class="form-control raw_input_quantity"><span class="input-group-addon" class="unit">'+data['dataResponse']['Raw_Material_Unit']+'</span><span class="input-group-addon deleteItem unit" style="background: red; color: #fff;cursor: pointer;" data-id="'+data['dataResponse']['Raw_Material_ID']+'"><i class="ti-trash"></i></span></div></div><div class="col-md-6 col-sm-12 col-xs-12 form-group  item'+data['dataResponse']['Raw_Material_ID']+'"><div class="input-group mb-15"> <span class="input-group-addon">Đơn giá</span><input value="0" min="0.00000001" step="0.00000001" required name="price_raw['+data['dataResponse']['Raw_Material_ID']+']" type="number" placeholder="" class="form-control raw_input_price"></div></div>');
                            }
                        }
                    });
                }
            }
            else{
                alert("Nguyên liệu thô đã được thêm");
            }
        });
        $("#warehouse_raw_array").on('click', '.deleteItem', function (){
            let _id = $(this).data('id');
            $('#warehouse_raw_array .item'+_id).remove();
            arr_select_raw = arr_select_raw.replace(_id+",", "");
            if($('#warehouse_raw_array').html() == ""){
                $("#show_hinh_thuc").hide();
            }
            Total();
        });
        $("select[id=warehouse_ingredient]").change(function(){
            let _id = $(this).val();
            if(arr_select_ing.search(_id+",") == -1){
                arr_select_ing+=_id+",";
                if(_id!=''){
                    $.ajax({
                        url: '{{ route('system.warehouse.getSelectIngredientImportAjax') }}',
                        type: "GET",
                        dataType: "json",
                        data: {id:_id},
                        success: function (data){
                            if(data.status){
                                if($('#warehouse_ingredient_array').html() == ""){
                                    $("#show_hinh_thuc").hide();
                                }
                                $("#show_hinh_thuc").show();
                                $('#warehouse_ingredient_array').append('<div class="col-md-6 col-sm-12 col-xs-12 form-group  item'+data['dataResponse']['Ingredient_ID']+'"><div class="input-group mb-15"> <span class="input-group-addon">'+data['dataResponse']['Ingredient_Name']+'</span><input value="0" min="0.00000001" step="0.00000001" required name="quantity_ingredient['+data['dataResponse']['Ingredient_ID']+']" type="number" placeholder="Số lượng nguyên liệu thô cần thiết" class="form-control ing_input_quantity"><span class="input-group-addon" class="unit">'+data['dataResponse']['Ingredient_Unit']+'</span><span class="input-group-addon deleteItem unit" style="background: red; color: #fff;cursor: pointer;" data-id="'+data['dataResponse']['Ingredient_ID']+'"><i class="ti-trash"></i></span></div></div><div class="col-md-6 col-sm-12 col-xs-12 form-group  item'+data['dataResponse']['Ingredient_ID']+'"><div class="input-group mb-15"> <span class="input-group-addon">Đơn giá</span><input value="0" min="0.00000001" step="0.00000001" required name="price_ingredient['+data['dataResponse']['Ingredient_ID']+']" type="number" placeholder="" class="form-control ing_input_price"></div></div>');
                            }
                        }
                    });
                }
            }
            else{
                alert("Nguyên liệu tinh đã được thêm");
            }
        });
        $("#warehouse_ingredient_array").on('click', '.deleteItem', function (){
            let _id = $(this).data('id');
            $('#warehouse_ingredient_array .item'+_id).remove();
            arr_select_ing = arr_select_ing.replace(_id+",", "");
            if($('#warehouse_ingredient_array').html() == ""){
                $("#show_hinh_thuc").hide();
            }
            Total();
        });
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false,
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
        }).on('dp.show', function() {
            if($(this).data("DateTimePicker").date() === null)
                $(this).data("DateTimePicker").date(moment());
        });
        $('#raw-material').change(function(){
            _id = $(this).val();
            $.ajax({
                url: '{{ route('system.warehouse.getRaw') }}',
                type: "GET",
                dataType: "json",
                data: {id:_id},
                success: function (data){
                    if(data['status'] == true){
                        _html = '';
                        if(data['raw'] == true){
                            $.each( data['data'], function( key, value ) {
                                _html += '<option value="'+value.Raw_Material_ID+'">' + value.Raw_Material_Name + '(' + value.Raw_Material_Unit + ')'  + '</option>'
                            });
                        }
                        else{
                            $.each( data['data'], function( key, value ) {
                                _html += '<option value="'+value.ingredient_ID+'">'+value.ingredient_Name + '(' + value.ingredient_Unit + ')' +'</option>'
                            });
                        }
                        $('#Name-Ware').html(_html);
                    }
                }
            });
        });
        _quantity = 0;
        _price = 0;
        $('#quantity-ware').keyup(function(){
            _quantity = $(this).val();
            _price = $('#price-ware').val();
            $('#total-price').val(_quantity*_price);
        });
        $('#price-ware').keyup(function(){
            _price = $(this).val();
            _quantity = $('#quantity-ware').val();
            $('#total-price').val(_quantity*_price);
        });

    </script>

@endsection
