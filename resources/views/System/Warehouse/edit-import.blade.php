@extends('System.Layouts.master')
@section('title', 'Warehouse/Edit import')
@section('css')
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Sửa Nhập Kho Hàng</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <form action="{{route('system.warehouse.postEditImport')}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Số chứng từ <i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                                    <input type="text" class="form-control" placeholder="Nhập số chứng từ" name="number" readonly value="{{$importData->Warehouse_Number}}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Người Nhập <i class="fa fa-hand-o-down" aria-hidden="true"></i></label>
                                    <input type="text" class="form-control"  name="username" readonly value="{{$importData->User_ID}}" placeholder="{{$importData->User_Name}}">
                                </div>
                                <input type="hidden" name="warehouse_id" value="{{$importData->Warehouse_ID}}">
                                <div class="form-group mt-30 mb-30">
                                    <label class="control-label mb-10 text-left">Loại Hàng <i class="fa fa-hand-o-down" aria-hidden="true"></i></label>
                                    <select class="form-control" name="raw" id="raw-material">
{{--                                        @php dd($importData)@endphp--}}
                                        @if(isset($importData->Warehouse_Ingredient))
                                            <option value="2" selected>Nguyên Liệu Tinh</option>
                                            <option value="1">Nguyên Liệu Thô</option>
                                            @else
                                            <option value="2" >Nguyên Liệu Tinh</option>
                                            <option value="1" selected>Nguyên Liệu Thô</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Tên Hàng Hoá<i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                                    <select class="form-control" name="material_id" id="Name-Ware">
                                        @if(isset($importData->Warehouse_Ingredient))
                                        <option value="{{$importData->Warehouse_Ingredient}}">{{$importData->ingredient_Name}}</option>
                                        @else
                                            <option value="{{$importData->Warehouse_Raw}}">{{$importData->Raw_Material_Name}}</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Ngày hết hạn <i class="fa fa-calendar" aria-hidden="true"></i></label>
                                    <input type="date" name="expiry" class="form-control" value="{{$importData->Warehouse_Expiry_Date}}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left" id="materialamuont">Số Lượng <i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                                    <input type="number" class="form-control" placeholder="Nhập số lượng hàng" name="quantity" id="quantity-ware" value="{{$importData->Warehouse_Quantity}}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Đơn Giá <i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                                    <input type="number" step="any" class="form-control" placeholder="Nhập đơn giá" name="price" id="price-ware" value="{{$importData->Warehouse_Price}}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Thành Tiền <i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                                    <input type="text" class="form-control"	name="total-price" id="total-price" value="{{$importData->Warehouse_Price*$importData->Warehouse_Quantity }}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Ghi Chú <i class="fa fa-commenting-o" aria-hidden="true"></i></label>
                                    <textarea class="form-control" rows="5" name="description">{{$importData->Warehouse_Description}}</textarea>
                                </div>
                                <div class="form-group pull-right">
                                    <button id="btn-them" type="submit" class="btn01 btn-lg btn-success">Sửa <i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection
@section('script')
    <script>
        $('#raw-material').change(function(){
            _id = $(this).val();
            console.log(_id);
            $.ajax({
                url: '{{ route('system.warehouse.getRaw') }}',
                type: "GET",
                dataType: "json",
                data: {id:_id},
                success: function (data){
                    if(data['status'] == true){
                        _html = '';
                        if(data['raw'] == true){
                            $.each( data['data'], function( key, value ) {
                                _html += '<option value="'+value.Raw_Material_ID+'">'+value.Raw_Material_Name+'</option>'
                            });
                        }
                        else{
                            $.each( data['data'], function( key, value ) {
                                _html += '<option value="'+value.ingredient_ID+'">'+value.ingredient_Name+'</option>'
                            });
                        }
                        $('#Name-Ware').html(_html);
                    }
                }
            });
        });
        _quantity = 0;
        _price = 0;
        $('#quantity-ware').keyup(function(){
            _quantity = $(this).val();
            _price = $('#price-ware').val();
            $('#total-price').val(_quantity*_price);
        });
        $('#price-ware').keyup(function(){
            _price = $(this).val();
            _quantity = $('#quantity-ware').val();
            $('#total-price').val(_quantity*_price);
        });
    </script>

@endsection
