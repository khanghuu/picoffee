@extends('System.Layouts.master')
@section('css')
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Doanh thu hàng giờ</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead class="color-white-th">
                                <tr>
                                    <th>ID</th>
                                    <th>Tên</th>
                                    <th>Email</th>
                                    <th>Điện thoại</th>
                                    <th class="text-right">Giảm giá %</th>
                                    <th class="text-right">Giảm giá tiền</th>
                                    <th class="text-right">Tổng</th>
                                    <th>Ngày giờ</th>
                                    <th>Người tạo</th>
                                    <th>Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($allOrder as $d)
                                    <tr>
                                        <td>{{$d->Bill_ID}}</td>
                                        <td>{{$d->Bill_CustomerName}}</td>
                                        <td>{{$d->Bill_CustomerEmail}}</td>
                                        <td>{{$d->Bill_CustomerPhone}}</td>
                                    <!-- 									<td>{{$d->Bill_TotalPrice}}</td> -->
                                        <td class="text-right">{{$d->Bill_DiscountPercent}}%</td>
                                        <td class="text-right">{{$d->Bill_DiscountAmount }}đ</td>
                                    <!-- 									<td>{{$d->Bill_IsTaxVAT == 0 ? 0 : 0.1 * $d->Bill_TotalPrice }}</td> -->
                                        <td class="text-right">{{$d->Bill_ExtantPrice}}</td>
                                        <td>{{$d->Bill_Time}}</td>
                                        <td>{{$d->Bill_User}}</td>
                                        <td>
                                            <a href="{{ route('system.changependdingorder',$d->Bill_ID) }}"><i class="fa fa-edit order-continue" id="edit" data-value="{{$d->Bill_ID}}"></i></a> &nbsp &nbsp<a href="{{ route('system.deletependdingorder',$d->Bill_ID) }}"onclick="return confirm('Bạn có chắc chắn')"><i class="fa fa-bitbucket" class="order-delete" ></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
    </script>
@endsection
