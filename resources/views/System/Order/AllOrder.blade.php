@extends('System.Layouts.master')
@section('css')
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
    </style>
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
        td>a:hover{
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Tất cả đơn hàng</h6>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <a style="margin: 10px;" href="{{route('billExport')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                        </div>
                    </div>
					<div class="table-responsive">
						<table id="datable_all_order" class="table table-hover mb-0">
							<thead class="color-white-th">
								<tr>
									<th>ID</th>
									<th>Tên</th>
									<th class="text-right">Giảm giá %</th>
									<th class="text-right">Giảm giá tiền</th>
									<th class="text-right">Tổng</th>
									<th>Ngày giờ</th>
									<th>Người tạo</th>
									<th>Trạng thái</th>
                                    @if(session('user')->User_Level == 1 || session('user')->User_Level ==3)
                                        <th>Cửa hàng</th>
                                    @endif
                                    <th>Chi tiết</th>
								</tr>
							</thead>
							<tbody>
								@foreach($allOrder as $d)
								<tr>
									<td>{{$d->Bill_ID}}</td>
									<td>{{$d->Bill_CustomerName}}</td>
									<td class="text-right">{{$d->Bill_DiscountPercent}}%</td>
									<td class="text-right">{{$d->Bill_DiscountAmount }}đ</td>
									<td class="text-right">{{$d->Bill_ExtantPrice}}</td>
									<td>{{$d->Bill_Time}}</td>
									<td>{{$d->User_Name}}</td>
									<td>
										@if($d->Bill_Status == 1)
											<span class="label label-success">Đã Thanh Toán</span>
										@else
											<span class="label label-danger">Chưa Thanh Toán</span>
										@endif
									</td>
                                    @if(session('user')->User_Level == 1 || session('user')->User_Level ==3)
                                        <th>{{$d->Shop_Name}}</th>
                                    @endif
                                    <td><span class="chi-tiet label label-info" data-value="{{$d->Bill_ID}}">Xem</span> </td>
								</tr>
								@endforeach
							</tbody>
						</table>
                        <div style="text-align: right;">
                            {{ $allOrder->appends(request()->input())->onEachSide(1)->links() }}
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="order-detail"  class="modal fade modal-datatable-signed" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title">Chi Tiết Hóa Đơn</h5>
            </div>
            <div class="modal-body">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-wrap">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-body">
                                                        <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account mr-10"></i>Thông tin hóa đơn</h6>
                                                        <hr class="light-grey-hr"/>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Hóa đơn số:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static" id="bill_id">15</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Cửa hàng:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static" id="bill_shop"></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!-- /Row -->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Giảm giá %:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static" id="discount_percent">%</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Giảm giá tiền:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static" id="discount_money">  </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">VAT:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static" id="bill_tax"> 0% </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Tổng bill:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static" id="bill_total"> 0% </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!-- /Row -->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Tổng tiền:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static" id="bill_extance">00 </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Thời gian:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static" id="bill_time"></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!-- /Row -->

                                                        <div class="seprator-block"></div>
                                                        <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>Sản phẩm</h6>
                                                        <hr class="light-grey-hr"/>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                    <table  class="table table-hover display  pb-30 dataTable" role="grid" aria-describedby="datable_1_info">
                                                                    <thead>
                                                                        <th>Mã sản phẩm</th>
                                                                        <th>Tên sản phẩm </th>
                                                                        <th>Đơn giá</th>
                                                                        <th>Số lượng</th>
                                                                        </thead>
                                                                        <tbody id="bill_product_list">

                                                                        </tbody>
                                                                    </table>

                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="form-actions mt-10">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-offset-10 col-md-12">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@endsection
@section('script')
    <!-- Data table JavaScript -->
    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="dist/js/dataTables-data.js?v=1"></script>
    <script>
        $('.chi-tiet').click(function () {
            order_id = $(this).attr('data-value');
            $.ajax({
                url: '{{route('system.getDetailBill')}}',
                type: 'GET',
                dataType: 'json',
                data: {bill_id: order_id},
                success: function (data) {
                    if(data.status) {
                        console.log(data);
                        $('#bill_id').text(data.dataResponse.bill[0].Bill_ID);
                        $('#bill_shop').text(data.dataResponse.bill[0].Shop_Name);
                        $('#discount_percent').text(data.dataResponse.bill[0].Bill_DiscountPercent + ' %');
                        $('#discount_money').text(data.dataResponse.bill[0].Bill_DiscountAmount + ' đ');
                        $('#bill_total').text(data.dataResponse.bill[0].Bill_TotalPrice + ' đ');
                        $('#bill_extance').text(data.dataResponse.bill[0].Bill_ExtantPrice + ' đ');
                        $('#bill_time').text(data.dataResponse.bill[0].Bill_Time);
                        if(data.dataResponse.bill[0].Bill_IsTaxVAT) {
                            $('#bill_tax').text('10%');
                        }
                        $('#bill_product_list').empty();
                        $.each(data.dataResponse.product, function(index, item) {
                            _html = "<tr><td>" + item.Product_ID + "</td><td>" + item.Product_Name +  "</td><td>" + item.Product_Price + "</td><td>" + item.Bill_Product_Quantity + "</td></tr>";
                            $('#bill_product_list').append(_html);
                        });
                    }
                }
            });
           $('#order-detail').modal('show');
        });
    </script>
@endsection
