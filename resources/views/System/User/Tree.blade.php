@extends('System.Layouts.master')
@section('title')
@section('css')
		<!-- Bootstrap Treeview -->
		<link href="vendors/bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.css" rel="stylesheet" type="text/css">type="text/css"/>
<style>
	.modal-dialog {
		min-width: 80%!important;
	}
</style>
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Cây thành viên</h6>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="tree-view mt-20" id="treeview4"></div>
				</div>	
			</div>	
		</div>	
	</div>	

</div>

@endsection
@section('script')
	<!-- Treeview JavaScript -->
	<script src="vendors/bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.js"></script>
<script>
	$(function() {
		"use strict";
			var json = {!!$Children!!}
			$('#treeview4').treeview({
			  data: json
			});
	}); 
</script>
@endsection
