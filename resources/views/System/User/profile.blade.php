@extends('System.Layouts.master')
@section('title', 'Profile')
@section('css')
<!-- Bootstrap Datetimepicker CSS -->
<link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
<style>
    .modal-dialog {
        min-width: 80%!important;
    }
    .counts-text{
        font-weight: bold;
        font-
    }
</style>
@endsection
@section('content')
        <!--Profile blade -->
        <div class="container pt-30">
            <!-- Row -->
            <div class="row">
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view  pa-0">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body  pa-0">
                                        <div class="profile-box">
                                            <div class="profile-cover-pic" style="background-image:url('../img/user_banner.png')">
                                                <div class="fileupload btn btn-default">
                                                    {{-- <span class="btn-text">edit</span> --}}
                                                    <input class="upload" type="file">
                                                </div>
                                                <div class="profile-image-overlay"></div>
                                            </div>
                                            <div class="profile-info text-center mb-15">
                                                <div class="profile-img-wrap">
                                                <img class="inline-block mb-10" src="@if($user->User_Image == NULL){{"../img/mock1.jpg"}} @else{{$user->User_Image}}@endif" alt="user"/>
                                                    <div class="fileupload btn btn-default">
                                                        <span class="btn-text">edit</span>
                                                        <input class="upload" type="file"  id="upload">
                                                    </div>
                                                </div>
                                                <h5 class="block mt-10 weight-500 capitalize-font txt-dark">{{$user->User_Name}}</h5>
                                                <h6 class="block capitalize-font">{{$user->level_Name}}</h6>
                                            </div>
                                            <div class="social-info">
                                                {{-- <div class="row">
                                                    <div class="col-xs-4 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">345</span></span>
                                                        <span class="counts-text block">post</span>
                                                    </div>
                                                    <div class="col-xs-4 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">246</span></span>
                                                        <span class="counts-text block">followers</span>
                                                    </div>
                                                    <div class="col-xs-4 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">898</span></span>
                                                        <span class="counts-text block">tweets</span>
                                                    </div>
                                                </div> --}}
                                                <button class="btn btn-orange btn-block btn-anim mt-15 btn-edit-password" data-toggle="modal" data-target="#change-password-modal"><i class="fa fa-pencil"></i><span class="btn-text">Đổi mật khẩu</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view pa-0">
                                <div class="panel-wrapper collapse in">
                                    <div  class="panel-body pb-0">
                                        <div  class="tab-struct custom-tab-1">
                                            <ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
                                                {{-- <li class="active" role="presentation"><a  data-toggle="tab" id="profile_tab_8" role="tab" href="#profile_8" aria-expanded="false"><span>profile</span></a></li> --}}
                                                {{-- <li  role="presentation" class="next"><a aria-expanded="true"  data-toggle="tab" role="tab" id="follo_tab_8" href="#follo_8"><span>followers<span class="inline-block">(246)</span></span></a></li> --}}
                                                {{-- <li role="presentation" class=""><a  data-toggle="tab" id="photos_tab_8" role="tab" href="#photos_8" aria-expanded="false"><span>photos</span></a></li> --}}
                                                {{-- <li role="presentation" class=""><a  data-toggle="tab" id="earning_tab_8" role="tab" href="#earnings_8" aria-expanded="false"><span>earnings</span></a></li> --}}
                                                <li class="active"role="presentation"><a  data-toggle="tab" id="settings_tab_8" role="tab" href="#settings_8" aria-expanded="false"><span>settings</span></a></li>
                                                {{-- <li class="dropdown" role="presentation">
                                                    <a  data-toggle="dropdown" class="dropdown-toggle" id="myTabDrop_7" href="#" aria-expanded="false"><span>More</span> <span class="caret"></span></a>
                                                    <ul id="myTabDrop_7_contents"  class="dropdown-menu">
                                                        <li class=""><a  data-toggle="tab" id="dropdown_13_tab" role="tab" href="#dropdown_13" aria-expanded="true">About</a></li>
                                                        <li class=""><a  data-toggle="tab" id="dropdown_14_tab" role="tab" href="#dropdown_14" aria-expanded="false">Followings</a></li>
                                                        <li class=""><a  data-toggle="tab" id="dropdown_15_tab" role="tab" href="#dropdown_15" aria-expanded="false">Likes</a></li>
                                                        <li class=""><a  data-toggle="tab" id="dropdown_16_tab" role="tab" href="#dropdown_16" aria-expanded="false">Reviews</a></li>
                                                    </ul>
                                                </li> --}}
                                            </ul>
                                            <div class="tab-content" id="myTabContent_8">
                                                <div  id="settings_8" class="tab-pane fade active in" role="tabpanel">
                                                    <!-- Row -->
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="">
                                                                <div class="panel-wrapper collapse in">
                                                                    <div class="panel-body pa-0">
                                                                        <div class="col-sm-12 col-xs-12">
                                                                            <div class="form-wrap">
                                                                                <form action="{{route('system.user.postEditUserInID')}}" method="post">
                                                                                    @csrf
                                                                                    <div class="form-body overflow-hide">
                                                                                        <div class="form-group">
                                                                                            <label class="control-label mb-10" for="exampleInputuname_01">Họ tên</label>
                                                                                            <div class="input-group">
                                                                                                <div class="input-group-addon"><i class="icon-user"></i></div>
                                                                                            <input placeholder="Họ tên" type="text" name="edit_name" class="form-control" id="exampleInputuname_01" value="{{$user->User_Name}}">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label mb-10" for="exampleInputEmail_01">Email</label>
                                                                                            <div class="input-group">
                                                                                                <div class="input-group-addon"><i class="icon-envelope-open"></i></div>
                                                                                                <input placeholder="Email" name="edit_email" type="email" class="form-control" id="exampleInputEmail_01" value="{{$user->User_Email}}">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label mb-10" for="exampleInputContact_01">Số điện thoại</label>
                                                                                            <div class="input-group">
                                                                                                <div class="input-group-addon"><i class="icon-phone"></i></div>
                                                                                                <input placeholder="Số điện thoại" name="edit_phone" type="number" class="form-control" id="exampleInputContact_01" value="{{$user->User_Phone}}">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label mb-10">Giới tính</label>
                                                                                            <div>
                                                                                                <div class="radio">
                                                                                                    <input type="radio" name="edit_gender" id="radio_01" value="Male" @if($user->User_Sex == "Male")checked @endif>
                                                                                                    <label for="radio_01">
                                                                                                    Nam
                                                                                                    </label>
                                                                                                </div>
                                                                                                <div class="radio">
                                                                                                    <input type="radio" name="edit_gender" id="radio_02" value="Female" @if($user->User_Sex == "Female")checked @endif>
                                                                                                    <label for="radio_02">
                                                                                                    Nữ
                                                                                                    </label>
                                                                                                </div>
                                                                                                <div class="radio">
                                                                                                    <input type="radio" name="edit_gender" id="radio_03" value="Other" @if($user->User_Sex == "Other")checked @endif>
                                                                                                    <label for="radio_02">
                                                                                                    Khác
                                                                                                    </label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label mb-10" for="exampleInputContact_01">Ngày sinh</label>
                                                                                            <div class="input-group">
                                                                                                <div class="input-group-addon"><i class="icon-calender"></i></div>
                                                                                                <input placeholder="Ngày sinh" type="text" class="form-control datetimepicker" name="edit_birthday"  id="exampleInputContact_01" value="{{$user->User_Birthday}}">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label mb-10" for="exampleInputContact_01">Địa Chỉ</label>
                                                                                            <div class="input-group">
                                                                                                <div class="input-group-addon"><i class="icon-map"></i></div>
                                                                                                <input placeholder="Địa chỉ" type="text" name="edit_address" class="form-control" id="exampleInputContact_01" value="{{$user->User_Address}}">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label mb-10" for="exampleInputContact_01">Ngày vào làm</label>
                                                                                            <div class="input-group">
                                                                                                <div class="input-group-addon"><i class="icon-calender"></i></div>
                                                                                                <input placeholder="Ngày vào làm" disabled type="text" class="form-control" id="exampleInputContact_01" value="{{$user->User_RegisteredDateTime}}">
                                                                                            </div>
                                                                                        </div>


                                                                                        @if(Session('user')->User_Level != 1 && Session('user')->User_Level != 3)
                                                                                        <div class="form-group">
                                                                                            <label class="control-label mb-10">Địa Điểm Cửa Hàng</label>
                                                                                            <select disabled class="form-control" data-placeholder="Choose a Category" tabindex="1">
                                                                                                <option value="{{$user->Shop_ID}}">{{$user->Shop_Name}}</option>
                                                                                            </select>
                                                                                        </div>
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="form-actions mt-10">
                                                                                        <button type="submit" class="btn btn-default mr-10 mb-30">Lưu</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>
        <!--/Profile blade -->
        <!--Modal Reset Password-->
        @component('Components.modal-reset-password')
        @endcomponent
        <!--Modal google map -->
        @component('Components.modal-google-map')
        @endcomponent
        <!--Modal Reset Password-->



@endsection
@section('script')
<!-- Bootstrap Datetimepicker JavaScript -->
    <script type="text/javascript" src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false,
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
        }).on('dp.show', function() {
            if($(this).data("DateTimePicker").date() === null)
                $(this).data("DateTimePicker").date(moment());
        });

        $("document").ready(function() {
            $('#upload').on('change', function () {
                var file_data = $('#upload').prop('files')[0];
                var type = file_data['type'];
                var match = ["image/gif", "image/png", "image/jpg", 'image/jpeg'];
                if (type == match[0] || type == match[1] || type == match[2] || type == match[3]) {
                    var form_data = new FormData();
                    var form_token = $("input[name='_token']").val();
                    form_data.append('file', file_data);
                    form_data.append('_token', form_token);
                    var request = new XMLHttpRequest();
                    request.open("POST","{{route('system.user.uploadimage')}}");
                    request.send(form_data);
                }
                window.location.reload(false);
            });
        });

    </script>
    <!--Modal Google Map-->

@endsection

