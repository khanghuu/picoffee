@extends('System.Layouts.master')
@section('title', 'All Users')
@section('css')
<!-- Bootstrap Datetimepicker CSS -->
<link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
<style>
    .modal-dialog {
        min-width: 80%!important;
    }
    a:hover{
        cursor: pointer;
    }
</style>
<!-- Custom CSS -->
<link href="dist/css/style.css" rel="stylesheet" type="text/css">
<style>
    .color-white-th{
        background: #ff6028;
    }
    .color-white-th tr th{
        color: white!important;
    }
    td>a:hover{
        cursor: pointer;
    }
    table.dataTable thead .sorting::after, table.dataTable thead .sorting_asc::after, table.dataTable thead .sorting_desc::after{
        display: block!important;
    }
</style>


@endsection
@section('content')
    <div class="panel panel-default border-panel card-view">
        <div class="panel-heading">
            <div class="pull-left">
                <h6 class="panel-title txt-dark">TẤT CẢ NGƯỜI DÙNG</h6>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="panel-wrapper collapse in">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="dt-buttons mb-10">
                            @if(Session('user')->User_Level != 3)
                            <button class="btn btn-primary btn-rounded" data-toggle="modal" data-target=".modal_add_user"><i class="fa fa-plus"></i> Thêm</button>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="table-wrap">
                    <div class="table-responsive">
                        <table id="datable_admin" class="table table-hover table-bordered display mb-30">
                            <thead>
                            <tr>
                                <th>ID </th>
                                <th>Email </th>
                                <th>Tên </th>
                                <th>Cấp bậc </th>
                                @if(Session('user')->User_Level != 2)
                                    <th>Cửa hàng </th>
                                @endif
                                <th>Liên Hệ </th>
                                <th>Ngày giờ </th>
                                @if(Session('user')->User_Level != 3)
                                    <th>Hành Động</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                               @foreach($AllUsers as $item)
                                    <tr>
                                        <td>{{ $item->User_ID }}</td>
                                        <td>{{ $item->User_Email }}</td>
                                        <td>{{ $item->User_Name }}</td>
                                        <td>{{ $item->level_Name }}</td>
                                        @if(Session('user')->User_Level != 2)
                                            <td>{{ $item->Shop_Name }}</td>
                                        @endif
                                        <td class="text-center">
                                            <a href="mailto:{{$item->User_Email}}" data-toggle="tooltip" title="" class="tooltip1" data-original-title="{{$item->User_Email}}"><i class="fa fa-envelope"></i></a>
                                            @if($item->User_Phone != NULL)
                                            <a href="tel:{{ $item->User_Phone }}" data-toggle="tooltip" title="" data-original-title="{{ $item->User_Phone }}"><i class="fa fa-phone"></i></a>
                                            @endif
                                            @if($item->User_Address != NULL)
                                            <a href="http://maps.google.com/?q={{$item->User_Address}}" target="_blank" data-toggle="tooltip" title="" data-original-title="{{$item->User_Address}}"><i class="fa fa-map-marker"></i></a>
                                            @endif
                                        </td>
                                        <td class="text-center">

                                            <i class="fa fa-birthday-cake" data-toggle="tooltip" title="" data-original-title="Sinh nhật: {{$item->User_Birthday}}" class="mr-10"></i>

                                            <i class="fa fa-sign-out" data-toggle="tooltip" title="" data-original-title="Lần đăng nhập cuối: {{date('d-m-Y',$item->User_Login)}}" class="m-5"></i>

                                            <i class="fa fa-history" data-toggle="tooltip" title="" data-original-title="Ngày đăng ký: {{$item->User_RegisteredDateTime}}" class="ml-10"></i>

                                        </td>
                                        @if(Session('user')->User_Level != 3)
                                        <td>
                                            <a data-toggle="modal" data-target=".modal_edit_user"  class="text-warning btn-edit-user" id="user-id-{{$item->User_ID}}" data-toggle="tooltip" title="" data-original-title="Sửa"><i class="fa fa-pencil"></i></a>
                                            <a href="{{ route('system.admin.getLoginByID',$item->User_ID) }}" class="text-primary" data-toggle="tooltip" title="" data-original-title="Đăng nhập"><i class="fa fa-sign-in"></i></a>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>
        <!--Modal edit Material -->

        <!--/Modal edit Material -->
    </div>
    <!--/Profile blade -->
    <!--Add User Modal -->
    @component('Components.modal-add-user', ['shop' => $shop, 'level' => $level])
    @endcomponent
    <!--Add User Modal -->

    <!--Edit User Modal -->
    @component('Components.modal-edit-user', ['shop' => $shop, 'level' => $level])
    @endcomponent
    <!--Edit User Modal -->
@endsection
@section('script')
    <!-- Bootstrap Datetimepicker JavaScript -->
    <script type="text/javascript" src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- Data table JavaScript -->
    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="dist/js/dataTables-data.js?v=1"></script>
    <script>
        $("select[name=level]").change(function(){
            let _id = $(this).val();
            if(_id == 1 || _id == 3){
                $('#select_shop').hide();
            }
            else{
                $('#select_shop').show();
            }
        });
        $("select[name=edit_level]").change(function(){
            let _id = $(this).val();
            if(_id == 1 || _id == 3){
                $('#select_edit_shop').hide();
            }
            else{
                $('#select_edit_shop').show();
            }
        });
        //add
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false,
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
        }).on('dp.show', function() {
            if($(this).data("DateTimePicker").date() === null)
                $(this).data("DateTimePicker").date(moment());
        });

        $( document ).ready(function() {
            $('.btn-edit-user').click(function(){
                btn_id = this.id;
                res = btn_id.split("-");
                _id = res[res.length-1];
                $.ajax({
                    url: '/system/users/edit-user',
                    type: "GET",
                    dataType: "json",
                    data: {id:_id},
                    success: function (data){
                        if(data['status'] == true){
                            $('#id_user').val(data['info_user'].User_ID);
                            $('#edit-name').val(data['info_user'].User_Name);
                            $('#edit-email').val(data['info_user'].User_Email);
                            $('#edit-store').val(data['info_user'].User_Location_Store);
                            $('#edit-birthday').val(data['info_user'].User_Birthday);
                            $('#edit-phone').val(data['info_user'].User_Phone);
                            $("#radio-"+data['info_user'].User_Sex).prop("checked", true );
                            $('#edit-address').val(data['info_user'].User_Address);
                            if(data['info_user'].User_Level == 1 || data['info_user'].User_Level == 3){
                                $('#select_edit_shop').hide();
                            }else{
                                $('#select_edit_shop').show();
                            }
                            $('#edit-level').val(data['info_user'].User_Level);
                            $('#edit-start-date').val(data['info_user'].User_RegisteredDateTime);
                            $('#edit-level option:selected').val(data['info_user'].User_Level);
                            $("#radio-"+data['info_user'].User_Sex).prop("checked", true );
                            // console.log(data['info_user'])
                            $('.modal_edit_user').modal('show');
                        }
                    }
                });
            });
        });
        $( document ).ready(function() {
            $('.btn-cance-user').click(function(){
                $('.modal_edit_user').modal('hide');
                $('.modal_add_user').modal('hide');
            });
        });

    </script>
@endsection
