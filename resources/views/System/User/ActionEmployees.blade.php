@extends('System.Layouts.master')
@section('css')
	<!-- Data table CSS -->
	<link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
    </style>
@endsection
@section('content')

<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Hoạt động nhân viên</h6>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="table-wrap">
						<div class="table-responsive">
                            <table class="table table-striped table-bordered mb-0">
								<thead class="color-white-th">
									<tr>
										<th>ID</th>
                                        <th>ID Tài Khoản</th>
                                        <th>Hành động</th>
                                        <th>Thời gian</th>
                                        <th>IP</th>
									</tr>
								</thead>
								<tbody>
									@foreach($actionUsers as $action)
                                        <tr>
                                            <td>{{ $action->Log_Name_ID }}</td>
                                            <td>{{ $action->Log_Name_User }}</td>
                                            <td>{{ $action->Log_Name_Log }}</td>
                                            <td>{{ $action->Log_Name_DateTime }}</td>
                                            <td>{{ $action->Log_Name_IP }}</td>

                                        </tr>
									@endforeach
								</tbody>
							</table>
                            <div style="text-align: right">
                                {{$actionUsers->onEachSide(1)->links()}}
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
	<!-- Data table JavaScript -->
	<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

@endsection
