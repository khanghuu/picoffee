@extends('System.Layouts.master')
@section('css')
<!-- Bootstrap Datetimepicker CSS -->
<link href="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
<style>
	.modal-dialog {
		min-width: 80%!important;
	}
</style>
@endsection
@section('content')

    <div class="col-sm-12">
		<div class="panel panel-default border-panel card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Danh sách thành viên</h6>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="table-wrap">
						<table data-toggle="table" class="table table-bordered table-striped m-b-0">
							<thead>
								<tr>
									<th>ID</th>
									<th>Email</th>
									<th>Tên</th>
									<th class="text-center">Liên hệ</th>
									<th>Ngày giờ</th>
									<th>Thành viên</th>
								</tr>
							</thead>
							<tbody>
								@foreach($user_list as $v)
								<tr>
									<td>{{ $v->User_ID }}</td>
									<td>{{ $v->User_Email }}</td>
									<td>{{ $v->User_Name }}</td>
									<td class="text-center">
										<a href="mailto:{{$v->User_Email}}" data-toggle="tooltip" title="" class="tooltip1" data-original-title="{{$v->User_Email}}"><i class="fa fa-envelope"></i></a>
										@if($v->User_Phone != NULL)
										<a href="tel:{{ $v->User_Phone }}" data-toggle="tooltip" title="" data-original-title="{{ $v->User_Phone }}"><i class="fa fa-phone"></i></a>
										@endif
										@if($v->User_Address != NULL)
										<a href="http://maps.google.com/?q={{$v->User_Address}}" target="_blank" data-toggle="tooltip" title="" data-original-title="{{$v->User_Address}}"><i class="fa fa-map-marker"></i></a>
										@endif
									</td>
									<td class="text-center">
											
										<i class="fa fa-birthday-cake" data-toggle="tooltip" title="" data-original-title="Sinh nhật: {{$v->User_Birthday}}" class="mr-10"></i>

										<i class="fa fa-sign-out" data-toggle="tooltip" title="" data-original-title="Lần đăng nhập cuối: {{date('d-m-Y',$v->User_Login)}}" class="m-5"></i>

										<i class="fa fa-history" data-toggle="tooltip" title="" data-original-title="Ngày đăng ký: {{$v->User_RegisteredDatetime}}" class="ml-10"></i>

									</td>
									<td>
										F{{$v->f}}
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>	
				</div>	
			</div>	
		</div>	
	</div>	



@endsection
@section('script')
<script>
	$('#dataTable').dataTable();
</script>
@endsection
