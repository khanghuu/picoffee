@extends('System.Layouts.master')
@section('title', 'All Users')
@section('css')

@endsection
@section('content')
    <div class="panel panel-default border-panel card-view">
        <div class="panel-heading">
            <div class="pull-left">
                <h6 class="panel-title txt-dark">Role Groups</h6>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="panel-wrapper collapse in">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="dt-buttons mb-10">
                            <a href="{{route('system.roles.createRolesGroup')}}"><button class="btn btn-primary btn-rounded" data-toggle="modal" data-target=".modal_add_user"><i class="fa fa-plus"></i> Thêm</button></a>
                        </div>
                    </div>
                </div>

                <div class="table-wrap">
                    <div class="table-responsive">
                        <table id="edit_datable_1" class="table table-bordered table-striped m-b-0">
                            <thead>
                            <tr>
                                <th>Role ID </th>
                                <th>Role Name </th>
                                <th>Hành Động</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $item)
                                <tr>
                                    <td>{{ $item->Role_ID }}</td>
                                    <td>{{ $item->Role_Name }}</td>
                                    <td>
                                        <a href="{{route('system.roles.editRoleGroup', $item->Role_ID)}}" class="text-warning btn-edit-user" id="user-id-{{$item->Role_ID}}" data-toggle="tooltip" title="" data-original-title="Sửa"><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>
    </div>

@endsection
@section('script')


@endsection
