@extends('System.Layouts.master')
@section('title', 'Create Role Group')
@section('css')

@endsection
@section('content')
    <div class="panel panel-default border-panel card-view">
        <div class="panel-heading">
            <div class="pull-left">
                <h6 class="panel-title txt-dark">Edit Role Group</h6>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-wrapper collapse in">
            <form action="{{route('system.roles.posteditRoleGroup', $role->Role_ID)}}" method="post">

                @csrf
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 float-lg-right" style="text-align: right">
                            <a href="{{route('system.roles.getManage')}}"><button class="btn left-icon btn-warning" type="button" id="btn-back" onclick="refresh()">Back</button></a>
                            <button class="btn left-icon btn-danger" type="button" id="btn-refresh" onclick="refresh()">reFresh</button>
                            <button type="submit" class="btn left-icon btn-success btn-anim" id="btn-submit">Edit Role Group</button>

                        </div>
                    </div>
                    <div class="form-group">
                        <p hidden>rolename:</p>
                        <label for="inputName" class="control-label mb-10">Nhập Tên Role Group</label>
                        <input type="text" class="form-control" name="rolename" id="role-name" placeholder="Tên Role Group" value="{{$role->Role_Name}}" required>
                    </div>
                    <div class="table-wrap">
                        <div class="table-responsive">
                            <table id="edit_datable_1" class="table table-bordered table-striped m-b-0 ">
                                <thead class="text-center">
                                <tr class="text-center">
                                    <th rowspan="2" >Permission Name</th>
                                    <th colspan="4" class="text-center">Hành Động</th>
                                </tr>
                                <tr>
                                    <td>View</td>
                                    <td>Add</td>
                                    <td>Edit</td>
                                    <td>Delete</td>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($permissions as $item)
                                    <tr>

                                        <p hidden>{{$item->Permission_Name}}:</p>
                                        <td>{{$item->Permission_Name}}</td>

                                            <td class="text-center"><input type="checkbox" name="data[{{$item->Permission_ID}}][view]" value="1" id ="{{$item->Permission_ID}}_view"></td>
                                        </label>
                                        @if($item->Permission_ID != 10 && $item->Permission_ID != 11 &&  $item->Permission_ID != 12  && $item->Permission_ID != 13 )

                                            <td class="text-center"><input type="checkbox" name="data[{{$item->Permission_ID}}][add]" value="1" id ="{{$item->Permission_ID}}_add"></td>
                                            <td class="text-center"><input type="checkbox" name="data[{{$item->Permission_ID}}][edit]" value="1" id ="{{$item->Permission_ID}}_edit"></td>
                                            <td class="text-center"><input type="checkbox" name="data[{{$item->Permission_ID}}][del]" value="1" id ="{{$item->Permission_ID}}_del"></td>
                                        @else
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        @endif

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection
@section('script')
    <script>
        function refresh(){
            $("#role-name").val(null);
            $('input[type=checkbox]').each(function () {
                $(":checkbox").attr("checked", false);
            });
        }

        $("document").ready(function() {
            var json_obj = {!! $permission_list !!};
            for (var item in json_obj) {

                if(json_obj[item][0] != 0)
                {
                    var view_id = '#'+item+"_view";
                    $(view_id).prop("checked", true);

                }
                if(json_obj[item][1] != 0)
                {
                    var add_id = '#'+item+"_add";
                    $(add_id).prop("checked", true);

                }
                if(json_obj[item][2] != 0)
                {
                    var edit_id = '#'+item+"_edit";

                    $(edit_id).prop("checked", true);

                }
                if(json_obj[item][3] != 0)
                {
                    var del_id = '#'+item+"_del";
                    $(del_id).prop("checked", true);

                }
            }

        });
        $("document").ready(function() {
            $("input[type=checkbox]").click( function(){
                if( $(this).is(':checked') ) {
                    var id = $(this).attr('id');
                    var permission_id = id.match(/\d+/g);
                    $("#"+permission_id+"_view").attr("checked", true);

                }
            });

        });

    </script>

@endsection
