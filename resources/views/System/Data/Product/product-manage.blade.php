@extends('System.Layouts.master')
@section('title', 'Danh sách sản phẩm')
@section('css')
    <!-- Data table CSS -->
    <link href="../vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap Dropify CSS -->
    <link href="vendors/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css"/>

    <link href="vendors/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" type="text/css"/>

    <!-- Jasny-bootstrap CSS -->
    <link href="vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>

    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
        td>a:hover{
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Danh sách sản phẩm</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                            @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                                    <a href="{{route('system.product.getProductAdd')}}"><button class="btn btn-primary btn-rounded btn-icon left-icon" > <i class="fa fa-plus"></i> <span>Thêm</span></button></a>
                            @endif
                            <a href="{{route('productExport')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                            </div>
                                <div class="col-md-6 col-sm-12 col-xs-12" style="text-align: right">
                                    <button class="btn btn-default btn-rounded btn-icon left-icon" id="btnLoc"> <i class="fa fa-filter"></i> <span>Lọc</span></button>
                                </div>
                                <!--Toggle filter product -->
                                @component('System.Data.Product.Component.toggle-filter-product', ['categoryList' => $categoryList, 'shopList'=>$shopList])
                                @endcomponent
                                <!--/Toggle filter product -->
                        </div>
                        <div class="table-wrap mt-20">
                            <div class="table-responsive">
                                <table id="datable_product" class="table table-hover table-bordered display mb-30 " >
                                    <thead class="color-white-th">
                                    <tr>
                                        <th style="text-align: center">Mã</th>
                                        <th style="text-align: center">Hình ảnh</th>
                                        <th style="text-align: center">Tên</th>
                                        <th style="text-align: center">Giá</th>
                                        @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 3)
                                            <th style="text-align: center">Cửa hàng</th>
                                        @endif
                                        <th style="text-align: center">Danh mục</th>
                                        <th style="text-align: center">Trạng thái</th>
                                        @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                                            <th style="text-align: center">Hành động</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($productList as $product)
                                        <tr>
                                            <td>{{ $product->Product_Code }}</td>
                                            <td><img src="product/{{$product->Product_Image}}" alt="{{$product->Product_Image}}" style="width:56px"></td>
                                            <td>{{ $product->Product_Name }}</td>
                                            <td>{{ number_format($product->Product_Price,0,",",".") }}</td>
                                            @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 3)
                                                <th>{{$product->Shop_Name}}</th>
                                            @endif
                                            <td>{{ $product->Category_Name }}</td>
                                            <td>
                                                @if($product->Product_Status == 1)
                                                    <span class="label label-success">Đang bán</span>
                                                @else
                                                    <span class="label label-info">Đã đóng </span>
                                                @endif
                                            </td>
                                            @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level == 2)
                                                <td>
                                                    <a href="{{route('system.product.getProductEdit', $product->Product_ID)}}"><i class="fa fa-edit"></i></a>
                                                        &nbsp <a href="{{route('system.product.getDelete', $product->Product_ID) }}" onclick="return confirm('Bạn muốn xóa sản phẩm ?')"><i class="fa fa-bitbucket"></i></a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <!-- Data table JavaScript -->
    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="dist/js/dataTables-data.js"></script>
    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="vendors/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>
    <script src="vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
    <!-- Form Flie Upload Data JavaScript -->
    <script src="dist/js/form-file-upload-data.js"></script>

    <!-- Slimscroll JavaScript -->
    <script src="dist/js/jquery.slimscroll.js"></script>


    <script>
            $("select[name=EditProductDetail]").change(function(){
                _id = $(this).val();
                if(_id!=''){
                    $.ajax({
                        url: '{{ route('system.ingredient.getAjaxIngredient') }}',
                        type: "GET",
                        dataType: "json",
                        data: {id:_id},
                        success: function (data){
                            if(data['status'] == true){
                                $('#ProductDetail').append('<div class="product-detail-item col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item' + data['data']['ingredient_ID'] + '"><div class="input-group mb-15"> <span class="input-group-addon">' + data['data']['ingredient_Name'] + '</span><input name="ingredient[' + data['data']['ingredient_ID'] + ']" type="text" placeholder="Số lượng nguyên liệu tinh cần thiết" class="form-control"><span class="input-group-addon" class="unit">' + data['data']['ingredient_Unit'] + '</span><span class="input-group-addon deleteItem unit" style="background: red; color: #fff;cursor: pointer;" data-id="' + data['data']['ingredient_ID'] + '"><i class="ti-trash remove-ingredient" ></i></span></div></div><div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item' + data['data']['ingredient_ID'] + '"></div>');
                            }
                        }
                    });
                }
            });

            $("#ProductDetail").on('click', '.deleteItem', function (){
                _id = $(this).data('id');
                $('#ProductDetail .item'+_id).remove();

            });

            $( "#btnLoc" ).click(function() {
            $("#divLoc").toggle();
        });
        $(".openModel").click(function(){
            _id = $(this).data('id');
            $.ajax({
                url: '{{ route('system.product.getProduct') }}',
                type: "GET",
                dataType: "json",
                data: {id:_id},
                success: function (data){
                    if(data['status'] == false){
                        $.toast({
                            heading: 'Error',
                            text: data['message'],
                            position: 'top-right',
                            loaderBg:'#fec107',
                            icon: 'error',
                            hideAfter: 3500,
                            stack: 6
                        });
                    }else{
                        $('input[name=EditName]').val(data['data']['Category_Name']);
                        $('input[name=EditOrder]').val(data['data']['Category_Order']);
                        $('input[name=eidtId]').val(data['data']['Category_ID']);
                        $("select[name=EditParent] > option").each(function() {

                            if($(this).val() == data['data']['Category_Parent']){
                                $(this).attr('selected', true);
                            }
                        });
                        if(data['data']['Category_Status'] == 1){
                            $('input[name=Editstatus]').attr('checked', true);
                        }
                    }
                }
            });
        });
    </script>
@endsection
