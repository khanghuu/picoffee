<div class="col-md-12 col-sm-12 col-xs-12 mt-20" id="divLoc" style="display: none">
    <div class="form-wrap">
        <form class="form-horizontal" method="get">
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Mã sản phẩm:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control"  name="search_product_code" value="{{request()->get('search_product_code')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Tên sản phẩm:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="search_product_name" value= "{{request()->get('search_product_name')}}">
                </div>
            </div>
            @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                <div class="form-group">
                    <label class="control-label mb-10 col-sm-2">Danh cửa hàng:</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="search_product_shop">
                            <option value="" disabled selected>----Chọn cửa hàng----</option>
                            @foreach($shopList as $shop)
                                <option value="{{$shop->Shop_ID}}" {{$shop->Shop_ID == request()->get('search_product_shop')?'selected':''}}>{{$shop->Shop_Name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

            @endif
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Danh mục:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="search_product_category">
                        <option value="" disabled selected>----Chọn Category----</option>
                        @foreach($categoryList as $category)
                            <option value="{{$category->Category_ID}}" {{$category->Category_ID == request()->get('search_product_category')?'selected':''}}>{{$category->Category_Name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Trạng thái:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="search_product_status">
                        <option value="" selected>----Chọn trạng thái----</option>
                        <option value="0" {{request()->get('search_product_status') === '0'?'selected':''}}>Đã đóng</option>
                        <option value="1" {{request()->get('search_product_status') === '1'?'selected':''}}>Đang bán</option>
                    </select>
                </div>
            </div>
            <div class="form-group mb-0">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-orange btn-anim"><i class="fa fa-search"></i><span class="btn-text">Tìm kiếm</span></button>
                    <a href="{{ route('system.product.getManage') }}"><button type="button" class="btn btn-default btn-anim"><i class="fa fa-times"></i><span class="btn-text">Huỷ</span></button></a>
                </div>
            </div>
        </form>
    </div>
</div>
