<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.product.postAdd') }}" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Thêm sản phẩm</h5>
                </div>
                <div class="modal-body">
                    <div class="form-wrap">
                        <div class="row" style="border-bottom: 1px solid #eee;">
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="panel-default border-panel card-view">

                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">
                                            <p class="text-muted">Hình sản phẩm (300x300)</p>
                                            <div class="mt-20">
                                                <input type="file" id="input-file-now" class="dropify" name="images" accept=".jpg,.png,.gif,.jpeg" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                        <input name="code" type="text" placeholder="Mã sản phẩm" class="form-control" disabled="disabled">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-graduation-cap"></i></span>
                                        <input name="name" type="text" placeholder="Tên sản phẩm" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <select class="form-control mb-15" name="category">
                                        <option value="">-- Chọn danh mục --</option>
                                        @foreach($category as $v)
                                            <option value="{{ $v->Category_ID }}">{{ $v->Category_Name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-share-alt"></i></span>
                                        <input name="price" type="text" placeholder="Giá" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="border-bottom: 1px solid #eee;">
                            <div class="col-md-12">
                                <h6 class="txt-dark capitalize-font"><i class="ti-layers-alt mr-10"></i>Thành phần chi tiết</h6>
                                <div id="ProductDetail">

                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 mt-20">
                                    <div class="form-group">
                                        <select class="form-control" id="ProductDetail" name="ProductDetail">
                                            <option value="">-- Chọn thành phần --</option>
                                            @foreach($ingredient as $v)
                                                <option value="{{ $v->ingredient_ID }}">{{ $v->ingredient_Name }} ({{ $v->ingredient_Unit }})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row" style="border-bottom: 1px solid #eee;">
                            <div class="col-md-12">
                                <h6 class="txt-dark capitalize-font"><i class="ti-harddrives mr-10"></i>Mở rộng</h6>
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                        <input readonly="" name="time" type="text" value="{{ date('Y-m-d H:i:s') }}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <select class="form-control" name="status">
                                        <option value="1">Hiển thị</option>
                                        <option value="0">Ẩn</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning">Thêm</button>
                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                    {{ csrf_field() }}
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
