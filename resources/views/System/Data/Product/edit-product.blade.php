@extends('System.Layouts.master')
@section('title', 'Sửa sản phẩm')
@section('css')
    <!-- Bootstrap Dropify CSS -->
    <link href="vendors/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css"/>

    <link href="vendors/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" type="text/css"/>

    <!-- Jasny-bootstrap CSS -->
    <link href="vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet"
          type="text/css"/>

    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Sửa sản phẩm</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                            <form action="{{route('system.product.postEdit')}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-wrap">
                                    <div class="row" style="border-bottom: 1px solid #eee;">
                                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                            <div class="panel-default border-panel card-view">
                                                <div class="panel-wrapper collapse in">
                                                    <div class="panel-body">
                                                        <p class="text-muted">Hình sản phẩm (300x300)</p>
                                                        <div class="mt-20">
                                                            <input type="file" class="dropify" name="edit_product_image" id='edit_product_image' accept=".jpg,.png,.gif,.jpeg" data-default-file="product/{{$editProduct->Product_Image}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                                    <input name="edit_product_code" id="edit_product_code" type="text" placeholder="Mã sản phẩm" class="form-control" value="{{$editProduct->Product_Code}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <input name="edit_product_name" id="edit_product_name" type="text" placeholder="Tên sản phẩm" class="form-control" value="{{$editProduct->Product_Name}}">
                                                </div>
                                            </div>
                                            @if(session('user')->User_Level == 1)
                                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-street-view"></i></span>
                                                        <input type="text"  class="form-control" value="{{$editProduct->Shop_Name}}"  readonly>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                <div class="input-group mb-15"><span class="input-group-addon"><i class="fa fa-list"></i></span>
                                                    <select class="form-control" name="edit_product_category">
                                                        <option value="">----Chọn danh mục----</option>
                                                        @foreach($categoryList as $category)
                                                            <option value="{{ $category->Category_ID }}" {{$category->Category_ID == $editProduct->Product_Category?'selected':''}}>{{ $category->Category_Name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                                <div class="input-group mb-15"><span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                                    <input name="edit_product_price" type="text" placeholder="Giá" class="form-control" value="{{$editProduct->Product_Price}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <h6 class="txt-dark capitalize-font"><i class="ti-layers-alt mr-10"></i>Thành phần chi tiết</h6>
                                            <div id="ProductDetail">

                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 mt-20">
                                                <div class="form-group">
                                                    <select class="form-control" id="edit_product_ingredient" name="">
                                                        <option value="">----Chọn thành phần----</option>
                                                        @foreach($ingredientList as $ingredient)
                                                            <option value="{{$ingredient->Ingredient_ID}}" data-value="{{$ingredient->Ingredient_Unit}}">{{ $ingredient->Ingredient_Name}}({{$ingredient->Ingredient_Unit}})</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <h6 class="txt-dark capitalize-font"><i class="ti-harddrives mr-10"></i>Mở rộng</h6>
                                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                                    <input readonly="" name="time" type="text" value="{{ date('Y-m-d H:i:s') }}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                <select class="form-control" name="edit_product_status">
                                                    <option value="1" {{$editProduct->Product_Status == 1?'selected':''}}>Hiển thị</option>
                                                    <option value="0" {{$editProduct->Product_Status == 0?'selected':''}}>Ẩn</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-right" style="margin-bottom: 10px;">
                                            <button type="submit" class="btn btn-warning">Sửa</button>
                                            <a href="{{route('system.product.getManage')}}" class="btn btn-danger">Hủy</a>
                                        </div>
                                    </div>
                                </div>
                            </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="vendors/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>
    <script src="vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
    <!-- Form Flie Upload Data JavaScript -->
    <script src="dist/js/form-file-upload-data.js"></script>

    <!-- Slimscroll JavaScript -->
    <script src="dist/js/jquery.slimscroll.js"></script>
    <script>
        $(document).ready(function () {
            productParams = @json($editProduct->Product_Params);
            console.log(productParams);
            $.each(productParams, function (index, ingredient) {
                ingredient_id = ingredient.Ingredient_ID;
                if (ingredient_id != 'null' && !$("div").hasClass("item-" + ingredient_id)) {
                    option_text = ingredient.Ingredient_Name;
                    option_unit = ingredient.Ingredient_Unit;
                    option_quantity = ingredient.Ingredient_Quantity;
                    _html = "<div class='product-detail-item col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item-"
                        +ingredient_id
                        + "'><div class='input-group mb-15'><span class='input-group-addon'>"
                        + option_text +
                        "</span><input name='ingredient["
                        + ingredient_id
                        + "]' type='number' placeholder='Số lượng nguyên liệu tinh cần thiết' class='form-control' min='0' step='0.0000000001' value='"
                        + option_quantity
                        +"'><span class='input-group-addon' class='unit'>"
                        + option_unit
                        + "</span><span class='input-group-addon delete-Item unit' style='background: red; color: #fff;cursor: pointer;' data-id='"
                        + ingredient_id
                        + "'><i class='ti-trash remove-ingredient'></i></span></div></div><div class='col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item-'"
                        + ingredient_id
                        + "'></div>";
                    $('#ProductDetail').append(_html);
                }
            });
            $("#edit_product_ingredient").change(function () {
                ingredient_id = $(this).val();
                if (ingredient_id != 'null' && !$("div").hasClass("item-" + ingredient_id)) {
                    option_text = $(this).find('option:selected').text();
                    option_unit = $(this).find('option:selected').attr('data-value');
                    _html = "<div class='product-detail-item col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item-"
                        +ingredient_id
                        + "'><div class='input-group mb-15'><span class='input-group-addon'>"
                        + option_text +
                        "</span><input name='ingredient["
                        + ingredient_id
                        + "]' type='number' placeholder='Số lượng nguyên liệu tinh cần thiết' class='form-control' min='0' step='0.0000000001'><span class='input-group-addon' class='unit'>"
                        + option_unit
                        + "</span><span class='input-group-addon delete-Item unit' style='background: red; color: #fff;cursor: pointer;' data-id='"
                        + ingredient_id
                        + "'><i class='ti-trash remove-ingredient'></i></span></div></div><div class='col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item-'"
                        + ingredient_id
                        + "'></div>";
                    $('#ProductDetail').append(_html);
                }
            });
            $("#ProductDetail").on('click', '.delete-Item', function (){
                _id = $(this).data('id');
                $('#ProductDetail .item-'+_id).remove();
            });
        });


    </script>
@endsection
