<div class="col-md-12 col-sm-12 col-xs-12 mt-20" id="divLoc" style="display: none">
    <div class="form-wrap">
        <form class="form-horizontal" method="get">
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Mã:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="" name="search_raw_code" value="{{request()->get('search_raw_code')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Tên:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="search_raw_name" value="{{request()->get('search_raw_name')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Đơn vị:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="search_raw_unit">
                        <option value="">----Chọn đơn vị----</option>
                        @foreach($rawUnit as $unit)
                            <option value="{{ $unit->Raw_Material_Unit }}" {{$unit->Raw_Material_Unit == request()->get('search_raw_unit')?'selected':''}}>{{ $unit->Raw_Material_Unit }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Trạng thái:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="search_raw_status">
                        <option value="" selected>----Chọn trạng thái----</option>
                        <option value="0" {{request()->get('search_raw_status') === '0'?'selected':''}}>Đóng</option>
                        <option value="1" {{request()->get('search_raw_status') == '1'?'selected':''}}>Mở</option>
                    </select>
                </div>
            </div>
            @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                <div class="form-group">
                    <label class="control-label mb-10 col-sm-2">Quán:</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="search_raw_shop">
                            <option value="" selected>----Chọn quán----</option>
                            @foreach($shopList as $shop)
                                <option value="{{ $shop->Shop_ID }}" {{ $shop->Shop_ID == request()->get('search_raw_shop')?'selected':''}}>{{ $shop->Shop_Name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @endif
            <div class="form-group mb-0">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-orange btn-anim"><i class="fa fa-search"></i><span class="btn-text">Tìm kiếm</span></button>
                    <a href="{{ route('system.raw.getManageRaw') }}"><button type="button" class="btn btn-default btn-anim"><i class="fa fa-search"></i><span class="btn-text">Huỷ</span></button></a>
                </div>
            </div>
        </form>
    </div>
</div>
