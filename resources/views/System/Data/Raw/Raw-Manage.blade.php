@extends('System.Layouts.master')
@section('title', 'Nguyên liệu thô')
@section('css')
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }
        table.dataTable thead .sorting::after, table.dataTable thead .sorting_asc::after, table.dataTable thead .sorting_desc::after{
            display: none!important;
        }
    </style>

  @endsection
@section('content')
        <div class="panel panel-default border-panel card-view">
            <div class="panel-heading">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Danh Sách Nguyên Liệu Thô</h6>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-wrapper collapse in">
                <div class="panel-body">
                    <div class="row">

                        <div class="col-md-6 col-sm-12 col-xs-12">
                            @if(session('user')->User_Level == 1 || session('user')->User_Level == 2)
                                <button class="btn btn-primary btn-rounded btn-icon left-icon" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive"> <i class="fa fa-plus"></i> <span>Thêm</span></button>
                                <!-- Adding new Material Modal -->
                                @component('System.Data.Raw.Component.modal-add-raw', ['shopList' => $shopList])
                                @endcomponent()
                            @endif
                            <a href="{{route('rawExport')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12" style="text-align: right">
                            <button class="btn btn-default btn-rounded btn-icon left-icon" id="btnLoc"> <i class="fa fa-filter"></i> <span>Lọc</span></button>
                        </div>
                        <!--Toggle filter material -->
                        @component('System.Data.Raw.Component.toggle-filter-raw', ['rawUnit' => $rawUnit, 'shopList' => $shopList])
                        @endcomponent()
                        <!--/Toggle filter material -->

                    </div>
                    <div class="table-wrap mt-20">
                        <div class="table-responsive">
                            <table id="datable_raw" class="table table-striped table-bordered mb-0">
                                <thead class="color-white-th">
                                    <tr>
                                        <th>Mã</th>
                                        <th>Tên</th>
                                        <th>Đơn Vị</th>
                                        <th>Ngày Tạo</th>
                                        @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                            <th>Cửa hàng</th>
                                        @endif
                                        <th>Trạng Thái</th>
                                        @if( session('user')->User_Level != 3)
                                            <th>Hành Động</th>
                                        @endif

                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($rawList as $item)
                                    <tr>
                                        <td>{{$item->Raw_Material_Code}}</td>
                                        <td>{{$item->Raw_Material_Name}}</td>
                                        <td>{{$item->Raw_Material_Unit}}</td>
                                        <td>{{$item->Raw_Material_Create}}</td>
                                        @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                            <td>{{$item->Shop_Name}}</td>
                                        @endif
                                        <td >
                                            @if($item->Raw_Material_Status)
                                                <span class="label label-success">Mở</span>
                                            @else
                                                <span class="label label-info">Đóng</span>
                                            @endif
                                        </td>
                                        @if( session('user')->User_Level != 3)
                                            <td><a id="dataopen"href="#" data-toggle="modal" data-target="#modal-edit-raw" data-id="{{ $item->Raw_Material_ID }}" class="openModel"><i class="fa fa-edit"></i></a> &nbsp &nbsp <a href="{{ route('system.raw.getDeleteRaw', $item->Raw_Material_ID) }}" onclick="return confirm('Bạn có chắc chắn')"><i class="fa fa-bitbucket"></i></a></td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div style="text-align: right">
                                {{$rawList->onEachSide(1)->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Modal edit Material -->
            @component('System.Data.Raw.Component.modal-edit-raw', ['shopList' => $shopList])
            @endcomponent
            <!--/Modal edit Material -->
        </div>
    <!--/Profile blade -->

@endsection
@section('script')
    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="dist/js/dataTables-data.js"></script>
    <script>
        $( "#btnLoc" ).click(function() {
            $("#divLoc").toggle();
        });
        $(".openModel").click(function(){
            _id = $(this).data('id');
            $.ajax({
                url: '{{ route('system.raw.getAjaxRaw') }}',
                type: "GET",
                dataType: "json",
                data: {id:_id},
                success: function (data){
                    if(data['status'] == false){
                        $.toast({
                            heading: 'Error',
                            text: data['message'],
                            position: 'top-right',
                            loaderBg:'#fec107',
                            icon: 'error',
                            hideAfter: 3500,
                            stack: 6
                        });
                    }else{
                        $("#edit_raw_shop").val(data['dataResponse']['Raw_Material_Shop']).change();
                        $('input[name=edit_raw_code]').val(data['dataResponse']['Raw_Material_Code']);
                        $('input[name=edit_raw_name]').val(data['dataResponse']['Raw_Material_Name']);
                        $('input[name=edit_raw_unit]').val(data['dataResponse']['Raw_Material_Unit']);
                        $('input[name=edit_raw_id]').val(data['dataResponse']['Raw_Material_ID']);
                        if(data['dataResponse']['Raw_Material_Status'] == 1){
                            $('input[name=edit_raw_status]').attr('checked', true);
                        }
                        $('#modal-edit-raw').modal('show');
                    }
                }
            });
        });

    </script>

@endsection
