<div class="modal fade modal-add-ingredient" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.ingredient.postAddIngredient') }}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Thêm nguyên liệu Tinh</h5>
                </div>
                <div class="modal-body">
                    <div class="form-wrap">
                        <div class="row">
                            @if(session('user')->User_Level == 1 )
                                <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">

                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                        <select class="form-control select2" name="add_ingredient_shop" id="add_ingredient_shop" required>
                                            <option value="" selected>--- Chọn cửa hàng ---</option>
                                            @foreach($shopList as $shop)
                                                <option value="{{ $shop->Shop_ID }}">{{ $shop->Shop_Name }}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                                        <input name="add_ingredient_name" type="text" placeholder="Tên nguyên liệu tinh" class="form-control" required>
                                    </div>


                                    <div class="checkbox checkbox-success">
                                        <input id="checkbox_33" type="checkbox" name="add_ingredient_status" value="1">
                                        <label for="checkbox_33">Kích hoạt</label>
                                    </div>
                                </div>
                            @endif
                                @if(session('user')->User_Level == 2 )
                                    <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">
                                        <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                                            <input name="add_ingredient_name" type="text" placeholder="Tên nguyên liệu tinh" class="form-control" required>
                                        </div>
                                        <div class="checkbox checkbox-success">
                                            <input id="checkbox_33" type="checkbox" name="add_ingredient_status" value="1">
                                            <label for="checkbox_33">Kích hoạt</label>
                                        </div>
                                    </div>
                                @endif
                            <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">
                                <div class="input-group mb-15"> <span class="input-group-addon">Số lượng</span>
                                    <input value="1" readonly name="add_ingredient_number" type="number" placeholder="Số lượng nguyên liệu tinh" class="form-control" {{--min="0" step="0.0001"--}} required>
                                    <input name="add_ingredient_unit" type="text" placeholder="Đơn vị" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">Thành phần đã chọn:</div>

                            <div id="RawArray"></div>
                            @if(session('user')->User_Level == 1)
                                <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">
                                    <select class="form-control select2" id="add_ingredient_raw">

                                    </select>
                                </div>
                            @endif
                            @if(session('user')->User_Level == 2)
                                <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">
                                    <select class="form-control select2" id="add_ingredient_raw">
                                        <option>--- Chọn nguyên liệu thô ---</option>
                                        @foreach($rawList as $raw)
                                            <option value="{{ $raw->Raw_Material_ID }}">{{ $raw->Raw_Material_Name }} ({{ $raw->Raw_Material_Unit }})</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning">Lưu</button>
                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                    {{ csrf_field() }}
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
