<div class="col-md-12 col-sm-12 col-xs-12 mt-20" id="divLoc" style="display: none">
    <div class="form-wrap">
        <form class="form-horizontal" method="get">
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Nhập mã:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control"  name="search_ingredient_code" value="{{request()->get('search_ingredient_code')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Nhập tên:</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" name="search_ingredient_name" value="{{request()->get('search_ingredient_name')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Đơn vị:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="search_ingredient_unit">
                        <option value="">----Chọn đơn vị----</option>

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Trạng thái</label>
                <div class="col-sm-10">
                    <select class="form-control" name="search_ingredient_status">
                        <option value="" selected>----Chọn trạng thái----</option>
                        <option value="0" {{request()->get('search_ingredient_status') === '0'?'selected':''}}>Đóng</option>
                        <option value="1" {{request()->get('search_ingredient_status') === '1'?'selected':''}}>Mở</option>
                    </select>
                </div>
            </div>
            <div class="form-group mb-0">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-orange btn-anim"><i class="fa fa-search"></i><span class="btn-text">Tìm kiếm</span></button>
                    <a href="{{ route('system.ingredient.getManageIngredient') }}"><button type="button" class="btn btn-default btn-anim"><i class="fa fa-times"></i><span class="btn-text">Hủy</span></button></a>
                </div>
            </div>
        </form>
    </div>
</div>
