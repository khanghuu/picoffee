<div id="modal-edit-ingredient" class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.ingredient.postEditIngredient') }}" method="post">
                @csrf
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Sửa nguyên liệu tinh</h5>
                </div>
                <div class="modal-body">
                    <div class="modal-body">
                        <div class="form-wrap">
                            <div class="row">
                                <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">
                                    @if(session('user')->User_Level == 1)
                                        <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                            <select class="form-control select2" name="edit_ingredient_shop" id="edit_ingredient_shop" disabled>

                                            </select>
                                        </div>
                                    @endif
                                    <div class="input-group mb-15"> <span class="input-group-addon">Mã</span>
                                        <input name="edit_ingredient_code" type="text" placeholder="Mã" class="form-control" disabled="disabled">
                                    </div>

                                    <div class="input-group mb-15"> <span class="input-group-addon">Tên</span>
                                        <input name="edit_ingredient_name" type="text" placeholder="Tên nguyên liệu thô" class="form-control">
                                    </div>

                                        <div class="checkbox checkbox-success">
                                            <input id="checkbox_33" type="checkbox" name="edit_ingredient_status">
                                            <label for="checkbox_33">Kích hoạt</label>
                                        </div>
                                </div>

                                <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">
                                    <div class="input-group mb-15"> <span class="input-group-addon">Số lượng</span>
                                        <input value="1" readonly name="edit_ingredient_number" type="text" placeholder="Số lượng nguyên liệu tinh" class="form-control">
                                        <input name="edit_ingredient_unit" type="text" placeholder="Đơn vị" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">Thành phần đã chọn:</div>

                                <div id="RawEditArray" class="RawArray"></div>
                                <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">
                                    <label class="control-label mb-10">Thành phần</label>
                                    <select class="form-control select2" id="edit_ingredient_raw" name="edit_ingredient_raw">
                                        <option>Chon nguyên liệu thô</option>
                                        @foreach($rawList as $raw)
                                            <option value="{{ $raw->Raw_Material_ID }}">{{ $raw->Raw_Material_Name }} ({{ $raw->Raw_Material_Unit }})</option>
                                        @endforeach

                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning">Lưu</button>
                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                    <input type="hidden" name="edit_ingredient_id" value="">
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
