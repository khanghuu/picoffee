@extends('System.Layouts.master')
@section('css')
    <!-- Data table CSS -->
    {{-- <link href="../vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/> --}}
    <!-- select2 CSS -->
    <link href="vendors/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>
    <style >
        .select2-container--default .select2-selection--single{border:none !important;}
        .select2-container--default .select2-selection--single .select2-selection__rendered { line-height: 40px !important;}
        .select2-container--default.select2-container--open .select2-selection__rendered{border-color: #aaaaaa !important;}

         .color-white-th{
             background: #ff6028;
         }
        .color-white-th tr th{
            color: white!important;
        }
        a:hover{
            cursor: pointer;
        }

        table.dataTable thead .sorting::after, table.dataTable thead .sorting_asc::after, table.dataTable thead .sorting_desc::after{
            display: none!important;
        }
    </style>

@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Danh sách nguyên liệu tinh</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                @if(Session('user')->User_Level == 1 || Session('user')->User_Level == 2)
                                    <button class="btn btn-primary btn-rounded btn-icon left-icon" data-toggle="modal" data-target=".modal-add-ingredient" class="model_img img-responsive"> <i class="fa fa-plus"></i> <span>Thêm</span></button>
                                    <!-- sample modal content -->
                                    @component('System.Data.Ingredient.Component.modal-add-ingredient', ['rawList' =>$rawList, 'shopList' => $shopList])
                                    @endcomponent
                                @endif
                                <a href="{{route('ingExport')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12" style="text-align: right">
                                <button class="btn btn-default btn-rounded btn-icon left-icon" id="btnLoc"> <i class="fa fa-filter"></i> <span>Lọc</span></button>
                            </div>
                             <!--Toggle filter product -->
                             @component('System.Data.Ingredient.Component.toggle-filter-ingredient', ['unitList' =>$unitList])
                             @endcomponent
                             <!--/Toggle filter product -->

                        </div>
                        <div class="table-wrap mt-20">
                            <div class="table-responsive">
                                <table id="datable_ing" class="table table-striped table-bordered mb-0">
                                    <thead class="color-white-th">
                                    <tr>
                                        <th style="text-align: center">Mã</th>
                                        <th style="text-align: center">Tên</th>
                                        <th style="text-align: center">Đơn vị</th>

                                        <th style="text-align: center">Ngày tạo</th>
                                        @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                            <th>Cửa hàng</th>
                                        @endif
                                        <th style="text-align: center">Trạng thái</th>
                                        @if(session('user')->User_Level == 1 || session('user')->User_Level == 2)
                                            <th style="text-align: center">Hành động</th>
                                        @endif

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($ingredientList as $ingredient)
                                        <tr>
                                            <td>{{ $ingredient->Ingredient_Code }}</td>
                                            <td>{{ $ingredient->Ingredient_Name }}</td>
                                            <td>{{ $ingredient->Ingredient_Unit }}</td>

                                            <td>{{ $ingredient->Created_At }}</td>

                                            @if(session('user')->User_Level == 1 || session('user')->User_Level == 3)
                                                <td>{{$ingredient->Shop_Name}}</td>
                                            @endif
                                            <td>
                                                @if($ingredient->Ingredient_Status)
                                                    <span class="label label-success">Mở</span>
                                                @else
                                                    <span class="label label-info">Đóng</span>
                                                @endif
                                            </td>
                                            @if(session('user')->User_Level == 1 || session('user')->User_Level == 2)
                                                <td>
                                                    <a data-id="{{ $ingredient->Ingredient_ID }}" class="openEditModal"><i class="fa fa-edit"></i></a>&nbsp
                                                    <a href="{{ route('system.ingredient.getDeleteIngredient', $ingredient->Ingredient_ID) }}" onclick="return confirm('Bạn có chắc chắn')"><i class="fa fa-bitbucket"></i></a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div style="text-align: right">
                                    {{$ingredientList->onEachSide(1)->links()}}
                                </div>
                                <!--Modal edit ingredient -->
                                    @component('System.Data.Ingredient.Component.modal-edit-ingredient', ['rawList' => $rawList, 'shopList' => $shopList])
                                    @endcomponent
                            <!--/Modal edit ingredient -->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection
@section('script')
    <script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
    <!-- Data table JavaScript -->
    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="dist/js/dataTables-data.js"></script>
    <script>

            $('#add_ingredient_shop').change(function () {
                shop_id = $(this).val();
                $.ajax({
                    url: '{{route('system.warehouse.getDataDestroyRaw')}}',
                    type: "GET",
                    dataType: "json",
                    data: {shop_id: shop_id},
                    success: function (data) {
                        if(data.status) {
                            console.log(data);
                            $("#add_ingredient_raw").empty();
                            $("#add_ingredient_raw").append(new Option('----Chọn nguyên liệu----', null));
                            $.each(data.dataResponse, function(index, raw) {
                                $("#add_ingredient_raw").append(new Option(raw.Raw_Material_Name + '(' + raw.Raw_Material_Unit +')', raw.Raw_Material_ID));
                            });
                        }
                    }
                });
            });

            $("select[id=add_ingredient_raw]").change(function(){
                let _id = $(this).val();
                if(_id!=''){
                    $.ajax({
                        url: '{{ route('system.raw.getAjaxRaw') }}',
                        type: "GET",
                        dataType: "json",
                        data: {id:_id},
                        success: function (data){
                            if(data.status) {
                                if (_id != 'null' && !$("div").hasClass("item-" + _id)) {
                                    if ($('#RawArray').html() == "") {
                                        $('#RawArray').append('<div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item-' + data['dataResponse']['Raw_Material_ID'] + '"><div class="input-group mb-15"> <span class="input-group-addon">' + data['dataResponse']['Raw_Material_Name'] + '</span><input name="raw[' + data['dataResponse']['Raw_Material_ID'] + ']" type="number" step="any" placeholder="Số lượng nguyên liệu thô cần thiết" class="form-control"><span class="input-group-addon" class="unit">' + data['dataResponse']['Raw_Material_Unit'] + '</span><span class="input-group-addon deleteItem unit" style="background: red; color: #fff;cursor: pointer;" data-id="' + data['dataResponse']['Raw_Material_ID'] + '"><i class="ti-trash"></i></span></div></div>');
                                    } else {
                                        $('#RawArray').append('<div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item-' + data['dataResponse']['Raw_Material_ID'] + '">+</div><div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item-' + data['dataResponse']['Raw_Material_ID'] + '"><div class="input-group mb-15"> <span class="input-group-addon">' + data['dataResponse']['Raw_Material_Name'] + '</span><input name="raw[' + data['dataResponse']['Raw_Material_ID'] + ']" type="number" step="any" placeholder="Số lượng nguyên liệu thô cần thiết" class="form-control"><span class="input-group-addon" class="unit">' + data['dataResponse']['Raw_Material_Unit'] + '</span><span class="input-group-addon deleteItem unit" style="background: red; color: #fff;cursor: pointer;" data-id="' + data['dataResponse']['Raw_Material_ID'] + '"><i class="ti-trash"></i></span></div></div>');
                                    }
                                }
                            }
                        }
                    });
                }
            });

            $("#edit_ingredient_raw").change(function(){
                _id = $(this).val();
                if(_id!=''){
                    $.ajax({
                        url: '{{ route('system.raw.getAjaxRaw') }}',
                        type: "GET",
                        dataType: "json",
                        data: {id:_id},
                        success: function (data){
                            if(data['status']){
                                if (_id != 'null' && !$("div").hasClass("item-" + _id)) {
                                    if ($('#RawEditArray').html() == "") {
                                        $('#RawEditArray').append('<div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item-' + data['dataResponse']['Raw_Material_ID'] + '"><div class="input-group mb-15"> <span class="input-group-addon">' + data['dataResponse']['Raw_Material_Name'] + '</span><input name="raw[' + data['dataResponse']['Raw_Material_ID'] + ']" type="number" step="any" placeholder="Số lượng nguyên liệu thô cần thiết" class="form-control"><span class="input-group-addon" class="unit">' + data['dataResponse']['Raw_Material_Unit'] + '</span><span class="input-group-addon deleteItem unit" style="background: red; color: #fff;cursor: pointer;" data-id="' + data['dataResponse']['Raw_Material_ID'] + '"><i class="ti-trash"></i></span></div></div>');
                                    } else {
                                        $('#RawEditArray').append('<div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item-' + data['dataResponse']['Raw_Material_ID'] + '">+</div><div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item-' + data['dataResponse']['Raw_Material_ID'] + '"><div class="input-group mb-15"> <span class="input-group-addon">' + data['dataResponse']['Raw_Material_Name'] + '</span><input name="raw[' + data['dataResponse']['Raw_Material_ID'] + ']" type="number" step="any" placeholder="Số lượng nguyên liệu thô cần thiết" class="form-control"><span class="input-group-addon" class="unit">' + data['dataResponse']['Raw_Material_Unit'] + '</span><span class="input-group-addon deleteItem unit" style="background: red; color: #fff;cursor: pointer;" data-id="' + data['dataResponse']['Raw_Material_ID'] + '"><i class="ti-trash"></i></span></div></div>');
                                    }
                                }
                            }
                        }
                    });
                }
            });

            $("#RawArray").on('click', '.deleteItem', function (){
                _id = $(this).attr('data-id');
                $('#RawArray .item-'+_id).remove();

            });

            $("#RawEditArray").on('click', '.deleteItem', function (){
                _id = $(this).attr('data-id');
                $('#RawEditArray .item-'+_id).remove();

            });

            $( "#btnLoc" ).click(function() {
                $("#divLoc").toggle();
            });


            $(".openEditModal").click(function(){
                _id = $(this).data('id');
                $('.RawArray').html('');
                $.ajax({
                    url: '{{ route('system.ingredient.getAjaxIngredient') }}',
                    type: "GET",
                    dataType: "json",
                    data: {id:_id},
                    success: function (data){
                        if(!data['status']){
                            $.toast({
                                heading: 'Error',
                                text: data['message'],
                                position: 'top-right',
                                loaderBg:'#fec107',
                                icon: 'error',
                                hideAfter: 3500,
                                stack: 6
                            });
                        }else{
                            console.log(data);
                            $("#edit_ingredient_shop").append(new Option(data['dataResponse']['Ingredient']['Shop_Name'], data['dataResponse']['Ingredient']['Ingredient_Shop']));
                            $('input[name=edit_ingredient_code]').val(data['dataResponse']['Ingredient']['Ingredient_Code']);
                            $('input[name=edit_ingredient_name]').val(data['dataResponse']['Ingredient']['Ingredient_Name']);
                            // $('input[name=edit_ingredient_number]').val(data['dataResponse']['Ingredient']['Ingredient_Number']);
                            $('input[name=edit_ingredient_unit]').val(data['dataResponse']['Ingredient']['Ingredient_Unit']);
                            $('input[name=edit_ingredient_id]').val(data['dataResponse']['Ingredient']['Ingredient_ID']);
                            if(data['dataResponse']['Ingredient']['Ingredient_Status'] == 1){
                                $('input[name=edit_ingredient_status]').attr('checked', true);
                            }

                            $.each(data['dataResponse']['Ingredient']['Ingredient_Params'], function(index, result) {
                                if($('.RawArray').html() == ""){
                                    $('.RawArray').append('<div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item-'+result['Raw_Material_ID']+'"><div class="input-group mb-15"> <span class="input-group-addon">'+result['Raw_Material_Name']+'</span><input name="raw['+result['Raw_Material_ID']+']" type="text" placeholder="Số lượng nguyên liệu thô cần thiết" value="'+result['number']+'" class="form-control"><span class="input-group-addon" class="unit">'+result['Raw_Material_Unit']+'</span><span class="input-group-addon deleteItem unit" style="background: red; color: #fff;cursor: pointer;" data-id="'+result['Raw_Material_ID']+'"><i class="ti-trash"></i></span></div></div>');
                                }
                                else{
                                    $('.RawArray').append('<div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item-'+result['Raw_Material_ID']+'">+</div><div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2 item-'+result['Raw_Material_ID']+'"><div class="input-group mb-15"> <span class="input-group-addon">'+result['Raw_Material_Name']+'</span><input name="raw['+result['Raw_Material_ID']+']" type="text" placeholder="Số lượng nguyên liệu thô cần thiết" value="'+result['number']+'" class="form-control"><span class="input-group-addon" class="unit">'+result['Raw_Material_Unit']+'</span><span class="input-group-addon deleteItem unit" style="background: red; color: #fff;cursor: pointer;" data-id="'+result['Raw_Material_ID']+'"><i class="ti-trash"></i></span></div></div>');
                                }
                            });
                            $("#edit_ingredient_raw").empty();
                            $.each(data['dataResponse']['RawList'], function (index, raw) {

                                $("#edit_ingredient_raw").append(new Option(raw.Raw_Material_Name + '(' + raw.Raw_Material_Unit +')', raw.Raw_Material_ID));
                            });
                            $('#modal-edit-ingredient').modal('show');
                        }
                    }
                });
            });


    </script>

@endsection
