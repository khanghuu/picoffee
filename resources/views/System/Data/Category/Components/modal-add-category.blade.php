<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.category.postAdd') }}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Thêm danh mục</h5>
                </div>
                <div class="modal-body">
                    <div class="form-wrap">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                    <input name="name" type="text" placeholder="Tên danh mục" class="form-control">
                                </div>
                            </div>
                            @if(Session('user')->User_Level == 1)
                                <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                    <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-coffee"></i></span>
                                        <select required class="form-control" name="shop">
                                            <option value="" selected disabled>--- Chọn cửa hàng ---</option>
                                            @foreach($shop as $v)
                                                <option value="{{ $v->Shop_ID }}">{{ $v->Shop_Name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-share-alt"></i></span>
                                    <input name="order" type="number" min="1" placeholder="Thứ tự" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input readonly="" name="datetime" type="text" class="form-control" value="{{ date('Y-m-d H:i:s') }}">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning">Thêm</button>
                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                    {{ csrf_field() }}
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
