<div id="editModal" class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.category.postEdit') }}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Sửa danh mục</h5>
                </div>
                <div class="modal-body">
                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                        <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                            <input name="EditName" minlength="2" type="text" placeholder="Tên danh mục" class="form-control">
                        </div>
                    </div>
                    @if(Session('user')->User_Level == 1)
                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                        <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-coffee"></i></span>
                            <select class="form-control" name="EditShop">
                                <option value="0" disabled>--- Chọn cửa hàng ---</option>
                                @foreach($shop as $v)
                                    <option value="{{ $v->Shop_ID }}">{{ $v->Shop_Name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endif
                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                        <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-share-alt"></i></span>
                            <input name="EditOrder" type="number" min="1" placeholder="Thứ tự" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                        <div class="checkbox checkbox-success">
                            <input id="checkbox_33" type="checkbox" name="Editstatus" value="1">
                            <label for="checkbox_33">Kích hoạt</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning">Lưu</button>
                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                    <input type="hidden" name="editId" value="">
                    {{ csrf_field() }}
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

