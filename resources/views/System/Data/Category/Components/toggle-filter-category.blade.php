<div class="col-md-12 col-sm-12 col-xs-12 mt-20" id="divLoc" style="display: none">
    <div class="form-wrap">
        <form class="form-horizontal" method="get">
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Mã:</label>
                <div class="col-sm-10">
                    <input value="{{request()->get('code')}}" type="text" class="form-control" id="pwd_hr" name="code">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Tên:</label>
                <div class="col-sm-10">
                    <input value="{{request()->get('name')}}" type="text" class="form-control" id="pwd_hr" name="name">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Thứ tự:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="unit">
                        <option value="">--- Chọn thứ tự ---</option>
                        @foreach($count as $v)
                            <option {{$v->Category_Order == request()->get('unit')?'selected':''}} value="{{ $v->Category_Order }}">{{ $v->Category_Order }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Trạng thái:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="search_category_status">
                        <option value="" selected>----Chọn trạng thái----</option>
                        <option value="0" {{request()->get('search_category_status') === '0'?'selected':''}}>Đóng</option>
                        <option value="1" {{request()->get('search_category_status') == '1'?'selected':''}}>Mở</option>
                    </select>
                </div>
            </div>
            @if(Session('user')->User_Level == 1 || Session('user')->User_Level == 3)
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Cửa hàng:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="shop">
                        <option value="">--- Chọn cửa hàng ---</option>
                        @foreach($shop as $v)
                            <option {{$v->Shop_ID == request()->get('shop')?'selected':''}}  value="{{ $v->Shop_ID }}">{{ $v->Shop_Name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @endif
            <div class="form-group mb-0">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-orange btn-anim"><i class="fa fa-search"></i><span class="btn-text">Tìm kiếm</span></button>
                    <a href="{{ route('system.category.getManage') }}"><button type="button" class="btn btn-default btn-anim btn-anim"><i class="fa fa-times"></i><span class="btn-text">Huỷ</span></button></a>
                </div>
            </div>
        </form>
    </div>
</div>

