@extends('System.Layouts.master')
@section('title', 'Category')
@section('css')
    <style>
        .color-white-th{
            background: #ff6028;
        }
        .color-white-th tr th{
            color: white!important;
        }

        table.dataTable thead .sorting::after, table.dataTable thead .sorting_asc::after, table.dataTable thead .sorting_desc::after{
            display: none!important;
        }
    </style>
@endsection
@section('content')
    <div class="panel panel-default border-panel card-view">

        <div class="panel-heading">
            <div class="pull-left">
                <h6 class="panel-title txt-dark">Danh sách danh mục</h6>
            </div>
            <div class="clearfix">

            </div>
        </div>

        <div class="panel-wrapper collapse in">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        @if( Session::get('user')->User_Level != 3)
                        <button class="btn btn-primary btn-rounded btn-icon left-icon" data-toggle="modal" data-target=".bs-example-modal-lg" class="model_img img-responsive"> <i class="fa fa-plus"></i> <span>Thêm</span></button>
                        <!-- Adding new Material Modal -->
                        @endif
                        <a href="{{route('categoryExport')}}" class="btn btn-primary btn-rounded btn-icon left-icon"> <i class="fa fa-file-excel-o"></i> <span>Xuất file Excel</span></a>
                        @component('System.Data.Category.Components.modal-add-category', ['category' => $category , 'shop' => $shop])
                        @endcomponent()
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12" style="text-align: right">
                        <button class="btn btn-default btn-rounded btn-icon left-icon" id="btnLoc"> <i class="fa fa-filter"></i> <span>Lọc</span></button>
                    </div>
                        <!--Toggle filter material -->
                    @component('System.Data.Category.Components.toggle-filter-category',['shop' => $shop, 'count' => $count])
                    @endcomponent()
                    <!--/Toggle filter material -->
                </div>
                <div class="table-wrap mt-20">
                    <div class="table-responsive">
                        <table id="datable_category" class="table table-hover table-bordered display mb-30 " >
                            <thead class="color-white-th">
                            <tr>
                                <th>Mã</th>
                                <th>Tên</th>
                                <th>Thứ Tự </th>
                                <th>Ngày Giờ</th>
                                <th>Cửa hàng</th>
                                <th>Trạng Thái</th>
                                @if( Session::get('user')->User_Level != 3)
                                    <th>Hành Động</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($row as $v)
                                <tr>
                                    <td>{{ $v->Category_Code }}</td>
                                    <td>{{ $v->Category_Name }}</td>
                                    <td>{{ $v->Category_Order }}</td>
                                    <td>{{ $v->Category_Datetime }}</td>
                                    <td>{{ $v->Shop_Name }}</td>
                                    <td>
                                        @if($v->Category_Status)
                                            <span class="label label-success">Mở</span>
                                        @else
                                            <span class="label label-info">Đóng</span>
                                        @endif
                                    </td>
                                    @if( Session::get('user')->User_Level != 3)
                                    <td><a href="#" title="Sửa" data-toggle="modal" data-target=".bs-edit-modal-lg" data-id="{{ $v->Category_ID }}" class="openModel"><i class="fa fa-edit"></i></a> &nbsp <a href="{{ route('system.category.getDelete', $v->Category_ID) }}" onclick="return confirm('Bạn có chắc chắn')"><i class="fa fa-bitbucket"></i></a></td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>
        <!--Modal edit Category -->
        @component('System.Data.Category.Components.modal-edit-category', ['category' => $category, 'shop' => $shop])
        @endcomponent
    <!--/Modal edit Category -->
    </div>
    <!--/Category Manage -->

@endsection
@section('script')
    <!-- Data table JavaScript -->
    <script src="../vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="dist/js/dataTables-data.js"></script>
    <script>
        $( "#btnLoc" ).click(function() {
            $("#divLoc").toggle();
        });
        $(".openModel").click(function(){
            _id = $(this).data('id');
            $.ajax({
                url: '/system/data/category/get',
                type: "GET",
                dataType: "json",
                data: {id:_id},
                success: function (data){
                    if(data['status'] == false){
                        $.toast({
                            heading: 'Error',
                            text: data['message'],
                            position: 'top-right',
                            loaderBg:'#fec107',
                            icon: 'error',
                            hideAfter: 3500,
                            stack: 6
                        });
                    }else{
                        $('input[name=EditName]').val(data['data']['Category_Name']);
                        $('input[name=EditOrder]').val(data['data']['Category_Order']);
                        $('input[name=editId]').val(data['data']['Category_ID']);
                        $("select[name=EditShop] > option").each(function() {

                            if($(this).val() == data['data']['Category_Shop']){
                                $(this).attr('selected', true);
                            }
                        });
                        if(data['data']['Category_Status'] == 1){
                            $('input[name=Editstatus]').attr('checked', true);
                        }
                    }
                }
            });
        });

    </script>

@endsection
