<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text-html; charset=utf-8">
    <title>Hóa đơn bán hàng</title>
    <base href="{{asset('')}}">
    <style>
        /* cyrillic-ext */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 300;
            src: local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwmhduz8A.woff2) format('woff2');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }
        /* cyrillic */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 300;
            src: local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwkxduz8A.woff2) format('woff2');
            unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }
        /* greek-ext */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 300;
            src: local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwmxduz8A.woff2) format('woff2');
            unicode-range: U+1F00-1FFF;
        }
        /* greek */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 300;
            src: local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwlBduz8A.woff2) format('woff2');
            unicode-range: U+0370-03FF;
        }
        /* vietnamese */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 300;
            src: local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwmBduz8A.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 300;
            src: local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwmRduz8A.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 300;
            src: local('Source Sans Pro Light'), local('SourceSansPro-Light'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwlxdu.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* cyrillic-ext */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 400;
            src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xK3dSBYKcSV-LCoeQqfX1RYOo3qNa7lqDY.woff2) format('woff2');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }
        /* cyrillic */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 400;
            src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xK3dSBYKcSV-LCoeQqfX1RYOo3qPK7lqDY.woff2) format('woff2');
            unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }
        /* greek-ext */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 400;
            src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xK3dSBYKcSV-LCoeQqfX1RYOo3qNK7lqDY.woff2) format('woff2');
            unicode-range: U+1F00-1FFF;
        }
        /* greek */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 400;
            src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xK3dSBYKcSV-LCoeQqfX1RYOo3qO67lqDY.woff2) format('woff2');
            unicode-range: U+0370-03FF;
        }
        /* vietnamese */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 400;
            src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xK3dSBYKcSV-LCoeQqfX1RYOo3qN67lqDY.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 400;
            src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xK3dSBYKcSV-LCoeQqfX1RYOo3qNq7lqDY.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 400;
            src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xK3dSBYKcSV-LCoeQqfX1RYOo3qOK7l.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* cyrillic-ext */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 700;
            src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwmhduz8A.woff2) format('woff2');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }
        /* cyrillic */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 700;
            src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwkxduz8A.woff2) format('woff2');
            unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }
        /* greek-ext */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 700;
            src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwmxduz8A.woff2) format('woff2');
            unicode-range: U+1F00-1FFF;
        }
        /* greek */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 700;
            src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwlBduz8A.woff2) format('woff2');
            unicode-range: U+0370-03FF;
        }
        /* vietnamese */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 700;
            src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwmBduz8A.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 700;
            src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwmRduz8A.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */

        @font-face {
            font-family: 'Source Sans Pro';
            font-style: normal;
            font-weight: 700;
            src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwlxdu.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        .breakk {
            page-break-after: always;
        }
    </style>
    <!-- <link rel="stylesheet" href="http://pos.picoffeetea.com/default/template/sass/main.css" media="screen" charset="utf-8"/> -->
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <link rel="stylesheet" media="print" href="css/print.css">
    <link rel="stylesheet" media="screen" href="css/print.css">

    <style media="print">

    </style>
</head>

<body>
<div class="breakk">
    <br>
    <br>
    <br>
    <div class="container center">
        <b class="h4">[BAR]((New))</b>
    </div>
    <header class="clearfix container">
        <div class="details clearfix" style="font-size:16px">

            <div class="client left">
                <b>Bán hàng - </b>
                <b>Bàn</b> {{$bill->Bill_TableNumber}} </div>
            <div class="data right">
                <b>No:</b> {{$bill->Bill_ID}} </div>
        </div>
    </header>
    <div class="container">
        <hr class="hr_style">
    </div>

    <section>
        <div class="container">

            <table border="0" cellspacing="0" cellpadding="0">

                <tbody style="font-size:15px">
                @foreach($bill->BillProduct as $product)
                    <tr class="">
                        <td class="desc">{{$product->Product->Product_Name}}</td>
                        <td class="qty">{{number_format($product->Product->Product_Price,0,'.',',')}}</td>
                        <td class="qty">{{$product->Bill_Product_Quantity}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="no-break" style="font-size:15px">

                <table class="money_td">
                    <tbody>
                    <tr style="border-bottom:1px solid">
                        <td colspan="2" class="text-left"><b>Thu ngân</b>: {{$bill->User->User_Name}}</td>
                        <td colspan="2" class="total text-right">

                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="container center">
                    <i>- - - {{$bill->Bill_Time}} - - -</i>
                </div>
            </div>
        </div>
    </section>

    <!-- <footer>
    <div class="container">
        <div class="thanks">Thank you!</div>
        <div class="notice">
            <div>NOTICE:</div>
            <div>A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
        </div>
        <div class="end">Invoice was created on a computer and is valid without the signature and seal.</div>
    </div>
</footer> -->
</div>

<div class="breakk">
    <br>
    <br>
    <br>
    <header class="clearfix container">
        <div class="details clearfix">
            <div class="client left">
                <div class="company-address">
                    <img src="http://pos.picoffeetea.com/default/template/images/logo-pipi.png" style="width:120px;height: 105px;">
                </div>
            </div>
            <div class="data center">
                <div class="margin-address"></div>
                {{$shopAddress}}
            </div>
        </div>
    </header>
    <div class="container">
        <hr class="hr_style">
    </div>
    <div class="container center">
        <b class="h4">BILL THANH TOÁN</b>
    </div>
    <section>
        <div class="container">
            <div class="details clearfix">

                <div class="client left">
                    <b>Bàn số:</b> {{$bill->Bill_TableNumber}}
                    <br>
                    <b>Vào:</b> {{$bill->Bill_Time}}
                </div>
                <div class="data right">
                    <b>Số bill:</b> {{$bill->Bill_ID}}
                    <br>
                    <br>
                    <b>Ký bill:</b> {{$bill->Bill_Signed == 1 ? "Có" : "Không ký bill"}}
                </div>
            </div>

            <table border="0" cellspacing="0" cellpadding="0">
                <thead>
                <tr>
                    <th class="center">
                        <b>Mặt hàng</b>
                    </th>
                    <th class="center">
                        <b>SL</b>
                    </th>
                    <th class="center">
                        <b>Giá</b>
                    </th>
                    <th class="center">
                        <b>Thành tiền</b>
                    </th>
                </tr>

                </thead>

                <tbody>

                @foreach($bill->BillProduct as $product)
                    <tr class="product_td">
                        <td class="desc">
                            {{$product->Product->Product_Name}}
                        </td>

                        <td class="qty">{{$product->Bill_Product_Quantity}}</td>
                        <td class="qty">

                            {{number_format($product->Product->Product_Price,0,'.',',')}}<sup>đ</sup>
                        </td>

                        <td class="unit">
                            {{number_format(($product->Product->Product_Price * $product->Bill_Product_Quantity),0,'.',',')}}<sup>đ</sup>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="no-break">
                <table class="money_td">
                    <tbody>
                    <tr style="border-bottom:1px solid">
                        <td colspan="2" class="fs_15 text-left">Thành tiền:</td>
                        <td colspan="2" class="total text-right">
                            {{number_format($bill->Bill_TotalPrice,0,'.',',')}}<sup>đ</sup>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" class="fs_15 text-left">Tổng:</td>
                        <td colspan="2" class="total text-right">
                            {{number_format($bill->Bill_ExtantPrice,0,'.',',')}}<sup>đ</sup>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <hr style="margin:0;border:1px solid;margin-bottom: 5px;">
                <table class="money_td">
                    <tbody>
                    <tr style="border-bottom:1px solid">
                        <td colspan="2" class="text-left"><b>Thu ngân</b>: {{$bill->User->User_Name}}</td>
                        <td colspan="2" class="total text-right">

                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="container center">
                    CẢM ƠN QUÝ KHÁCH - HẸN GẶP LẠI
                    <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MẬT KHẨU WIFI: 11111111
                </div>
            </div>
        </div>
    </section>

</div>
<script>
    setTimeout(function() {
        {
            window.print();
            window.location.replace('{{route('getIndex')}}');
        };
    }, 500);
</script>

</body>

</html>
