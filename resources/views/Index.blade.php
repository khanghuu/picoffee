@extends('System.Layouts.master')
@section('css')
    <!-- bootstrap-touchspin CSS -->
    <link href="vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="css/easy-numpad.css">
    <style>
        .img-responsive {
            width: 250px !important;
            height: 240px !important;
        }

        #wait-invoice {
            width: 99% !important;
            margin: 0 auto;

            color: #0c0c0c;
            background-color: #e0f0e0;

        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-7">
            <div class="panel panel-default border-panel card-view" >
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
{{--                                                <div class="input-group mb-10">--}}
{{--                                                    <input type="text" name="example-input1-group2" id="search-product" class="form-control"--}}
{{--                                                           placeholder="Tìm kiếm">--}}
{{--                                                    <span class="input-group-btn">--}}
{{--                        										<button type="button" class="btn  btn-default" id="btn-search-product" >--}}
{{--                                                                    <i class="zmdi zmdi-search"></i>--}}
{{--                                                                </button>--}}
{{--                                                    </span>--}}
{{--                                                </div>--}}
                        <div class="pills-struct">
                            <ul role="tablist" class="nav nav-pills" id="myTabs_6">
                                <li class="active" role="presentation"><a aria-expanded="true" data-toggle="tab"
                                                                          role="tab" id="category_tab_all"
                                                                          href="#category-all">Tất cả</a></li>
                                @foreach($categories as $c)
                                    <li role="presentation" style="margin: 3px;"><a aria-expanded="true"
                                                                                    data-toggle="tab" role="tab"
                                                                                    id="category_tab_{{$c->Category_ID}}"
                                                                                    href="#category-{{$c->Category_ID}}">{{ $c->Category_Name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="tab-content" id="myTabContent_6">
                                <div id="category-all" class="tab-pane fade active in" role="tabpanel">
                                    <div class="row">
                                        @foreach($products as $p)
                                            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div class="panel-wrapper collapse in">
                                                        <div class="panel-body pa-0">
                                                            <article class="col-item">
                                                                <div class="photo">
                                                                    <img src="product/{{$p->Product_Image}}"
                                                                         class=" img-responsive add-item"
                                                                         id="product-image-{{$p->Product_ID}}"
                                                                         alt="Product Image"/>
                                                                </div>

                                                                <div class="info">
                                                                    <a href="javascript:void(0);" class="add-item"
                                                                       id="product-name-{{$p->Product_ID}}">
                                                                        <h6>{{$p->Product_Name}}</h6>
                                                                        <span
                                                                            class="head-font block txt-orange-light-1 font-16">{{$p->Product_Price}}<sup>đ</sup></span>
                                                                    </a>
                                                                </div>
                                                            </article>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                @foreach($categories as $category)
                                    <div id="category-{{$category->Category_ID}}" class="tab-pane fade "
                                         role="tabpanel">
                                        <div class="row">
                                            @if(count($category->Product))
                                                @foreach($category->Product as $p)
                                                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                                                        <div class="panel panel-default card-view pa-0">
                                                            <div class="panel-wrapper collapse in">
                                                                <div class="panel-body pa-0">
                                                                    <article class="col-item">
                                                                        <div class="photo">
                                                                            <a href="javascript:void(0);"> <img
                                                                                    src="product/{{$p->Product_Image}}"
                                                                                    class="img-responsive add-item"
                                                                                    id="product-image-{{$p->Product_ID}}"
                                                                                    alt="Product Image"/> </a>
                                                                        </div>
                                                                        <div class="info">
                                                                            <a href="javascript:void(0);"
                                                                               class="add-item"
                                                                               id="product-name-{{$p->Product_ID}}">
                                                                                <h6>{{$p->Product_Name}}</h6>
                                                                                <span
                                                                                    class="head-font block txt-orange-light-1 font-16"><sup>đ</sup>{{$p->Product_Price}}</span>
                                                                            </a>
                                                                        </div>
                                                                    </article>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-5">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">

                        <h6 class="panel-title txt-dark">{{$shopName}}- Hoá đơn</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                        <!-- 										<form action="{{ route('postPay') }}" method="post" accept-charset="utf-8"> -->
                            <div class="table-responsive">
                                <table id="order-table" class="table table-hover mb-0">
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>

                            <div class="form-group">
                                <label>Giảm giá</label>
                                <div class="input-group" >
                                    <input type="number" id="amount-discount" min="0" step="any" class="form-control" value="0">
                                    <div class="input-group-addon">
                                        <select id="type-discount">
                                            <option value="1">đ</option>
                                            <option value="2">%</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-inline" style="padding: 0px;">
                                <div class="checkbox checkbox-primary" style="padding-left: 0px;">
                                    <input id="signed-bill" type="checkbox">
                                    <label for="signed-bill">
                                        Ký Bill
                                    </label>
                                </div>
{{--                                <div class="checkbox checkbox-primary">--}}
{{--                                    <input id="pendding-bill" type="checkbox">--}}
{{--                                    <label for="pendding-bill">--}}
{{--                                        Ký gửi--}}
{{--                                    </label>--}}
{{--                                </div>--}}
                                <div class="checkbox checkbox-danger">
                                    <input id="tax-vat" type="checkbox">
                                    <label for="tax-vat">
                                        VAT
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="pull-left">
                                    <h7 class="panel-title txt-dark">Giảm giá: <span
                                            class="discount-total">0</span><sup>đ</sup></h7>
                                </div>
                                <div class="pull-right">
                                    <h6 class="panel-title txt-dark">Thuế: <span class="tax-total">0</span><sup>đ</sup>
                                    </h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label>Bàn số</label>
                                <div class="input-group easy-get">
                                    <input type="number" name="table_number" id="table-number" min="0" step="1"
                                           class="form-control easy-input" value="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">Tổng cộng</h6>
                                </div>
                                <div class="pull-right">
                                    <h4 class="text-success text-right"><span class="order-total-price">0</span> đ</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-rounded btn-primary" id="pay-bill">Thanh Toán</button>
                                <button class="btn btn-rounded btn-warning" id="pause-bill">Tạm dừng</button>
                                <button class="btn btn-rounded btn-danger" id="delete-all">Xoá</button>
                            </div>

                        @csrf
                        <!-- 										</form> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default border-panel card-view">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default border-panel card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-dark">Hóa đơn đang chờ</h6>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-sm"
                                                   id="wait-invoice">
                                                <thead>
                                                <tr>
                                                    <th>Hóa đơn số</th>
                                                    <th class="text-right">Thao tác</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($order_wait as $item)
                                                    <tr>
                                                        <td>{{$item->Bill_ID}}</td>
                                                        @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level ==2)
                                                            <td class="text-right">
                                                                <i class="fa fa-edit order-continue"
                                                                   data-value="{{$item->Bill_ID}}"></i>
                                                                &nbsp &nbsp<a
                                                                    href="{{ route('deletePrintBillTemp', $item->Bill_ID) }}"
                                                                    onclick="return confirm('Bạn có chắc chắn')">
                                                                    <i class="fa fa-bitbucket" class="order-delete"></i>
                                                                </a>
                                                            </td>
                                                        @else
                                                            <td class="text-right">
                                                                <i class="fa fa-edit order-continue"
                                                                   data-value="{{$item->Bill_ID}}"></i>
                                                            </td>

                                                        @endif
                                                    </tr>
                                                @endforeach`
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Product Row Four -->


@endsection
@section('script')
    <!-- Bootstrap Touchspin JavaScript -->
    <script src="vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>
    <!-- keyboard optional extensions - include ALL (includes mousewheel) --><script src="js/easy-numpad.js"></script>
    <script>

        _dataProduct = [];
        _countItem = 0;
        _waitdataProduct = [];
        // thêm sản phẩm
        $('.add-item').click(function () {
            _idClass = this.id;
            _idClassArr = _idClass.split('-');
            _idProduct = _idClassArr[_idClassArr.length - 1];
            $.ajax({
                url: '{{ route('ajax.getProductInfo') }}',
                type: "GET",
                dataType: "json",
                data: {id: _idProduct},
                success: function (data) {
                    if (data['status'] == true) {
                        _checkData = checkProductInArray(data['product'].Product_ID, _dataProduct);
                        if (_checkData == false) {
                            _product = {
                                id: data['product'].Product_ID,
                                name: data['product'].Product_Name,
                                price: data['product'].Product_Price,
                                quantity: 1,
                            };
                            _dataProduct.push(_product);
                        } else {
                            _dataProduct = _checkData;
                        }
                        // cập nhật giá tiền
                        _countItem++;
                        _html = '';
                        _total = 0;
                        $.each(_dataProduct, function (key, value) {
                            _html += '<tr><td>' + value.name + '</td><td width="50%"><input class="quantity-product" id="product-quantity-' + value.id + '" type="text" data-bts-button-down-class="btn btn-danger btn-circle" data-bts-button-up-class="btn btn-success btn-circle" value="' + value.quantity + '"></td><td><span class="text-danger text-semibold"><sup>đ</sup> <span id="total-price-product-' + value.id + '">' + number_format(value.price * value.quantity, 0) + '</span></span> </td><td><button type="button" class="btn btn-sm btn-rounded btn-danger btn-delete" id="product-delete-' + value.id + '"><i class="ti-trash"></i></button></td></tr>';

                            {{--                            @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level ==2)--}}
                                {{--                                _html += '<tr><td>' + value.name + '</td><td width="50%"><input class="quantity-product" id="product-quantity-' + value.id + '" type="text" data-bts-button-down-class="btn btn-danger btn-circle" data-bts-button-up-class="btn btn-success btn-circle" value="' + value.quantity + '"></td><td><span class="text-danger text-semibold"><sup>đ</sup> <span id="total-price-product-' + value.id + '">' + number_format(value.price * value.quantity, 0) + '</span></span> </td><td><button type="button" class="btn btn-sm btn-rounded btn-danger btn-delete" id="product-delete-' + value.id + '"><i class="ti-trash"></i></button></td></tr>';--}}
                                {{--                            @elseif--}}
                                {{--                                check = _waitdataProduct.find(product => product.id === value.id);--}}
                                {{--                                if (check) {--}}
                                {{--                                    _html += '<tr><td>' + value.name + '</td><td width="50%"><input class="quantity-product" id="product-quantity-' + value.id + '" type="text" data-bts-button-down-class="btn btn-danger btn-circle"   data-bts-button-up-class="btn btn-success btn-circle" value="' + value.quantity + '"></td><td><span class="text-danger text-semibold"><sup>đ</sup> <span id="total-price-product-' + value.id + '">' + number_format(value.price * value.quantity, 0) + '</span></span> </td></tr>';--}}
                                {{--                                }--}}
                                {{--                                else {--}}
                                {{--                                    _html += '<tr><td>' + value.name + '</td><td width="50%"><input class="quantity-product" id="product-quantity-' + value.id + '" type="text" data-bts-button-down-class="btn btn-danger btn-circle" data-bts-button-up-class="btn btn-success btn-circle" value="' + value.quantity + '"></td><td><span class="text-danger text-semibold"><sup>đ</sup> <span id="total-price-product-' + value.id + '">' + number_format(value.price * value.quantity, 0) + '</span></span> </td><td><button type="button" class="btn btn-sm btn-rounded btn-danger btn-delete" id="product-delete-' + value.id + '"><i class="ti-trash"></i></button></td></tr>';--}}
                                {{--                                }--}}
                                {{--                            @endif--}}
                                _total += value.price * value.quantity;
                        });
                        _tax_vat = 0;
                        if ($('#tax-vat').is(':checked')) {
                            _tax_vat = _total * 0.1;
                            $('.tax-total').html(_tax_vat);
                        }
                        _discount = 0;
                        if ($('#amount-discount').val() != 0) {
                            _amount_discount = $('#amount-discount').val();
                            _type_discount = $('#type-discount').val();
                            if (_type_discount == 1) {
                                _discount = parseInt(_amount_discount);
                            } else {
                                _discount = (_total * (parseInt(_amount_discount) / 100));
                            }
                            $('.discount-total').html(_discount);
                        }
                        $('.order-total-price').html(_total + _tax_vat - _discount);
                        $('#order-table tbody').html(_html);
                        $(".quantity-product").TouchSpin();
                    }
                }
            });

        });
        //xoá tất cả sp
        $('#delete-all').click(function () {
            _dataProduct = [];
            $('#order-table tbody').html('');
            $('#signed-bill').prop('checked', false);
            $('#tax-vat').prop('checked', false);
            $('#table-number').val(0);
            $('#amount-discount').val(0);
            $('.discount-total').html(0);
            $('.tax-total').html(0);
            $('.order-total-price').html(0);
        });

        //search sản phẩm
        $('#btn-search-product').click(function () {
            _keyword = $("#search-product").val();
            $.ajax({
                url: '{{ route('ajax.getProductKeyword') }}',
                type: "GET",
                dataType: "json",
                data: {keyword: _keyword},
                success: function (data) {
                    if (data['status'] == true) {
                        _html = '';
                        $.each(data['products'], function (key, value) {
                            _html += '<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6"><div class="panel panel-default card-view pa-0"><div class="panel-wrapper collapse in"><div class="panel-body pa-0"><article class="col-item"><div class="photo"><div class="options"><a href="javascript:void(0);" class="font-18 txt-grey mr-10 pull-left"><i class="zmdi zmdi-edit"></i></a><a href="javascript:void(0);" class="font-18 txt-grey pull-left sa-warning"><i class="zmdi zmdi-close"></i></a></div><a href="javascript:void(0);"> <img src="product/' + value.Product_Image + '" class="img-responsive add-item" id="product-image-' + value.Product_ID + '" alt="Product Image" /> </a></div><div class="info"><a href="javascript:void(0);" class="add-item" id="product-name-' + value.Product_ID + '"><h6>' + value.Product_Name + '</h6><span class="head-font block txt-orange-light-1 font-16" ><sup>đ</sup>' + value.Product_Price + '</span></a></div></article></div></div></div></div>'
                        });
                        $('#category-all').html(_html);
                    }
                }
            });
            $('#category_tab_all').trigger('click');
        });

        $('#products-all').on('click', '.add-item', function () {

            _idClass = this.id;
            _idClassArr = _idClass.split('-');
            _idProduct = _idClassArr[_idClassArr.length - 1];
            $.ajax({
                url: '{{ route('ajax.getProductInfo') }}',
                type: "GET",
                dataType: "json",
                data: {id: _idProduct},
                success: function (data) {
                    if (data['status'] == true) {

                        _checkData = checkProductInArray(data['product'].Product_ID, _dataProduct);
                        if (_checkData == false) {
                            _product = {
                                id: data['product'].Product_ID,
                                name: data['product'].Product_Name,
                                price: data['product'].Product_Price,
                                quantity: 1,
                            };
                            _dataProduct.push(_product);
                        } else {
                            _dataProduct = _checkData;
                        }
                        // cập nhật giá tiền
                        _countItem++;
                        _html = '';
                        _total = 0;
                        $.each(_dataProduct, function (key, value) {

                            _html += '<tr><td>' + value.name + '</td><td width="50%"><input class="quantity-product" id="product-quantity-' + value.id + '" type="text" data-bts-button-down-class="btn btn-danger btn-circle" data-bts-button-up-class="btn btn-success btn-circle" value="' + value.quantity + '"></td><td><span class="text-danger text-semibold"><sup>đ</sup> <span id="total-price-product-' + value.id + '">' + number_format(value.price * value.quantity, 0) + '</span></span> </td><td><button type="button" class="btn btn-sm btn-rounded btn-danger btn-delete" id="product-delete-' + value.id + '"><i class="ti-trash"></i></button></td></tr>';

                            _total += value.price * value.quantity;
                        });
                        _tax_vat = 0;
                        if ($('#tax-vat').is(':checked')) {
                            _tax_vat = _total * 0.1;
                            $('.tax-total').html(_tax_vat);
                        }
                        _discount = 0;
                        if ($('#amount-discount').val() != 0) {
                            _amount_discount = $('#amount-discount').val();
                            _type_discount = $('#type-discount').val();
                            if (_type_discount == 1) {
                                _discount = parseInt(_amount_discount);
                            } else {
                                _discount = (_total * (parseInt(_amount_discount) / 100));
                            }
                            $('.discount-total').html(_discount);
                        }
                        $('.order-total-price').html(_total + _tax_vat - _discount);
                        $('#order-table tbody').html(_html);
                        $(".quantity-product").TouchSpin();
                    }
                }
            });
        });
        // sửa số lượng sản phẩm
        $('#order-table tbody').on('change', '.quantity-product', function () {
            _value = parseInt($(this).val());
            _idProduct = $(this).attr("id");

            _Arr = _idProduct.split("-");
            id = _Arr[_Arr.length - 1];
            count = _dataProduct.length;
            for (var i = 0; i < count; i++) {
                if (_dataProduct[i]['id'] == id) {
                    if (_value > _dataProduct[i]['quantity']) {
                        _total += (_value - _dataProduct[i]['quantity']) * _dataProduct[i]['price'];
                    } else {
                        _total -= (_dataProduct[i]['quantity'] - _value) * _dataProduct[i]['price'];
                    }
                    _dataProduct[i]['quantity'] = _value;
                    total = _dataProduct[i]['quantity'] * _dataProduct[i]['price'];
                    _tax_vat = 0;
                    if ($('#tax-vat').is(':checked')) {
                        _tax_vat = _total * 0.1;
                        $('.tax-total').html(_tax_vat);
                    }
                    _discount = 0;
                    if ($('#amount-discount').val() != 0) {
                        _amount_discount = $('#amount-discount').val();
                        _type_discount = $('#type-discount').val();
                        if (_type_discount == 1) {
                            _discount = parseInt(_amount_discount);
                        } else {
                            _discount = (_total * (parseInt(_amount_discount) / 100));
                        }
                        $('.discount-total').html(_discount);
                    }
                    $('.order-total-price').html(_total + _tax_vat - _discount);
                    break;
                }
            }

            $('#total-price-product-' + id).html(total);
        });
        //giảm giá
        $('#type-discount').change(function () {
            _val = $(this).val();
            _amount_discount = $('#amount-discount').val();
            if (_amount_discount == '') {
                0;
            } else {
                if (_val == 1) {
                    _discount = parseInt(_amount_discount);
                } else {
                    _discount = (_total * (parseInt(_amount_discount) / 100));
                }
                $('.discount-total').html(_discount);

                _extant = _total - _discount;
                if (typeof _extant_tax !== 'undefined') {
                    _extant -= _extant_tax;
                }
                $('.order-total-price').html(_extant);
            }


        });
        $('#amount-discount').keyup(function () {
            _amount_discount = $(this).val();
            _type_discount = $('#type-discount').val();
            _tax_vat = 0;
            if ($('#tax-vat').is(':checked')) {
                _tax_vat = _total * 0.1;
                $('.tax-total').html(_tax_vat);
            }
            if (_amount_discount == '' || _amount_discount == 0) {
                $('.discount-total').html(0);
                $('.order-total-price').html(_total + _tax_vat);

            } else {
                if (_type_discount == 1) {
                    _discount = parseInt(_amount_discount);
                } else {
                    _discount = (_total * (parseInt(_amount_discount) / 100));

                }
                $('.discount-total').html(_discount);

                $('.order-total-price').html(_total + _tax_vat - _discount);
            }

        });
        //thuế
        $('#tax-vat').change(function () {
            _tax_vat = 0;

            if ($('#tax-vat').is(':checked')) {
                _tax_vat = _total * 0.1;
            }
            _discount = 0;
            if ($('#amount-discount').val() != 0) {
                _amount_discount = $('#amount-discount').val();
                _type_discount = $('#type-discount').val();
                if (_type_discount == 1) {
                    _discount = parseInt(_amount_discount);
                } else {
                    _discount = (_total * (parseInt(_amount_discount) / 100));
                }
                $('.discount-total').html(_discount);
            }
            $('.tax-total').html(_tax_vat);
            $('.order-total-price').html(_total + _tax_vat - _discount);
        });
        $('#order-table tbody').on('click', '.btn-delete', function () {
            _idProduct = $(this).attr("id");
            _Arr = _idProduct.split("-");
            id = _Arr[_Arr.length - 1];
            var index = _dataProduct.findIndex(p => p.id == id);
            _dataProduct.splice(index, 1);
            _html = '';
            _total = 0;
            $.each(_dataProduct, function (key, value) {

                _html += '<tr><td>' + value.name + '</td><td width="50%"><input class="quantity-product" id="product-quantity-' + value.id + '" type="text" data-bts-button-down-class="btn btn-danger btn-circle" data-bts-button-up-class="btn btn-success btn-circle" value="' + value.quantity + '"></td><td><span class="text-danger text-semibold"><sup>đ</sup> <span id="total-price-product-' + value.id + '">' + number_format(value.price * value.quantity, 0) + '</span></span> </td><td><button type="button" class="btn btn-sm btn-rounded btn-danger btn-delete" id="product-delete-' + value.id + '"><i class="ti-trash"></i></button></td></tr>';

                _total += value.price * value.quantity;
            });
            _tax_vat = 0;
            if ($('#tax-vat').is(':checked')) {
                _tax_vat = _total * 0.1;
                $('.tax-total').html(_tax_vat);
            }
            _discount = 0;
            if ($('#amount-discount').val() != 0) {
                _amount_discount = $('#amount-discount').val();
                _type_discount = $('#type-discount').val();
                if (_type_discount == 1) {
                    _discount = parseInt(_amount_discount);
                } else {
                    _discount = (_total * (parseInt(_amount_discount) / 100));
                }
                $('.discount-total').html(_discount);
            }
            $('.order-total-price').html(_total + _tax_vat - _discount);
            $('#order-table tbody').html(_html);
            $(".quantity-product").TouchSpin();
        });

        //kiểm tra sản phẩm đã thêm vào chưa
        function checkProductInArray(idProduct, products) {
            var count = products.length;
            for (var i = 0; i < count; i++) {
                if (products[i]['id'] == idProduct) {
                    products[i]['quantity'] += 1;
                    return products;
                }
            }
            return false;
        }

        // thanh toán
        $('#pay-bill').click(function () {
            _tempID = $(this).attr('data-value');
            _checkTax = false;
            if ($('#tax-vat').is(':checked')) {
                _checkTax = true;
            }
            // _penddingBill = false;
            // if ($('#pendding-bill').is(':checked')) {
            //     _penddingBill = true;
            // }
            _checkSignedBill = false;
            if ($('#signed-bill').is(':checked')) {
                _checkSignedBill = true;
            }
            _typeDiscount = $('#type-discount').val();
            _amountDiscount = $('#amount-discount').val();
            _table = $('#table-number').val();

            $.ajax({
                url: '{{ route('postPay') }}',
                type: 'GET',
                data: {
                    temp_bill: _tempID,
                    // pendding_bill: _penddingBill,
                    signed_bill: _checkSignedBill,
                    tax_vat: _checkTax,
                    type_discount: _typeDiscount,
                    amount_discount: _amountDiscount,
                    products: _dataProduct,
                    table_number: _table,
                    "_token": "{{ csrf_token() }}",
                },
                dataType: 'json',
                success: function (data) {
                    if (data['status'] == false) {
                        $.toast({
                            heading: 'Error',
                            text: data['message'],
                            position: 'top-right',
                            loaderBg: '#fec107',
                            icon: 'error',
                            hideAfter: 3500,
                            stack: 6
                        });
                    } else {
                        window.location.replace('{{route('getPrintBill')}}?id=' + data['id']);
                    }
                }
            });
        });

        function number_format(number, decimals, dec_point, thousands_sep) {
            var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
            var d = dec_point == undefined ? "," : dec_point;
            var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
            var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;

            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        }

        //pause bill
        $('#pause-bill').click(function () {
            id = $(this).attr('data-value');
            _checkTax = false;
            if ($('#tax-vat').is(':checked')) {
                _checkTax = true;
            }
            _checkSignedBill = false;
            if ($('#signed-bill').is(':checked')) {
                _checkSignedBill = true;
            }
            _typeDiscount = $('#type-discount').val();
            _amountDiscount = $('#amount-discount').val();
            _table = $('#table-number').val();

            $.ajax({
                url: '{{ route('postPayTemp') }}',
                type: 'GET',
                data: {
                    id: id,
                    signed_bill: _checkSignedBill,
                    tax_vat: _checkTax,
                    type_discount: _typeDiscount,
                    amount_discount: _amountDiscount,
                    products: _dataProduct,
                    table_number: _table,
                    "_token": "{{ csrf_token() }}",
                    "bill_status": 2
                },
                dataType: 'json',
                success: function (data) {

                    if (data['status'] == false) {
                        $.toast({
                            heading: 'Error',
                            text: data['message'],
                            position: 'top-right',
                            loaderBg: '#fec107',
                            icon: 'error',
                            hideAfter: 3500,
                            stack: 6
                        });
                    } else {
                        window.location.replace('{{route('getPrintBillTemp')}}?id=' + data['id']);
                    }

                }
            });
        });

        $('.order-continue').click(function () {
            id = $(this).attr('data-value');
            $.ajax({
                url: '{{ route('ajax.getWaitOrder') }}',
                type: 'GET',
                data: {'id': id},
                dataType: 'json',
                success: function (data) {

                    if (data['status'] == false) {
                        $.toast({
                            heading: 'Error',
                            text: data['message'],
                            position: 'top-right',
                            loaderBg: '#fec107',
                            icon: 'error',
                            hideAfter: 3500,
                            stack: 6
                        });
                    } else {
                        products = data['products'];
                        bill = data['bill'];
                        _dataProduct = [];
                        $('#order-table tbody').html('');
                        for (var i = 0; i < products.length; i++) {
                            _product = {
                                id: products[i]['Bill_Product_ProductID'],
                                name: products[i]['Product_Name'],
                                price: products[i]['Product_Price'],
                                quantity: products[i]['Bill_Product_Quantity'],
                            };
                            _dataProduct.push(_product);
                        }
                        _waitdataProduct = _dataProduct;
                        _html = '';
                        _total = 0;
                        $.each(_dataProduct, function (key, value) {
                            @if(Session::get('user')->User_Level == 1 || Session::get('user')->User_Level ==2)
                                _html += '<tr><td>' + value.name + '</td><td width="50%"><input class="quantity-product" id="product-quantity-' + value.id + '" type="text" data-bts-button-down-class="btn btn-danger btn-circle disabled-touchspin"  data-bts-button-up-class="btn btn-success btn-circle" value="' + value.quantity + '"></td><td><span class="text-danger text-semibold"><sup>đ</sup> <span id="total-price-product-' + value.id + '">' + number_format(value.price * value.quantity, 0) + '</span></span> </td><td><button type="button" class="btn btn-sm btn-rounded btn-danger btn-delete " id="product-delete-' + value.id + '"><i class="ti-trash"></i></button></td></tr>';
                            @else
                                _html += '<tr><td>' + value.name + '</td><td width="50%"><input class="quantity-product" id="product-quantity-' + value.id + '" type="text" data-bts-button-down-class="btn btn-danger btn-circle disabled-touchspin" data-bts-min="' + value.quantity + '" data-bts-button-up-class="btn btn-success btn-circle" value="' + value.quantity + '"></td><td><span class="text-danger text-semibold"><sup>đ</sup> <span id="total-price-product-' + value.id + '">' + number_format(value.price * value.quantity, 0) + '</span></span> </td>';
                            @endif
                                _total += value.price * value.quantity;
                        });
                        _tax_vat = 0;
                        $('#order-table tbody').html(_html);
                        if (bill['Bill_IsSigned']) {
                            $('#signed-bill').prop('checked', true);
                        } else {
                            $('#signed-bill').prop('checked', false);
                        }

                        if (bill['Bill_IsTaxVAT']) {
                            $('#tax-vat').prop('checked', true);
                            _tax_vat += _total * 0.1;
                        } else {
                            $('#tax-vat').prop('checked', false);
                        }
                        $('#table-number').val(bill['Bill_TableNumber']);
                        $('#amount-discount').val(bill['Bill_DiscountAmount']);
                        $('.discount-total').html(bill['Bill_DiscountAmount']);
                        $('.tax-total').html(_tax_vat);
                        $('.order-total-price').html(bill['Bill_ExtantPrice']);
                        $('.quantity-product').TouchSpin();
                        $('#pay-bill').attr('data-value', id);
                        $('#pause-bill').attr('data-value', id);
                    }

                }

            });
        });

        // $("#signed-bill").change(function () {
        //     if ($(this).is(':checked')) {
        //         $("#pendding-bill").prop("checked", false);
        //         $('#pendding-bill').attr('disabled', true);
        //
        //     } else {
        //         $("#pendding-bill").prop("checked", false);
        //         $('#pendding-bill').attr('disabled', false);
        //
        //     }
        // });
        // $("#pendding-bill").change(function () {
        //     if ($(this).is(':checked')) {
        //         $("#signed-bill").prop("checked", false);
        //         $('#signed-bill').attr('disabled', true);
        //
        //     } else {
        //         $("#signed-bill").prop("checked", false);
        //         $('#signed-bill').attr('disabled', false);

        //     }
        // });
        $(document).ready(function () {
            $('#wait-invoice').DataTable({
                "bPaginate": true,
                "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]]
            });

        });

    </script>
@endsection
