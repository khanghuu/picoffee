<div class="col-md-12 col-sm-12 col-xs-12 mt-20" id="divLoc" style="display: none">
    <div class="form-wrap">
        <form class="form-horizontal" method="get">
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Mã:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="code">
                        <option value="">--- Chọn mã ---</option>
                        @foreach($code as $v)
                            <option value="{{$v}}">{{$v}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Tên:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="pwd_hr" name="name">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Đơn vị:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="unit">
                        <option value="">--- Chọn đơn vị ---</option>
                        @foreach($unit as $v)
                            <option value="{{ $v}}">{{ $v}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group mb-0">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-orange btn-anim"><i class="fa fa-search"></i><span class="btn-text">Tìm kiếm</span></button>
                    <a href="{{route('system.warehouse.getManage')}}"><button type="button" class="btn btn-default btn-anim"><i class="fa fa-search"></i><span class="btn-text">Huỷ</span></button></a>
                </div>
            </div>
        </form>
    </div>
</div>
