<div id="change-password-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <form method="post" action="{{route('getChangePass')}}">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title">Đổi mật khẩu</h5>
                </div>
                <div class="modal-body">

                    @csrf
                    <div class="form-group">
                        <label for="recipient-name" class="control-label mb-10">Mật khẩu hiện tại</label>
                        <input type="password" name="txtPassword" class="form-control" id="">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label mb-10">Mật khẩu mới</label>
                        <input type="password" name="txtNewPassword" class="form-control" id="">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label mb-10">Xác nhận mật khẩu mới</label>
                        <input type="password" name="txtNewPasswordConfirm" class="form-control" id="">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                    <button type="submit" class="btn btn-danger">Cập nhật</button>
                </div>
            </div>
        </form>
    </div>
</div>
