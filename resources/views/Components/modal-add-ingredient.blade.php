<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.ingredient.postAdd') }}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Thêm nguyên liệu Tinh</h5>
                </div>
                <div class="modal-body">
                    <div class="form-wrap">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                    <input name="code" type="text" placeholder="Mã" class="form-control" disabled="disabled">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                                    <input name="name" type="text" placeholder="Tên nguyên liệu tinh" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">
                                <div class="input-group mb-15"> <span class="input-group-addon">Số lượng</span>
                                    <input name="number" type="text" placeholder="Số lượng nguyên liệu tinh" class="form-control">
                                    <input name="unit" type="text" placeholder="Đơn vị" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">=</div>

                            <div id="RawArray">

                            </div>
                            <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">
                                <label class="control-label mb-10">Thành phần</label>
                                <select class="form-control select2" name="Raw">
                                    <option>--- Chọn nguyên liệu thô ---</option>
                                    @foreach($raw as $v)
                                        <option value="{{ $v->Raw_Material_ID }}">{{ $v->Raw_Material_Name }} ({{ $v->Raw_Material_Unit }})</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning">Lưu</button>
                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                    {{ csrf_field() }}
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
