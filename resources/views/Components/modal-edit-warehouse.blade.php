<div id="editModal" class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.ingredient.postEdit') }}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Sửa nhập kho</h5>
                </div>
                <div class="modal-body">
                    <div class="modal-body">
                        <div class="form-wrap">
                            <form action="{{ route('system.warehouse.postAdd') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Số chứng từ <i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                                    <input type="text" class="form-control" placeholder="Số chứng từ" name="number" disabled="disabled">
                                </div>
                                @if($user->User_Level == 1)
                                    <div class="form-group">
                                        <label class="control-label mb-10 text-left">Người Nhập <i class="fa fa-hand-o-down" aria-hidden="true"></i></label>
                                        <select class="form-control" name="user">
                                            <option value="">--- Chọn tên ---</option>
                                            @foreach($ListUser as $u)
                                                <option value="{{$u->User_ID}}">{{$u->User_Email}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @else
                                    <input type="hidden" name="user" value="{{$user->User_ID}}">
                                @endif
                                <div class="form-group mt-30 mb-30">
                                    <label class="control-label mb-10 text-left">Loại Hàng <i class="fa fa-hand-o-down" aria-hidden="true"></i></label>
                                    <select class="form-control" name="raw" id="raw-material">
                                        <option value="0">--- Chọn loại ---</option>
                                        <option value="2">Nguyên Liệu Tinh</option>
                                        <option value="1">Nguyên Liệu Thô</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Tên Hàng Hoá<i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                                    <select class="form-control" name="name" id="Name-Ware">
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Ngày hết hạn <i class="fa fa-calendar" aria-hidden="true"></i></label>
                                    <input type="date" name="Expiry" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Số Lượng <i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                                    <input type="number" class="form-control" placeholder="Nhập số lượng hàng" name="quantity" id="quantity-ware">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Đơn Giá <i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                                    <input type="number" step="any" class="form-control" placeholder="Nhập đơn giá" name="price" id="price-ware">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Thành Tiền <i class="fa fa-pencil-square-o" aria-hidden="true"></i></label>
                                    <input type="text" class="form-control"	name="total-price" id="total-price">
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10 text-left">Ghi Chú <i class="fa fa-commenting-o" aria-hidden="true"></i></label>
                                    <textarea class="form-control" rows="5" name="description"></textarea>
                                </div>
                                <div class="form-group pull-right">
                                    <button id="btn-them" type="submit" class="btn01 btn-lg btn-success">Nhập kho <i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                    <input type="hidden" name="editID" value="">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
