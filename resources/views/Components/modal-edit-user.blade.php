<!-- sample modal content -->
<div class="modal fade modal_edit_user" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="myLargeModalLabel">Sửa người dùng</h5>
            </div>
            <div class="modal-body">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <form action="{{ route('system.user.postEditUser') }}" method="post">
                                <div class="col-sm-6 col-xs-6">
                                    <div class="form-wrap">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputuname_1">Họ tên</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-user"></i></div>
                                                <input required type="text" class="form-control" name="edit_name" id="edit-name" placeholder="Họ tên">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputEmail_1">Email</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-envelope-open"></i></div>
                                                <input required type="email" class="form-control" id="edit-email" name="edit_email" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="form-group" id="select_edit_shop">
                                            <label class="control-label mb-10" for="exampleInputpwd_2">Địa điểm cửa hàng</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-location-pin"></i></div>
                                                <select required class="form-control" id='edit-store' name="edit_store">
                                                    <option value="" selected disabled>---Chọn cửa hàng---</option>
                                                    @foreach($shop as $item)
                                                        <option value="{{ $item->Shop_ID }}">{{ $item->Shop_Name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10">Giới tính</label>
                                            <div>
                                                <div class="radio">
                                                    <input type="radio" name="edit_gender" id="radio-Male" value="Male">
                                                    <label for="radio-male">
                                                        Nam
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" name="edit_gender" id="radio-Female" value="Female">
                                                    <label for="radio-female">
                                                        Nữ
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <input type="radio" name="edit_gender" id="radio-Other" value="Other">
                                                    <label for="radio-other">
                                                        Khác
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="hidden" class="form-control" name="id_user" id="id_user" value="">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-6 col-xs-6">
                                    <div class="form-wrap">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputuname_1">Ngày sinh</label>
                                            <div class="input-group date">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                <input required type="text" class="form-control datetimepicker" id="edit-birthday" name='edit_birthday' placeholder="Ngày sinh">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1">Di động</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-phone"></i></div>
                                                <input required type="number" class="form-control" name="edit_phone" id="edit-phone" placeholder="Di động">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_2">Địa chỉ</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-location-pin"></i></div>
                                                <input required type="text" class="form-control" name="edit_address" id="edit-address"  placeholder="Địa chỉ">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputEmail_1">Cấp bậc</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="icon-user"></i></div>
                                                <select required class="form-control" id="edit-level" name="edit_level">
                                                    <option value="" selected disabled>---Chọn cấp bậc---</option>
                                                    @foreach($level as $item)
                                                        <option value="{{ $item->level_ID }}">{{ $item->level_Name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputuname_1">Ngày vào làm</label>
                                            <div class="input-group date">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                <input required type="text" class="form-control datetimepicker" name="edit_start_date" id="edit-start-date" placeholder="Enter DateTime">
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-success mr-10">Lưu</button>
                                        <a class="btn btn-dark mr-10 btn-cance-user">Huỷ bỏ</a>
                                    </div>
                                </div>
                                @csrf
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
