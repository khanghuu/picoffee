<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.shop.postAdd') }}" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Thêm cửa hàng</h5>
                </div>
                <div class="modal-body">
                    <div class="form-wrap">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                                    <input name="name" type="text" placeholder="Tên cửa hàng" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-wrap">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                                    <input name="address" type="text" placeholder="Địa chỉ cửa hàng" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning">Lưu</button>
                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                    {{ csrf_field() }}
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
