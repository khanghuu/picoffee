<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.raw.postAdd') }}" method="post">
                @csrf
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Thêm nguyên liệu thô</h5>
                </div>
                <div class="modal-body">
                    <div class="form-wrap">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-graduation-cap"></i></span>
                                    <input name="name" type="text" placeholder="Tên đơn vị " class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                    <input name="code" type="text" placeholder="Mã đơn vị" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-8 col-sm-12 col-xs-12 form-group col-md-offset-2">
                                <label class="control-label mb-10">Thành phần</label>
                                <select class="form-control select2" name="Raw">
                                    <option>Chon nguyên liệu thô</option>
                                    @foreach($row as $v)
                                        <option value="{{ $v->Raw_Material_ID }}">{{ $v->Raw_Material_Name }} ({{ $v->Raw_Material_Unit }})</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-share-alt"></i></span>
                                    <input name="unit" type="text" placeholder="Quy đổi" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input readonly="" name="datetime" type="text" class="form-control" value="{{ date('Y-m-d H:i:s') }}">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning">Lưu</button>
                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                    {{ csrf_field() }}
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
