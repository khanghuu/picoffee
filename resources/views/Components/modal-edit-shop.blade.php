<div id="editModal" class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('system.shop.postEdit') }}" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h5 class="modal-title" id="myLargeModalLabel">Sửa thông tin cửa hàng</h5>
                </div>
                <div class="modal-body">
                	<div class="form-wrap">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                                    <input name="name" id="editName" type="text" placeholder="Tên cửa hàng" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-wrap">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                                <div class="input-group mb-15"> <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                                    <input name="address" id="editAddress" type="text" placeholder="Địa chỉ cửa hàng" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-wrap">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                                <select class="form-control" id="editStatus" name="status">
                                    <option value="1">Đang mở cửa</option>
                                    <option value="0">Đóng cửa</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning">Lưu</button>
                    <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                    <input type="hidden" name="eidtId" value="">
                    {{ csrf_field() }}
                    <input type="hidden" id="editID" name="id" value="">
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
