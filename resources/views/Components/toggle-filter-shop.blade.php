<div class="col-md-12 col-sm-12 col-xs-12 mt-20" id="divLoc" style="display: none">
    <div class="form-wrap">
        <form class="form-horizontal" method="get">
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Mã cửa hàng:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="id">
                        <option value="" disabled selected>--- Chọn mã ---</option>
                        @foreach($shopIDs as $item)
                            <option {{request()->input('id') == $item->Shop_ID ? "selected" : ""}} value="{{$item->Shop_ID}}">{{$item->Shop_ID}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Tên cửa hàng:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="pwd_hr" name="name">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Địa chỉ:</label>
                <div class="col-sm-10">
                	<textarea name="address" class="form-control" rows="3" cols="3" placeholder="Nhập địa chỉ cửa hàng"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label mb-10 col-sm-2">Trạng thái:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="status">
                        <option value="" selected>--chọn trạng thái</option>
                        <option value="1">Đang hoạt động</option>
                        <option value="-1">Đã xoá</option>
                        <option value="0">Đã đóng cửa</option>
                    </select>
                </div>
            </div>
            <div class="form-group mb-0">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-orange btn-anim"><i class="fa fa-search"></i><span class="btn-text">Tìm kiếm</span></button>
                    <a href="{{ route('system.shop.getAllShop') }}"><button type="button" class="btn btn-default btn-anim"><i class="fa fa-times"></i><span class="btn-text">Huỷ</span></button></a>
                </div>
            </div>
        </form>
    </div>
</div>
