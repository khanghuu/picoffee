<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'/', 'middleware'=>'LoginMiddleware'], function (){
    Route::get('/', 'Home\PageController@getIndex')->name('getIndex');

    Route::get('pay', 'Home\PageController@postPay')->name('postPay');
    Route::get('paytemp', 'Home\PageController@postPayTemp')->name('postPayTemp');
    Route::get('bill', 'Home\PageController@getPrintBill')->name('getPrintBill');
    Route::get('bill-temp', 'Home\PageController@getPrintBillTemp')->name('getPrintBillTemp');
    Route::get('del-bill-temp/{id}', 'Home\PageController@deletePrintBillTemp')->name('deletePrintBillTemp');
    Route::group(['prefix'=>'ajax'], function (){
        Route::get('get-info', 'Home\PageController@getProductInfo')->name('ajax.getProductInfo');
        Route::get('get-keyword', 'Home\PageController@getProductKeyword')->name('ajax.getProductKeyword');
        Route::get('get-order-wait', 'Home\PageController@getWaitOrder')->name('ajax.getWaitOrder');
    });
    Route::get('export-bill', 'System\FileController@billExport')->name('billExport');
});

Route::get('register', 'Auth\RegisterController@getRegister')->name('getRegister');
Route::post('register', 'Auth\RegisterController@postRegister')->name('postRegister')->middleware('check.admin.manager');

Route::get('login', 'Auth\LoginController@getLogin')->name('getLogin');
Route::post('login', 'Auth\LoginController@postLogin')->name('postLogin');
Route::get('logout', 'Auth\LoginController@getLogout')->name('getLogout');
Route::post('change-password', 'Auth\ForgotPasswordController@getChangePass')->name('getChangePass');
Route::get('forgot-password', 'Auth\ForgotPasswordController@getForgotPass')->name('getForgotPass');
Route::post('forgot-password', 'Auth\ForgotPasswordController@postForgotPass')->name('PostForget');


Route::group(['prefix'=>'system', 'middleware'=>'LoginMiddleware'], function (){
    Route::get('/', 'System\DashboardController@getDashboard')->name('system.dashboard');
    Route::get('get-pendding-bill', 'System\ReportController@getPenddingBill')->name('system.getpenddingbill');
    Route::get('change-pending-order/{id}', 'System\ReportController@changePenddingOrder')->name('system.changependdingorder');
    Route::get('delete-pending-order/{id}', 'System\ReportController@deletePenddingOrder')->name('system.deletependdingorder');
    Route::get('all-order', 'System\ReportController@getAllBill')->name('system.getAllBill');
    Route::get('detail-order', 'System\ReportController@getDetailBill')->name('system.getDetailBill');

    Route::group(['prefix'=>'users'], function (){
        Route::get('profile', 'System\UserController@getProfile')->name('system.user.getProfile');
        Route::get('activities', 'System\UserController@getActionEmployees')->name('system.user.getActionEmployees');
        Route::get('activity', 'System\UserController@getAction')->name('system.user.getAction');
        Route::get('all-users', 'System\UserController@getAllUsers')->name('system.user.getAllUsers');
        Route::get('edit-user', 'System\UserController@getEditUser')->name('system.user.getEditUser');
        Route::post('edit-user', 'System\UserController@postEditUser')->name('system.user.postEditUser')->middleware('check.admin.manager');
        Route::post('edit-user-id', 'System\UserController@postEditUserInID')->name('system.user.postEditUserInID');
        Route::post('uploadimage', 'System\UserController@uploadImage')->name('system.user.uploadimage');

    });
    Route::group(['prefix'=>'data'], function () {
        //Categori
        Route::get('category/manage', 'System\CategoryController@getManage')->name('system.category.getManage');
        Route::get('category/get', 'System\CategoryController@getCategory')->name('system.category.getCategory');
        Route::get('category/delete/{id}', 'System\CategoryController@getDelete')->name('system.category.getDelete');
        Route::group(['middleware' => 'check.admin.manager'], function () {
            Route::post('category/add', 'System\CategoryController@postAdd')->name('system.category.postAdd');
            Route::post('category/edit', 'System\CategoryController@postEdit')->name('system.category.postEdit');
        });
        Route::get('export-category', 'System\FileController@categoryExport')->name('categoryExport');

        //Raw
        Route::get('raw/manage', 'System\RawController@getManageRaw')->name('system.raw.getManageRaw');

        Route::group(['middleware' => 'check.admin.manager'], function () {
            Route::post('raw/add', 'System\RawController@postAddRaw')->name('system.raw.postAddRaw');
            Route::post('raw/edit', 'System\RawController@postEditRaw')->name('system.raw.postEditRaw');
            Route::get('raw/delete/{id}', 'System\RawController@getDeleteRaw')->name('system.raw.getDeleteRaw');
            Route::get('raw/get', 'System\RawController@getAjaxRaw')->name('system.raw.getAjaxRaw');
        });
        Route::get('export-raw', 'System\FileController@rawExport')->name('rawExport');

        //Ingredient
        Route::get('ingredient/manage', 'System\IngredientController@getManageIngredient')->name('system.ingredient.getManageIngredient');
        Route::group(['middleware' => 'check.admin.manager'], function () {
            Route::post('ingredient/add', 'System\IngredientController@postAddIngredient')->name('system.ingredient.postAddIngredient');
            Route::get('ingredient/get', 'System\IngredientController@getAjaxIngredient')->name('system.ingredient.getAjaxIngredient');
            Route::post('ingredient/edit', 'System\IngredientController@postEditIngredient')->name('system.ingredient.postEditIngredient');
            Route::get('ingredient/delete/{id}', 'System\IngredientController@getDeleteIngredient')->name('system.ingredient.getDeleteIngredient');
        });
        Route::get('export-ing', 'System\FileController@ingExport')->name('ingExport');

        //Product
        Route::get('product/manage', 'System\ProductController@getManage')->name('system.product.getManage');
        Route::group(['middleware' => 'check.admin.manager'], function () {
            Route::get('product/add', 'System\ProductController@getProductAdd')->name('system.product.getProductAdd');
            Route::get('product/get-ingredient', 'System\ProductController@getProductAddParams')->name('system.product.getProductAddParams');
            Route::post('product/add', 'System\ProductController@postAdd')->name('system.product.postAdd');
            Route::get('product/get', 'System\ProductController@getProduct')->name('system.product.getProduct');
            Route::get('product/edit/{id}', 'System\ProductController@getProductEdit')->name('system.product.getProductEdit');
            Route::post('product/edit', 'System\ProductController@postEdit')->name('system.product.postEdit');
            Route::get('product/delete/{id}', 'System\ProductController@getDelete')->name('system.product.getDelete');
            Route::get('product/disable/{action}/{id}', 'System\ProductController@getDisableProduct')->name('system.product.getDisableProduct');
        });
        Route::get('export-product', 'System\FileController@productExport')->name('productExport');

        Route::group(['prefix'=>'warehouse'], function (){
            Route::get('destroy-raw', 'System\WarehouseController@getDestroyRaw')->name('system.warehouse.getDestroyRaw');
            Route::get('destroy-ingredient', 'System\WarehouseController@getDestroyIngredient')->name('system.warehouse.getDestroyIngredient');
            Route::group(['middleware' => 'check.admin.manager'], function () {
                Route::get('import', 'System\WarehouseController@getImport')->name('system.warehouse.getImport');
                Route::get('select-shop-import', 'System\WarehouseController@getSelectShopImportAjax')->name('system.warehouse.getSelectShopImportAjax');
                Route::get('select-raw-import', 'System\WarehouseController@getSelectRawImportAjax')->name('system.warehouse.getSelectRawImportAjax');
                Route::get('select-ingredient-import', 'System\WarehouseController@getSelectIngredientImportAjax')->name('system.warehouse.getSelectIngredientImportAjax');
                Route::post('add', 'System\WarehouseController@postAdd')->name('system.warehouse.postAdd');
                Route::get('get-edit-import/{id}', 'System\WarehouseController@getEditImport')->name('system.warehouse.getEditImport');
                Route::post('post-edit-import', 'System\WarehouseController@postEditImport')->name('system.warehouse.postEditImport');
                Route::get('edit/{id}', 'System\WarehouseController@getEdit')->name('system.warehouse.getEdit');
                Route::post('edit/{id}', 'System\WarehouseController@postEdit')->name('system.warehouse.postEdit');
                Route::get('ajax/raw', 'System\WarehouseController@getRaw')->name('system.warehouse.getRaw');
                //destroy raw
                Route::post('search-destroy-raw', 'System\WarehouseController@postSearchDestroyRaw')->name('system.warehouse.searchDestroyRaw');
                Route::get('data-destroy-raw', 'System\WarehouseController@getDataDestroyRaw')->name('system.warehouse.getDataDestroyRaw');
                Route::get('data-edit-destroy-raw', 'System\WarehouseController@getDataEditDestroyRaw')->name('system.warehouse.getDataEditDestroyRaw');
                Route::post('edit-destroy-raw', 'System\WarehouseController@postEditDestroyRaw')->name('system.warehouse.postEditDestroyRaw');
                Route::post('destroy-raw', 'System\WarehouseController@postDestroyRaw')->name('system.warehouse.postDestroyRaw');
                Route::get('delete-destroy-raw/{id}', 'System\WarehouseController@postDeleteDestroyRaw')->name('system.warehouse.postDeleteDestroyRaw');
                Route::get('enable-destroy-raw/{id}', 'System\WarehouseController@postEnableDestroyRaw')->name('system.warehouse.postEnableDestroyRaw');
                Route::get('export-destroy-raw', 'System\FileController@destroyRawExport')->name('destroyRawExport');
                //destroy ingredient
                Route::get('data-destroy-ingredient', 'System\WarehouseController@getDataDestroyIngredient')->name('system.warehouse.getDataDestroyIngredient');
                Route::post('destroy-ingredient', 'System\WarehouseController@postDestroyIngredient')->name('system.warehouse.postDestroyIngredient');
                Route::get('delete-destroy-ingredient/{id}', 'System\WarehouseController@postDeleteDestroyIngredient')->name('system.warehouse.postDeleteDestroyIngredient');
                Route::get('enable-destroy-ingredient/{id}', 'System\WarehouseController@postEnableDestroyIngredient')->name('system.warehouse.postEnableDestroyIngredient');
                Route::get('data-edit-destroy-ingredient', 'System\WarehouseController@getDataEditDestroyIngredient')->name('system.warehouse.getDataEditDestroyIngredient');
                Route::post('edit-destroy-ingredient', 'System\WarehouseController@postEditDestroyIngredient')->name('system.warehouse.postEditDestroyIngredient');
                Route::get('export-destroy-ingredient', 'System\FileController@destroyIngredientExport')->name('destroyIngredientExport');

                Route::get('get-edit-manage-raw', 'System\WarehouseController@getEditRawManage')->name('system.warehouse.getEditRawManage');
                Route::post('post-edit-manage-raw', 'System\WarehouseController@postEditRawManage')->name('system.warehouse.postEditRawManage');
                Route::get('get-edit-manage-ing', 'System\WarehouseController@getEditIngManage')->name('system.warehouse.getEditIngManage');
                Route::post('post-edit-manage-ing', 'System\WarehouseController@postEditIngManage')->name('system.warehouse.postEditIngManage');

                Route::post('warehouse-import', 'System\FileController@warehouseImport')->name('warehouseImport');
            });

            Route::get('warehouse-export-template', 'System\FileController@warehouseExportTemplate')->name('warehouseExportTemplate');
            Route::get('warehouse-raw-export', 'System\FileController@warehouseRawExport')->name('warehouseRawExport');
            Route::get('warehouse-ing-export', 'System\FileController@warehouseIngExport')->name('warehouseIngExport');

            Route::get('manage', 'System\WarehouseController@getManage')->name('system.warehouse.getManage');
            Route::get('history', 'System\WarehouseController@getHistory')->name('system.warehouse.getHistory');
            Route::get('export-history', 'System\FileController@warehouseHistoryExport')->name('warehouseHistoryExport');
        });
    });

    Route::group(['prefix'=>'admin'], function (){
        Route::get('login/{id}', 'System\AdminController@getLoginByID')->name('system.admin.getLoginByID')->middleware('check.admin.manager');
    });

    Route::group(['middleware' => 'check.admin', 'prefix'=>'shop'], function (){
        Route::get('/', 'System\ShopController@getAllShop')->name('system.shop.getAllShop');
        Route::get('add', 'System\ProductController@getShopAdd')->name('system.product.getShopAdd');
        Route::post('add', 'System\ShopController@postAdd')->name('system.shop.postAdd');
        Route::get('get-edit', 'System\ShopController@getShop')->name('system.shop.getShop');
        Route::post('edit', 'System\ShopController@postEdit')->name('system.shop.postEdit');
        Route::get('delete/{id}', 'System\ShopController@getDelete')->name('system.shop.getDelete');
    });

    Route::group(['prefix'=>'report'], function (){
        Route::get('sales', 'System\ReportController@getSales')->name('system.report.getSales');
        Route::get('get-revenue-customer', 'System\ReportController@getRevenueCustomer')->name('getRevenueCustomer');
        Route::get('get-revenue-date', 'System\ReportController@getRevenueDate')->name('getRevenueDate');
        Route::get('get-revenue-product', 'System\ReportController@getRevenueProduct')->name('getRevenueProduct');
        Route::get('get-revenue-staff', 'System\ReportController@getRevenueStaff')->name('getRevenueStaff');
        Route::get('get-revenue-shift', 'System\ReportController@getRevenueShift')->name('getRevenueShift');
        Route::get('export-revenue-shift', 'System\FileController@revenueShiftExport')->name('revenueShiftExport');
        Route::get('export-revenue-date', 'System\FileController@exportRevenueDate')->name('exportRevenueDate');
        Route::get('export-revenue-staff', 'System\FileController@exportRevenueStaff')->name('exportRevenueStaff');
        Route::get('export-revenue-product', 'System\FileController@exportRevenueProduct')->name('exportRevenueProduct');
        Route::get('export-revenue-customer', 'System\FileController@exportRevenueCustomer')->name('exportRevenueCustomer');

        Route::get('ajax-bill', 'System\ReportController@getAjaxBill')->name('system.report.getAjaxBill');
        Route::get('revenue', 'System\ReportController@getRevenue')->name('system.report.getRevenue');
        Route::get('top-product', 'System\ReportController@getTopProduct')->name('system.report.getTopProduct');
//        Route::get('revenue-raw', 'System\ReportController@getRevenueRaw')->name('getRevenueRaw');
//        Route::get('revenue-ingredient', 'System\ReportController@getRevenueIngredient')->name('getRevenueIngredient');
        Route::get('signed-bill', 'System\ReportController@getSignedBill')->name('system.report.getSignedBill');

        Route::get('export-signed-bill', 'System\FileController@signedBillExport')->name('signedBillExport');
        Route::get('export-revenue', 'System\FileController@revenueExport')->name('revenueExport');
        Route::get('export-sales', 'System\FileController@salesExport')->name('salesExport');
	});
    Route::group(['prefix' => 'roles'], function () {
        Route::get('manage', 'System\RolesController@getManage')->name('system.roles.getManage');
        Route::get('edit/{id}', 'System\RolesController@getTree')->name('system.roles.editRole');
        Route::get('createRolesGroup', 'System\RolesController@createRolesGroup')->name('system.roles.createRolesGroup');
        Route::post('postcreateRolesGroup', 'System\RolesController@postcreateRolesGroup')->name('system.roles.postcreateRolesGroup');
        Route::get('editRolesGroup/{id}', 'System\RolesController@editRolesGroup')->name('system.roles.editRoleGroup');
        Route::post('posteditRolesGroup/{id}', 'System\RolesController@posteditRolesGroup')->name('system.roles.posteditRoleGroup');
    });
});

