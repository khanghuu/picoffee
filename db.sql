-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 27, 2019 lúc 07:44 PM
-- Phiên bản máy phục vụ: 10.4.6-MariaDB
-- Phiên bản PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `pi`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bill`
--

CREATE TABLE `bill` (
  `Bill_ID` int(10) UNSIGNED NOT NULL,
  `Bill_CustomerName` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Bill_CustomerEmail` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Bill_DiscountPercent` double(8,2) NOT NULL DEFAULT 0.00,
  `Bill_DiscountAmount` double(8,2) NOT NULL DEFAULT 0.00,
  `Bill_TotalPrice` double(8,2) NOT NULL DEFAULT 0.00,
  `Bill_ExtantPrice` double(8,2) NOT NULL DEFAULT 0.00,
  `Bill_Time` datetime DEFAULT NULL,
  `Bill_User` int(11) DEFAULT NULL,
  `Bill_Status` tinyint(4) NOT NULL DEFAULT 0,
  `Bill_IsSigned` tinyint(4) NOT NULL DEFAULT 0,
  `Bill_IsTaxVAT` tinyint(4) NOT NULL DEFAULT 0,
  `Bill_TableNumber` int(11) DEFAULT NULL,
  `Bill_Shop_ID` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bill_product`
--

CREATE TABLE `bill_product` (
  `Bill_Product_ID` int(10) UNSIGNED NOT NULL,
  `Bill_Product_BillID` int(11) NOT NULL,
  `Bill_Product_ProductID` int(11) NOT NULL,
  `Bill_Product_Quantity` int(11) NOT NULL,
  `Bill_Product_Time` datetime NOT NULL,
  `Bill_Product_Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bill_product_temp`
--

CREATE TABLE `bill_product_temp` (
  `Bill_Product_ID` int(10) UNSIGNED NOT NULL,
  `Bill_Product_BillID` int(11) NOT NULL,
  `Bill_Product_ProductID` int(11) NOT NULL,
  `Bill_Product_Quantity` int(11) NOT NULL,
  `Bill_Product_Time` datetime NOT NULL,
  `Bill_Product_Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bill_temp`
--

CREATE TABLE `bill_temp` (
  `Bill_ID` int(10) UNSIGNED NOT NULL,
  `Bill_CustomerName` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Bill_CustomerEmail` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Bill_DiscountPercent` double(8,2) NOT NULL DEFAULT 0.00,
  `Bill_DiscountAmount` double(8,2) NOT NULL DEFAULT 0.00,
  `Bill_TotalPrice` double(8,2) NOT NULL DEFAULT 0.00,
  `Bill_ExtantPrice` double(8,2) NOT NULL DEFAULT 0.00,
  `Bill_Time` datetime DEFAULT NULL,
  `Bill_User` int(11) DEFAULT NULL,
  `Bill_Status` tinyint(4) NOT NULL DEFAULT 0,
  `Bill_IsSigned` tinyint(4) NOT NULL DEFAULT 0,
  `Bill_IsTaxVAT` tinyint(4) NOT NULL DEFAULT 0,
  `Bill_TableNumber` int(11) DEFAULT NULL,
  `Bill_Shop_ID` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `Category_ID` int(10) UNSIGNED NOT NULL,
  `Category_Code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Category_Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Category_Parent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Category_Order` int(11) DEFAULT 0,
  `Category_Status` tinyint(4) NOT NULL,
  `Category_Datetime` datetime NOT NULL,
  `Category_Shop` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`Category_ID`, `Category_Code`, `Category_Name`, `Category_Parent`, `Category_Order`, `Category_Status`, `Category_Datetime`, `Category_Shop`) VALUES
(26, 'GA684', 'Gà', NULL, 1, 1, '2019-10-27 23:42:19', 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `countries`
--

CREATE TABLE `countries` (
  `Countries_ID` int(10) UNSIGNED NOT NULL,
  `Countries_SortName` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Countries_Name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Countries_PhoneCode` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `export`
--

CREATE TABLE `export` (
  `Export_ID` int(10) UNSIGNED NOT NULL,
  `Export_User` int(11) NOT NULL,
  `Export_Customer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Export_Raw` int(11) DEFAULT NULL,
  `Export_Price` int(11) DEFAULT NULL,
  `Export_Quantity` int(11) NOT NULL,
  `Export_Total_Price` float(22,0) DEFAULT 0,
  `Export_DateTime` datetime NOT NULL,
  `Export_Description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Export_Status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ingredient`
--

CREATE TABLE `ingredient` (
  `Ingredient_ID` int(10) UNSIGNED NOT NULL,
  `Ingredient_Code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Ingredient_Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Ingredient_Number` double NOT NULL,
  `Ingredient_Quantity` decimal(20,10) NOT NULL,
  `Ingredient_Unit` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Ingredient_Params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Ingredient_Status` tinyint(1) NOT NULL,
  `Ingredient_Shop` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Created_At` datetime NOT NULL,
  `Updated_At` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ingredient`
--

INSERT INTO `ingredient` (`Ingredient_ID`, `Ingredient_Code`, `Ingredient_Name`, `Ingredient_Number`, `Ingredient_Quantity`, `Ingredient_Unit`, `Ingredient_Params`, `Ingredient_Status`, `Ingredient_Shop`, `Created_At`, `Updated_At`) VALUES
(63, 'VA811', 'Vải Ướp Đường', 1, '41.0000000000', 'Hộp', '{\"84\":\"2\",\"81\":\"5\"}', 1, '3', '2019-10-27 17:43:50', '2019-10-27 18:13:15'),
(64, 'DA656', 'Đào Ướp Đường', 1, '10.0000000000', 'Hộp', '{\"85\":\"2\",\"82\":\"5\"}', 1, '4', '2019-10-27 17:44:31', '2019-10-27 23:58:44'),
(65, 'MI942', 'Mít Dầm', 1, '10.0000000000', 'Hộp', '{\"85\":\"5\",\"83\":\"10\"}', 1, '4', '2019-10-27 17:45:33', '2019-10-27 23:58:44');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `level`
--

CREATE TABLE `level` (
  `level_ID` int(11) NOT NULL,
  `level_Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `level`
--

INSERT INTO `level` (`level_ID`, `level_Name`) VALUES
(1, 'Admin'),
(3, 'Finace'),
(2, 'Manager'),
(4, 'Staff');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `log_name`
--

CREATE TABLE `log_name` (
  `Log_Name_ID` int(10) UNSIGNED NOT NULL,
  `Log_Name_Log` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Log_Name_DateTime` datetime NOT NULL,
  `Log_Name_IP` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Log_Name_System` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Log_Name_User` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `log_name`
--

INSERT INTO `log_name` (`Log_Name_ID`, `Log_Name_Log`, `Log_Name_DateTime`, `Log_Name_IP`, `Log_Name_System`, `Log_Name_User`) VALUES
(985, 'Thêm cửa hàng ID: 3', '2019-10-27 16:47:40', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(986, 'Thêm cửa hàng ID: 4', '2019-10-27 16:47:53', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(987, 'Sửa cửa hàng ID: 4', '2019-10-27 16:48:15', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(988, 'User Logout', '2019-10-27 16:58:45', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 954212),
(989, 'User Login', '2019-10-27 16:58:45', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(990, 'User Logout', '2019-10-27 17:00:35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 254124),
(991, 'User Login', '2019-10-27 17:00:35', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(992, 'User Logout', '2019-10-27 17:01:05', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 783663),
(993, 'User Login', '2019-10-27 17:01:05', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(994, 'User Logout', '2019-10-27 17:01:33', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 683623),
(995, 'User Login', '2019-10-27 17:01:33', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(996, 'Thêm nguyên liệu thô ID: 70', '2019-10-27 17:02:08', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(997, 'Thêm nguyên liệu thô ID: 71', '2019-10-27 17:02:30', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(998, 'Thêm nguyên liệu thô ID: 72', '2019-10-27 17:02:51', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(999, 'Thêm nguyên liệu thô ID: 73', '2019-10-27 17:03:05', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1000, 'Thêm nguyên liệu thô ID: 74', '2019-10-27 17:03:19', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1001, 'Thêm nguyên liệu thô ID: 75', '2019-10-27 17:03:48', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1002, 'Thêm nguyên liệu thô ID: 76', '2019-10-27 17:03:55', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1003, 'Xóa nguyên liệu thô ID: 76', '2019-10-27 17:03:59', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1004, 'Thêm nguyên liệu thô ID: 77', '2019-10-27 17:04:19', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1005, 'Thêm nguyên liệu thô ID: 78', '2019-10-27 17:04:29', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1006, 'Sửa nguyên liệu thô ID: ', '2019-10-27 17:04:37', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1007, 'Sửa nguyên liệu thô ID: ', '2019-10-27 17:04:49', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1008, 'Sửa nguyên liệu thô ID: ', '2019-10-27 17:04:57', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1009, 'Thêm nguyên liệu thô ID: 79', '2019-10-27 17:08:08', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1010, 'Thêm nguyên liệu thô ID: 80', '2019-10-27 17:08:23', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1011, 'Sửa nguyên liệu thô ID: ', '2019-10-27 17:18:46', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1012, 'User Logout', '2019-10-27 17:19:27', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 683623),
(1013, 'User Login', '2019-10-27 17:19:27', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1014, 'Sửa nguyên liệu thô ID: ', '2019-10-27 17:21:21', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 783663),
(1015, 'User Logout', '2019-10-27 17:21:41', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 783663),
(1016, 'User Login', '2019-10-27 17:21:41', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1017, 'Thêm nguyên liệu tinh ID: 62', '2019-10-27 17:22:42', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1018, 'Sửa nguyên liệu tinh ID: 62', '2019-10-27 17:22:51', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 666666),
(1019, 'Thêm nguyên liệu thô ID: 81', '2019-10-27 17:35:29', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 157463),
(1020, 'Thêm nguyên liệu thô ID: 82', '2019-10-27 17:36:27', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 157463),
(1021, 'Thêm nguyên liệu thô ID: 83', '2019-10-27 17:36:41', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 157463),
(1022, 'Thêm nguyên liệu thô ID: 84', '2019-10-27 17:38:01', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 157463),
(1023, 'Thêm nguyên liệu thô ID: 85', '2019-10-27 17:40:27', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 157463),
(1024, 'Thêm nguyên liệu tinh ID: 63', '2019-10-27 17:43:50', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 157463),
(1025, 'Thêm nguyên liệu tinh ID: 64', '2019-10-27 17:44:31', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 157463),
(1026, 'Thêm nguyên liệu tinh ID: 65', '2019-10-27 17:45:33', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 157463),
(1027, 'Sửa người dùng: boss@gmail.com', '2019-10-27 17:48:43', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 157463),
(1028, 'User Logout', '2019-10-27 17:49:03', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 157463),
(1029, 'User Login', '2019-10-27 17:49:36', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 666666),
(1030, 'Nhập kho nguyên liệu: 108', '2019-10-27 18:08:19', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1031, 'Nhập kho nguyên liệu: 109', '2019-10-27 18:10:48', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1032, 'Nhập kho nguyên liệu: 110', '2019-10-27 18:12:05', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1033, 'Nhập kho nguyên liệu: 112', '2019-10-27 18:13:15', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1034, 'User Logout', '2019-10-27 18:26:02', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 254124),
(1035, 'User Login', '2019-10-27 18:26:02', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1036, 'User Logout', '2019-10-27 18:30:59', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 783663),
(1037, 'User Login', '2019-10-27 18:30:59', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1038, 'User Login', '2019-10-27 23:31:34', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1039, 'User Logout', '2019-10-27 23:34:50', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 483653),
(1040, 'User Login', '2019-10-27 23:34:50', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1041, 'Thêm danh mục ID: 26', '2019-10-27 23:42:44', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 783663),
(1042, 'User Logout', '2019-10-27 23:45:46', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 783663),
(1043, 'User Login', '2019-10-27 23:45:46', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1044, 'Nhập kho nguyên liệu: 117', '2019-10-27 23:58:44', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1045, 'User Logout', '2019-10-28 01:03:33', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 254124),
(1046, 'User Login', '2019-10-28 01:03:33', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1047, 'User Logout', '2019-10-28 01:05:07', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 783663),
(1048, 'User Login', '2019-10-28 01:05:07', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1049, 'User Logout', '2019-10-28 01:13:48', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 683623),
(1050, 'User Login', '2019-10-28 01:13:48', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1051, 'Sửa người dùng: e@mail.com', '2019-10-28 01:14:19', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159),
(1052, 'User Logout', '2019-10-28 01:38:57', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 483653),
(1053, 'User Login', '2019-10-28 01:38:57', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 448159);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(2, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(3, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(4, '2016_06_01_000004_create_oauth_clients_table', 1),
(5, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(9, '2019_07_13_054941_create_category_table', 2),
(10, '2019_10_23_131235_create_shop_table', 3),
(11, '2019_10_23_100327_add_shop_id_column_in_raw_material_table', 4),
(12, '2019_10_23_110935_drop_units_table', 4),
(13, '2019_10_23_110946_drop_measure_table', 4),
(14, '2019_10_23_111022_drop_countries_table', 4),
(15, '2019_10_23_112901_add__ingredient__shop_ingredient_table', 4),
(16, '2019_10_24_115443_edit_all_columns_name_in_ingredient_table', 5),
(17, '2019_10_25_090421_edit_datatype_to_number_ingredient_table', 6),
(18, '2019_10_25_131522_change_all_name_column_product_table', 7),
(20, '2019_10_25_180542_edit_datatype_product_price_in_product_table', 8),
(21, '2019_10_25_151511_create_update_table', 9),
(22, '2019_10_25_215442_edit_product_shop_column_on_product_table', 10),
(23, '2019_10_26_001527_edit_datatype_product_params_column_on_product_table', 10),
(26, '2019_10_26_084811_change_datatype_warehouse_price_in_warehouse__table', 11),
(27, '2019_10_26_175913_create_add_comment__warehouse__status', 12),
(28, '2019_10_26_174137_create_add_column_ingredient', 13),
(29, '2019_10_26_173846_create_add_column_raw_material', 14),
(30, '2019_10_26_020937_change_column_in_warehouse_table', 15);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `Product_ID` int(10) UNSIGNED NOT NULL,
  `Product_Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Product_Code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Product_Category` int(11) NOT NULL,
  `Product_Price` decimal(18,2) UNSIGNED DEFAULT 0.00,
  `Product_Image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Product_Params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Product_Status` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Product_Shop` int(11) NOT NULL,
  `Created_At` datetime NOT NULL,
  `Updated_At` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `raw_material`
--

CREATE TABLE `raw_material` (
  `Raw_Material_ID` int(10) UNSIGNED NOT NULL,
  `Raw_Material_Code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Raw_Material_Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Raw_Material_Quantity` decimal(20,10) NOT NULL,
  `Raw_Material_Unit` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Raw_Material_Status` tinyint(4) NOT NULL,
  `Raw_Material_Create` datetime NOT NULL,
  `Raw_Material_Update` datetime NOT NULL,
  `Raw_Material_Shop` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `raw_material`
--

INSERT INTO `raw_material` (`Raw_Material_ID`, `Raw_Material_Code`, `Raw_Material_Name`, `Raw_Material_Quantity`, `Raw_Material_Unit`, `Raw_Material_Status`, `Raw_Material_Create`, `Raw_Material_Update`, `Raw_Material_Shop`) VALUES
(81, 'VA590', 'Vải', '0.0000000000', 'kg', 1, '2019-10-27 17:35:29', '2019-10-27 18:13:15', '3'),
(82, 'DA983', 'Đào', '50.0000000000', 'kg', 1, '2019-10-27 17:36:27', '2019-10-27 23:58:44', '4'),
(83, 'MI895', 'Mít', '0.0000000000', 'kg', 1, '2019-10-27 17:36:41', '2019-10-27 23:58:44', '4'),
(84, 'DU584', 'Đường', '40.0000000000', 'kg', 1, '2019-10-27 17:38:01', '2019-10-27 18:13:15', '3'),
(85, 'DU216', 'Đường', '130.0000000000', 'kg', 1, '2019-10-27 17:40:27', '2019-10-27 23:58:44', '4');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `shop`
--

CREATE TABLE `shop` (
  `Shop_ID` bigint(20) UNSIGNED NOT NULL,
  `Shop_Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Shop_Address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Shop_Status` tinyint(4) NOT NULL COMMENT '-1:Deleted, 0:Cancel, 1:Open',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `shop`
--

INSERT INTO `shop` (`Shop_ID`, `Shop_Name`, `Shop_Address`, `Shop_Status`, `created_at`, `updated_at`) VALUES
(3, 'The Pi Vườn Lài', 'số 15-phường 5-quận Tân Phú-Tp HCM', 1, '2019-10-27 09:47:40', '2019-10-27 09:47:40'),
(4, 'The Pi Phan Đăng Lưu', 'số 22- phường Phú Thọ Hòa- quận Bình Thạnh-Tp HCM', 1, '2019-10-27 09:47:53', '2019-10-27 09:47:53');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `User_ID` int(10) UNSIGNED NOT NULL,
  `User_Name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `User_Email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `User_EmailActive` tinyint(4) NOT NULL,
  `User_RegisteredDateTime` date NOT NULL,
  `User_Parent` int(10) UNSIGNED DEFAULT NULL,
  `User_Tree` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `User_Level` int(10) UNSIGNED NOT NULL,
  `User_Address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `User_Location_Store` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `User_Sex` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `User_Birthday` date DEFAULT NULL,
  `User_Password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `User_Image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `User_Phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `User_Status` tinyint(4) NOT NULL,
  `User_Verify` tinyint(4) NOT NULL,
  `User_Confirm` tinyint(4) NOT NULL,
  `User_Login` bigint(20) DEFAULT NULL,
  `User_Token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `User_OTP` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`User_ID`, `User_Name`, `User_Email`, `User_EmailActive`, `User_RegisteredDateTime`, `User_Parent`, `User_Tree`, `User_Level`, `User_Address`, `User_Location_Store`, `User_Sex`, `User_Birthday`, `User_Password`, `User_Image`, `User_Phone`, `User_Status`, `User_Verify`, `User_Confirm`, `User_Login`, `User_Token`, `User_OTP`, `remember_token`) VALUES
(254124, 'Quản Lý VLAI', 'd@mail.com', 1, '2019-07-17', NULL, NULL, 2, 'số 3-phường 6-quận Bình Thạnh-Tp HCM', '4', 'Male', '1999-12-27', '$2y$10$JkWcqwAXVrQs2Q8c.381/uRuhf.hj0cXQ8Zf6kyftm8sGUqK8DjTG', NULL, '0974854224', 1, 1, 1, NULL, NULL, NULL, NULL),
(448159, 'Boss', 'boss@gmail.com', 1, '2019-10-27', NULL, NULL, 1, 'Ho Chi Minh', '', 'Male', '1993-10-27', '$2y$10$sWcH7sT4m3OxFydVS6cTG.h7zWGpnVIetlGoCdeWiYNRPsfFUBrRS', NULL, '12345678', 1, 1, 1, NULL, NULL, 0, NULL),
(483653, 'Nhân Viên VLAI', 'e@mail.com', 1, '2019-10-27', NULL, NULL, 4, 'số 4-phường 6-quận Bình Thạnh-Tp HCM', '4', 'Male', '1999-12-27', '$2y$10$JkWcqwAXVrQs2Q8c.381/uRuhf.hj0cXQ8Zf6kyftm8sGUqK8DjTG', NULL, '0974854225', 1, 1, 1, NULL, NULL, NULL, NULL),
(666666, 'admin', 'khangdh9@gmail.com', 1, '2019-07-13', 666666, '', 1, 'Ho Chi Minh', '', 'Male', '1993-01-23', '$2y$10$Fj9UikDv.DbiqJbqCtUe8ucOvneFR5bvCuyO.zCTOShtuAtQATFdS', '../users/666666/kjdfh666666.jpg', '123123423', 1, 1, 1, NULL, NULL, NULL, NULL),
(683623, 'Tài Chính', 'c@mail.com', 1, '2019-10-27', NULL, NULL, 3, 'số 2-phường 6-quận Bình Thạnh-Tp HCM', '', 'Male', '1999-12-27', '$2y$10$JkWcqwAXVrQs2Q8c.381/uRuhf.hj0cXQ8Zf6kyftm8sGUqK8DjTG', NULL, '0974854223', 1, 1, 1, NULL, NULL, NULL, NULL),
(783663, 'Quản Lý PDL', 'a@mail.com', 1, '2019-06-18', NULL, NULL, 2, 'số 22-phường 6-quận Bình Thạnh-Tp HCM', '3', 'Male', '1999-12-27', '$2y$10$JkWcqwAXVrQs2Q8c.381/uRuhf.hj0cXQ8Zf6kyftm8sGUqK8DjTG', NULL, '0974854221', 1, 1, 1, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `warehouse`
--

CREATE TABLE `warehouse` (
  `Warehouse_ID` int(10) UNSIGNED NOT NULL,
  `Warehouse_User` int(11) NOT NULL,
  `Warehouse_Raw` int(11) NOT NULL DEFAULT 0,
  `Warehouse_Ingredient` int(11) DEFAULT NULL,
  `Warehouse_Number` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Warehouse_Expiry_Date` date DEFAULT NULL,
  `Warehouse_Price` decimal(18,2) DEFAULT NULL,
  `Warehouse_Quantity` float NOT NULL,
  `Warehouse_Total` decimal(20,10) UNSIGNED NOT NULL,
  `Warehouse_Description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Warehouse_IsDestroy` tinyint(4) NOT NULL DEFAULT 0,
  `Created_At` datetime NOT NULL,
  `Warehouse_Status` int(11) NOT NULL COMMENT '1: Nhập, 2: Chế biến',
  `Warehouse_Shop` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Updated_At` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `warehouse`
--

INSERT INTO `warehouse` (`Warehouse_ID`, `Warehouse_User`, `Warehouse_Raw`, `Warehouse_Ingredient`, `Warehouse_Number`, `Warehouse_Expiry_Date`, `Warehouse_Price`, `Warehouse_Quantity`, `Warehouse_Total`, `Warehouse_Description`, `Warehouse_IsDestroy`, `Created_At`, `Warehouse_Status`, `Warehouse_Shop`, `Updated_At`) VALUES
(107, 448159, 81, 0, '201910270', NULL, '25300.00', 100, '2779000.0000000000', 'Nhập Nguyên Liệu Vải, Đường', 0, '2019-10-27 18:08:19', 1, '3', '2019-10-27 11:08:19'),
(108, 448159, 84, 0, '201910270', NULL, '12450.00', 20, '2779000.0000000000', 'Nhập Nguyên Liệu Vải, Đường', 0, '2019-10-27 18:08:19', 1, '3', '2019-10-27 11:08:19'),
(109, 448159, 0, 63, '201910272', NULL, '24400.00', 1, '24400.0000000000', NULL, 0, '2019-10-27 18:10:48', 1, '3', '2019-10-27 11:10:48'),
(110, 448159, 0, 63, '201910273', NULL, '23.00', 10, '230.0000000000', NULL, 0, '2019-10-27 18:12:05', 2, '3', '2019-10-27 11:12:05'),
(111, 448159, 84, 0, '201910274', NULL, '12.35', 100, '1595.3400000000', NULL, 0, '2019-10-27 18:13:15', 2, '3', '2019-10-27 11:13:15'),
(112, 448159, 0, 63, '201910274', NULL, '12.00', 30, '1595.3400000000', NULL, 0, '2019-10-27 18:13:15', 2, '3', '2019-10-27 11:13:15'),
(113, 448159, 82, 0, '201910276', NULL, '123.00', 100, '38430.0000000000', NULL, 0, '2019-10-27 23:58:44', 2, '4', '2019-10-27 16:58:44'),
(114, 448159, 85, 0, '201910276', NULL, '123.00', 100, '38430.0000000000', NULL, 0, '2019-10-27 23:58:44', 2, '4', '2019-10-27 16:58:44'),
(115, 448159, 83, 0, '201910276', NULL, '123.50', 100, '38430.0000000000', NULL, 0, '2019-10-27 23:58:44', 2, '4', '2019-10-27 16:58:44'),
(116, 448159, 0, 64, '201910276', NULL, '13.00', 10, '38430.0000000000', NULL, 0, '2019-10-27 23:58:44', 2, '4', '2019-10-27 16:58:44'),
(117, 448159, 0, 65, '201910276', NULL, '135.00', 10, '38430.0000000000', NULL, 0, '2019-10-27 23:58:44', 2, '4', '2019-10-27 16:58:44');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`Bill_ID`);

--
-- Chỉ mục cho bảng `bill_product`
--
ALTER TABLE `bill_product`
  ADD PRIMARY KEY (`Bill_Product_ID`);

--
-- Chỉ mục cho bảng `bill_product_temp`
--
ALTER TABLE `bill_product_temp`
  ADD PRIMARY KEY (`Bill_Product_ID`);

--
-- Chỉ mục cho bảng `bill_temp`
--
ALTER TABLE `bill_temp`
  ADD PRIMARY KEY (`Bill_ID`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`Category_ID`);

--
-- Chỉ mục cho bảng `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`Countries_ID`);

--
-- Chỉ mục cho bảng `export`
--
ALTER TABLE `export`
  ADD PRIMARY KEY (`Export_ID`);

--
-- Chỉ mục cho bảng `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`Ingredient_ID`);

--
-- Chỉ mục cho bảng `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`level_ID`),
  ADD UNIQUE KEY `l_level_Name_uindex` (`level_Name`);

--
-- Chỉ mục cho bảng `log_name`
--
ALTER TABLE `log_name`
  ADD PRIMARY KEY (`Log_Name_ID`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`Product_ID`);

--
-- Chỉ mục cho bảng `raw_material`
--
ALTER TABLE `raw_material`
  ADD PRIMARY KEY (`Raw_Material_ID`);

--
-- Chỉ mục cho bảng `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`Shop_ID`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`User_ID`);

--
-- Chỉ mục cho bảng `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`Warehouse_ID`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bill`
--
ALTER TABLE `bill`
  MODIFY `Bill_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=404;

--
-- AUTO_INCREMENT cho bảng `bill_product`
--
ALTER TABLE `bill_product`
  MODIFY `Bill_Product_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=499;

--
-- AUTO_INCREMENT cho bảng `bill_product_temp`
--
ALTER TABLE `bill_product_temp`
  MODIFY `Bill_Product_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT cho bảng `bill_temp`
--
ALTER TABLE `bill_temp`
  MODIFY `Bill_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `Category_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT cho bảng `countries`
--
ALTER TABLE `countries`
  MODIFY `Countries_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `export`
--
ALTER TABLE `export`
  MODIFY `Export_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `Ingredient_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT cho bảng `level`
--
ALTER TABLE `level`
  MODIFY `level_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `log_name`
--
ALTER TABLE `log_name`
  MODIFY `Log_Name_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1054;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `Product_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `raw_material`
--
ALTER TABLE `raw_material`
  MODIFY `Raw_Material_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT cho bảng `shop`
--
ALTER TABLE `shop`
  MODIFY `Shop_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `warehouse`
--
ALTER TABLE `warehouse`
  MODIFY `Warehouse_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
