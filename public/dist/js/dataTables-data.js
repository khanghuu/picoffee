/*DataTable Init*/

// "use strict";

$(document).ready(function() {
	// "use strict";
    $('#datable_warehouse_raw').DataTable({
        ordering: false,
        // dom: 'Bfrtip',
        responsive: true,
        "paging": true,
        "pageLength": 50,
        "bInfo" : false,
        "searching": true,
        language: {
            searchPlaceholder: "Tìm kiếm . . .",
            search : '' /*Empty to remove the label*/
        },
        buttons: [
            'copy',
            {
                extend: 'excel',
                filename: 'Thong-Ke-Nguyen-Lieu-Tho'
            },
            {
                extend: 'print',
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                },
                title: "Thống kê nguyên liệu thô trong kho"
            }
        ]
    });
    $('#datable_warehouse_ing').DataTable({
        ordering: false,
        // dom: 'Bfrtip',
        responsive: true,
        "paging": true,
        "pageLength": 50,
        "bInfo" : false,
        "searching": true,
        language: {
            searchPlaceholder: "Tìm kiếm . . .",
            search : '' /*Empty to remove the label*/
        },
        buttons: [
            'copy',
            {
                extend: 'excel',
                filename: 'Thong-Ke-Nguyen-Lieu-Tinh'
            },
            {
                extend: 'print',
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                },
                title: "Thống kê nguyên liệu tinh trong kho"
            }
        ]
    });
    $('#datable_warehouse_history').DataTable({
        ordering: false,
        // dom: 'Bfrtip',
        responsive: true,
        "paging": true,
        "pageLength": 50,
        "bInfo" : false,
        "searching": true,
        language: {
            searchPlaceholder: "Tìm kiếm . . .",
            search : '' /*Empty to remove the label*/
        },
        buttons: [
            'copy',
            {
                extend: 'excel',
                filename: 'Lich-Su-Nhap-Kho'
            },
            {
                extend: 'print',
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                },
                title: "Lịch sử nhập kho"
            }
        ]
    });
    $('#datable_destroy_raw').DataTable({
        ordering: false,
        // dom: 'Bfrtip',
        responsive: true,
        "paging": true,
        "pageLength": 50,
        "bInfo" : false,
        "searching": true,
        language: {
            searchPlaceholder: "Tìm kiếm . . .",
            search : '' /*Empty to remove the label*/
        },
        buttons: [
            'copy',
            {
                extend: 'excel',
                filename: 'Danh-Sach-Huy-Nguyen-Lieu-Tho'
            },
            {
                extend: 'print',
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                },
                title: "Danh Sách Hủy Nguyên Liệu Thô"
            }
        ]
    });
    $('#datable_destroy_ing').DataTable({
        ordering: false,
        // dom: 'Bfrtip',
        responsive: true,
        "paging": true,
        "pageLength": 50,
        "bInfo" : false,
        "searching": true,
        language: {
            searchPlaceholder: "Tìm kiếm . . .",
            search : '' /*Empty to remove the label*/
        },
        buttons: [
            'copy',
            {
                extend: 'excel',
                filename: 'Danh-Sach-Huy-Nguyen-Lieu-Tho'
            },
            {
                extend: 'print',
                exportOptions: {
                    stripHtml: false,
                    stripNewlines: false,
                },
                title: "Danh Sách Hủy Nguyên Liệu Thô"
            }
        ]
    });
    $('#datable_admin').DataTable({
        "order": [[ 3, "asc" ]],
        "paging": true,
        "pageLength": 50,
    });
    $('#datable_product').DataTable({
        "order": [[ 3, "desc" ]],
        "paging": true,
        "pageLength": 50,
    });
    $('#datable_raw').DataTable({
        "order": [[ 3, "desc" ]],
        "paging": false,
    });
    $('#datable_ing').DataTable({
        "order": [[ 3, "desc" ]],
        "paging": false,
    });
    $('#datable_category').DataTable({
        "order": [[ 2, "asc" ]],
        "paging": false,
    });
    $('#datable_all_order').DataTable({
        ordering: false,
        "paging": true,
        "pageLength": 50,
    });
    $('#datable_revenue_shift').DataTable({
        "order": [[ 0, "desc" ]],
        "paging": true,
        "pageLength": 50,
    });
    $('#datatable_hour').DataTable({
        "order": false,
        "paging": true,
        "pageLength": 50,
    });
    $('#datatable_signed_bill').DataTable({
        "order": false,
        "paging": true,
        "pageLength": 50,
    });
    $('#datatable_revenue_date').DataTable({
        "order": [[ 0, "desc" ]],
        "paging": true,
        "pageLength": 50,
    });
    $('#datatable_revenue_staff').DataTable({

        "order": [[ 5, "desc" ]],
        "paging": true,
        "pageLength": 50,
    });
    $('#datatable_revenue_product').DataTable({
        "order": [[ 1, "desc" ]],
        "paging": true,
        "pageLength": 50,
    });
    $('#datatable_revenue_customer').DataTable({
        "order": [[ 6, "desc" ]],
        "paging": true,
        "pageLength": 50,
    });
} );
